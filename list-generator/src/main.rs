use std::{io::{BufWriter, Write, stdout}, fs::File, env, process::exit, collections::HashSet};

use regex::Regex;
use reqwest::{blocking::Client, StatusCode};

fn main() {
    let cwd = env::current_dir().unwrap().to_str().unwrap().to_string();
    if !cwd.ends_with("/list-generator") {
        eprintln!("This helper script assumes it's running from the directory list-generator, which is not the case ({cwd}).");
        exit(1);
    }

    const OUTPUT_DIRECTORY: &str = "../src/original_names/generated";
    // let group_names = vec![
    //     "cod", "dev", "fae", "fa", "iec", "logos", "oct", "ple", "pom", "seti", "weather", "md", "material",
    // ];

    let groups = vec![
        GroupDefinition::new("cod", "cod", Some("codicons"), "Codicons"),
        GroupDefinition::new("dev", "dev", Some("devicons"), "Devicons"),
        GroupDefinition::new("fae", "fae", Some("font_awesome_extension"), "Font Awesome Extension"),
        GroupDefinition::new("fa", "fa", Some("font_awesome"), "Font Awesome"),
        GroupDefinition::new("iec", "iec", Some("iec_power_symbols"), "IEC Power Symbols"),
        GroupDefinition::new("logos", "logos", None, "Logos (of Linux distros?)"),
        GroupDefinition::new("oct", "oct", Some("Octicons"), "Octicons"),
        GroupDefinition::new("ple", "ple", Some("powerline_extra_symbols"), "Powerline Extra Symbols"),
        GroupDefinition::new("pom", "pom", Some("pomicons"), "Pomicons"),
        GroupDefinition::new("seti", "seti", Some("seti_ui"), "Seti-UI + Custom"),
        GroupDefinition::new("weather", "weather", None, "Weather"),
        GroupDefinition::new("md", "md", Some("material_design"), "Material Design"),
        GroupDefinition::new("material", "mdi", Some("material_design_legacy"), "Material Design Icons (legacy)"),
    ];

    let mut present_groups = Vec::with_capacity(groups.len());

    let longest_group_name = groups.iter().map(|group| group.source.len()).max().unwrap();

    let mut mod_file: BufWriter<File> = BufWriter::new(File::create(format!("{OUTPUT_DIRECTORY}/mod.rs")).unwrap());

    let mut icon_count_total = 0usize;

    let regex = Regex::new("i='(?P<icon>.+)' i_(?P<group>[a-z]+)_(?P<icon_name>[a-z0-9_]+)=\\$i").unwrap();
    for group in &groups {
        print!("Processing {name}: {padding}", name = group.source, padding = " ".repeat(longest_group_name - group.source.len()));
        let _ = stdout().flush();

        let content = fetch_script(group.source);
        if content.is_none() {
            println!("Failed (not found) ");
            continue;
        }

        let content = content.unwrap();

        print!("Downloaded ");
        let _ = stdout().flush();

        present_groups.push(group);

        let icons = content
            .lines()
            .filter_map(|line| {
                if !line.starts_with("i='") && line.ends_with("=$i") {
                    return None;
                }

                regex.captures(line)
                .and_then(|captures| Some(ParsedLine::new(
                    captures["group"].to_string(),
                    captures["icon_name"].to_string(),
                    captures["icon"].to_string(),
                )))
            })
            // .for_each(|parsed| {});
            // A HashSet avoids duplicate entries, as some icons are defined multiple times in i_md.sh (with the exact name and symbol).
            .collect::<HashSet<ParsedLine>>();

        print!("=> Parsed ");
        let _ = stdout().flush();

        icon_count_total += icons.len();

        let prefix = group.prefix;
        let mut output = BufWriter::new(File::create(format!("{OUTPUT_DIRECTORY}/{prefix}.rs")).unwrap());
        output.write(format!("// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_{prefix}.sh\n\n").as_bytes()).unwrap();
        output.write(format!("\n/// Count of all {} Icons (Not an icon itself)\npub const {}_ICON_COUNT: usize = {};\n\n", group.human_name, group.prefix.to_uppercase(), icons.len()).as_bytes()).unwrap();

        for icon in &icons {
            output.write(format!(
                "/// <span style=\"font-family: 'MesloLGS NF'; font-size: 2em;\">{}</span>\n\
                pub const {}_{}: &'static str = \"{}\";\n\
                \n",
                icon.icon,
                icon.icon_group.to_uppercase(),
                icon.icon_name.to_uppercase(),
                icon.icon,
            ).as_bytes()).unwrap();
        }

        print!("=> Wrote Constants ");
        let _ = stdout().flush();

        output.write(format!(
            "\
            #[cfg(feature = \"search\")]\n\
            use phf::phf_map;\n\
            \n\
            // Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)\n\
            #[cfg(feature = \"search\")]\n\
            pub const {}_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {{\n\
            ",
            group.prefix.to_uppercase(),
        ).as_bytes()).unwrap();

        for icon in &icons {
            output.write(format!(
                "    \"nf-{}-{}\" => {}_{},\n",
                icon.icon_group,
                icon.icon_name.replace("_", "-"),
                icon.icon_group.to_uppercase(),
                icon.icon_name.to_uppercase(),
            ).as_bytes()).unwrap();
        }

        output.write("};\n".as_bytes()).unwrap();
        print!("=> Wrote Search Mapping ");
        let _ = stdout().flush();

        mod_file.write(format!("/// {} – {} Icons\npub mod {prefix};\n\n", group.human_name, icons.len()).as_bytes()).unwrap();

        println!("=> Appended mod.rs");
    }

    append_alias_uses(&present_groups, &mut mod_file);
    mod_file.write(format!("\n/// Count of all supported icons.\npub const TOTAL_ICON_COUNT: usize = {};\n", icon_count_total).as_bytes()).unwrap();
    insert_search_functions(&present_groups, &mut mod_file, longest_group_name);
}

fn fetch_script(name: &str) -> Option<String> {
    let url = format!(
        "https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_{name}.sh"
    );

    let response = Client::builder()
        .build()
        .unwrap()
        .get(url)
        .header("User-Agent", "Nerd Font List Generator")
        .send()
        .unwrap();

    if response.status() == StatusCode::NOT_FOUND {
        return None;
    }

    // If the response text cannot be parsed, threat it as an error (panic).
    Some(response.text().unwrap())
}

fn append_alias_uses(present_groups: &[&GroupDefinition<'_>], mod_file: &mut BufWriter<File>) {
    mod_file.write("// Aliases with longer/human readable names\n".as_bytes()).unwrap();

    present_groups.iter()
        .filter(|item| item.alternative.is_some())
        .for_each(|group| {
            let prefix = group.prefix;
            let alternative = group.alternative.unwrap();

            mod_file.write(format!("pub use {prefix} as {alternative};\n").as_bytes()).unwrap();
        });
}

fn insert_search_functions(groups: &[&GroupDefinition], mod_file: &mut impl Write, longest_group_length: usize) {
    print!("Generating search functions ");
    let _ = stdout().flush();

    mod_file.write(r#"
#[cfg(feature = "search")]
pub mod search {
    use phf::Map;
    use std::collections::HashMap;

"#.as_bytes()).unwrap();

    for group in groups {
        if let Some(alternative) = group.alternative {
            mod_file.write(format!("    pub use self::search_{} as search_{};\n", group.prefix, alternative).as_bytes()).unwrap();
        }
    }

    mod_file.write(r#"
    fn search_single_map(haystack: Map<&str, &str>, term: &str, result: &mut HashMap<&str, &str>) {
        haystack
            .entries
            .iter()
            .filter(|(key, value)| key.contains(&term) || value == &term)
            .for_each(|(key, value)| {
                result.insert(key, value);
            });
    }
"#.as_bytes()).unwrap();

    fn build_single_search_call(group_name: &str) -> String {
        format!(
            "        search_single_map(super::{}::{}_MAPPING, &term, &mut result);",
            group_name,
            group_name.to_uppercase(),
        )
    }

    for group in groups {
        mod_file.write(format!(
            r#"
    pub fn search_{}(term: &str) -> HashMap<&str, &str> {{
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

{}

        result
    }}
"#,
            group.prefix,
            build_single_search_call(group.prefix)
        ).as_bytes()).unwrap();
    }

    fn build_all_search_calls(groups: &[&GroupDefinition], longest_group_length: usize) -> String {
        let capacity = (70 + (longest_group_length * 2)) * groups.len();
        groups.iter()
            .map(|group| build_single_search_call(group.prefix))
            .fold(String::with_capacity(capacity), |mut a, line| {
                a.push_str(&line);
                a.push_str("\n");
                a
            })
    }

    mod_file.write(format!(r#"
    pub fn search_all(term: &str) -> HashMap<&str, &str> {{
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

{}
        result
    }}
"#, build_all_search_calls(&groups, longest_group_length)).as_bytes()).unwrap();

    mod_file.write("}\n".as_bytes()).unwrap();

    println!("- done");
}

#[derive(PartialEq, Eq, Hash)]
struct ParsedLine {
    icon_group: String,
    icon_name: String,
    icon: String,
}

impl ParsedLine {
    fn new(group: String, icon_name: String, icon: String) -> Self { Self { icon_group: group, icon_name, icon } }
}

struct GroupDefinition<'a> {
    source: &'a str,
    prefix: &'a str,
    alternative: Option<&'a str>,
    human_name: &'a str,
}

impl<'a> GroupDefinition<'a> {
    fn new(source: &'a str, prefix: &'a str, alternative: Option<&'a str>, human_name: &'a str) -> Self { Self { source, prefix, alternative, human_name } }
}
