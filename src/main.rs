mod cli;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("{0}")]
    Generic(String),
}


pub type Result<T> = core::result::Result<T, Error>;

fn main() {
    cli::run();
}
