pub mod application;

#[cfg(feature = "search")]
pub mod search {
    use phf::Map;
    use std::collections::HashMap;

    use super::application::APPLICATION_MAPPING;

    pub fn search(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase(); // .retain(|char| !char.is_whitespace());
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_map(APPLICATION_MAPPING, &term, &mut result);

        result
    }

    pub fn search_application(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase(); // .retain(|char| !char.is_whitespace());
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_map(APPLICATION_MAPPING, &term, &mut result);

        result
    }

    fn search_map(haystack: Map<&str, &str>, term: &str, result: &mut HashMap<&str, &str>) {
        haystack
            .entries
            .iter()
            .filter(|(key, _)| key.contains(&term))
            .for_each(|(key, value)| {
                result.insert(key, value);
            });
    }
}
