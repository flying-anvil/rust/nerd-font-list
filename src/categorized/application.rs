use crate::original_names::dev::*;

/// 
pub const GIT: &'static str = DEV_GIT;

/// 
pub const MYSQL: &'static str = DEV_MYSQL;

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const APPLICATION_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "application-git" => GIT,
    "application-mysql" => DEV_MYSQL,
};
