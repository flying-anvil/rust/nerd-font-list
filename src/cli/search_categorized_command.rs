use std::collections::HashMap;

use clap::{Args, ValueEnum};

use nerd_fonts_list::categorized::search::{search, search_application};

use crate::Result;

#[derive(Args, Clone, Debug)]
pub struct SearchCategorizedCommand {
    /// Search term
    term: String,

    /// Limit search to a specific group
    #[arg(value_enum, short, long)]
    group: Option<IconCategory>,
}

impl SearchCategorizedCommand {
    pub(crate) fn run(&self) -> Result<()> {
        let matches = self.perform_search();
        print_result(matches);

        Ok(())
    }

    fn perform_search(&self) -> HashMap<&str, &str> {
        match self.group.unwrap_or_default() {
            IconCategory::All => search(&self.term),
            IconCategory::Application | IconCategory::App => search_application(&self.term),
            IconCategory::Os => todo!(),
            IconCategory::Brand => todo!(),
            IconCategory::License => todo!(),
            IconCategory::Language => todo!(),
        }
    }
}

fn print_result(matches: HashMap<&str, &str>) {
    println!("Found {} matches", matches.len());

    // let longest_name = matches.keys().max_by(|left, right| left.len().cmp(&right.len())).map_or(0, |key| key.len());
    // This is slightly faster (by 6.4% / 1.6 ns) (lol)
    let longest_key = matches.keys().map(|item| item.len()).max().unwrap_or_default();

    for (name, icon) in matches {
        let padding = longest_key - name.len();
        println!("{name}: {}{icon}", " ".repeat(padding));
    }
}

#[derive(ValueEnum, Default, Clone, Copy, Debug)]
pub enum IconCategory {
    #[default]
    All,
    Application,
    App,
    Os,
    Brand,
    License,
    Language,
}
