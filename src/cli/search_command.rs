use std::{collections::HashMap, time::Instant};

use clap::{Args, ValueEnum};

use nerd_fonts_list::original_names::search::{search_all, search_logos, search_dev, search_fa, search_fae, search_iec, search_oct, search_ple, search_pom, search_weather, search_md, search_mdi, search_cod, search_seti};

use crate::Result;

#[derive(Args, Clone, Debug)]
pub struct SearchCommand {
    /// Search term
    term: String,

    /// Limit search to a specific group
    #[arg(value_enum, short, long)]
    group: Option<IconGroup>,
}

impl SearchCommand {
    pub(crate) fn run(&self) -> Result<()> {
        let start = Instant::now();
        let matches = self.perform_search();
        let duration = start.elapsed().as_secs_f64();

        print_result(matches, duration);

        Ok(())
    }

    fn perform_search(&self) -> HashMap<&str, &str> {
        match self.group.unwrap_or_default() {
            IconGroup::All => search_all(&self.term),

            IconGroup::Dev | IconGroup::DevIcon => search_dev(&self.term),

            IconGroup::FA => search_fa(&self.term),
            IconGroup::FontAwesome => search_fa(&self.term),

            IconGroup::FAE => search_fae(&self.term),
            IconGroup::FontAwesomeExtension => search_fae(&self.term),

            IconGroup::IEC => search_iec(&self.term),
            IconGroup::PowerSymbol => search_iec(&self.term),
            IconGroup::PowerSymbols => search_iec(&self.term),

            IconGroup::Logo | IconGroup::Logos => search_logos(&self.term),

            IconGroup::Oct => search_oct(&self.term),
            IconGroup::Octicon => search_oct(&self.term),
            IconGroup::Octicons => search_oct(&self.term),

            IconGroup::PLE => search_ple(&self.term),
            IconGroup::Powerline => search_ple(&self.term),

            IconGroup::POM => search_pom(&self.term),
            IconGroup::Pomicon => search_pom(&self.term),
            IconGroup::Pomicons => search_pom(&self.term),

            IconGroup::Weather => search_weather(&self.term),

            IconGroup::MD => search_md(&self.term),
            IconGroup::Material => search_md(&self.term),

            IconGroup::MDI => search_mdi(&self.term),
            IconGroup::MaterialLegacy => search_mdi(&self.term),

            IconGroup::COD => search_cod(&self.term),
            IconGroup::Codeicon => search_cod(&self.term),
            IconGroup::Codeicons => search_cod(&self.term),

            IconGroup::Seti => search_seti(&self.term),
            IconGroup::SetiUi => search_seti(&self.term),

            other => todo!("Searching {other:?} is not set implemented"),
        }
    }
}

fn print_result(matches: HashMap<&str, &str>, search_duration: f64) {
    println!("Found {} matches (took {:.0} µs)", matches.len(), search_duration * 1e6);

    // let longest_name = matches.keys().max_by(|left, right| left.len().cmp(&right.len())).map_or(0, |key| key.len());
    // This is slightly faster (by 6.4% / 1.6 ns) (lol)
    let longest_key = matches.keys().map(|item| item.len()).max().unwrap_or_default();

    for (name, icon) in matches {
        let padding = longest_key - name.len();
        println!("{name}: {}{icon}", " ".repeat(padding));
    }
}

#[derive(ValueEnum, Default, Clone, Copy, Debug)]
pub enum IconGroup {
    #[default]
    All,

    Dev,
    DevIcon,

    FA,
    FontAwesome,

    FAE,
    FontAwesomeExtension,

    IEC,
    PowerSymbol,
    PowerSymbols,

    Logo,
    Logos,

    Oct,
    Octicon,
    Octicons,

    PLE,
    Powerline,

    POM,
    Pomicon,
    Pomicons,

    Weather,

    MD,
    Material,

    MDI,
    MaterialLegacy,

    COD,
    Codeicon,
    Codeicons,

    Seti,
    SetiUi,
}
