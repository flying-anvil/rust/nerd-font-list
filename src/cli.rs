use std::process::exit;

use clap::{Parser, Subcommand};

use self::{search_command::SearchCommand, search_categorized_command::SearchCategorizedCommand};
use crate::Error;

mod search_command;
mod search_categorized_command;

#[derive(Parser, Debug)]
#[command(propagate_version = true)]
#[command(author, version, about, long_about = None)]
#[command(disable_colored_help = false)]
#[command(
    // help_template = "{author-with-newline} {about-section}Version: {version} \n {usage-heading} {usage} \n {all-args} {tab}"
    help_template = "{usage-heading} {usage} \n\n{all-args}{tab}\n\n\x1B[1;4mAbout:\x1B[0m\n\x1B[3m  Authors: {author-with-newline}  Version: {version}\x1B[0m"
)]
struct Cli {
    /// What to do
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, Clone, Debug)]
enum Command {
    /// List some or all icons.
    // List(ListCommand),

    /// Search an icon by name
    Search(SearchCommand),

    /// Search an icon by name (within the categorized set)
    #[command(name = "search-cat")]
    SearchCategorized(SearchCategorizedCommand),

    #[cfg(debug_assertions)]
    Debug,
}

pub(crate) fn run() {
    let args = Cli::parse();

    #[cfg(debug_assertions)]
    println!("{args:#?}");

    let result = match args.command {
        Command::Search(command) => command.run(),
        Command::SearchCategorized(command) => command.run(),

        #[cfg(debug_assertions)]
        Command::Debug => debug(),
    };

    if let Err(error) = result {
        eprintln!("{error}");
        exit(1);
    }
}

#[cfg(debug_assertions)]
use crate::Result;

#[cfg(debug_assertions)]
fn debug() -> Result<()> {
    Err(Error::Generic("Nothing here".to_string()))
}
