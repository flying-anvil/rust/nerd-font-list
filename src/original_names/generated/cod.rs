// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_cod.sh


/// Count of all Codicons Icons (Not an icon itself)
pub const COD_ICON_COUNT: usize = 387;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE_MEDIA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BROWSER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SMILEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COMPASS_ACTIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_STACKFRAME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TABLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIVE_SHARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_KEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PREVIEW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GRAPH_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TOOLS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MENU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RUN_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILTER_FILLED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SERVER_PROCESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ACCOUNT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_PAUSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYNC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EXPAND_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CASE_SENSITIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_STAR_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MILESTONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE_BINARY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GITHUB_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PROJECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_COLOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SEARCH_STOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TERMINAL_CMD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE_PDF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GEAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIT_COMPARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VERSIONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE_ZIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MEGAPHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SPLIT_VERTICAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FOLDER_OPENED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIT_PULL_REQUEST_CREATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_LINE_BY_LINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ACTIVATE_BREAKPOINTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIST_FLAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_NEWLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_STAR_EMPTY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ZOOM_IN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DIFF_MODIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE_SYMLINK_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEVICE_CAMERA_VIDEO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHROME_MINIMIZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CIRCLE_SMALL_FILLED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EMPTY_WINDOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_JERSEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_STOP_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WINDOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_STRING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DASHBOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CIRCUIT_BOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TERMINAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SCREEN_NORMAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHECKLIST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE_SYMLINK_DIRECTORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TRIANGLE_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RECORD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VM_RUNNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MERGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FOLDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ROOT_FOLDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WORKSPACE_UNTRUSTED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_METHOD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PERSON_ADD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_FUNCTION_UNVERIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYNC_IGNORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SERVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LAYERS_DOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_SMALL_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_STEP_OUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_THUMBSDOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CIRCLE_LARGE_FILLED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_STEP_OVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_CONSTANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ISSUE_DRAFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VM_CONNECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DIFF_REMOVED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_UNSUPPORTED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIST_SECRET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MUTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_CONDITIONAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TERMINAL_DEBIAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_STRUCTURE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CIRCLE_FILLED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIST_UNORDERED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_STOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GRIPPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LINK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_NOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PRESERVE_CASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_QUESTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SHIELD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_PROPERTY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REGEX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_UNVERIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_DISCONNECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VM_ACTIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ZOOM_OUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIST_SELECTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FOLD_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PULSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPO_PULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REMOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CLOUD_UPLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_FUNCTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_ALT_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_NOTEBOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RUN_ABOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WAND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIT_PULL_REQUEST_CLOSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_HOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_STACKFRAME_ACTIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TRASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EXTENSIONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PACKAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WARNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_STEP_BACK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CLIPPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_HUBOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHROME_RESTORE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_START: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REFERENCES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CLOSE_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WHITESPACE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GRAPH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PINNED_DIRTY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ELLIPSIS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TERMINAL_UBUNTU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TERMINAL_TMUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_OUTPUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RUN_BELOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_VARIABLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BROADCAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TERMINAL_BASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_CONTINUE_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EYE_CLOSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DIFF_RENAMED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COMMENT_DISCUSSION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPLACE_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RUBY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GITHUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TYPE_HIERARCHY_SUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PLAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_THREE_BARS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PIE_CHART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VERIFIED_FILLED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ADD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BOLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REACTIONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DISCARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_NO_NEWLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIT_COMMIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DIFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIGHTBULB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_HORIZONTAL_RULE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REMOTE_EXPLORER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_NEW_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DIFF_ADDED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CLEAR_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_OPEN_PREVIEW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ORGANIZATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MARKDOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPO_FORKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CIRCLE_LARGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_RESTART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BOOKMARK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_UNMUTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ISSUES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GROUP_BY_REF_TYPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PRIMITIVE_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TERMINAL_LINUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIGHTBULB_AUTOFIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VARIABLE_GROUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_HEART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BELL_DOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MIRROR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_PARAMETER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SIGN_OUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PINNED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TRIANGLE_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_KEYWORD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_UNGROUP_BY_REF_TYPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LOADING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BRACKET_DOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_RULER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHEVRON_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIT_PULL_REQUEST_DRAFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SAVE_AS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIBRARY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CIRCLE_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LOCATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPLY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GLOBE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_RERUN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_UNLOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SIGN_IN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RSS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SEARCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SORT_PRECEDENCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_OCTOFACE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CALL_INCOMING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_BOTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPO_PUSH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PLAY_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WHOLE_WORD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_CONDITIONAL_UNVERIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WATCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARCHIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEVICE_CAMERA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SOURCE_CONTROL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_NOTEBOOK_TEMPLATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_INSPECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GITHUB_ACTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BRIEFCASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PERSON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FOLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CLOSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MULTIPLE_WINDOWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PAINTCAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COMMENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GO_TO_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RADIO_TOWER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DATABASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_LOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPLACE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SAVE_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SQUIRREL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WORD_WRAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_MISC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TASKLIST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PASS_FILLED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_NEW_FOLDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WORKSPACE_TRUSTED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHROME_CLOSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIST_TREE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COMPASS_DOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE_CODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_AZURE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHROME_MAXIMIZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FOLD_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GRAPH_SCATTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_HISTORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TYPE_HIERARCHY_SUPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LOCK_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_SMALL_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TWITTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BEAKER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REQUEST_CHANGES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MAGNET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPO_CLONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_JSON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPO_FORCE_PUSH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_DATA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_WORKSPACE_UNKNOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MORTAR_BOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SAVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_FIELD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TEXT_SIZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EXPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CLOUD_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FEEDBACK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MAIL_READ: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_BOOLEAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LAYERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LAYERS_ACTIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_INTERFACE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ISSUE_REOPENED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FLAME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REFRESH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SERVER_ENVIRONMENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REDO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EDIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_OPERATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIST_FILTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DIFF_IGNORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_SWAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BEAKER_STOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VM_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIT_MERGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TERMINAL_POWERSHELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_KEBAB_VERTICAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EYE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_THUMBSUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SETTINGS_GEAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_STAR_HALF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_SMALL_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_NAMESPACE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_QUOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COMPASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_AZURE_DEVOPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_EVENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LAYOUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COLLAPSE_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SETTINGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_STEP_INTO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TYPE_HIERARCHY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_ENUM_MEMBER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GITHUB_INVERTED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LIST_ORDERED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CREDIT_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_REVERSE_CONTINUE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ERROR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EXCLUDE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LINK_EXTERNAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_NUMERIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_ENUM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_SMALL_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHEVRON_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GRABBER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ROCKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TRIANGLE_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_RESTART_FRAME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_MENTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RUN_ERRORS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ROOT_FOLDER_OPENED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_SNIPPET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CALENDAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SCREEN_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ITALIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_DATA_UNVERIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_INFO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_REMOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_BREAKPOINT_LOG_UNVERIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_PLUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COLOR_MODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GIT_PULL_REQUEST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DESKTOP_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_KEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SPLIT_HORIZONTAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_CLASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_UNFOLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_CONSOLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_GRAPH_LINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_ARROW_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEVICE_MOBILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_EDITOR_LAYOUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_CONTINUE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_INBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHECK_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FOLDER_LIBRARY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_COMBINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BRACKET_ERROR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_RECORD_KEYS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHEVRON_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FOLDER_ACTIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_LAW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TRIANGLE_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_DEBUG_COVERAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_BOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_VERIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CHEVRON_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_FILE_SUBMODULE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_CALL_OUTGOING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_TELESCOPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const COD_SYMBOL_ARRAY: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const COD_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-cod-file-media" => COD_FILE_MEDIA,
    "nf-cod-browser" => COD_BROWSER,
    "nf-cod-smiley" => COD_SMILEY,
    "nf-cod-pass" => COD_PASS,
    "nf-cod-compass-active" => COD_COMPASS_ACTIVE,
    "nf-cod-debug-stackframe" => COD_DEBUG_STACKFRAME,
    "nf-cod-arrow-up" => COD_ARROW_UP,
    "nf-cod-table" => COD_TABLE,
    "nf-cod-live-share" => COD_LIVE_SHARE,
    "nf-cod-tag" => COD_TAG,
    "nf-cod-key" => COD_KEY,
    "nf-cod-preview" => COD_PREVIEW,
    "nf-cod-debug" => COD_DEBUG,
    "nf-cod-graph-left" => COD_GRAPH_LEFT,
    "nf-cod-tools" => COD_TOOLS,
    "nf-cod-menu" => COD_MENU,
    "nf-cod-run-all" => COD_RUN_ALL,
    "nf-cod-filter-filled" => COD_FILTER_FILLED,
    "nf-cod-server-process" => COD_SERVER_PROCESS,
    "nf-cod-account" => COD_ACCOUNT,
    "nf-cod-debug-pause" => COD_DEBUG_PAUSE,
    "nf-cod-sync" => COD_SYNC,
    "nf-cod-expand-all" => COD_EXPAND_ALL,
    "nf-cod-case-sensitive" => COD_CASE_SENSITIVE,
    "nf-cod-star-full" => COD_STAR_FULL,
    "nf-cod-milestone" => COD_MILESTONE,
    "nf-cod-file-binary" => COD_FILE_BINARY,
    "nf-cod-github-alt" => COD_GITHUB_ALT,
    "nf-cod-project" => COD_PROJECT,
    "nf-cod-symbol-color" => COD_SYMBOL_COLOR,
    "nf-cod-debug-alt" => COD_DEBUG_ALT,
    "nf-cod-repo" => COD_REPO,
    "nf-cod-search-stop" => COD_SEARCH_STOP,
    "nf-cod-terminal-cmd" => COD_TERMINAL_CMD,
    "nf-cod-file-pdf" => COD_FILE_PDF,
    "nf-cod-check" => COD_CHECK,
    "nf-cod-gear" => COD_GEAR,
    "nf-cod-git-compare" => COD_GIT_COMPARE,
    "nf-cod-versions" => COD_VERSIONS,
    "nf-cod-file-zip" => COD_FILE_ZIP,
    "nf-cod-copy" => COD_COPY,
    "nf-cod-megaphone" => COD_MEGAPHONE,
    "nf-cod-split-vertical" => COD_SPLIT_VERTICAL,
    "nf-cod-folder-opened" => COD_FOLDER_OPENED,
    "nf-cod-git-pull-request-create" => COD_GIT_PULL_REQUEST_CREATE,
    "nf-cod-debug-line-by-line" => COD_DEBUG_LINE_BY_LINE,
    "nf-cod-activate-breakpoints" => COD_ACTIVATE_BREAKPOINTS,
    "nf-cod-list-flat" => COD_LIST_FLAT,
    "nf-cod-debug-all" => COD_DEBUG_ALL,
    "nf-cod-newline" => COD_NEWLINE,
    "nf-cod-star-empty" => COD_STAR_EMPTY,
    "nf-cod-zoom-in" => COD_ZOOM_IN,
    "nf-cod-diff-modified" => COD_DIFF_MODIFIED,
    "nf-cod-file-symlink-file" => COD_FILE_SYMLINK_FILE,
    "nf-cod-device-camera-video" => COD_DEVICE_CAMERA_VIDEO,
    "nf-cod-chrome-minimize" => COD_CHROME_MINIMIZE,
    "nf-cod-circle-small-filled" => COD_CIRCLE_SMALL_FILLED,
    "nf-cod-empty-window" => COD_EMPTY_WINDOW,
    "nf-cod-jersey" => COD_JERSEY,
    "nf-cod-stop-circle" => COD_STOP_CIRCLE,
    "nf-cod-window" => COD_WINDOW,
    "nf-cod-symbol-string" => COD_SYMBOL_STRING,
    "nf-cod-dashboard" => COD_DASHBOARD,
    "nf-cod-circuit-board" => COD_CIRCUIT_BOARD,
    "nf-cod-terminal" => COD_TERMINAL,
    "nf-cod-screen-normal" => COD_SCREEN_NORMAL,
    "nf-cod-checklist" => COD_CHECKLIST,
    "nf-cod-file-symlink-directory" => COD_FILE_SYMLINK_DIRECTORY,
    "nf-cod-triangle-down" => COD_TRIANGLE_DOWN,
    "nf-cod-record" => COD_RECORD,
    "nf-cod-move" => COD_MOVE,
    "nf-cod-vm-running" => COD_VM_RUNNING,
    "nf-cod-merge" => COD_MERGE,
    "nf-cod-folder" => COD_FOLDER,
    "nf-cod-root-folder" => COD_ROOT_FOLDER,
    "nf-cod-workspace-untrusted" => COD_WORKSPACE_UNTRUSTED,
    "nf-cod-symbol-method" => COD_SYMBOL_METHOD,
    "nf-cod-person-add" => COD_PERSON_ADD,
    "nf-cod-debug-breakpoint-function-unverified" => COD_DEBUG_BREAKPOINT_FUNCTION_UNVERIFIED,
    "nf-cod-sync-ignored" => COD_SYNC_IGNORED,
    "nf-cod-server" => COD_SERVER,
    "nf-cod-layers-dot" => COD_LAYERS_DOT,
    "nf-cod-arrow-small-down" => COD_ARROW_SMALL_DOWN,
    "nf-cod-debug-step-out" => COD_DEBUG_STEP_OUT,
    "nf-cod-thumbsdown" => COD_THUMBSDOWN,
    "nf-cod-circle-large-filled" => COD_CIRCLE_LARGE_FILLED,
    "nf-cod-debug-step-over" => COD_DEBUG_STEP_OVER,
    "nf-cod-symbol-constant" => COD_SYMBOL_CONSTANT,
    "nf-cod-issue-draft" => COD_ISSUE_DRAFT,
    "nf-cod-vm-connect" => COD_VM_CONNECT,
    "nf-cod-diff-removed" => COD_DIFF_REMOVED,
    "nf-cod-debug-breakpoint-unsupported" => COD_DEBUG_BREAKPOINT_UNSUPPORTED,
    "nf-cod-gist-secret" => COD_GIST_SECRET,
    "nf-cod-mute" => COD_MUTE,
    "nf-cod-debug-breakpoint-conditional" => COD_DEBUG_BREAKPOINT_CONDITIONAL,
    "nf-cod-symbol-file" => COD_SYMBOL_FILE,
    "nf-cod-terminal-debian" => COD_TERMINAL_DEBIAN,
    "nf-cod-dash" => COD_DASH,
    "nf-cod-symbol-structure" => COD_SYMBOL_STRUCTURE,
    "nf-cod-circle-filled" => COD_CIRCLE_FILLED,
    "nf-cod-list-unordered" => COD_LIST_UNORDERED,
    "nf-cod-debug-stop" => COD_DEBUG_STOP,
    "nf-cod-gripper" => COD_GRIPPER,
    "nf-cod-link" => COD_LINK,
    "nf-cod-note" => COD_NOTE,
    "nf-cod-preserve-case" => COD_PRESERVE_CASE,
    "nf-cod-question" => COD_QUESTION,
    "nf-cod-shield" => COD_SHIELD,
    "nf-cod-symbol-property" => COD_SYMBOL_PROPERTY,
    "nf-cod-regex" => COD_REGEX,
    "nf-cod-unverified" => COD_UNVERIFIED,
    "nf-cod-debug-disconnect" => COD_DEBUG_DISCONNECT,
    "nf-cod-vm-active" => COD_VM_ACTIVE,
    "nf-cod-zoom-out" => COD_ZOOM_OUT,
    "nf-cod-list-selection" => COD_LIST_SELECTION,
    "nf-cod-fold-down" => COD_FOLD_DOWN,
    "nf-cod-pulse" => COD_PULSE,
    "nf-cod-repo-pull" => COD_REPO_PULL,
    "nf-cod-remove" => COD_REMOVE,
    "nf-cod-cloud-upload" => COD_CLOUD_UPLOAD,
    "nf-cod-debug-breakpoint-function" => COD_DEBUG_BREAKPOINT_FUNCTION,
    "nf-cod-debug-alt-small" => COD_DEBUG_ALT_SMALL,
    "nf-cod-notebook" => COD_NOTEBOOK,
    "nf-cod-run-above" => COD_RUN_ABOVE,
    "nf-cod-wand" => COD_WAND,
    "nf-cod-git-pull-request-closed" => COD_GIT_PULL_REQUEST_CLOSED,
    "nf-cod-home" => COD_HOME,
    "nf-cod-debug-stackframe-active" => COD_DEBUG_STACKFRAME_ACTIVE,
    "nf-cod-trash" => COD_TRASH,
    "nf-cod-extensions" => COD_EXTENSIONS,
    "nf-cod-package" => COD_PACKAGE,
    "nf-cod-bell" => COD_BELL,
    "nf-cod-filter" => COD_FILTER,
    "nf-cod-warning" => COD_WARNING,
    "nf-cod-debug-step-back" => COD_DEBUG_STEP_BACK,
    "nf-cod-clippy" => COD_CLIPPY,
    "nf-cod-hubot" => COD_HUBOT,
    "nf-cod-chrome-restore" => COD_CHROME_RESTORE,
    "nf-cod-debug-start" => COD_DEBUG_START,
    "nf-cod-references" => COD_REFERENCES,
    "nf-cod-close-all" => COD_CLOSE_ALL,
    "nf-cod-whitespace" => COD_WHITESPACE,
    "nf-cod-bug" => COD_BUG,
    "nf-cod-graph" => COD_GRAPH,
    "nf-cod-pinned-dirty" => COD_PINNED_DIRTY,
    "nf-cod-ellipsis" => COD_ELLIPSIS,
    "nf-cod-files" => COD_FILES,
    "nf-cod-terminal-ubuntu" => COD_TERMINAL_UBUNTU,
    "nf-cod-terminal-tmux" => COD_TERMINAL_TMUX,
    "nf-cod-output" => COD_OUTPUT,
    "nf-cod-run-below" => COD_RUN_BELOW,
    "nf-cod-symbol-variable" => COD_SYMBOL_VARIABLE,
    "nf-cod-broadcast" => COD_BROADCAST,
    "nf-cod-terminal-bash" => COD_TERMINAL_BASH,
    "nf-cod-debug-continue-small" => COD_DEBUG_CONTINUE_SMALL,
    "nf-cod-eye-closed" => COD_EYE_CLOSED,
    "nf-cod-arrow-right" => COD_ARROW_RIGHT,
    "nf-cod-diff-renamed" => COD_DIFF_RENAMED,
    "nf-cod-comment-discussion" => COD_COMMENT_DISCUSSION,
    "nf-cod-replace-all" => COD_REPLACE_ALL,
    "nf-cod-ruby" => COD_RUBY,
    "nf-cod-github" => COD_GITHUB,
    "nf-cod-type-hierarchy-sub" => COD_TYPE_HIERARCHY_SUB,
    "nf-cod-play" => COD_PLAY,
    "nf-cod-three-bars" => COD_THREE_BARS,
    "nf-cod-pie-chart" => COD_PIE_CHART,
    "nf-cod-verified-filled" => COD_VERIFIED_FILLED,
    "nf-cod-add" => COD_ADD,
    "nf-cod-bold" => COD_BOLD,
    "nf-cod-reactions" => COD_REACTIONS,
    "nf-cod-discard" => COD_DISCARD,
    "nf-cod-no-newline" => COD_NO_NEWLINE,
    "nf-cod-git-commit" => COD_GIT_COMMIT,
    "nf-cod-diff" => COD_DIFF,
    "nf-cod-lightbulb" => COD_LIGHTBULB,
    "nf-cod-horizontal-rule" => COD_HORIZONTAL_RULE,
    "nf-cod-remote-explorer" => COD_REMOTE_EXPLORER,
    "nf-cod-new-file" => COD_NEW_FILE,
    "nf-cod-diff-added" => COD_DIFF_ADDED,
    "nf-cod-clear-all" => COD_CLEAR_ALL,
    "nf-cod-open-preview" => COD_OPEN_PREVIEW,
    "nf-cod-organization" => COD_ORGANIZATION,
    "nf-cod-markdown" => COD_MARKDOWN,
    "nf-cod-repo-forked" => COD_REPO_FORKED,
    "nf-cod-circle-large" => COD_CIRCLE_LARGE,
    "nf-cod-debug-restart" => COD_DEBUG_RESTART,
    "nf-cod-bookmark" => COD_BOOKMARK,
    "nf-cod-unmute" => COD_UNMUTE,
    "nf-cod-issues" => COD_ISSUES,
    "nf-cod-group-by-ref-type" => COD_GROUP_BY_REF_TYPE,
    "nf-cod-primitive-square" => COD_PRIMITIVE_SQUARE,
    "nf-cod-terminal-linux" => COD_TERMINAL_LINUX,
    "nf-cod-circle" => COD_CIRCLE,
    "nf-cod-lightbulb-autofix" => COD_LIGHTBULB_AUTOFIX,
    "nf-cod-variable-group" => COD_VARIABLE_GROUP,
    "nf-cod-mail" => COD_MAIL,
    "nf-cod-heart" => COD_HEART,
    "nf-cod-bell-dot" => COD_BELL_DOT,
    "nf-cod-mirror" => COD_MIRROR,
    "nf-cod-symbol-parameter" => COD_SYMBOL_PARAMETER,
    "nf-cod-sign-out" => COD_SIGN_OUT,
    "nf-cod-pinned" => COD_PINNED,
    "nf-cod-triangle-up" => COD_TRIANGLE_UP,
    "nf-cod-symbol-keyword" => COD_SYMBOL_KEYWORD,
    "nf-cod-ungroup-by-ref-type" => COD_UNGROUP_BY_REF_TYPE,
    "nf-cod-loading" => COD_LOADING,
    "nf-cod-bracket-dot" => COD_BRACKET_DOT,
    "nf-cod-lock" => COD_LOCK,
    "nf-cod-symbol-ruler" => COD_SYMBOL_RULER,
    "nf-cod-chevron-down" => COD_CHEVRON_DOWN,
    "nf-cod-git-pull-request-draft" => COD_GIT_PULL_REQUEST_DRAFT,
    "nf-cod-save-as" => COD_SAVE_AS,
    "nf-cod-library" => COD_LIBRARY,
    "nf-cod-circle-slash" => COD_CIRCLE_SLASH,
    "nf-cod-location" => COD_LOCATION,
    "nf-cod-reply" => COD_REPLY,
    "nf-cod-globe" => COD_GLOBE,
    "nf-cod-debug-rerun" => COD_DEBUG_RERUN,
    "nf-cod-unlock" => COD_UNLOCK,
    "nf-cod-sign-in" => COD_SIGN_IN,
    "nf-cod-rss" => COD_RSS,
    "nf-cod-search" => COD_SEARCH,
    "nf-cod-sort-precedence" => COD_SORT_PRECEDENCE,
    "nf-cod-octoface" => COD_OCTOFACE,
    "nf-cod-call-incoming" => COD_CALL_INCOMING,
    "nf-cod-vm" => COD_VM,
    "nf-cod-arrow-both" => COD_ARROW_BOTH,
    "nf-cod-repo-push" => COD_REPO_PUSH,
    "nf-cod-play-circle" => COD_PLAY_CIRCLE,
    "nf-cod-whole-word" => COD_WHOLE_WORD,
    "nf-cod-debug-breakpoint-conditional-unverified" => COD_DEBUG_BREAKPOINT_CONDITIONAL_UNVERIFIED,
    "nf-cod-watch" => COD_WATCH,
    "nf-cod-archive" => COD_ARCHIVE,
    "nf-cod-device-camera" => COD_DEVICE_CAMERA,
    "nf-cod-source-control" => COD_SOURCE_CONTROL,
    "nf-cod-notebook-template" => COD_NOTEBOOK_TEMPLATE,
    "nf-cod-inspect" => COD_INSPECT,
    "nf-cod-github-action" => COD_GITHUB_ACTION,
    "nf-cod-briefcase" => COD_BRIEFCASE,
    "nf-cod-person" => COD_PERSON,
    "nf-cod-fold" => COD_FOLD,
    "nf-cod-close" => COD_CLOSE,
    "nf-cod-multiple-windows" => COD_MULTIPLE_WINDOWS,
    "nf-cod-gift" => COD_GIFT,
    "nf-cod-paintcan" => COD_PAINTCAN,
    "nf-cod-comment" => COD_COMMENT,
    "nf-cod-go-to-file" => COD_GO_TO_FILE,
    "nf-cod-radio-tower" => COD_RADIO_TOWER,
    "nf-cod-database" => COD_DATABASE,
    "nf-cod-debug-breakpoint-log" => COD_DEBUG_BREAKPOINT_LOG,
    "nf-cod-replace" => COD_REPLACE,
    "nf-cod-save-all" => COD_SAVE_ALL,
    "nf-cod-squirrel" => COD_SQUIRREL,
    "nf-cod-word-wrap" => COD_WORD_WRAP,
    "nf-cod-symbol-misc" => COD_SYMBOL_MISC,
    "nf-cod-tasklist" => COD_TASKLIST,
    "nf-cod-pass-filled" => COD_PASS_FILLED,
    "nf-cod-new-folder" => COD_NEW_FOLDER,
    "nf-cod-workspace-trusted" => COD_WORKSPACE_TRUSTED,
    "nf-cod-chrome-close" => COD_CHROME_CLOSE,
    "nf-cod-list-tree" => COD_LIST_TREE,
    "nf-cod-compass-dot" => COD_COMPASS_DOT,
    "nf-cod-file-code" => COD_FILE_CODE,
    "nf-cod-azure" => COD_AZURE,
    "nf-cod-chrome-maximize" => COD_CHROME_MAXIMIZE,
    "nf-cod-fold-up" => COD_FOLD_UP,
    "nf-cod-graph-scatter" => COD_GRAPH_SCATTER,
    "nf-cod-history" => COD_HISTORY,
    "nf-cod-type-hierarchy-super" => COD_TYPE_HIERARCHY_SUPER,
    "nf-cod-lock-small" => COD_LOCK_SMALL,
    "nf-cod-arrow-small-left" => COD_ARROW_SMALL_LEFT,
    "nf-cod-twitter" => COD_TWITTER,
    "nf-cod-beaker" => COD_BEAKER,
    "nf-cod-pin" => COD_PIN,
    "nf-cod-request-changes" => COD_REQUEST_CHANGES,
    "nf-cod-magnet" => COD_MAGNET,
    "nf-cod-repo-clone" => COD_REPO_CLONE,
    "nf-cod-json" => COD_JSON,
    "nf-cod-repo-force-push" => COD_REPO_FORCE_PUSH,
    "nf-cod-cloud" => COD_CLOUD,
    "nf-cod-debug-breakpoint-data" => COD_DEBUG_BREAKPOINT_DATA,
    "nf-cod-workspace-unknown" => COD_WORKSPACE_UNKNOWN,
    "nf-cod-mortar-board" => COD_MORTAR_BOARD,
    "nf-cod-save" => COD_SAVE,
    "nf-cod-symbol-field" => COD_SYMBOL_FIELD,
    "nf-cod-text-size" => COD_TEXT_SIZE,
    "nf-cod-export" => COD_EXPORT,
    "nf-cod-cloud-download" => COD_CLOUD_DOWNLOAD,
    "nf-cod-feedback" => COD_FEEDBACK,
    "nf-cod-mail-read" => COD_MAIL_READ,
    "nf-cod-symbol-boolean" => COD_SYMBOL_BOOLEAN,
    "nf-cod-layers" => COD_LAYERS,
    "nf-cod-layers-active" => COD_LAYERS_ACTIVE,
    "nf-cod-symbol-interface" => COD_SYMBOL_INTERFACE,
    "nf-cod-issue-reopened" => COD_ISSUE_REOPENED,
    "nf-cod-flame" => COD_FLAME,
    "nf-cod-refresh" => COD_REFRESH,
    "nf-cod-server-environment" => COD_SERVER_ENVIRONMENT,
    "nf-cod-redo" => COD_REDO,
    "nf-cod-arrow-down" => COD_ARROW_DOWN,
    "nf-cod-edit" => COD_EDIT,
    "nf-cod-symbol-operator" => COD_SYMBOL_OPERATOR,
    "nf-cod-list-filter" => COD_LIST_FILTER,
    "nf-cod-diff-ignored" => COD_DIFF_IGNORED,
    "nf-cod-arrow-swap" => COD_ARROW_SWAP,
    "nf-cod-beaker-stop" => COD_BEAKER_STOP,
    "nf-cod-vm-outline" => COD_VM_OUTLINE,
    "nf-cod-git-merge" => COD_GIT_MERGE,
    "nf-cod-terminal-powershell" => COD_TERMINAL_POWERSHELL,
    "nf-cod-kebab-vertical" => COD_KEBAB_VERTICAL,
    "nf-cod-eye" => COD_EYE,
    "nf-cod-thumbsup" => COD_THUMBSUP,
    "nf-cod-settings-gear" => COD_SETTINGS_GEAR,
    "nf-cod-star-half" => COD_STAR_HALF,
    "nf-cod-arrow-small-up" => COD_ARROW_SMALL_UP,
    "nf-cod-symbol-namespace" => COD_SYMBOL_NAMESPACE,
    "nf-cod-report" => COD_REPORT,
    "nf-cod-quote" => COD_QUOTE,
    "nf-cod-compass" => COD_COMPASS,
    "nf-cod-azure-devops" => COD_AZURE_DEVOPS,
    "nf-cod-symbol-event" => COD_SYMBOL_EVENT,
    "nf-cod-layout" => COD_LAYOUT,
    "nf-cod-collapse-all" => COD_COLLAPSE_ALL,
    "nf-cod-settings" => COD_SETTINGS,
    "nf-cod-debug-step-into" => COD_DEBUG_STEP_INTO,
    "nf-cod-type-hierarchy" => COD_TYPE_HIERARCHY,
    "nf-cod-symbol-enum-member" => COD_SYMBOL_ENUM_MEMBER,
    "nf-cod-github-inverted" => COD_GITHUB_INVERTED,
    "nf-cod-list-ordered" => COD_LIST_ORDERED,
    "nf-cod-credit-card" => COD_CREDIT_CARD,
    "nf-cod-debug-reverse-continue" => COD_DEBUG_REVERSE_CONTINUE,
    "nf-cod-error" => COD_ERROR,
    "nf-cod-file" => COD_FILE,
    "nf-cod-exclude" => COD_EXCLUDE,
    "nf-cod-link-external" => COD_LINK_EXTERNAL,
    "nf-cod-symbol-numeric" => COD_SYMBOL_NUMERIC,
    "nf-cod-symbol-enum" => COD_SYMBOL_ENUM,
    "nf-cod-arrow-small-right" => COD_ARROW_SMALL_RIGHT,
    "nf-cod-chevron-right" => COD_CHEVRON_RIGHT,
    "nf-cod-grabber" => COD_GRABBER,
    "nf-cod-rocket" => COD_ROCKET,
    "nf-cod-triangle-left" => COD_TRIANGLE_LEFT,
    "nf-cod-debug-restart-frame" => COD_DEBUG_RESTART_FRAME,
    "nf-cod-mention" => COD_MENTION,
    "nf-cod-run-errors" => COD_RUN_ERRORS,
    "nf-cod-root-folder-opened" => COD_ROOT_FOLDER_OPENED,
    "nf-cod-symbol-snippet" => COD_SYMBOL_SNIPPET,
    "nf-cod-calendar" => COD_CALENDAR,
    "nf-cod-screen-full" => COD_SCREEN_FULL,
    "nf-cod-italic" => COD_ITALIC,
    "nf-cod-debug-breakpoint-data-unverified" => COD_DEBUG_BREAKPOINT_DATA_UNVERIFIED,
    "nf-cod-info" => COD_INFO,
    "nf-cod-remote" => COD_REMOTE,
    "nf-cod-debug-breakpoint-log-unverified" => COD_DEBUG_BREAKPOINT_LOG_UNVERIFIED,
    "nf-cod-plug" => COD_PLUG,
    "nf-cod-color-mode" => COD_COLOR_MODE,
    "nf-cod-git-pull-request" => COD_GIT_PULL_REQUEST,
    "nf-cod-desktop-download" => COD_DESKTOP_DOWNLOAD,
    "nf-cod-symbol-key" => COD_SYMBOL_KEY,
    "nf-cod-split-horizontal" => COD_SPLIT_HORIZONTAL,
    "nf-cod-symbol-class" => COD_SYMBOL_CLASS,
    "nf-cod-unfold" => COD_UNFOLD,
    "nf-cod-debug-console" => COD_DEBUG_CONSOLE,
    "nf-cod-graph-line" => COD_GRAPH_LINE,
    "nf-cod-arrow-left" => COD_ARROW_LEFT,
    "nf-cod-device-mobile" => COD_DEVICE_MOBILE,
    "nf-cod-editor-layout" => COD_EDITOR_LAYOUT,
    "nf-cod-debug-continue" => COD_DEBUG_CONTINUE,
    "nf-cod-inbox" => COD_INBOX,
    "nf-cod-check-all" => COD_CHECK_ALL,
    "nf-cod-folder-library" => COD_FOLDER_LIBRARY,
    "nf-cod-combine" => COD_COMBINE,
    "nf-cod-bracket-error" => COD_BRACKET_ERROR,
    "nf-cod-record-keys" => COD_RECORD_KEYS,
    "nf-cod-chevron-up" => COD_CHEVRON_UP,
    "nf-cod-folder-active" => COD_FOLDER_ACTIVE,
    "nf-cod-law" => COD_LAW,
    "nf-cod-triangle-right" => COD_TRIANGLE_RIGHT,
    "nf-cod-debug-coverage" => COD_DEBUG_COVERAGE,
    "nf-cod-book" => COD_BOOK,
    "nf-cod-code" => COD_CODE,
    "nf-cod-verified" => COD_VERIFIED,
    "nf-cod-chevron-left" => COD_CHEVRON_LEFT,
    "nf-cod-file-submodule" => COD_FILE_SUBMODULE,
    "nf-cod-call-outgoing" => COD_CALL_OUTGOING,
    "nf-cod-telescope" => COD_TELESCOPE,
    "nf-cod-symbol-array" => COD_SYMBOL_ARRAY,
};
