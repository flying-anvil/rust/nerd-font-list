// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_weather.sh


/// Count of all Weather Icons (Not an icon itself)
pub const WEATHER_ICON_COUNT: usize = 228;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_HORIZON_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_CRESCENT_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_GIBBOUS_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_9: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_CLOUDY_HIGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SOLAR_ECLIPSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DIRECTION_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_GIBBOUS_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TRAIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_THUNDERSTORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_GIBBOUS_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_GALE_WARNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_CRESCENT_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_RAIN_MIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_CRESCENT_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_SHOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DIRECTION_UP_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_RAIN_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_THUNDERSTORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_VOLCANO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_CRESCENT_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOONRISE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_8: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_CRESCENT_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_GIBBOUS_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_PARTLY_CLOUDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_NORTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_FOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_CRESCENT_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_STARS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_10: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_WINDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_FIRST_QUARTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DIRECTION_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_CLOUDY_WINDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_CLOUDY_WINDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DIRECTION_DOWN_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_8: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SANDSTORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SNOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_CLOUDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_GIBBOUS_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_THERMOMETER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_STORM_SHOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_EARTHQUAKE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_GIBBOUS_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_GIBBOUS_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_LIGHTNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_HAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SUNRISE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_BAROMETER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_HAZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_GIBBOUS_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_CLOUDY_GUSTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_THIRD_QUARTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_SNOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SNOW_THUNDERSTORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_SNOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_STORM_SHOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_HURRICANE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SPRINKLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_CLOUDY_HIGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DUST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_CRESCENT_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_CRESCENT_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SNOWFLAKE_COLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_SNOW_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SUNNY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DEGREES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SMOKE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_GIBBOUS_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_12: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SNOW_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_LIGHTNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SNOW_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_RAIN_MIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_GIBBOUS_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DIRECTION_UP_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_STORM_SHOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_RAIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_ALIENS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_LUNAR_ECLIPSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_11: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_FAHRENHEIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TSUNAMI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_HURRICANE_WARNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_NORTH_WEST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_GIBBOUS_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_HAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_CLOUDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_CELSIUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_GIBBOUS_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_LIGHT_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_12: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_CRESCENT_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SUNSET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_SNOW_THUNDERSTORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_HUMIDITY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_GIBBOUS_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_CRESCENT_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_GIBBOUS_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SHOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_CRESCENT_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_SPRINKLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_GIBBOUS_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_CLOUDY_GUSTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_9: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_10: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_CLOUDY_GUSTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_7: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_CRESCENT_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_RAINDROP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_SPRINKLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_THUNDERSTORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_CLOUDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_STORM_WARNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_CRESCENT_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_GIBBOUS_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_REFRESH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_SOUTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DIRECTION_DOWN_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_CLOUD_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_EAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_GIBBOUS_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_SLEET_STORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_CLOUD_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_HAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SPRINKLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_HOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SNOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_CRESCENT_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_CLOUD_REFRESH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SLEET_STORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_GIBBOUS_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOONSET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SMALL_CRAFT_ADVISORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_CRESCENT_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SMOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_RAIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_FOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_RAINDROPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_CRESCENT_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SHOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_THUNDERSTORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_THERMOMETER_EXTERIOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_STRONG_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_RAIN_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_GIBBOUS_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_UMBRELLA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_11: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WINDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_CLEAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_DIRECTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_SLEET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_SOUTH_WEST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TORNADO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_CRESCENT_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_SHOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_SLEET_STORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_FIRE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_7: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_CRESCENT_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_CLOUDY_WINDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_SNOW_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_RAIN_MIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_SLEET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_FLOOD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_FOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_RAIN_MIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_WEST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_NEW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_PARTLY_CLOUDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_LIGHTNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SUNNY_OVERCAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_0: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_SOUTH_EAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DIRECTION_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_CRESCENT_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_CLOUDY_GUSTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_GIBBOUS_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_CRESCENT_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_REFRESH_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_STORM_SHOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_RAIN_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_GIBBOUS_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WAXING_CRESCENT_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_WANING_CRESCENT_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_FIRST_QUARTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_CLOUDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_ALIEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_CLOUDY_WINDY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DIRECTION_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_RAIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_GIBBOUS_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_HORIZON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_GIBBOUS_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WANING_GIBBOUS_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_HAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_SNOW_THUNDERSTORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_THERMOMETER_INTERNAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_TIME_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_THIRD_QUARTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_NORTH_EAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_RAIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_CRESCENT_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_NEW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_SLEET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_CLOUDY_HIGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_METEOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_DAY_SLEET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_WIND_BEAUFORT_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_LIGHTNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_NIGHT_ALT_RAIN_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const WEATHER_MOON_ALT_WAXING_CRESCENT_4: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const WEATHER_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-weather-horizon-alt" => WEATHER_HORIZON_ALT,
    "nf-weather-moon-waning-crescent-4" => WEATHER_MOON_WANING_CRESCENT_4,
    "nf-weather-moon-alt-waxing-gibbous-5" => WEATHER_MOON_ALT_WAXING_GIBBOUS_5,
    "nf-weather-wind-beaufort-9" => WEATHER_WIND_BEAUFORT_9,
    "nf-weather-day-cloudy-high" => WEATHER_DAY_CLOUDY_HIGH,
    "nf-weather-solar-eclipse" => WEATHER_SOLAR_ECLIPSE,
    "nf-weather-direction-down" => WEATHER_DIRECTION_DOWN,
    "nf-weather-moon-waxing-gibbous-4" => WEATHER_MOON_WAXING_GIBBOUS_4,
    "nf-weather-train" => WEATHER_TRAIN,
    "nf-weather-day-thunderstorm" => WEATHER_DAY_THUNDERSTORM,
    "nf-weather-time-5" => WEATHER_TIME_5,
    "nf-weather-moon-waning-gibbous-1" => WEATHER_MOON_WANING_GIBBOUS_1,
    "nf-weather-gale-warning" => WEATHER_GALE_WARNING,
    "nf-weather-moon-alt-waxing-crescent-6" => WEATHER_MOON_ALT_WAXING_CRESCENT_6,
    "nf-weather-night-rain-mix" => WEATHER_NIGHT_RAIN_MIX,
    "nf-weather-moon-alt-waning-crescent-4" => WEATHER_MOON_ALT_WANING_CRESCENT_4,
    "nf-weather-night-showers" => WEATHER_NIGHT_SHOWERS,
    "nf-weather-direction-up-left" => WEATHER_DIRECTION_UP_LEFT,
    "nf-weather-time-3" => WEATHER_TIME_3,
    "nf-weather-wind-beaufort-3" => WEATHER_WIND_BEAUFORT_3,
    "nf-weather-night-rain-wind" => WEATHER_NIGHT_RAIN_WIND,
    "nf-weather-thunderstorm" => WEATHER_THUNDERSTORM,
    "nf-weather-volcano" => WEATHER_VOLCANO,
    "nf-weather-moon-waning-crescent-2" => WEATHER_MOON_WANING_CRESCENT_2,
    "nf-weather-moonrise" => WEATHER_MOONRISE,
    "nf-weather-wind-beaufort-8" => WEATHER_WIND_BEAUFORT_8,
    "nf-weather-moon-waxing-crescent-1" => WEATHER_MOON_WAXING_CRESCENT_1,
    "nf-weather-moon-alt-waxing-gibbous-3" => WEATHER_MOON_ALT_WAXING_GIBBOUS_3,
    "nf-weather-night-alt-partly-cloudy" => WEATHER_NIGHT_ALT_PARTLY_CLOUDY,
    "nf-weather-wind-north" => WEATHER_WIND_NORTH,
    "nf-weather-wind-beaufort-5" => WEATHER_WIND_BEAUFORT_5,
    "nf-weather-fog" => WEATHER_FOG,
    "nf-weather-moon-waxing-crescent-6" => WEATHER_MOON_WAXING_CRESCENT_6,
    "nf-weather-stars" => WEATHER_STARS,
    "nf-weather-wind-beaufort-10" => WEATHER_WIND_BEAUFORT_10,
    "nf-weather-day-windy" => WEATHER_DAY_WINDY,
    "nf-weather-moon-alt-first-quarter" => WEATHER_MOON_ALT_FIRST_QUARTER,
    "nf-weather-direction-up" => WEATHER_DIRECTION_UP,
    "nf-weather-day-cloudy-windy" => WEATHER_DAY_CLOUDY_WINDY,
    "nf-weather-night-cloudy-windy" => WEATHER_NIGHT_CLOUDY_WINDY,
    "nf-weather-direction-down-right" => WEATHER_DIRECTION_DOWN_RIGHT,
    "nf-weather-time-8" => WEATHER_TIME_8,
    "nf-weather-sandstorm" => WEATHER_SANDSTORM,
    "nf-weather-day-snow" => WEATHER_DAY_SNOW,
    "nf-weather-night-alt-cloudy" => WEATHER_NIGHT_ALT_CLOUDY,
    "nf-weather-moon-alt-waning-gibbous-5" => WEATHER_MOON_ALT_WANING_GIBBOUS_5,
    "nf-weather-thermometer" => WEATHER_THERMOMETER,
    "nf-weather-storm-showers" => WEATHER_STORM_SHOWERS,
    "nf-weather-earthquake" => WEATHER_EARTHQUAKE,
    "nf-weather-moon-alt-waxing-gibbous-1" => WEATHER_MOON_ALT_WAXING_GIBBOUS_1,
    "nf-weather-moon-waxing-gibbous-5" => WEATHER_MOON_WAXING_GIBBOUS_5,
    "nf-weather-day-lightning" => WEATHER_DAY_LIGHTNING,
    "nf-weather-day-hail" => WEATHER_DAY_HAIL,
    "nf-weather-sunrise" => WEATHER_SUNRISE,
    "nf-weather-barometer" => WEATHER_BAROMETER,
    "nf-weather-day-haze" => WEATHER_DAY_HAZE,
    "nf-weather-moon-alt-waxing-gibbous-4" => WEATHER_MOON_ALT_WAXING_GIBBOUS_4,
    "nf-weather-night-cloudy-gusts" => WEATHER_NIGHT_CLOUDY_GUSTS,
    "nf-weather-moon-alt-third-quarter" => WEATHER_MOON_ALT_THIRD_QUARTER,
    "nf-weather-night-snow" => WEATHER_NIGHT_SNOW,
    "nf-weather-day-snow-thunderstorm" => WEATHER_DAY_SNOW_THUNDERSTORM,
    "nf-weather-night-alt-snow" => WEATHER_NIGHT_ALT_SNOW,
    "nf-weather-night-storm-showers" => WEATHER_NIGHT_STORM_SHOWERS,
    "nf-weather-hurricane" => WEATHER_HURRICANE,
    "nf-weather-sprinkle" => WEATHER_SPRINKLE,
    "nf-weather-night-cloudy-high" => WEATHER_NIGHT_CLOUDY_HIGH,
    "nf-weather-dust" => WEATHER_DUST,
    "nf-weather-moon-waning-crescent-5" => WEATHER_MOON_WANING_CRESCENT_5,
    "nf-weather-moon-alt-waning-crescent-5" => WEATHER_MOON_ALT_WANING_CRESCENT_5,
    "nf-weather-snowflake-cold" => WEATHER_SNOWFLAKE_COLD,
    "nf-weather-night-alt-snow-wind" => WEATHER_NIGHT_ALT_SNOW_WIND,
    "nf-weather-day-sunny" => WEATHER_DAY_SUNNY,
    "nf-weather-na" => WEATHER_NA,
    "nf-weather-degrees" => WEATHER_DEGREES,
    "nf-weather-smoke" => WEATHER_SMOKE,
    "nf-weather-moon-waxing-gibbous-1" => WEATHER_MOON_WAXING_GIBBOUS_1,
    "nf-weather-time-12" => WEATHER_TIME_12,
    "nf-weather-day-snow-wind" => WEATHER_DAY_SNOW_WIND,
    "nf-weather-night-lightning" => WEATHER_NIGHT_LIGHTNING,
    "nf-weather-snow-wind" => WEATHER_SNOW_WIND,
    "nf-weather-night-alt-rain-mix" => WEATHER_NIGHT_ALT_RAIN_MIX,
    "nf-weather-moon-full" => WEATHER_MOON_FULL,
    "nf-weather-moon-alt-waning-gibbous-1" => WEATHER_MOON_ALT_WANING_GIBBOUS_1,
    "nf-weather-direction-up-right" => WEATHER_DIRECTION_UP_RIGHT,
    "nf-weather-night-alt-storm-showers" => WEATHER_NIGHT_ALT_STORM_SHOWERS,
    "nf-weather-rain" => WEATHER_RAIN,
    "nf-weather-aliens" => WEATHER_ALIENS,
    "nf-weather-lunar-eclipse" => WEATHER_LUNAR_ECLIPSE,
    "nf-weather-time-11" => WEATHER_TIME_11,
    "nf-weather-fahrenheit" => WEATHER_FAHRENHEIT,
    "nf-weather-tsunami" => WEATHER_TSUNAMI,
    "nf-weather-hurricane-warning" => WEATHER_HURRICANE_WARNING,
    "nf-weather-wind-north-west" => WEATHER_WIND_NORTH_WEST,
    "nf-weather-moon-alt-waxing-gibbous-2" => WEATHER_MOON_ALT_WAXING_GIBBOUS_2,
    "nf-weather-hail" => WEATHER_HAIL,
    "nf-weather-wind-beaufort-4" => WEATHER_WIND_BEAUFORT_4,
    "nf-weather-day-cloudy" => WEATHER_DAY_CLOUDY,
    "nf-weather-celsius" => WEATHER_CELSIUS,
    "nf-weather-moon-waning-gibbous-3" => WEATHER_MOON_WANING_GIBBOUS_3,
    "nf-weather-day-light-wind" => WEATHER_DAY_LIGHT_WIND,
    "nf-weather-wind-beaufort-12" => WEATHER_WIND_BEAUFORT_12,
    "nf-weather-moon-waning-crescent-1" => WEATHER_MOON_WANING_CRESCENT_1,
    "nf-weather-sunset" => WEATHER_SUNSET,
    "nf-weather-night-snow-thunderstorm" => WEATHER_NIGHT_SNOW_THUNDERSTORM,
    "nf-weather-humidity" => WEATHER_HUMIDITY,
    "nf-weather-time-2" => WEATHER_TIME_2,
    "nf-weather-moon-waning-gibbous-5" => WEATHER_MOON_WANING_GIBBOUS_5,
    "nf-weather-moon-alt-waning-crescent-2" => WEATHER_MOON_ALT_WANING_CRESCENT_2,
    "nf-weather-moon-waning-gibbous-2" => WEATHER_MOON_WANING_GIBBOUS_2,
    "nf-weather-showers" => WEATHER_SHOWERS,
    "nf-weather-moon-waxing-crescent-2" => WEATHER_MOON_WAXING_CRESCENT_2,
    "nf-weather-night-sprinkle" => WEATHER_NIGHT_SPRINKLE,
    "nf-weather-moon-waxing-gibbous-6" => WEATHER_MOON_WAXING_GIBBOUS_6,
    "nf-weather-cloudy-gusts" => WEATHER_CLOUDY_GUSTS,
    "nf-weather-time-9" => WEATHER_TIME_9,
    "nf-weather-time-10" => WEATHER_TIME_10,
    "nf-weather-day-cloudy-gusts" => WEATHER_DAY_CLOUDY_GUSTS,
    "nf-weather-wind-beaufort-7" => WEATHER_WIND_BEAUFORT_7,
    "nf-weather-moon-alt-waning-crescent-6" => WEATHER_MOON_ALT_WANING_CRESCENT_6,
    "nf-weather-raindrop" => WEATHER_RAINDROP,
    "nf-weather-night-alt-sprinkle" => WEATHER_NIGHT_ALT_SPRINKLE,
    "nf-weather-night-thunderstorm" => WEATHER_NIGHT_THUNDERSTORM,
    "nf-weather-cloudy" => WEATHER_CLOUDY,
    "nf-weather-storm-warning" => WEATHER_STORM_WARNING,
    "nf-weather-cloud" => WEATHER_CLOUD,
    "nf-weather-moon-alt-waning-crescent-1" => WEATHER_MOON_ALT_WANING_CRESCENT_1,
    "nf-weather-moon-waning-gibbous-4" => WEATHER_MOON_WANING_GIBBOUS_4,
    "nf-weather-refresh" => WEATHER_REFRESH,
    "nf-weather-wind-beaufort-1" => WEATHER_WIND_BEAUFORT_1,
    "nf-weather-wind-south" => WEATHER_WIND_SOUTH,
    "nf-weather-direction-down-left" => WEATHER_DIRECTION_DOWN_LEFT,
    "nf-weather-cloud-up" => WEATHER_CLOUD_UP,
    "nf-weather-wind-east" => WEATHER_WIND_EAST,
    "nf-weather-moon-waxing-gibbous-3" => WEATHER_MOON_WAXING_GIBBOUS_3,
    "nf-weather-night-alt-sleet-storm" => WEATHER_NIGHT_ALT_SLEET_STORM,
    "nf-weather-cloud-down" => WEATHER_CLOUD_DOWN,
    "nf-weather-night-alt-hail" => WEATHER_NIGHT_ALT_HAIL,
    "nf-weather-day-sprinkle" => WEATHER_DAY_SPRINKLE,
    "nf-weather-hot" => WEATHER_HOT,
    "nf-weather-snow" => WEATHER_SNOW,
    "nf-weather-moon-alt-waxing-crescent-1" => WEATHER_MOON_ALT_WAXING_CRESCENT_1,
    "nf-weather-cloud-refresh" => WEATHER_CLOUD_REFRESH,
    "nf-weather-day-sleet-storm" => WEATHER_DAY_SLEET_STORM,
    "nf-weather-moon-waxing-gibbous-2" => WEATHER_MOON_WAXING_GIBBOUS_2,
    "nf-weather-moonset" => WEATHER_MOONSET,
    "nf-weather-small-craft-advisory" => WEATHER_SMALL_CRAFT_ADVISORY,
    "nf-weather-moon-alt-waning-crescent-3" => WEATHER_MOON_ALT_WANING_CRESCENT_3,
    "nf-weather-smog" => WEATHER_SMOG,
    "nf-weather-night-alt-rain" => WEATHER_NIGHT_ALT_RAIN,
    "nf-weather-night-fog" => WEATHER_NIGHT_FOG,
    "nf-weather-raindrops" => WEATHER_RAINDROPS,
    "nf-weather-moon-alt-waxing-crescent-3" => WEATHER_MOON_ALT_WAXING_CRESCENT_3,
    "nf-weather-day-showers" => WEATHER_DAY_SHOWERS,
    "nf-weather-night-alt-thunderstorm" => WEATHER_NIGHT_ALT_THUNDERSTORM,
    "nf-weather-thermometer-exterior" => WEATHER_THERMOMETER_EXTERIOR,
    "nf-weather-strong-wind" => WEATHER_STRONG_WIND,
    "nf-weather-day-rain-wind" => WEATHER_DAY_RAIN_WIND,
    "nf-weather-moon-alt-waning-gibbous-6" => WEATHER_MOON_ALT_WANING_GIBBOUS_6,
    "nf-weather-umbrella" => WEATHER_UMBRELLA,
    "nf-weather-wind-beaufort-11" => WEATHER_WIND_BEAUFORT_11,
    "nf-weather-windy" => WEATHER_WINDY,
    "nf-weather-night-clear" => WEATHER_NIGHT_CLEAR,
    "nf-weather-wind-direction" => WEATHER_WIND_DIRECTION,
    "nf-weather-night-alt-sleet" => WEATHER_NIGHT_ALT_SLEET,
    "nf-weather-wind-south-west" => WEATHER_WIND_SOUTH_WEST,
    "nf-weather-tornado" => WEATHER_TORNADO,
    "nf-weather-moon-waning-crescent-3" => WEATHER_MOON_WANING_CRESCENT_3,
    "nf-weather-night-alt-showers" => WEATHER_NIGHT_ALT_SHOWERS,
    "nf-weather-night-sleet-storm" => WEATHER_NIGHT_SLEET_STORM,
    "nf-weather-moon-alt-full" => WEATHER_MOON_ALT_FULL,
    "nf-weather-fire" => WEATHER_FIRE,
    "nf-weather-time-7" => WEATHER_TIME_7,
    "nf-weather-moon-waxing-crescent-4" => WEATHER_MOON_WAXING_CRESCENT_4,
    "nf-weather-night-alt-cloudy-windy" => WEATHER_NIGHT_ALT_CLOUDY_WINDY,
    "nf-weather-night-snow-wind" => WEATHER_NIGHT_SNOW_WIND,
    "nf-weather-day-rain-mix" => WEATHER_DAY_RAIN_MIX,
    "nf-weather-sleet" => WEATHER_SLEET,
    "nf-weather-flood" => WEATHER_FLOOD,
    "nf-weather-day-fog" => WEATHER_DAY_FOG,
    "nf-weather-rain-mix" => WEATHER_RAIN_MIX,
    "nf-weather-wind-west" => WEATHER_WIND_WEST,
    "nf-weather-moon-new" => WEATHER_MOON_NEW,
    "nf-weather-night-partly-cloudy" => WEATHER_NIGHT_PARTLY_CLOUDY,
    "nf-weather-night-alt-lightning" => WEATHER_NIGHT_ALT_LIGHTNING,
    "nf-weather-day-sunny-overcast" => WEATHER_DAY_SUNNY_OVERCAST,
    "nf-weather-wind-beaufort-0" => WEATHER_WIND_BEAUFORT_0,
    "nf-weather-wind-south-east" => WEATHER_WIND_SOUTH_EAST,
    "nf-weather-time-4" => WEATHER_TIME_4,
    "nf-weather-direction-left" => WEATHER_DIRECTION_LEFT,
    "nf-weather-moon-alt-waxing-crescent-5" => WEATHER_MOON_ALT_WAXING_CRESCENT_5,
    "nf-weather-wind-beaufort-6" => WEATHER_WIND_BEAUFORT_6,
    "nf-weather-night-alt-cloudy-gusts" => WEATHER_NIGHT_ALT_CLOUDY_GUSTS,
    "nf-weather-moon-waning-gibbous-6" => WEATHER_MOON_WANING_GIBBOUS_6,
    "nf-weather-moon-waxing-crescent-3" => WEATHER_MOON_WAXING_CRESCENT_3,
    "nf-weather-refresh-alt" => WEATHER_REFRESH_ALT,
    "nf-weather-day-storm-showers" => WEATHER_DAY_STORM_SHOWERS,
    "nf-weather-rain-wind" => WEATHER_RAIN_WIND,
    "nf-weather-moon-alt-waning-gibbous-3" => WEATHER_MOON_ALT_WANING_GIBBOUS_3,
    "nf-weather-moon-waxing-crescent-5" => WEATHER_MOON_WAXING_CRESCENT_5,
    "nf-weather-moon-waning-crescent-6" => WEATHER_MOON_WANING_CRESCENT_6,
    "nf-weather-moon-first-quarter" => WEATHER_MOON_FIRST_QUARTER,
    "nf-weather-night-cloudy" => WEATHER_NIGHT_CLOUDY,
    "nf-weather-alien" => WEATHER_ALIEN,
    "nf-weather-cloudy-windy" => WEATHER_CLOUDY_WINDY,
    "nf-weather-direction-right" => WEATHER_DIRECTION_RIGHT,
    "nf-weather-night-rain" => WEATHER_NIGHT_RAIN,
    "nf-weather-time-6" => WEATHER_TIME_6,
    "nf-weather-moon-alt-waning-gibbous-4" => WEATHER_MOON_ALT_WANING_GIBBOUS_4,
    "nf-weather-horizon" => WEATHER_HORIZON,
    "nf-weather-moon-alt-waxing-gibbous-6" => WEATHER_MOON_ALT_WAXING_GIBBOUS_6,
    "nf-weather-moon-alt-waning-gibbous-2" => WEATHER_MOON_ALT_WANING_GIBBOUS_2,
    "nf-weather-night-hail" => WEATHER_NIGHT_HAIL,
    "nf-weather-night-alt-snow-thunderstorm" => WEATHER_NIGHT_ALT_SNOW_THUNDERSTORM,
    "nf-weather-thermometer-internal" => WEATHER_THERMOMETER_INTERNAL,
    "nf-weather-time-1" => WEATHER_TIME_1,
    "nf-weather-moon-third-quarter" => WEATHER_MOON_THIRD_QUARTER,
    "nf-weather-wind-north-east" => WEATHER_WIND_NORTH_EAST,
    "nf-weather-day-rain" => WEATHER_DAY_RAIN,
    "nf-weather-moon-alt-waxing-crescent-2" => WEATHER_MOON_ALT_WAXING_CRESCENT_2,
    "nf-weather-moon-alt-new" => WEATHER_MOON_ALT_NEW,
    "nf-weather-night-sleet" => WEATHER_NIGHT_SLEET,
    "nf-weather-night-alt-cloudy-high" => WEATHER_NIGHT_ALT_CLOUDY_HIGH,
    "nf-weather-meteor" => WEATHER_METEOR,
    "nf-weather-day-sleet" => WEATHER_DAY_SLEET,
    "nf-weather-wind-beaufort-2" => WEATHER_WIND_BEAUFORT_2,
    "nf-weather-lightning" => WEATHER_LIGHTNING,
    "nf-weather-night-alt-rain-wind" => WEATHER_NIGHT_ALT_RAIN_WIND,
    "nf-weather-moon-alt-waxing-crescent-4" => WEATHER_MOON_ALT_WAXING_CRESCENT_4,
};
