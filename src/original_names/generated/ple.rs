// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_ple.sh


/// Count of all Powerline Extra Symbols Icons (Not an icon itself)
pub const PLE_ICON_COUNT: usize = 38;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PL_HOSTNAME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_BACKSLASH_SEPARATOR_REDUNDANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_RIGHT_HALF_CIRCLE_THICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_COLUMN_NUMBER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_FORWARDSLASH_SEPARATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_FLAME_THIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_PIXELATED_SQUARES_BIG_MIRRORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PL_LINE_NUMBER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_FLAME_THICK_MIRRORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_UPPER_RIGHT_TRIANGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PL_RIGHT_SOFT_DIVIDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_ICE_WAVEFORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_LEFT_HALF_CIRCLE_THIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_FLAME_THIN_MIRRORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_ICE_WAVEFORM_MIRRORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_HONEYCOMB_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_LEGO_BLOCK_SIDEWAYS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_UPPER_LEFT_TRIANGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_LEGO_SEPARATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_TRAPEZOID_TOP_BOTTOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_TRAPEZOID_TOP_BOTTOM_MIRRORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_BACKSLASH_SEPARATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PL_RIGHT_HARD_DIVIDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_LOWER_RIGHT_TRIANGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PL_BRANCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_RIGHT_HALF_CIRCLE_THIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_FORWARDSLASH_SEPARATOR_REDUNDANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_LOWER_LEFT_TRIANGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_LEFT_HALF_CIRCLE_THICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_PIXELATED_SQUARES_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_HONEYCOMB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_FLAME_THICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PL_LEFT_HARD_DIVIDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_LEGO_SEPARATOR_THIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_LEGO_BLOCK_FACING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_PIXELATED_SQUARES_BIG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PL_LEFT_SOFT_DIVIDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const PLE_PIXELATED_SQUARES_SMALL_MIRRORED: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const PLE_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-pl-hostname" => PL_HOSTNAME,
    "nf-ple-backslash-separator-redundant" => PLE_BACKSLASH_SEPARATOR_REDUNDANT,
    "nf-ple-right-half-circle-thick" => PLE_RIGHT_HALF_CIRCLE_THICK,
    "nf-ple-column-number" => PLE_COLUMN_NUMBER,
    "nf-ple-forwardslash-separator" => PLE_FORWARDSLASH_SEPARATOR,
    "nf-ple-flame-thin" => PLE_FLAME_THIN,
    "nf-ple-pixelated-squares-big-mirrored" => PLE_PIXELATED_SQUARES_BIG_MIRRORED,
    "nf-pl-line-number" => PL_LINE_NUMBER,
    "nf-ple-flame-thick-mirrored" => PLE_FLAME_THICK_MIRRORED,
    "nf-ple-upper-right-triangle" => PLE_UPPER_RIGHT_TRIANGLE,
    "nf-pl-right-soft-divider" => PL_RIGHT_SOFT_DIVIDER,
    "nf-ple-ice-waveform" => PLE_ICE_WAVEFORM,
    "nf-ple-left-half-circle-thin" => PLE_LEFT_HALF_CIRCLE_THIN,
    "nf-ple-flame-thin-mirrored" => PLE_FLAME_THIN_MIRRORED,
    "nf-ple-ice-waveform-mirrored" => PLE_ICE_WAVEFORM_MIRRORED,
    "nf-ple-honeycomb-outline" => PLE_HONEYCOMB_OUTLINE,
    "nf-ple-lego-block-sideways" => PLE_LEGO_BLOCK_SIDEWAYS,
    "nf-ple-upper-left-triangle" => PLE_UPPER_LEFT_TRIANGLE,
    "nf-ple-lego-separator" => PLE_LEGO_SEPARATOR,
    "nf-ple-trapezoid-top-bottom" => PLE_TRAPEZOID_TOP_BOTTOM,
    "nf-ple-trapezoid-top-bottom-mirrored" => PLE_TRAPEZOID_TOP_BOTTOM_MIRRORED,
    "nf-ple-backslash-separator" => PLE_BACKSLASH_SEPARATOR,
    "nf-pl-right-hard-divider" => PL_RIGHT_HARD_DIVIDER,
    "nf-ple-lower-right-triangle" => PLE_LOWER_RIGHT_TRIANGLE,
    "nf-pl-branch" => PL_BRANCH,
    "nf-ple-right-half-circle-thin" => PLE_RIGHT_HALF_CIRCLE_THIN,
    "nf-ple-forwardslash-separator-redundant" => PLE_FORWARDSLASH_SEPARATOR_REDUNDANT,
    "nf-ple-lower-left-triangle" => PLE_LOWER_LEFT_TRIANGLE,
    "nf-ple-left-half-circle-thick" => PLE_LEFT_HALF_CIRCLE_THICK,
    "nf-ple-pixelated-squares-small" => PLE_PIXELATED_SQUARES_SMALL,
    "nf-ple-honeycomb" => PLE_HONEYCOMB,
    "nf-ple-flame-thick" => PLE_FLAME_THICK,
    "nf-pl-left-hard-divider" => PL_LEFT_HARD_DIVIDER,
    "nf-ple-lego-separator-thin" => PLE_LEGO_SEPARATOR_THIN,
    "nf-ple-lego-block-facing" => PLE_LEGO_BLOCK_FACING,
    "nf-ple-pixelated-squares-big" => PLE_PIXELATED_SQUARES_BIG,
    "nf-pl-left-soft-divider" => PL_LEFT_SOFT_DIVIDER,
    "nf-ple-pixelated-squares-small-mirrored" => PLE_PIXELATED_SQUARES_SMALL_MIRRORED,
};
