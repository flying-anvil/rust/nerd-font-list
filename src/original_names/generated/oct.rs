// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_oct.sh


/// Count of all Octicons Icons (Not an icon itself)
pub const OCT_ICON_COUNT: usize = 310;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PAPER_AIRPLANE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_RUBY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BOOKMARK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHEVRON_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MORTAR_BOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_UNREAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_STAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_APPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DIFF_REMOVED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPO_LOCKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_KEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BRIEFCASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CLOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PERSON_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TRIANGLE_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HOURGLASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SIDEBAR_EXPAND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_UNLOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HEART_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TRIANGLE_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ELLIPSIS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_STRIKETHROUGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ZOOM_OUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_VIDEO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MOVE_TO_TOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_UNVERIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TRIANGLE_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SHIELD_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_SYMLINK_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PAPERCLIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIT_MERGE_QUEUE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SHIELD_X: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TABLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_KEBAB_HORIZONTAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BOOKMARK_SLASH_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_REPO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BELL_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_TROPHY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TASKLIST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PACKAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_EYE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CODESCAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LIST_ORDERED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_NO_ENTRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPO_DELETED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MILESTONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_THUMBSUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MARKDOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_ADDED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TAB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DIFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DEVICE_CAMERA_VIDEO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COLUMNS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SINGLE_SELECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TYPOGRAPHY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_NOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DESKTOP_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPO_PULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ALERT_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MULTI_SELECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REL_FILE_PATH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SPARKLE_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PAINTBRUSH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SPONSOR_TIERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MEGAPHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DOT_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ISSUE_CLOSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CACHE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ZOOM_IN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_MOVED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SCREEN_NORMAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_DIRECTORY_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COMMAND_PALETTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SEARCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MARK_GITHUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_UNMUTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COMMENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHECKLIST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BELL_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LOGO_GITHUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_UP_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HUBOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPO_CLONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIT_PULL_REQUEST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TELESCOPE_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PERSON_ADD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PLUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPO_PUSH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_MEDIA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COPILOT_ERROR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_EYE_CLOSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_FORKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_TAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PENCIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_STOPWATCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CPU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BOOKMARK_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CREDIT_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TERMINAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BROWSER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MIRROR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPO_FORKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PROJECT_ROADMAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_MERGED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_X: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CODE_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TELESCOPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PACKAGE_DEPENDENCIES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DEPENDABOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BLOCKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ISSUE_OPENED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_X_CIRCLE_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DEVICE_MOBILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SERVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MUTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CLOCK_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LINK_EXTERNAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHEVRON_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CONTAINER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_STACK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ISSUE_DRAFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_SWITCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LOGO_GIST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_DIRECTORY_OPEN_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PULSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SYNC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_X_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PASSKEY_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LOCATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TRIANGLE_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SMILEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COPILOT_WARNING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIT_PULL_REQUEST_DRAFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_ZIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_STAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_UNLINK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_QUOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HEADING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_VERSIONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SORT_ASC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CODE_REVIEW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COPILOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PROJECT_TEMPLATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIT_MERGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ROCKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SQUIRREL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PROJECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_ROCKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BOLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PERSON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CODESCAN_CHECKMARK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DIAMOND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PACKAGE_DEPENDENTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHEVRON_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SORT_DESC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_STAR_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_INFINITY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MOVE_TO_START: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BOOKMARK_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SUN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPO_TEMPLATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HISTORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FOLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">⚡</span>
pub const OCT_ZAP: &'static str = "⚡";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GEAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DUPLICATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DEVICE_DESKTOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_REMOVED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DISCUSSION_OUTDATED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SKIP_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_THREE_BARS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIT_COMPARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PLUS_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HOME_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TROPHY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_WEBHOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_INBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HORIZONTAL_RULE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DATABASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ITALIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DISCUSSION_DUPLICATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ACCESSIBILITY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_BOTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SCREEN_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ITERATIONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ALERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_VERIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_QUESTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_NORTH_STAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MOVE_TO_BOTTOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SHIELD_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_UPLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_SYMLINK_DIRECTORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CODE_OF_CONDUCT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIT_COMMIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PASTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CROSS_REFERENCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GOAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SLIDERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARCHIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ROWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MOVE_TO_END: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_INFO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHECK_CIRCLE_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_DIRECTORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SHARE_ANDROID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_UNFOLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BROADCAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FOLD_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SHIELD_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SIDEBAR_COLLAPSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TAB_EXTERNAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_METER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GLOBE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FISCAL_HOST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_UP_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DISCUSSION_CLOSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ISSUE_TRACKS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">♥</span>
pub const OCT_HEART: &'static str = "♥";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_SUBMODULE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PROJECT_SYMLINK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FOLD_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_WORKFLOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LINK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DIFF_RENAMED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SQUARE_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CIRCLE_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHEVRON_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SKIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FLAME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_THUMBSDOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CLOUD_OFFLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_DISCUSSION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DIFF_MODIFIED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MENTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PLAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_REPLY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHECKBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIT_PULL_REQUEST_CLOSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_RSS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ISSUE_REOPENED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_MOON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_HEART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ISSUE_TRACKED_BY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GIT_BRANCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_DOWN_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DEVICE_CAMERA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_PEOPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ID_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FEED_PERSON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TOOLS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_STOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_TRASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_DIFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ARROW_DOWN_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CALENDAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_BEAKER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GRAPH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_KEY_ASTERISK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ACCESSIBILITY_INSET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LIST_UNORDERED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LIGHT_BULB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SIGN_OUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DIFF_ADDED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LAW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_IMAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SHARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_DIFF_IGNORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CODESPACES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_HASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_READ: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COMMENT_DISCUSSION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SHIELD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_COMMIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_NUMBER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_BINARY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_CHECK_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_ORGANIZATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_FILE_CODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_SIGN_IN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const OCT_GRABBER: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const OCT_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-oct-paper-airplane" => OCT_PAPER_AIRPLANE,
    "nf-oct-ruby" => OCT_RUBY,
    "nf-oct-bookmark" => OCT_BOOKMARK,
    "nf-oct-chevron-down" => OCT_CHEVRON_DOWN,
    "nf-oct-plus" => OCT_PLUS,
    "nf-oct-mortar-board" => OCT_MORTAR_BOARD,
    "nf-oct-unread" => OCT_UNREAD,
    "nf-oct-pin" => OCT_PIN,
    "nf-oct-feed-star" => OCT_FEED_STAR,
    "nf-oct-apps" => OCT_APPS,
    "nf-oct-diff-removed" => OCT_DIFF_REMOVED,
    "nf-oct-repo-locked" => OCT_REPO_LOCKED,
    "nf-oct-key" => OCT_KEY,
    "nf-oct-briefcase" => OCT_BRIEFCASE,
    "nf-oct-clock" => OCT_CLOCK,
    "nf-oct-person-fill" => OCT_PERSON_FILL,
    "nf-oct-triangle-right" => OCT_TRIANGLE_RIGHT,
    "nf-oct-hourglass" => OCT_HOURGLASS,
    "nf-oct-sidebar-expand" => OCT_SIDEBAR_EXPAND,
    "nf-oct-unlock" => OCT_UNLOCK,
    "nf-oct-heart-fill" => OCT_HEART_FILL,
    "nf-oct-triangle-left" => OCT_TRIANGLE_LEFT,
    "nf-oct-ellipsis" => OCT_ELLIPSIS,
    "nf-oct-strikethrough" => OCT_STRIKETHROUGH,
    "nf-oct-zoom-out" => OCT_ZOOM_OUT,
    "nf-oct-video" => OCT_VIDEO,
    "nf-oct-move-to-top" => OCT_MOVE_TO_TOP,
    "nf-oct-unverified" => OCT_UNVERIFIED,
    "nf-oct-triangle-up" => OCT_TRIANGLE_UP,
    "nf-oct-code" => OCT_CODE,
    "nf-oct-shield-slash" => OCT_SHIELD_SLASH,
    "nf-oct-file-symlink-file" => OCT_FILE_SYMLINK_FILE,
    "nf-oct-paperclip" => OCT_PAPERCLIP,
    "nf-oct-git-merge-queue" => OCT_GIT_MERGE_QUEUE,
    "nf-oct-shield-x" => OCT_SHIELD_X,
    "nf-oct-table" => OCT_TABLE,
    "nf-oct-kebab-horizontal" => OCT_KEBAB_HORIZONTAL,
    "nf-oct-bookmark-slash-fill" => OCT_BOOKMARK_SLASH_FILL,
    "nf-oct-feed-repo" => OCT_FEED_REPO,
    "nf-oct-bell-slash" => OCT_BELL_SLASH,
    "nf-oct-feed-trophy" => OCT_FEED_TROPHY,
    "nf-oct-tasklist" => OCT_TASKLIST,
    "nf-oct-package" => OCT_PACKAGE,
    "nf-oct-arrow-down" => OCT_ARROW_DOWN,
    "nf-oct-eye" => OCT_EYE,
    "nf-oct-codescan" => OCT_CODESCAN,
    "nf-oct-list-ordered" => OCT_LIST_ORDERED,
    "nf-oct-no-entry" => OCT_NO_ENTRY,
    "nf-oct-repo-deleted" => OCT_REPO_DELETED,
    "nf-oct-bug" => OCT_BUG,
    "nf-oct-milestone" => OCT_MILESTONE,
    "nf-oct-thumbsup" => OCT_THUMBSUP,
    "nf-oct-markdown" => OCT_MARKDOWN,
    "nf-oct-file-added" => OCT_FILE_ADDED,
    "nf-oct-tab" => OCT_TAB,
    "nf-oct-diff" => OCT_DIFF,
    "nf-oct-device-camera-video" => OCT_DEVICE_CAMERA_VIDEO,
    "nf-oct-columns" => OCT_COLUMNS,
    "nf-oct-single-select" => OCT_SINGLE_SELECT,
    "nf-oct-typography" => OCT_TYPOGRAPHY,
    "nf-oct-note" => OCT_NOTE,
    "nf-oct-desktop-download" => OCT_DESKTOP_DOWNLOAD,
    "nf-oct-repo-pull" => OCT_REPO_PULL,
    "nf-oct-alert-fill" => OCT_ALERT_FILL,
    "nf-oct-multi-select" => OCT_MULTI_SELECT,
    "nf-oct-rel-file-path" => OCT_REL_FILE_PATH,
    "nf-oct-sparkle-fill" => OCT_SPARKLE_FILL,
    "nf-oct-paintbrush" => OCT_PAINTBRUSH,
    "nf-oct-sponsor-tiers" => OCT_SPONSOR_TIERS,
    "nf-oct-megaphone" => OCT_MEGAPHONE,
    "nf-oct-dot-fill" => OCT_DOT_FILL,
    "nf-oct-issue-closed" => OCT_ISSUE_CLOSED,
    "nf-oct-cache" => OCT_CACHE,
    "nf-oct-dot" => OCT_DOT,
    "nf-oct-zoom-in" => OCT_ZOOM_IN,
    "nf-oct-file-moved" => OCT_FILE_MOVED,
    "nf-oct-screen-normal" => OCT_SCREEN_NORMAL,
    "nf-oct-file-directory-fill" => OCT_FILE_DIRECTORY_FILL,
    "nf-oct-tag" => OCT_TAG,
    "nf-oct-command-palette" => OCT_COMMAND_PALETTE,
    "nf-oct-search" => OCT_SEARCH,
    "nf-oct-mark-github" => OCT_MARK_GITHUB,
    "nf-oct-unmute" => OCT_UNMUTE,
    "nf-oct-comment" => OCT_COMMENT,
    "nf-oct-checklist" => OCT_CHECKLIST,
    "nf-oct-copy" => OCT_COPY,
    "nf-oct-bell-fill" => OCT_BELL_FILL,
    "nf-oct-logo-github" => OCT_LOGO_GITHUB,
    "nf-oct-arrow-up-left" => OCT_ARROW_UP_LEFT,
    "nf-oct-hubot" => OCT_HUBOT,
    "nf-oct-repo-clone" => OCT_REPO_CLONE,
    "nf-oct-git-pull-request" => OCT_GIT_PULL_REQUEST,
    "nf-oct-telescope-fill" => OCT_TELESCOPE_FILL,
    "nf-oct-person-add" => OCT_PERSON_ADD,
    "nf-oct-plug" => OCT_PLUG,
    "nf-oct-repo-push" => OCT_REPO_PUSH,
    "nf-oct-file-media" => OCT_FILE_MEDIA,
    "nf-oct-file" => OCT_FILE,
    "nf-oct-copilot-error" => OCT_COPILOT_ERROR,
    "nf-oct-square" => OCT_SQUARE,
    "nf-oct-eye-closed" => OCT_EYE_CLOSED,
    "nf-oct-feed-forked" => OCT_FEED_FORKED,
    "nf-oct-feed-tag" => OCT_FEED_TAG,
    "nf-oct-pencil" => OCT_PENCIL,
    "nf-oct-stopwatch" => OCT_STOPWATCH,
    "nf-oct-cpu" => OCT_CPU,
    "nf-oct-bookmark-slash" => OCT_BOOKMARK_SLASH,
    "nf-oct-credit-card" => OCT_CREDIT_CARD,
    "nf-oct-terminal" => OCT_TERMINAL,
    "nf-oct-browser" => OCT_BROWSER,
    "nf-oct-mirror" => OCT_MIRROR,
    "nf-oct-repo-forked" => OCT_REPO_FORKED,
    "nf-oct-project-roadmap" => OCT_PROJECT_ROADMAP,
    "nf-oct-feed-merged" => OCT_FEED_MERGED,
    "nf-oct-x" => OCT_X,
    "nf-oct-code-square" => OCT_CODE_SQUARE,
    "nf-oct-telescope" => OCT_TELESCOPE,
    "nf-oct-package-dependencies" => OCT_PACKAGE_DEPENDENCIES,
    "nf-oct-filter" => OCT_FILTER,
    "nf-oct-dependabot" => OCT_DEPENDABOT,
    "nf-oct-mail" => OCT_MAIL,
    "nf-oct-blocked" => OCT_BLOCKED,
    "nf-oct-issue-opened" => OCT_ISSUE_OPENED,
    "nf-oct-x-circle-fill" => OCT_X_CIRCLE_FILL,
    "nf-oct-device-mobile" => OCT_DEVICE_MOBILE,
    "nf-oct-server" => OCT_SERVER,
    "nf-oct-mute" => OCT_MUTE,
    "nf-oct-clock-fill" => OCT_CLOCK_FILL,
    "nf-oct-link-external" => OCT_LINK_EXTERNAL,
    "nf-oct-chevron-left" => OCT_CHEVRON_LEFT,
    "nf-oct-container" => OCT_CONTAINER,
    "nf-oct-stack" => OCT_STACK,
    "nf-oct-issue-draft" => OCT_ISSUE_DRAFT,
    "nf-oct-arrow-switch" => OCT_ARROW_SWITCH,
    "nf-oct-logo-gist" => OCT_LOGO_GIST,
    "nf-oct-file-directory-open-fill" => OCT_FILE_DIRECTORY_OPEN_FILL,
    "nf-oct-pulse" => OCT_PULSE,
    "nf-oct-sync" => OCT_SYNC,
    "nf-oct-x-circle" => OCT_X_CIRCLE,
    "nf-oct-passkey-fill" => OCT_PASSKEY_FILL,
    "nf-oct-location" => OCT_LOCATION,
    "nf-oct-triangle-down" => OCT_TRIANGLE_DOWN,
    "nf-oct-smiley" => OCT_SMILEY,
    "nf-oct-copilot-warning" => OCT_COPILOT_WARNING,
    "nf-oct-git-pull-request-draft" => OCT_GIT_PULL_REQUEST_DRAFT,
    "nf-oct-log" => OCT_LOG,
    "nf-oct-file-zip" => OCT_FILE_ZIP,
    "nf-oct-star" => OCT_STAR,
    "nf-oct-file-badge" => OCT_FILE_BADGE,
    "nf-oct-unlink" => OCT_UNLINK,
    "nf-oct-quote" => OCT_QUOTE,
    "nf-oct-cloud" => OCT_CLOUD,
    "nf-oct-heading" => OCT_HEADING,
    "nf-oct-versions" => OCT_VERSIONS,
    "nf-oct-sort-asc" => OCT_SORT_ASC,
    "nf-oct-code-review" => OCT_CODE_REVIEW,
    "nf-oct-copilot" => OCT_COPILOT,
    "nf-oct-project-template" => OCT_PROJECT_TEMPLATE,
    "nf-oct-git-merge" => OCT_GIT_MERGE,
    "nf-oct-rocket" => OCT_ROCKET,
    "nf-oct-squirrel" => OCT_SQUIRREL,
    "nf-oct-project" => OCT_PROJECT,
    "nf-oct-feed-rocket" => OCT_FEED_ROCKET,
    "nf-oct-bold" => OCT_BOLD,
    "nf-oct-person" => OCT_PERSON,
    "nf-oct-codescan-checkmark" => OCT_CODESCAN_CHECKMARK,
    "nf-oct-diamond" => OCT_DIAMOND,
    "nf-oct-package-dependents" => OCT_PACKAGE_DEPENDENTS,
    "nf-oct-chevron-up" => OCT_CHEVRON_UP,
    "nf-oct-sort-desc" => OCT_SORT_DESC,
    "nf-oct-star-fill" => OCT_STAR_FILL,
    "nf-oct-infinity" => OCT_INFINITY,
    "nf-oct-download" => OCT_DOWNLOAD,
    "nf-oct-move-to-start" => OCT_MOVE_TO_START,
    "nf-oct-bookmark-fill" => OCT_BOOKMARK_FILL,
    "nf-oct-sun" => OCT_SUN,
    "nf-oct-repo-template" => OCT_REPO_TEMPLATE,
    "nf-oct-history" => OCT_HISTORY,
    "nf-oct-fold" => OCT_FOLD,
    "nf-oct-zap" => OCT_ZAP,
    "nf-oct-gear" => OCT_GEAR,
    "nf-oct-duplicate" => OCT_DUPLICATE,
    "nf-oct-device-desktop" => OCT_DEVICE_DESKTOP,
    "nf-oct-file-removed" => OCT_FILE_REMOVED,
    "nf-oct-discussion-outdated" => OCT_DISCUSSION_OUTDATED,
    "nf-oct-skip-fill" => OCT_SKIP_FILL,
    "nf-oct-three-bars" => OCT_THREE_BARS,
    "nf-oct-git-compare" => OCT_GIT_COMPARE,
    "nf-oct-plus-circle" => OCT_PLUS_CIRCLE,
    "nf-oct-check" => OCT_CHECK,
    "nf-oct-home-fill" => OCT_HOME_FILL,
    "nf-oct-trophy" => OCT_TROPHY,
    "nf-oct-webhook" => OCT_WEBHOOK,
    "nf-oct-inbox" => OCT_INBOX,
    "nf-oct-bell" => OCT_BELL,
    "nf-oct-horizontal-rule" => OCT_HORIZONTAL_RULE,
    "nf-oct-database" => OCT_DATABASE,
    "nf-oct-italic" => OCT_ITALIC,
    "nf-oct-discussion-duplicate" => OCT_DISCUSSION_DUPLICATE,
    "nf-oct-accessibility" => OCT_ACCESSIBILITY,
    "nf-oct-arrow-both" => OCT_ARROW_BOTH,
    "nf-oct-screen-full" => OCT_SCREEN_FULL,
    "nf-oct-iterations" => OCT_ITERATIONS,
    "nf-oct-alert" => OCT_ALERT,
    "nf-oct-verified" => OCT_VERIFIED,
    "nf-oct-question" => OCT_QUESTION,
    "nf-oct-north-star" => OCT_NORTH_STAR,
    "nf-oct-move-to-bottom" => OCT_MOVE_TO_BOTTOM,
    "nf-oct-shield-check" => OCT_SHIELD_CHECK,
    "nf-oct-upload" => OCT_UPLOAD,
    "nf-oct-file-symlink-directory" => OCT_FILE_SYMLINK_DIRECTORY,
    "nf-oct-code-of-conduct" => OCT_CODE_OF_CONDUCT,
    "nf-oct-git-commit" => OCT_GIT_COMMIT,
    "nf-oct-paste" => OCT_PASTE,
    "nf-oct-cross-reference" => OCT_CROSS_REFERENCE,
    "nf-oct-goal" => OCT_GOAL,
    "nf-oct-sliders" => OCT_SLIDERS,
    "nf-oct-archive" => OCT_ARCHIVE,
    "nf-oct-report" => OCT_REPORT,
    "nf-oct-rows" => OCT_ROWS,
    "nf-oct-move-to-end" => OCT_MOVE_TO_END,
    "nf-oct-info" => OCT_INFO,
    "nf-oct-check-circle-fill" => OCT_CHECK_CIRCLE_FILL,
    "nf-oct-file-directory" => OCT_FILE_DIRECTORY,
    "nf-oct-share-android" => OCT_SHARE_ANDROID,
    "nf-oct-unfold" => OCT_UNFOLD,
    "nf-oct-repo" => OCT_REPO,
    "nf-oct-broadcast" => OCT_BROADCAST,
    "nf-oct-fold-up" => OCT_FOLD_UP,
    "nf-oct-shield-lock" => OCT_SHIELD_LOCK,
    "nf-oct-arrow-up" => OCT_ARROW_UP,
    "nf-oct-sidebar-collapse" => OCT_SIDEBAR_COLLAPSE,
    "nf-oct-tab-external" => OCT_TAB_EXTERNAL,
    "nf-oct-gift" => OCT_GIFT,
    "nf-oct-meter" => OCT_METER,
    "nf-oct-globe" => OCT_GLOBE,
    "nf-oct-fiscal-host" => OCT_FISCAL_HOST,
    "nf-oct-arrow-up-right" => OCT_ARROW_UP_RIGHT,
    "nf-oct-discussion-closed" => OCT_DISCUSSION_CLOSED,
    "nf-oct-issue-tracks" => OCT_ISSUE_TRACKS,
    "nf-oct-heart" => OCT_HEART,
    "nf-oct-file-submodule" => OCT_FILE_SUBMODULE,
    "nf-oct-arrow-right" => OCT_ARROW_RIGHT,
    "nf-oct-project-symlink" => OCT_PROJECT_SYMLINK,
    "nf-oct-fold-down" => OCT_FOLD_DOWN,
    "nf-oct-workflow" => OCT_WORKFLOW,
    "nf-oct-link" => OCT_LINK,
    "nf-oct-diff-renamed" => OCT_DIFF_RENAMED,
    "nf-oct-square-fill" => OCT_SQUARE_FILL,
    "nf-oct-circle-slash" => OCT_CIRCLE_SLASH,
    "nf-oct-chevron-right" => OCT_CHEVRON_RIGHT,
    "nf-oct-skip" => OCT_SKIP,
    "nf-oct-flame" => OCT_FLAME,
    "nf-oct-thumbsdown" => OCT_THUMBSDOWN,
    "nf-oct-cloud-offline" => OCT_CLOUD_OFFLINE,
    "nf-oct-book" => OCT_BOOK,
    "nf-oct-circle" => OCT_CIRCLE,
    "nf-oct-home" => OCT_HOME,
    "nf-oct-feed-discussion" => OCT_FEED_DISCUSSION,
    "nf-oct-diff-modified" => OCT_DIFF_MODIFIED,
    "nf-oct-mention" => OCT_MENTION,
    "nf-oct-play" => OCT_PLAY,
    "nf-oct-dash" => OCT_DASH,
    "nf-oct-reply" => OCT_REPLY,
    "nf-oct-checkbox" => OCT_CHECKBOX,
    "nf-oct-git-pull-request-closed" => OCT_GIT_PULL_REQUEST_CLOSED,
    "nf-oct-rss" => OCT_RSS,
    "nf-oct-arrow-left" => OCT_ARROW_LEFT,
    "nf-oct-issue-reopened" => OCT_ISSUE_REOPENED,
    "nf-oct-moon" => OCT_MOON,
    "nf-oct-feed-heart" => OCT_FEED_HEART,
    "nf-oct-issue-tracked-by" => OCT_ISSUE_TRACKED_BY,
    "nf-oct-git-branch" => OCT_GIT_BRANCH,
    "nf-oct-arrow-down-left" => OCT_ARROW_DOWN_LEFT,
    "nf-oct-device-camera" => OCT_DEVICE_CAMERA,
    "nf-oct-people" => OCT_PEOPLE,
    "nf-oct-id-badge" => OCT_ID_BADGE,
    "nf-oct-feed-person" => OCT_FEED_PERSON,
    "nf-oct-tools" => OCT_TOOLS,
    "nf-oct-stop" => OCT_STOP,
    "nf-oct-trash" => OCT_TRASH,
    "nf-oct-file-diff" => OCT_FILE_DIFF,
    "nf-oct-arrow-down-right" => OCT_ARROW_DOWN_RIGHT,
    "nf-oct-calendar" => OCT_CALENDAR,
    "nf-oct-beaker" => OCT_BEAKER,
    "nf-oct-graph" => OCT_GRAPH,
    "nf-oct-key-asterisk" => OCT_KEY_ASTERISK,
    "nf-oct-accessibility-inset" => OCT_ACCESSIBILITY_INSET,
    "nf-oct-list-unordered" => OCT_LIST_UNORDERED,
    "nf-oct-light-bulb" => OCT_LIGHT_BULB,
    "nf-oct-sign-out" => OCT_SIGN_OUT,
    "nf-oct-diff-added" => OCT_DIFF_ADDED,
    "nf-oct-law" => OCT_LAW,
    "nf-oct-image" => OCT_IMAGE,
    "nf-oct-share" => OCT_SHARE,
    "nf-oct-diff-ignored" => OCT_DIFF_IGNORED,
    "nf-oct-codespaces" => OCT_CODESPACES,
    "nf-oct-hash" => OCT_HASH,
    "nf-oct-read" => OCT_READ,
    "nf-oct-comment-discussion" => OCT_COMMENT_DISCUSSION,
    "nf-oct-shield" => OCT_SHIELD,
    "nf-oct-commit" => OCT_COMMIT,
    "nf-oct-number" => OCT_NUMBER,
    "nf-oct-lock" => OCT_LOCK,
    "nf-oct-file-binary" => OCT_FILE_BINARY,
    "nf-oct-check-circle" => OCT_CHECK_CIRCLE,
    "nf-oct-organization" => OCT_ORGANIZATION,
    "nf-oct-file-code" => OCT_FILE_CODE,
    "nf-oct-sign-in" => OCT_SIGN_IN,
    "nf-oct-grabber" => OCT_GRABBER,
};
