// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_iec.sh


/// Count of all IEC Power Symbols Icons (Not an icon itself)
pub const IEC_ICON_COUNT: usize = 5;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">⏾</span>
pub const IEC_SLEEP_MODE: &'static str = "⏾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">⏼</span>
pub const IEC_TOGGLE_POWER: &'static str = "⏼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">⏽</span>
pub const IEC_POWER_ON: &'static str = "⏽";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">⭘</span>
pub const IEC_POWER_OFF: &'static str = "⭘";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">⏻</span>
pub const IEC_POWER: &'static str = "⏻";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const IEC_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-iec-sleep-mode" => IEC_SLEEP_MODE,
    "nf-iec-toggle-power" => IEC_TOGGLE_POWER,
    "nf-iec-power-on" => IEC_POWER_ON,
    "nf-iec-power-off" => IEC_POWER_OFF,
    "nf-iec-power" => IEC_POWER,
};
