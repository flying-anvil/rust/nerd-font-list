// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_fa.sh


/// Count of all Font Awesome Icons (Not an icon itself)
pub const FA_ICON_COUNT: usize = 675;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_AMERICAN_SIGN_LANGUAGE_INTERPRETING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CALENDAR_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CALENDAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_CIRCLE_O_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LIST_OL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STEP_FORWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WINDOW_CLOSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_APPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SIGN_LANGUAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_RUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DATABASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_OUTDENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GRAV: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PICTURE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TIMES_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_CIRCLE_O_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC_PAYPAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHOPPING_BAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHEVRON_CIRCLE_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_XING_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SUBWAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STOP_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REPLY_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CAMERA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_H_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_NEWSPAPER_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ETSY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GLIDE_G: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SIGN_IN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CARET_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CAMERA_RETRO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WINDOW_MAXIMIZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_WORD_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANDROID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT_ALPHA_DESC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WHEELCHAIR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_INDUSTRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_UNDERLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHEVRON_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MIXCLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRADEMARK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THEMEISLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAYPAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MEANPATH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT_DESC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CART_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SUITCASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CODEPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THERMOMETER_QUARTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_SCISSORS_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CODIEPIE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PINTEREST_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DRUPAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_I_CURSOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WINDOW_CLOSE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USER_MD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHROME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_PDF_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THUMBS_O_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GRADUATION_CAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHECK_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_QUESTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STUMBLEUPON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STEP_BACKWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ENVELOPE_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VENUS_DOUBLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BALANCE_SCALE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HOURGLASS_START: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CALENDAR_CHECK_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_RANDOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BELL_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ASTERISK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MAXCDN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FOLDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGLE_DOUBLE_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHILD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CREDIT_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EXCLAMATION_TRIANGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WEIXIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SLIDESHARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USER_TIMES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CLOUD_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC_JCB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_YOUTUBE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_VIDEO_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CALENDAR_PLUS_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ITALIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_KEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TASKS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_UPLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLUS_SQUARE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STEAM_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC_DISCOVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_AREA_CHART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHIRTSINBULK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLANE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BATTERY_THREE_QUARTERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAUSE_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_UMBRELLA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VIADEO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THERMOMETER_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLAY_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_AUDIO_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_RSS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SNAPCHAT_GHOST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MICROCHIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STRIKETHROUGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_OPENID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LOCATION_ARROW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ADJUST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STACK_EXCHANGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TREE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_OBJECT_GROUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REDDIT_ALIEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HOURGLASS_END: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SCRIBD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WINDOW_RESTORE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BLUETOOTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_POINTER_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SNOWFLAKE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_QRCODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FLAG_CHECKERED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LONG_ARROW_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EXTERNAL_LINK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HOURGLASS_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FACEBOOK_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EXCHANGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHARE_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_YELP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WPFORMS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_AUDIO_DESCRIPTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GET_POCKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VOLUME_CONTROL_PHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PARAGRAPH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRAIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SOUNDCLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHARE_ALT_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHECK_SQUARE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TELEVISION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_OBJECT_UNGROUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THUMB_TACK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LEANPUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VOLUME_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MINUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TERMINAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_RENREN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REPEAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC_VISA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHARE_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAPER_PLANE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGLE_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGLE_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HACKER_NEWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BUYSELLADS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DOT_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EDGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BOOKMARK_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MEDKIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROWS_H: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PENCIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SNAPCHAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FACEBOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FIRE_EXTINGUISHER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LEVEL_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAPERCLIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CONNECTDEVELOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MALE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BANDCAMP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_OPERA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ELLIPSIS_H: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SERVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRANSGENDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHECK_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LONG_ARROW_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MARS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HEART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SEARCH_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROWS_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GAVEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COFFEE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT_AMOUNT_ASC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLAY_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MARS_STROKE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CLONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EJECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHEVRON_CIRCLE_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FLOPPY_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANCHOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STICKY_NOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_YAHOO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGLE_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARCHIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FOURSQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CARET_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAPER_PLANE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SEARCH_MINUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CALENDAR_TIMES_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MAP_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HOUZZ: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ILS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WINDOWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MOBILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COLUMNS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHEVRON_CIRCLE_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REDDIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COMPASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GENDERLESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAUSE_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USER_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THUMBS_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TAXI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_JOOMLA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DELICIOUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAINT_BRUSH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GG_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PERCENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_NEUTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_CIRCLE_O_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BACKWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MAGNET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LINK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LINKEDIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VOLUME_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SKYPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GITLAB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STACK_OVERFLOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PHONE_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COMMENT_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TABLET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DEVIANTART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HASHTAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BLIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PENCIL_SQUARE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WIKIPEDIA_W: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EYE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THUMBS_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EERCAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CROP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PENCIL_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VENUS_MARS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MAP_SIGNS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ADN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GOOGLE_PLUS_OFFICIAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WINDOW_MINIMIZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CROSSHAIRS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BATTERY_QUARTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GAMEPAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WRENCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHEVRON_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MAP_PIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BLACK_TIE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PIED_PIPER_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SMILE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FORUMBEE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FAST_BACKWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TUMBLR_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_QUOTE_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_CIRCLE_O_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STAR_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PRINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHEVRON_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SIGN_OUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_O_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FOLDER_OPEN_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EXCLAMATION_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CLOUD_UPLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LANGUAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DIGG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BEER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FLAG_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WEIBO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BOOKMARK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TIMES_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EXCLAMATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SLACK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BEHANCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FIRE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_IOXHOST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PINTEREST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SKYATLAS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GITHUB_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REDDIT_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TOGGLE_ON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CONTAO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ENVIRA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GOOGLE_PLUS_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_UNIVERSAL_ACCESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WHEELCHAIR_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VIADEO_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PIED_PIPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TELEGRAM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MINUS_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGLE_DOUBLE_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_QUESTION_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGLE_DOUBLE_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TOGGLE_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FIRST_ORDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DESKTOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_SPOCK_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ID_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_QUORA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TWITTER_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LEVEL_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_YOUTUBE_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC_MASTERCARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GOOGLE_WALLET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PINTEREST_P: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ADDRESS_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EYE_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BOLT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SCISSORS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BTC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT_NUMERIC_ASC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PODCAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_CIRCLE_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_UNLOCK_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CUBES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COMMENTING_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BATTERY_HALF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT_ALPHA_ASC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TABLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SUN_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EMPIRE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MUSIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT_ASC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_INFO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VOLUME_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_PAPER_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FONT_AWESOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ROCKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SUPERPOWERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAGELINES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TH_LIST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HEADER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BITBUCKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CSS3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STEAM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CIRCLE_THIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PIED_PIPER_PP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SLIDERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SPINNER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_UNDO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHEVRON_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LINE_CHART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MOTORCYCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_PEACE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MONEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_TEXT_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FAST_FORWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_IMAGE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_ARCHIVE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_OPTIN_MONSTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LIST_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BITBUCKET_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHIELD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ID_CARD_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COMMENTING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ODNOKLASSNIKI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TH_LARGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CARET_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HDD_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHECK_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EXTERNAL_LINK_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLUS_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_ROCK_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LOW_VISION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILES_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_AMBULANCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SNAPCHAT_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FONT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BARS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VIMEO_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GIFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LASTFM_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HANDSHAKE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MODX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_CODE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ODNOKLASSNIKI_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_OPENCART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_RETWEET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BARCODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FROWN_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BICYCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CART_ARROW_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC_DINERS_CLUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EXPAND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BOLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LEMON_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HOSPITAL_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SAFARI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LINUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DASHCUBE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BATTERY_EMPTY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BINOCULARS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CREDIT_CARD_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CUBE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TROPHY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC_STRIPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_KEYBOARD_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GOOGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CALENDAR_MINUS_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ID_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRELLO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TAGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TENCENT_WEIBO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WIFI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VIDEO_CAMERA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_QUOTE_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MARS_DOUBLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MEETUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SPOON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MICROPHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CREATIVE_COMMONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MAGIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CODE_FORK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EUR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MARS_STROKE_V: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_YOAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT_AMOUNT_DESC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BULLHORN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GITHUB_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_CIRCLE_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_O_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GIT_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ENVELOPE_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLUS_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_POWERPOINT_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USER_SECRET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_Y_COMBINATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_QQ: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ERASER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VENUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THERMOMETER_THREE_QUARTERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CARET_SQUARE_O_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THERMOMETER_EMPTY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BUILDING_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GOOGLE_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CALCULATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FORT_AWESOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ROAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TUMBLR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGLE_DOUBLE_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MOON_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_EXCEL_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ADDRESS_CARD_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STAR_HALF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FORWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TICKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_O_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PIE_CHART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CLOCK_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LINKEDIN_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SUPERSCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GLASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SUBSCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LIFE_RING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TEXT_WIDTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BEHANCE_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LINODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ADDRESS_BOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BATH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRIPADVISOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GRATIPAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REPLY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_INSTAGRAM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHOPPING_CART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ALIGN_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BRAILLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CARET_SQUARE_O_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PLUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LIST_UL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ALIGN_CENTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BLUETOOTH_B: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BOMB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HOURGLASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TTY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FONTICONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LEAF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THUMBS_O_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_THERMOMETER_HALF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_INBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MAP_MARKER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FAX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WPEXPLORER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MOUSE_POINTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRANSGENDER_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BAR_CHART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_INTERNET_EXPLORER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BRIEFCASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MINUS_SQUARE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_KRW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_RECYCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LASTFM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DEAF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PAUSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GITHUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CERTIFICATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TWITCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FREE_CODE_CAMP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_AMAZON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MERCURY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FLAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HEART_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRASH_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_CIRCLE_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REFRESH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHARE_SQUARE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LONG_ARROW_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_INR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SORT_NUMERIC_DESC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SIMPLYBUILT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PUZZLE_PIECE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FLICKR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SPOTIFY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SITEMAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SEARCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COMMENTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HEADPHONES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGLE_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USER_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_RSS_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHAIN_BROKEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EYEDROPPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WORDPRESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CARET_SQUARE_O_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_JSFIDDLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DROPBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BIRTHDAY_CAKE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FACEBOOK_OFFICIAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MEDIUM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_O_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VIMEO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_UNIVERSITY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROW_CIRCLE_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TEXT_HEIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FOLDER_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COMPRESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_YOUTUBE_PLAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USER_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TACHOMETER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HTML5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHEVRON_CIRCLE_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BULLSEYE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STETHOSCOPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REBEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_INFO_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MICROPHONE_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BELL_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_POWER_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GLIDE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_AT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STOP_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ALIGN_JUSTIFY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LIGHTBULB_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FEMALE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USER_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FIREFOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GLOBE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ELLIPSIS_V: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HISTORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHOPPING_BASKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DRIBBBLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COMMENTS_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CARET_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ENVELOPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_XING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COPYRIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VIACOIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_RAVELRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FUTBOL_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_INDENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TRUCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CARET_SQUARE_O_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TWITTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_DIAMOND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SPACE_SHUTTLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CUTLERY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_VINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_EXPEDITEDSSL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STICKY_NOTE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ARROWS_V: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TIMES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WHATSAPP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HEARTBEAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ADDRESS_BOOK_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CIRCLE_O_NOTCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BELL_SLASH_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_TH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_JPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LAPTOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SHOWER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_COMMENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SIGNAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HAND_LIZARD_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_USB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ENVELOPE_OPEN_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_IMDB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_UNLOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_REGISTERED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CLIPBOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MINUS_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_PRODUCT_HUNT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_500PX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LIST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FILE_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_HOURGLASS_HALF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BUILDING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FLASK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FIGHTER_JET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_BATTERY_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_FOLDER_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_LONG_ARROW_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STUMBLEUPON_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_WPBEGINNER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STREET_VIEW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SQUARE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_QUESTION_CIRCLE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MEH_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_SELLSY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_STAR_HALF_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_GBP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_CC_AMEX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ASSISTIVE_LISTENING_SYSTEMS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ENVELOPE_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_MARS_STROKE_H: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ALIGN_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FA_ANGELLIST: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const FA_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-fa-american-sign-language-interpreting" => FA_AMERICAN_SIGN_LANGUAGE_INTERPRETING,
    "nf-fa-calendar-o" => FA_CALENDAR_O,
    "nf-fa-calendar" => FA_CALENDAR,
    "nf-fa-arrow-circle-o-right" => FA_ARROW_CIRCLE_O_RIGHT,
    "nf-fa-list-ol" => FA_LIST_OL,
    "nf-fa-step-forward" => FA_STEP_FORWARD,
    "nf-fa-user" => FA_USER,
    "nf-fa-window-close" => FA_WINDOW_CLOSE,
    "nf-fa-apple" => FA_APPLE,
    "nf-fa-sign-language" => FA_SIGN_LANGUAGE,
    "nf-fa-rub" => FA_RUB,
    "nf-fa-database" => FA_DATABASE,
    "nf-fa-outdent" => FA_OUTDENT,
    "nf-fa-map" => FA_MAP,
    "nf-fa-grav" => FA_GRAV,
    "nf-fa-picture-o" => FA_PICTURE_O,
    "nf-fa-times-circle" => FA_TIMES_CIRCLE,
    "nf-fa-arrow-circle-o-up" => FA_ARROW_CIRCLE_O_UP,
    "nf-fa-arrow-left" => FA_ARROW_LEFT,
    "nf-fa-cc-paypal" => FA_CC_PAYPAL,
    "nf-fa-shopping-bag" => FA_SHOPPING_BAG,
    "nf-fa-chevron-circle-down" => FA_CHEVRON_CIRCLE_DOWN,
    "nf-fa-xing-square" => FA_XING_SQUARE,
    "nf-fa-subway" => FA_SUBWAY,
    "nf-fa-stop-circle-o" => FA_STOP_CIRCLE_O,
    "nf-fa-reply-all" => FA_REPLY_ALL,
    "nf-fa-camera" => FA_CAMERA,
    "nf-fa-h-square" => FA_H_SQUARE,
    "nf-fa-newspaper-o" => FA_NEWSPAPER_O,
    "nf-fa-etsy" => FA_ETSY,
    "nf-fa-glide-g" => FA_GLIDE_G,
    "nf-fa-sort" => FA_SORT,
    "nf-fa-sign-in" => FA_SIGN_IN,
    "nf-fa-caret-left" => FA_CARET_LEFT,
    "nf-fa-square" => FA_SQUARE,
    "nf-fa-camera-retro" => FA_CAMERA_RETRO,
    "nf-fa-try" => FA_TRY,
    "nf-fa-window-maximize" => FA_WINDOW_MAXIMIZE,
    "nf-fa-file-word-o" => FA_FILE_WORD_O,
    "nf-fa-android" => FA_ANDROID,
    "nf-fa-sort-alpha-desc" => FA_SORT_ALPHA_DESC,
    "nf-fa-wheelchair" => FA_WHEELCHAIR,
    "nf-fa-industry" => FA_INDUSTRY,
    "nf-fa-underline" => FA_UNDERLINE,
    "nf-fa-chevron-left" => FA_CHEVRON_LEFT,
    "nf-fa-mixcloud" => FA_MIXCLOUD,
    "nf-fa-download" => FA_DOWNLOAD,
    "nf-fa-trademark" => FA_TRADEMARK,
    "nf-fa-themeisle" => FA_THEMEISLE,
    "nf-fa-paypal" => FA_PAYPAL,
    "nf-fa-meanpath" => FA_MEANPATH,
    "nf-fa-sort-desc" => FA_SORT_DESC,
    "nf-fa-cart-plus" => FA_CART_PLUS,
    "nf-fa-suitcase" => FA_SUITCASE,
    "nf-fa-codepen" => FA_CODEPEN,
    "nf-fa-thermometer-quarter" => FA_THERMOMETER_QUARTER,
    "nf-fa-users" => FA_USERS,
    "nf-fa-hand-scissors-o" => FA_HAND_SCISSORS_O,
    "nf-fa-codiepie" => FA_CODIEPIE,
    "nf-fa-pinterest-square" => FA_PINTEREST_SQUARE,
    "nf-fa-drupal" => FA_DRUPAL,
    "nf-fa-i-cursor" => FA_I_CURSOR,
    "nf-fa-window-close-o" => FA_WINDOW_CLOSE_O,
    "nf-fa-cogs" => FA_COGS,
    "nf-fa-user-md" => FA_USER_MD,
    "nf-fa-chrome" => FA_CHROME,
    "nf-fa-file-pdf-o" => FA_FILE_PDF_O,
    "nf-fa-thumbs-o-down" => FA_THUMBS_O_DOWN,
    "nf-fa-graduation-cap" => FA_GRADUATION_CAP,
    "nf-fa-check-circle" => FA_CHECK_CIRCLE,
    "nf-fa-question" => FA_QUESTION,
    "nf-fa-stumbleupon" => FA_STUMBLEUPON,
    "nf-fa-step-backward" => FA_STEP_BACKWARD,
    "nf-fa-envelope-square" => FA_ENVELOPE_SQUARE,
    "nf-fa-venus-double" => FA_VENUS_DOUBLE,
    "nf-fa-balance-scale" => FA_BALANCE_SCALE,
    "nf-fa-hourglass-start" => FA_HOURGLASS_START,
    "nf-fa-calendar-check-o" => FA_CALENDAR_CHECK_O,
    "nf-fa-random" => FA_RANDOM,
    "nf-fa-bell-o" => FA_BELL_O,
    "nf-fa-asterisk" => FA_ASTERISK,
    "nf-fa-maxcdn" => FA_MAXCDN,
    "nf-fa-folder" => FA_FOLDER,
    "nf-fa-angle-double-down" => FA_ANGLE_DOUBLE_DOWN,
    "nf-fa-child" => FA_CHILD,
    "nf-fa-credit-card" => FA_CREDIT_CARD,
    "nf-fa-exclamation-triangle" => FA_EXCLAMATION_TRIANGLE,
    "nf-fa-weixin" => FA_WEIXIN,
    "nf-fa-filter" => FA_FILTER,
    "nf-fa-slideshare" => FA_SLIDESHARE,
    "nf-fa-user-times" => FA_USER_TIMES,
    "nf-fa-bell" => FA_BELL,
    "nf-fa-cloud-download" => FA_CLOUD_DOWNLOAD,
    "nf-fa-cc-jcb" => FA_CC_JCB,
    "nf-fa-youtube" => FA_YOUTUBE,
    "nf-fa-file-video-o" => FA_FILE_VIDEO_O,
    "nf-fa-calendar-plus-o" => FA_CALENDAR_PLUS_O,
    "nf-fa-italic" => FA_ITALIC,
    "nf-fa-key" => FA_KEY,
    "nf-fa-tasks" => FA_TASKS,
    "nf-fa-code" => FA_CODE,
    "nf-fa-upload" => FA_UPLOAD,
    "nf-fa-plus-square-o" => FA_PLUS_SQUARE_O,
    "nf-fa-steam-square" => FA_STEAM_SQUARE,
    "nf-fa-cc-discover" => FA_CC_DISCOVER,
    "nf-fa-area-chart" => FA_AREA_CHART,
    "nf-fa-shirtsinbulk" => FA_SHIRTSINBULK,
    "nf-fa-plane" => FA_PLANE,
    "nf-fa-battery-three-quarters" => FA_BATTERY_THREE_QUARTERS,
    "nf-fa-pause-circle-o" => FA_PAUSE_CIRCLE_O,
    "nf-fa-share" => FA_SHARE,
    "nf-fa-umbrella" => FA_UMBRELLA,
    "nf-fa-viadeo" => FA_VIADEO,
    "nf-fa-thermometer-full" => FA_THERMOMETER_FULL,
    "nf-fa-play-circle-o" => FA_PLAY_CIRCLE_O,
    "nf-fa-file-audio-o" => FA_FILE_AUDIO_O,
    "nf-fa-rss" => FA_RSS,
    "nf-fa-snapchat-ghost" => FA_SNAPCHAT_GHOST,
    "nf-fa-microchip" => FA_MICROCHIP,
    "nf-fa-strikethrough" => FA_STRIKETHROUGH,
    "nf-fa-openid" => FA_OPENID,
    "nf-fa-location-arrow" => FA_LOCATION_ARROW,
    "nf-fa-adjust" => FA_ADJUST,
    "nf-fa-stack-exchange" => FA_STACK_EXCHANGE,
    "nf-fa-tree" => FA_TREE,
    "nf-fa-object-group" => FA_OBJECT_GROUP,
    "nf-fa-reddit-alien" => FA_REDDIT_ALIEN,
    "nf-fa-hourglass-end" => FA_HOURGLASS_END,
    "nf-fa-scribd" => FA_SCRIBD,
    "nf-fa-window-restore" => FA_WINDOW_RESTORE,
    "nf-fa-bluetooth" => FA_BLUETOOTH,
    "nf-fa-hand-pointer-o" => FA_HAND_POINTER_O,
    "nf-fa-snowflake-o" => FA_SNOWFLAKE_O,
    "nf-fa-qrcode" => FA_QRCODE,
    "nf-fa-flag-checkered" => FA_FLAG_CHECKERED,
    "nf-fa-long-arrow-left" => FA_LONG_ARROW_LEFT,
    "nf-fa-bug" => FA_BUG,
    "nf-fa-external-link" => FA_EXTERNAL_LINK,
    "nf-fa-hourglass-o" => FA_HOURGLASS_O,
    "nf-fa-facebook-square" => FA_FACEBOOK_SQUARE,
    "nf-fa-exchange" => FA_EXCHANGE,
    "nf-fa-share-alt" => FA_SHARE_ALT,
    "nf-fa-yelp" => FA_YELP,
    "nf-fa-wpforms" => FA_WPFORMS,
    "nf-fa-audio-description" => FA_AUDIO_DESCRIPTION,
    "nf-fa-get-pocket" => FA_GET_POCKET,
    "nf-fa-volume-control-phone" => FA_VOLUME_CONTROL_PHONE,
    "nf-fa-arrows" => FA_ARROWS,
    "nf-fa-paragraph" => FA_PARAGRAPH,
    "nf-fa-train" => FA_TRAIN,
    "nf-fa-soundcloud" => FA_SOUNDCLOUD,
    "nf-fa-share-alt-square" => FA_SHARE_ALT_SQUARE,
    "nf-fa-play" => FA_PLAY,
    "nf-fa-check-square-o" => FA_CHECK_SQUARE_O,
    "nf-fa-television" => FA_TELEVISION,
    "nf-fa-film" => FA_FILM,
    "nf-fa-object-ungroup" => FA_OBJECT_UNGROUP,
    "nf-fa-thumb-tack" => FA_THUMB_TACK,
    "nf-fa-leanpub" => FA_LEANPUB,
    "nf-fa-volume-down" => FA_VOLUME_DOWN,
    "nf-fa-minus" => FA_MINUS,
    "nf-fa-terminal" => FA_TERMINAL,
    "nf-fa-renren" => FA_RENREN,
    "nf-fa-repeat" => FA_REPEAT,
    "nf-fa-cc-visa" => FA_CC_VISA,
    "nf-fa-share-square" => FA_SHARE_SQUARE,
    "nf-fa-paper-plane" => FA_PAPER_PLANE,
    "nf-fa-angle-left" => FA_ANGLE_LEFT,
    "nf-fa-angle-down" => FA_ANGLE_DOWN,
    "nf-fa-hacker-news" => FA_HACKER_NEWS,
    "nf-fa-buysellads" => FA_BUYSELLADS,
    "nf-fa-dot-circle-o" => FA_DOT_CIRCLE_O,
    "nf-fa-edge" => FA_EDGE,
    "nf-fa-bookmark-o" => FA_BOOKMARK_O,
    "nf-fa-medkit" => FA_MEDKIT,
    "nf-fa-arrows-h" => FA_ARROWS_H,
    "nf-fa-pencil" => FA_PENCIL,
    "nf-fa-snapchat" => FA_SNAPCHAT,
    "nf-fa-facebook" => FA_FACEBOOK,
    "nf-fa-fire-extinguisher" => FA_FIRE_EXTINGUISHER,
    "nf-fa-level-down" => FA_LEVEL_DOWN,
    "nf-fa-stop" => FA_STOP,
    "nf-fa-paperclip" => FA_PAPERCLIP,
    "nf-fa-connectdevelop" => FA_CONNECTDEVELOP,
    "nf-fa-male" => FA_MALE,
    "nf-fa-bandcamp" => FA_BANDCAMP,
    "nf-fa-opera" => FA_OPERA,
    "nf-fa-ellipsis-h" => FA_ELLIPSIS_H,
    "nf-fa-cog" => FA_COG,
    "nf-fa-server" => FA_SERVER,
    "nf-fa-transgender" => FA_TRANSGENDER,
    "nf-fa-check-circle-o" => FA_CHECK_CIRCLE_O,
    "nf-fa-long-arrow-up" => FA_LONG_ARROW_UP,
    "nf-fa-mars" => FA_MARS,
    "nf-fa-file-o" => FA_FILE_O,
    "nf-fa-heart" => FA_HEART,
    "nf-fa-arrow-right" => FA_ARROW_RIGHT,
    "nf-fa-search-plus" => FA_SEARCH_PLUS,
    "nf-fa-arrows-alt" => FA_ARROWS_ALT,
    "nf-fa-gavel" => FA_GAVEL,
    "nf-fa-coffee" => FA_COFFEE,
    "nf-fa-sort-amount-asc" => FA_SORT_AMOUNT_ASC,
    "nf-fa-play-circle" => FA_PLAY_CIRCLE,
    "nf-fa-mars-stroke" => FA_MARS_STROKE,
    "nf-fa-clone" => FA_CLONE,
    "nf-fa-eject" => FA_EJECT,
    "nf-fa-chevron-circle-right" => FA_CHEVRON_CIRCLE_RIGHT,
    "nf-fa-tint" => FA_TINT,
    "nf-fa-floppy-o" => FA_FLOPPY_O,
    "nf-fa-anchor" => FA_ANCHOR,
    "nf-fa-sticky-note" => FA_STICKY_NOTE,
    "nf-fa-yahoo" => FA_YAHOO,
    "nf-fa-angle-up" => FA_ANGLE_UP,
    "nf-fa-archive" => FA_ARCHIVE,
    "nf-fa-foursquare" => FA_FOURSQUARE,
    "nf-fa-caret-down" => FA_CARET_DOWN,
    "nf-fa-paper-plane-o" => FA_PAPER_PLANE_O,
    "nf-fa-search-minus" => FA_SEARCH_MINUS,
    "nf-fa-calendar-times-o" => FA_CALENDAR_TIMES_O,
    "nf-fa-map-o" => FA_MAP_O,
    "nf-fa-houzz" => FA_HOUZZ,
    "nf-fa-ils" => FA_ILS,
    "nf-fa-windows" => FA_WINDOWS,
    "nf-fa-cloud" => FA_CLOUD,
    "nf-fa-mobile" => FA_MOBILE,
    "nf-fa-columns" => FA_COLUMNS,
    "nf-fa-chevron-circle-left" => FA_CHEVRON_CIRCLE_LEFT,
    "nf-fa-reddit" => FA_REDDIT,
    "nf-fa-compass" => FA_COMPASS,
    "nf-fa-genderless" => FA_GENDERLESS,
    "nf-fa-pause-circle" => FA_PAUSE_CIRCLE,
    "nf-fa-user-circle" => FA_USER_CIRCLE,
    "nf-fa-arrow-down" => FA_ARROW_DOWN,
    "nf-fa-thumbs-up" => FA_THUMBS_UP,
    "nf-fa-taxi" => FA_TAXI,
    "nf-fa-trash" => FA_TRASH,
    "nf-fa-joomla" => FA_JOOMLA,
    "nf-fa-delicious" => FA_DELICIOUS,
    "nf-fa-home" => FA_HOME,
    "nf-fa-paint-brush" => FA_PAINT_BRUSH,
    "nf-fa-gg-circle" => FA_GG_CIRCLE,
    "nf-fa-percent" => FA_PERCENT,
    "nf-fa-neuter" => FA_NEUTER,
    "nf-fa-arrow-circle-o-down" => FA_ARROW_CIRCLE_O_DOWN,
    "nf-fa-backward" => FA_BACKWARD,
    "nf-fa-magnet" => FA_MAGNET,
    "nf-fa-link" => FA_LINK,
    "nf-fa-linkedin" => FA_LINKEDIN,
    "nf-fa-volume-off" => FA_VOLUME_OFF,
    "nf-fa-skype" => FA_SKYPE,
    "nf-fa-gitlab" => FA_GITLAB,
    "nf-fa-stack-overflow" => FA_STACK_OVERFLOW,
    "nf-fa-phone-square" => FA_PHONE_SQUARE,
    "nf-fa-comment-o" => FA_COMMENT_O,
    "nf-fa-vk" => FA_VK,
    "nf-fa-tablet" => FA_TABLET,
    "nf-fa-deviantart" => FA_DEVIANTART,
    "nf-fa-hashtag" => FA_HASHTAG,
    "nf-fa-cc" => FA_CC,
    "nf-fa-blind" => FA_BLIND,
    "nf-fa-pencil-square-o" => FA_PENCIL_SQUARE_O,
    "nf-fa-wikipedia-w" => FA_WIKIPEDIA_W,
    "nf-fa-eye" => FA_EYE,
    "nf-fa-thumbs-down" => FA_THUMBS_DOWN,
    "nf-fa-eercast" => FA_EERCAST,
    "nf-fa-crop" => FA_CROP,
    "nf-fa-pencil-square" => FA_PENCIL_SQUARE,
    "nf-fa-venus-mars" => FA_VENUS_MARS,
    "nf-fa-map-signs" => FA_MAP_SIGNS,
    "nf-fa-adn" => FA_ADN,
    "nf-fa-google-plus-official" => FA_GOOGLE_PLUS_OFFICIAL,
    "nf-fa-window-minimize" => FA_WINDOW_MINIMIZE,
    "nf-fa-crosshairs" => FA_CROSSHAIRS,
    "nf-fa-battery-quarter" => FA_BATTERY_QUARTER,
    "nf-fa-gamepad" => FA_GAMEPAD,
    "nf-fa-wrench" => FA_WRENCH,
    "nf-fa-chevron-down" => FA_CHEVRON_DOWN,
    "nf-fa-map-pin" => FA_MAP_PIN,
    "nf-fa-black-tie" => FA_BLACK_TIE,
    "nf-fa-pied-piper-alt" => FA_PIED_PIPER_ALT,
    "nf-fa-smile-o" => FA_SMILE_O,
    "nf-fa-forumbee" => FA_FORUMBEE,
    "nf-fa-fast-backward" => FA_FAST_BACKWARD,
    "nf-fa-tumblr-square" => FA_TUMBLR_SQUARE,
    "nf-fa-quote-left" => FA_QUOTE_LEFT,
    "nf-fa-arrow-circle-o-left" => FA_ARROW_CIRCLE_O_LEFT,
    "nf-fa-arrow-up" => FA_ARROW_UP,
    "nf-fa-star-o" => FA_STAR_O,
    "nf-fa-print" => FA_PRINT,
    "nf-fa-chevron-right" => FA_CHEVRON_RIGHT,
    "nf-fa-sign-out" => FA_SIGN_OUT,
    "nf-fa-tag" => FA_TAG,
    "nf-fa-hand-o-right" => FA_HAND_O_RIGHT,
    "nf-fa-folder-open-o" => FA_FOLDER_OPEN_O,
    "nf-fa-exclamation-circle" => FA_EXCLAMATION_CIRCLE,
    "nf-fa-cloud-upload" => FA_CLOUD_UPLOAD,
    "nf-fa-language" => FA_LANGUAGE,
    "nf-fa-digg" => FA_DIGG,
    "nf-fa-beer" => FA_BEER,
    "nf-fa-flag-o" => FA_FLAG_O,
    "nf-fa-weibo" => FA_WEIBO,
    "nf-fa-bookmark" => FA_BOOKMARK,
    "nf-fa-times-circle-o" => FA_TIMES_CIRCLE_O,
    "nf-fa-exclamation" => FA_EXCLAMATION,
    "nf-fa-slack" => FA_SLACK,
    "nf-fa-behance" => FA_BEHANCE,
    "nf-fa-fire" => FA_FIRE,
    "nf-fa-ioxhost" => FA_IOXHOST,
    "nf-fa-pinterest" => FA_PINTEREST,
    "nf-fa-skyatlas" => FA_SKYATLAS,
    "nf-fa-github-alt" => FA_GITHUB_ALT,
    "nf-fa-reddit-square" => FA_REDDIT_SQUARE,
    "nf-fa-toggle-on" => FA_TOGGLE_ON,
    "nf-fa-contao" => FA_CONTAO,
    "nf-fa-envira" => FA_ENVIRA,
    "nf-fa-google-plus-square" => FA_GOOGLE_PLUS_SQUARE,
    "nf-fa-universal-access" => FA_UNIVERSAL_ACCESS,
    "nf-fa-wheelchair-alt" => FA_WHEELCHAIR_ALT,
    "nf-fa-viadeo-square" => FA_VIADEO_SQUARE,
    "nf-fa-pied-piper" => FA_PIED_PIPER,
    "nf-fa-telegram" => FA_TELEGRAM,
    "nf-fa-minus-circle" => FA_MINUS_CIRCLE,
    "nf-fa-angle-double-left" => FA_ANGLE_DOUBLE_LEFT,
    "nf-fa-question-circle" => FA_QUESTION_CIRCLE,
    "nf-fa-angle-double-up" => FA_ANGLE_DOUBLE_UP,
    "nf-fa-toggle-off" => FA_TOGGLE_OFF,
    "nf-fa-bed" => FA_BED,
    "nf-fa-first-order" => FA_FIRST_ORDER,
    "nf-fa-desktop" => FA_DESKTOP,
    "nf-fa-hand-spock-o" => FA_HAND_SPOCK_O,
    "nf-fa-id-card" => FA_ID_CARD,
    "nf-fa-quora" => FA_QUORA,
    "nf-fa-twitter-square" => FA_TWITTER_SQUARE,
    "nf-fa-level-up" => FA_LEVEL_UP,
    "nf-fa-youtube-square" => FA_YOUTUBE_SQUARE,
    "nf-fa-cc-mastercard" => FA_CC_MASTERCARD,
    "nf-fa-google-wallet" => FA_GOOGLE_WALLET,
    "nf-fa-pinterest-p" => FA_PINTEREST_P,
    "nf-fa-address-card" => FA_ADDRESS_CARD,
    "nf-fa-eye-slash" => FA_EYE_SLASH,
    "nf-fa-bolt" => FA_BOLT,
    "nf-fa-scissors" => FA_SCISSORS,
    "nf-fa-btc" => FA_BTC,
    "nf-fa-sort-numeric-asc" => FA_SORT_NUMERIC_ASC,
    "nf-fa-podcast" => FA_PODCAST,
    "nf-fa-arrow-circle-down" => FA_ARROW_CIRCLE_DOWN,
    "nf-fa-unlock-alt" => FA_UNLOCK_ALT,
    "nf-fa-cubes" => FA_CUBES,
    "nf-fa-commenting-o" => FA_COMMENTING_O,
    "nf-fa-battery-half" => FA_BATTERY_HALF,
    "nf-fa-sort-alpha-asc" => FA_SORT_ALPHA_ASC,
    "nf-fa-table" => FA_TABLE,
    "nf-fa-plus" => FA_PLUS,
    "nf-fa-sun-o" => FA_SUN_O,
    "nf-fa-empire" => FA_EMPIRE,
    "nf-fa-music" => FA_MUSIC,
    "nf-fa-sort-asc" => FA_SORT_ASC,
    "nf-fa-info" => FA_INFO,
    "nf-fa-volume-up" => FA_VOLUME_UP,
    "nf-fa-hand-paper-o" => FA_HAND_PAPER_O,
    "nf-fa-font-awesome" => FA_FONT_AWESOME,
    "nf-fa-rocket" => FA_ROCKET,
    "nf-fa-superpowers" => FA_SUPERPOWERS,
    "nf-fa-pagelines" => FA_PAGELINES,
    "nf-fa-th-list" => FA_TH_LIST,
    "nf-fa-header" => FA_HEADER,
    "nf-fa-bitbucket" => FA_BITBUCKET,
    "nf-fa-css3" => FA_CSS3,
    "nf-fa-bus" => FA_BUS,
    "nf-fa-steam" => FA_STEAM,
    "nf-fa-circle-thin" => FA_CIRCLE_THIN,
    "nf-fa-pied-piper-pp" => FA_PIED_PIPER_PP,
    "nf-fa-sliders" => FA_SLIDERS,
    "nf-fa-spinner" => FA_SPINNER,
    "nf-fa-undo" => FA_UNDO,
    "nf-fa-chevron-up" => FA_CHEVRON_UP,
    "nf-fa-line-chart" => FA_LINE_CHART,
    "nf-fa-motorcycle" => FA_MOTORCYCLE,
    "nf-fa-hand-peace-o" => FA_HAND_PEACE_O,
    "nf-fa-money" => FA_MONEY,
    "nf-fa-file-text-o" => FA_FILE_TEXT_O,
    "nf-fa-fast-forward" => FA_FAST_FORWARD,
    "nf-fa-file-image-o" => FA_FILE_IMAGE_O,
    "nf-fa-file-archive-o" => FA_FILE_ARCHIVE_O,
    "nf-fa-optin-monster" => FA_OPTIN_MONSTER,
    "nf-fa-list-alt" => FA_LIST_ALT,
    "nf-fa-bitbucket-square" => FA_BITBUCKET_SQUARE,
    "nf-fa-shield" => FA_SHIELD,
    "nf-fa-id-card-o" => FA_ID_CARD_O,
    "nf-fa-commenting" => FA_COMMENTING,
    "nf-fa-odnoklassniki" => FA_ODNOKLASSNIKI,
    "nf-fa-th-large" => FA_TH_LARGE,
    "nf-fa-caret-up" => FA_CARET_UP,
    "nf-fa-hdd-o" => FA_HDD_O,
    "nf-fa-check-square" => FA_CHECK_SQUARE,
    "nf-fa-external-link-square" => FA_EXTERNAL_LINK_SQUARE,
    "nf-fa-plus-square" => FA_PLUS_SQUARE,
    "nf-fa-hand-rock-o" => FA_HAND_ROCK_O,
    "nf-fa-low-vision" => FA_LOW_VISION,
    "nf-fa-files-o" => FA_FILES_O,
    "nf-fa-ambulance" => FA_AMBULANCE,
    "nf-fa-snapchat-square" => FA_SNAPCHAT_SQUARE,
    "nf-fa-font" => FA_FONT,
    "nf-fa-bars" => FA_BARS,
    "nf-fa-vimeo-square" => FA_VIMEO_SQUARE,
    "nf-fa-gift" => FA_GIFT,
    "nf-fa-lastfm-square" => FA_LASTFM_SQUARE,
    "nf-fa-handshake-o" => FA_HANDSHAKE_O,
    "nf-fa-modx" => FA_MODX,
    "nf-fa-file-code-o" => FA_FILE_CODE_O,
    "nf-fa-odnoklassniki-square" => FA_ODNOKLASSNIKI_SQUARE,
    "nf-fa-opencart" => FA_OPENCART,
    "nf-fa-retweet" => FA_RETWEET,
    "nf-fa-barcode" => FA_BARCODE,
    "nf-fa-frown-o" => FA_FROWN_O,
    "nf-fa-bicycle" => FA_BICYCLE,
    "nf-fa-cart-arrow-down" => FA_CART_ARROW_DOWN,
    "nf-fa-cc-diners-club" => FA_CC_DINERS_CLUB,
    "nf-fa-file" => FA_FILE,
    "nf-fa-expand" => FA_EXPAND,
    "nf-fa-bold" => FA_BOLD,
    "nf-fa-lock" => FA_LOCK,
    "nf-fa-lemon-o" => FA_LEMON_O,
    "nf-fa-hospital-o" => FA_HOSPITAL_O,
    "nf-fa-safari" => FA_SAFARI,
    "nf-fa-linux" => FA_LINUX,
    "nf-fa-dashcube" => FA_DASHCUBE,
    "nf-fa-battery-empty" => FA_BATTERY_EMPTY,
    "nf-fa-binoculars" => FA_BINOCULARS,
    "nf-fa-credit-card-alt" => FA_CREDIT_CARD_ALT,
    "nf-fa-cube" => FA_CUBE,
    "nf-fa-trophy" => FA_TROPHY,
    "nf-fa-cc-stripe" => FA_CC_STRIPE,
    "nf-fa-keyboard-o" => FA_KEYBOARD_O,
    "nf-fa-google" => FA_GOOGLE,
    "nf-fa-calendar-minus-o" => FA_CALENDAR_MINUS_O,
    "nf-fa-id-badge" => FA_ID_BADGE,
    "nf-fa-trello" => FA_TRELLO,
    "nf-fa-tags" => FA_TAGS,
    "nf-fa-book" => FA_BOOK,
    "nf-fa-tencent-weibo" => FA_TENCENT_WEIBO,
    "nf-fa-wifi" => FA_WIFI,
    "nf-fa-video-camera" => FA_VIDEO_CAMERA,
    "nf-fa-quote-right" => FA_QUOTE_RIGHT,
    "nf-fa-mars-double" => FA_MARS_DOUBLE,
    "nf-fa-meetup" => FA_MEETUP,
    "nf-fa-spoon" => FA_SPOON,
    "nf-fa-microphone" => FA_MICROPHONE,
    "nf-fa-creative-commons" => FA_CREATIVE_COMMONS,
    "nf-fa-magic" => FA_MAGIC,
    "nf-fa-git" => FA_GIT,
    "nf-fa-code-fork" => FA_CODE_FORK,
    "nf-fa-eur" => FA_EUR,
    "nf-fa-mars-stroke-v" => FA_MARS_STROKE_V,
    "nf-fa-yoast" => FA_YOAST,
    "nf-fa-ban" => FA_BAN,
    "nf-fa-sort-amount-desc" => FA_SORT_AMOUNT_DESC,
    "nf-fa-bullhorn" => FA_BULLHORN,
    "nf-fa-github-square" => FA_GITHUB_SQUARE,
    "nf-fa-arrow-circle-right" => FA_ARROW_CIRCLE_RIGHT,
    "nf-fa-hand-o-down" => FA_HAND_O_DOWN,
    "nf-fa-git-square" => FA_GIT_SQUARE,
    "nf-fa-envelope-open" => FA_ENVELOPE_OPEN,
    "nf-fa-paw" => FA_PAW,
    "nf-fa-plus-circle" => FA_PLUS_CIRCLE,
    "nf-fa-file-powerpoint-o" => FA_FILE_POWERPOINT_O,
    "nf-fa-user-secret" => FA_USER_SECRET,
    "nf-fa-y-combinator" => FA_Y_COMBINATOR,
    "nf-fa-qq" => FA_QQ,
    "nf-fa-eraser" => FA_ERASER,
    "nf-fa-venus" => FA_VENUS,
    "nf-fa-thermometer-three-quarters" => FA_THERMOMETER_THREE_QUARTERS,
    "nf-fa-caret-square-o-down" => FA_CARET_SQUARE_O_DOWN,
    "nf-fa-thermometer-empty" => FA_THERMOMETER_EMPTY,
    "nf-fa-building-o" => FA_BUILDING_O,
    "nf-fa-google-plus" => FA_GOOGLE_PLUS,
    "nf-fa-gg" => FA_GG,
    "nf-fa-calculator" => FA_CALCULATOR,
    "nf-fa-fort-awesome" => FA_FORT_AWESOME,
    "nf-fa-road" => FA_ROAD,
    "nf-fa-tumblr" => FA_TUMBLR,
    "nf-fa-angle-double-right" => FA_ANGLE_DOUBLE_RIGHT,
    "nf-fa-moon-o" => FA_MOON_O,
    "nf-fa-file-excel-o" => FA_FILE_EXCEL_O,
    "nf-fa-address-card-o" => FA_ADDRESS_CARD_O,
    "nf-fa-star-half" => FA_STAR_HALF,
    "nf-fa-forward" => FA_FORWARD,
    "nf-fa-ticket" => FA_TICKET,
    "nf-fa-hand-o-up" => FA_HAND_O_UP,
    "nf-fa-pie-chart" => FA_PIE_CHART,
    "nf-fa-clock-o" => FA_CLOCK_O,
    "nf-fa-linkedin-square" => FA_LINKEDIN_SQUARE,
    "nf-fa-superscript" => FA_SUPERSCRIPT,
    "nf-fa-glass" => FA_GLASS,
    "nf-fa-subscript" => FA_SUBSCRIPT,
    "nf-fa-life-ring" => FA_LIFE_RING,
    "nf-fa-text-width" => FA_TEXT_WIDTH,
    "nf-fa-behance-square" => FA_BEHANCE_SQUARE,
    "nf-fa-linode" => FA_LINODE,
    "nf-fa-address-book" => FA_ADDRESS_BOOK,
    "nf-fa-bath" => FA_BATH,
    "nf-fa-tripadvisor" => FA_TRIPADVISOR,
    "nf-fa-gratipay" => FA_GRATIPAY,
    "nf-fa-reply" => FA_REPLY,
    "nf-fa-instagram" => FA_INSTAGRAM,
    "nf-fa-shopping-cart" => FA_SHOPPING_CART,
    "nf-fa-align-left" => FA_ALIGN_LEFT,
    "nf-fa-braille" => FA_BRAILLE,
    "nf-fa-caret-square-o-right" => FA_CARET_SQUARE_O_RIGHT,
    "nf-fa-plug" => FA_PLUG,
    "nf-fa-list-ul" => FA_LIST_UL,
    "nf-fa-circle" => FA_CIRCLE,
    "nf-fa-align-center" => FA_ALIGN_CENTER,
    "nf-fa-bluetooth-b" => FA_BLUETOOTH_B,
    "nf-fa-bomb" => FA_BOMB,
    "nf-fa-hourglass" => FA_HOURGLASS,
    "nf-fa-tty" => FA_TTY,
    "nf-fa-fonticons" => FA_FONTICONS,
    "nf-fa-leaf" => FA_LEAF,
    "nf-fa-thumbs-o-up" => FA_THUMBS_O_UP,
    "nf-fa-thermometer-half" => FA_THERMOMETER_HALF,
    "nf-fa-inbox" => FA_INBOX,
    "nf-fa-map-marker" => FA_MAP_MARKER,
    "nf-fa-fax" => FA_FAX,
    "nf-fa-wpexplorer" => FA_WPEXPLORER,
    "nf-fa-mouse-pointer" => FA_MOUSE_POINTER,
    "nf-fa-transgender-alt" => FA_TRANSGENDER_ALT,
    "nf-fa-bar-chart" => FA_BAR_CHART,
    "nf-fa-internet-explorer" => FA_INTERNET_EXPLORER,
    "nf-fa-briefcase" => FA_BRIEFCASE,
    "nf-fa-minus-square-o" => FA_MINUS_SQUARE_O,
    "nf-fa-krw" => FA_KRW,
    "nf-fa-recycle" => FA_RECYCLE,
    "nf-fa-lastfm" => FA_LASTFM,
    "nf-fa-deaf" => FA_DEAF,
    "nf-fa-pause" => FA_PAUSE,
    "nf-fa-github" => FA_GITHUB,
    "nf-fa-certificate" => FA_CERTIFICATE,
    "nf-fa-twitch" => FA_TWITCH,
    "nf-fa-ship" => FA_SHIP,
    "nf-fa-free-code-camp" => FA_FREE_CODE_CAMP,
    "nf-fa-amazon" => FA_AMAZON,
    "nf-fa-mercury" => FA_MERCURY,
    "nf-fa-flag" => FA_FLAG,
    "nf-fa-heart-o" => FA_HEART_O,
    "nf-fa-trash-o" => FA_TRASH_O,
    "nf-fa-arrow-circle-left" => FA_ARROW_CIRCLE_LEFT,
    "nf-fa-refresh" => FA_REFRESH,
    "nf-fa-share-square-o" => FA_SHARE_SQUARE_O,
    "nf-fa-long-arrow-down" => FA_LONG_ARROW_DOWN,
    "nf-fa-inr" => FA_INR,
    "nf-fa-usd" => FA_USD,
    "nf-fa-sort-numeric-desc" => FA_SORT_NUMERIC_DESC,
    "nf-fa-simplybuilt" => FA_SIMPLYBUILT,
    "nf-fa-puzzle-piece" => FA_PUZZLE_PIECE,
    "nf-fa-flickr" => FA_FLICKR,
    "nf-fa-spotify" => FA_SPOTIFY,
    "nf-fa-sitemap" => FA_SITEMAP,
    "nf-fa-search" => FA_SEARCH,
    "nf-fa-comments" => FA_COMMENTS,
    "nf-fa-headphones" => FA_HEADPHONES,
    "nf-fa-star" => FA_STAR,
    "nf-fa-phone" => FA_PHONE,
    "nf-fa-angle-right" => FA_ANGLE_RIGHT,
    "nf-fa-user-circle-o" => FA_USER_CIRCLE_O,
    "nf-fa-rss-square" => FA_RSS_SQUARE,
    "nf-fa-chain-broken" => FA_CHAIN_BROKEN,
    "nf-fa-eyedropper" => FA_EYEDROPPER,
    "nf-fa-wordpress" => FA_WORDPRESS,
    "nf-fa-caret-square-o-left" => FA_CARET_SQUARE_O_LEFT,
    "nf-fa-jsfiddle" => FA_JSFIDDLE,
    "nf-fa-dropbox" => FA_DROPBOX,
    "nf-fa-birthday-cake" => FA_BIRTHDAY_CAKE,
    "nf-fa-facebook-official" => FA_FACEBOOK_OFFICIAL,
    "nf-fa-medium" => FA_MEDIUM,
    "nf-fa-hand-o-left" => FA_HAND_O_LEFT,
    "nf-fa-vimeo" => FA_VIMEO,
    "nf-fa-university" => FA_UNIVERSITY,
    "nf-fa-arrow-circle-up" => FA_ARROW_CIRCLE_UP,
    "nf-fa-text-height" => FA_TEXT_HEIGHT,
    "nf-fa-folder-open" => FA_FOLDER_OPEN,
    "nf-fa-compress" => FA_COMPRESS,
    "nf-fa-youtube-play" => FA_YOUTUBE_PLAY,
    "nf-fa-user-plus" => FA_USER_PLUS,
    "nf-fa-tachometer" => FA_TACHOMETER,
    "nf-fa-html5" => FA_HTML5,
    "nf-fa-chevron-circle-up" => FA_CHEVRON_CIRCLE_UP,
    "nf-fa-bullseye" => FA_BULLSEYE,
    "nf-fa-stethoscope" => FA_STETHOSCOPE,
    "nf-fa-rebel" => FA_REBEL,
    "nf-fa-info-circle" => FA_INFO_CIRCLE,
    "nf-fa-microphone-slash" => FA_MICROPHONE_SLASH,
    "nf-fa-bell-slash" => FA_BELL_SLASH,
    "nf-fa-power-off" => FA_POWER_OFF,
    "nf-fa-glide" => FA_GLIDE,
    "nf-fa-at" => FA_AT,
    "nf-fa-stop-circle" => FA_STOP_CIRCLE,
    "nf-fa-align-justify" => FA_ALIGN_JUSTIFY,
    "nf-fa-lightbulb-o" => FA_LIGHTBULB_O,
    "nf-fa-female" => FA_FEMALE,
    "nf-fa-user-o" => FA_USER_O,
    "nf-fa-firefox" => FA_FIREFOX,
    "nf-fa-globe" => FA_GLOBE,
    "nf-fa-ellipsis-v" => FA_ELLIPSIS_V,
    "nf-fa-history" => FA_HISTORY,
    "nf-fa-shopping-basket" => FA_SHOPPING_BASKET,
    "nf-fa-dribbble" => FA_DRIBBBLE,
    "nf-fa-comments-o" => FA_COMMENTS_O,
    "nf-fa-caret-right" => FA_CARET_RIGHT,
    "nf-fa-envelope" => FA_ENVELOPE,
    "nf-fa-xing" => FA_XING,
    "nf-fa-copyright" => FA_COPYRIGHT,
    "nf-fa-viacoin" => FA_VIACOIN,
    "nf-fa-ravelry" => FA_RAVELRY,
    "nf-fa-futbol-o" => FA_FUTBOL_O,
    "nf-fa-indent" => FA_INDENT,
    "nf-fa-check" => FA_CHECK,
    "nf-fa-truck" => FA_TRUCK,
    "nf-fa-caret-square-o-up" => FA_CARET_SQUARE_O_UP,
    "nf-fa-twitter" => FA_TWITTER,
    "nf-fa-diamond" => FA_DIAMOND,
    "nf-fa-space-shuttle" => FA_SPACE_SHUTTLE,
    "nf-fa-cutlery" => FA_CUTLERY,
    "nf-fa-vine" => FA_VINE,
    "nf-fa-expeditedssl" => FA_EXPEDITEDSSL,
    "nf-fa-sticky-note-o" => FA_STICKY_NOTE_O,
    "nf-fa-arrows-v" => FA_ARROWS_V,
    "nf-fa-times" => FA_TIMES,
    "nf-fa-whatsapp" => FA_WHATSAPP,
    "nf-fa-heartbeat" => FA_HEARTBEAT,
    "nf-fa-circle-o" => FA_CIRCLE_O,
    "nf-fa-address-book-o" => FA_ADDRESS_BOOK_O,
    "nf-fa-circle-o-notch" => FA_CIRCLE_O_NOTCH,
    "nf-fa-bell-slash-o" => FA_BELL_SLASH_O,
    "nf-fa-th" => FA_TH,
    "nf-fa-jpy" => FA_JPY,
    "nf-fa-laptop" => FA_LAPTOP,
    "nf-fa-shower" => FA_SHOWER,
    "nf-fa-comment" => FA_COMMENT,
    "nf-fa-signal" => FA_SIGNAL,
    "nf-fa-hand-lizard-o" => FA_HAND_LIZARD_O,
    "nf-fa-usb" => FA_USB,
    "nf-fa-envelope-open-o" => FA_ENVELOPE_OPEN_O,
    "nf-fa-imdb" => FA_IMDB,
    "nf-fa-unlock" => FA_UNLOCK,
    "nf-fa-registered" => FA_REGISTERED,
    "nf-fa-clipboard" => FA_CLIPBOARD,
    "nf-fa-minus-square" => FA_MINUS_SQUARE,
    "nf-fa-product-hunt" => FA_PRODUCT_HUNT,
    "nf-fa-500px" => FA_500PX,
    "nf-fa-list" => FA_LIST,
    "nf-fa-file-text" => FA_FILE_TEXT,
    "nf-fa-hourglass-half" => FA_HOURGLASS_HALF,
    "nf-fa-building" => FA_BUILDING,
    "nf-fa-flask" => FA_FLASK,
    "nf-fa-fighter-jet" => FA_FIGHTER_JET,
    "nf-fa-battery-full" => FA_BATTERY_FULL,
    "nf-fa-car" => FA_CAR,
    "nf-fa-folder-o" => FA_FOLDER_O,
    "nf-fa-long-arrow-right" => FA_LONG_ARROW_RIGHT,
    "nf-fa-stumbleupon-circle" => FA_STUMBLEUPON_CIRCLE,
    "nf-fa-wpbeginner" => FA_WPBEGINNER,
    "nf-fa-street-view" => FA_STREET_VIEW,
    "nf-fa-square-o" => FA_SQUARE_O,
    "nf-fa-question-circle-o" => FA_QUESTION_CIRCLE_O,
    "nf-fa-meh-o" => FA_MEH_O,
    "nf-fa-sellsy" => FA_SELLSY,
    "nf-fa-star-half-o" => FA_STAR_HALF_O,
    "nf-fa-gbp" => FA_GBP,
    "nf-fa-cc-amex" => FA_CC_AMEX,
    "nf-fa-assistive-listening-systems" => FA_ASSISTIVE_LISTENING_SYSTEMS,
    "nf-fa-envelope-o" => FA_ENVELOPE_O,
    "nf-fa-mars-stroke-h" => FA_MARS_STROKE_H,
    "nf-fa-align-right" => FA_ALIGN_RIGHT,
    "nf-fa-angellist" => FA_ANGELLIST,
};
