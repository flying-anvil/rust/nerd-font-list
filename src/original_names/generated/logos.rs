// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_logos.sh


/// Count of all Logos (of Linux distros?) Icons (Not an icon itself)
pub const LOGOS_ICON_COUNT: usize = 48;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ENDEAVOUR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_LINUXMINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_MAGEIA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_MANDRIVA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ILLUMOS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_FEDORA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ALMALINUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_GNU_GUIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_SABAYON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ARCHLABS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ROCKY_LINUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_GENTOO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_UBUNTU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_COREOS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_UBUNTU_INVERSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_APPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_FLATHUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ARTIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_PARROT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_KALI_LINUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_SLACKWARE_INVERSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_REDHAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_FEDORA_INVERSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_AOSC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_VOID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_RASPBERRY_PI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_OPENSUSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_BUDGIE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ARCHLINUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ZORIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_DEEPIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_CENTOS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_FREEBSD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_MANJARO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_FERRIS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_NIXOS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_DEVUAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_LINUXMINT_INVERSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_SOLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_OPENBSD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_DEBIAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_DOCKER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ALPINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_TUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_POP_OS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_SNAPPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_SLACKWARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const LINUX_ELEMENTARY: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const LOGOS_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-linux-endeavour" => LINUX_ENDEAVOUR,
    "nf-linux-linuxmint" => LINUX_LINUXMINT,
    "nf-linux-mageia" => LINUX_MAGEIA,
    "nf-linux-mandriva" => LINUX_MANDRIVA,
    "nf-linux-illumos" => LINUX_ILLUMOS,
    "nf-linux-fedora" => LINUX_FEDORA,
    "nf-linux-almalinux" => LINUX_ALMALINUX,
    "nf-linux-gnu-guix" => LINUX_GNU_GUIX,
    "nf-linux-sabayon" => LINUX_SABAYON,
    "nf-linux-archlabs" => LINUX_ARCHLABS,
    "nf-linux-rocky-linux" => LINUX_ROCKY_LINUX,
    "nf-linux-gentoo" => LINUX_GENTOO,
    "nf-linux-ubuntu" => LINUX_UBUNTU,
    "nf-linux-coreos" => LINUX_COREOS,
    "nf-linux-ubuntu-inverse" => LINUX_UBUNTU_INVERSE,
    "nf-linux-apple" => LINUX_APPLE,
    "nf-linux-flathub" => LINUX_FLATHUB,
    "nf-linux-artix" => LINUX_ARTIX,
    "nf-linux-parrot" => LINUX_PARROT,
    "nf-linux-kali-linux" => LINUX_KALI_LINUX,
    "nf-linux-slackware-inverse" => LINUX_SLACKWARE_INVERSE,
    "nf-linux-redhat" => LINUX_REDHAT,
    "nf-linux-fedora-inverse" => LINUX_FEDORA_INVERSE,
    "nf-linux-aosc" => LINUX_AOSC,
    "nf-linux-void" => LINUX_VOID,
    "nf-linux-raspberry-pi" => LINUX_RASPBERRY_PI,
    "nf-linux-opensuse" => LINUX_OPENSUSE,
    "nf-linux-budgie" => LINUX_BUDGIE,
    "nf-linux-archlinux" => LINUX_ARCHLINUX,
    "nf-linux-zorin" => LINUX_ZORIN,
    "nf-linux-deepin" => LINUX_DEEPIN,
    "nf-linux-centos" => LINUX_CENTOS,
    "nf-linux-freebsd" => LINUX_FREEBSD,
    "nf-linux-manjaro" => LINUX_MANJARO,
    "nf-linux-ferris" => LINUX_FERRIS,
    "nf-linux-nixos" => LINUX_NIXOS,
    "nf-linux-devuan" => LINUX_DEVUAN,
    "nf-linux-linuxmint-inverse" => LINUX_LINUXMINT_INVERSE,
    "nf-linux-solus" => LINUX_SOLUS,
    "nf-linux-openbsd" => LINUX_OPENBSD,
    "nf-linux-debian" => LINUX_DEBIAN,
    "nf-linux-docker" => LINUX_DOCKER,
    "nf-linux-alpine" => LINUX_ALPINE,
    "nf-linux-tux" => LINUX_TUX,
    "nf-linux-pop-os" => LINUX_POP_OS,
    "nf-linux-snappy" => LINUX_SNAPPY,
    "nf-linux-slackware" => LINUX_SLACKWARE,
    "nf-linux-elementary" => LINUX_ELEMENTARY,
};
