/// Codicons – 387 Icons
pub mod cod;

/// Devicons – 191 Icons
pub mod dev;

/// Font Awesome Extension – 170 Icons
pub mod fae;

/// Font Awesome – 675 Icons
pub mod fa;

/// IEC Power Symbols – 5 Icons
pub mod iec;

/// Logos (of Linux distros?) – 48 Icons
pub mod logos;

/// Octicons – 310 Icons
pub mod oct;

/// Powerline Extra Symbols – 38 Icons
pub mod ple;

/// Pomicons – 11 Icons
pub mod pom;

/// Seti-UI + Custom – 180 Icons
pub mod seti;

/// Weather – 228 Icons
pub mod weather;

/// Material Design – 6880 Icons
pub mod md;

/// Material Design Icons (legacy) – 2119 Icons
pub mod mdi;

// Aliases with longer/human readable names
pub use cod as codicons;
pub use dev as devicons;
pub use fae as font_awesome_extension;
pub use fa as font_awesome;
pub use iec as iec_power_symbols;
pub use oct as Octicons;
pub use ple as powerline_extra_symbols;
pub use pom as pomicons;
pub use seti as seti_ui;
pub use md as material_design;
pub use mdi as material_design_legacy;

/// Count of all supported icons.
pub const TOTAL_ICON_COUNT: usize = 11242;

#[cfg(feature = "search")]
pub mod search {
    use phf::Map;
    use std::collections::HashMap;

    pub use self::search_cod as search_codicons;
    pub use self::search_dev as search_devicons;
    pub use self::search_fae as search_font_awesome_extension;
    pub use self::search_fa as search_font_awesome;
    pub use self::search_iec as search_iec_power_symbols;
    pub use self::search_oct as search_Octicons;
    pub use self::search_ple as search_powerline_extra_symbols;
    pub use self::search_pom as search_pomicons;
    pub use self::search_seti as search_seti_ui;
    pub use self::search_md as search_material_design;
    pub use self::search_mdi as search_material_design_legacy;

    fn search_single_map(haystack: Map<&str, &str>, term: &str, result: &mut HashMap<&str, &str>) {
        haystack
            .entries
            .iter()
            .filter(|(key, value)| key.contains(&term) || value == &term)
            .for_each(|(key, value)| {
                result.insert(key, value);
            });
    }

    pub fn search_cod(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::cod::COD_MAPPING, &term, &mut result);

        result
    }

    pub fn search_dev(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::dev::DEV_MAPPING, &term, &mut result);

        result
    }

    pub fn search_fae(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::fae::FAE_MAPPING, &term, &mut result);

        result
    }

    pub fn search_fa(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::fa::FA_MAPPING, &term, &mut result);

        result
    }

    pub fn search_iec(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::iec::IEC_MAPPING, &term, &mut result);

        result
    }

    pub fn search_logos(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::logos::LOGOS_MAPPING, &term, &mut result);

        result
    }

    pub fn search_oct(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::oct::OCT_MAPPING, &term, &mut result);

        result
    }

    pub fn search_ple(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::ple::PLE_MAPPING, &term, &mut result);

        result
    }

    pub fn search_pom(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::pom::POM_MAPPING, &term, &mut result);

        result
    }

    pub fn search_seti(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::seti::SETI_MAPPING, &term, &mut result);

        result
    }

    pub fn search_weather(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::weather::WEATHER_MAPPING, &term, &mut result);

        result
    }

    pub fn search_md(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::md::MD_MAPPING, &term, &mut result);

        result
    }

    pub fn search_mdi(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::mdi::MDI_MAPPING, &term, &mut result);

        result
    }

    pub fn search_all(term: &str) -> HashMap<&str, &str> {
        let term = term.to_lowercase().replace("_", "-");
        let mut result: HashMap<&str, &str> = HashMap::new();

        search_single_map(super::cod::COD_MAPPING, &term, &mut result);
        search_single_map(super::dev::DEV_MAPPING, &term, &mut result);
        search_single_map(super::fae::FAE_MAPPING, &term, &mut result);
        search_single_map(super::fa::FA_MAPPING, &term, &mut result);
        search_single_map(super::iec::IEC_MAPPING, &term, &mut result);
        search_single_map(super::logos::LOGOS_MAPPING, &term, &mut result);
        search_single_map(super::oct::OCT_MAPPING, &term, &mut result);
        search_single_map(super::ple::PLE_MAPPING, &term, &mut result);
        search_single_map(super::pom::POM_MAPPING, &term, &mut result);
        search_single_map(super::seti::SETI_MAPPING, &term, &mut result);
        search_single_map(super::weather::WEATHER_MAPPING, &term, &mut result);
        search_single_map(super::md::MD_MAPPING, &term, &mut result);
        search_single_map(super::mdi::MDI_MAPPING, &term, &mut result);

        result
    }
}
