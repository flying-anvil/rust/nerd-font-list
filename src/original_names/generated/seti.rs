// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_seti.sh


/// Count of all Seti-UI + Custom Icons (Not an icon itself)
pub const SETI_ICON_COUNT: usize = 180;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ASM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_C_SHARP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_NIM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const INDENT_LINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_CPP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CLOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_BICEP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_TYPESCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_WORD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SETTINGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_MSDOS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_STYLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GRADLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_IGNORED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_DOCKER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_MAKEFILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SEARCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_HAPPENINGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GODOT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_RUST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_BABEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_HTML: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_EMACS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CHECKBOX_UNCHECKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_JADE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_HOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_REASONML: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PHP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_FOLDER_CONFIG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GO2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CAKE_PHP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_FONT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_LIVESCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CAKE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_COFFEE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_DEFAULT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GRAPHQL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ETHEREUM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_FOLDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_IMAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_FIREBASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_WAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_NUNJUCKS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CRYSTAL_EMBEDDED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_DEFAULT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_TWIG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_BSL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_DB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_PLAY_ARROW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_D: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_BOWER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_VUE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CSS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_R: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_WGT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_KOTLIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_ASM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_V_LANG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_TODO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ROLLUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_FIREFOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_LUA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_TSCONFIG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ZIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SPRING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_FOLDER_OCT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_JENKINS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ELIXIR_SCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ESLINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CHECKBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_FOLDER_GIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GITHUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_JINJA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PHOTOSHOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_ELM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_FAVICON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_HAML: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_VIDEO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_RESCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_YARN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CONFIG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_NPM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_PURESCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_VIM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_STYLELINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ODATA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_HAXE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_XML: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PUPPET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CODE_SEARCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SUBLIME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_TERRAFORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_FOLDER_GITHUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CODE_CLIMATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GITLAB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PROJECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_FOLDER_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_EJS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GRAILS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_HEX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_INFO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_NEW_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PLAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PROLOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_TEX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_YML: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ARGDOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_HACKLANG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_MAVEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_JSON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PERL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_LICENSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SMARTY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_KARMA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_IONIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_CRYSTAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_HEROKU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_MARKDOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_AUDIO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_EDITORCONFIG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_F_SHARP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_COLDFUSION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_FOLDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CSV: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_LIQUID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_RUBY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GULP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_FOLDER_NPM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_WINDOWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_JAVASCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SLIM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_REACT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CLOJURE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PDF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SWIFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_C: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_RAILS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SBT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_GO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PIPELINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_DART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GRUNT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SALESFORCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ILLUSTRATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_GO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_JAVA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PDDL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_VALA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_BAZEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PYTHON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_DEPRECATION_COP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_WEBPACK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SVELTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_APPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_CPP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_OCAML: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_XLS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_MUSTACHE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ZIG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_ELECTRON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_ELIXIR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SCALA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const CUSTOM_ORGMODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_C: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_NOTEBOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_WASM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_ERROR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SHELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_HASKELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_MDO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PRISMA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_POWERSHELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_PLATFORMIO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_JULIA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const SETI_SVG: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const SETI_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-seti-asm" => SETI_ASM,
    "nf-seti-c-sharp" => SETI_C_SHARP,
    "nf-seti-nim" => SETI_NIM,
    "nf-indent-line" => INDENT_LINE,
    "nf-custom-cpp" => CUSTOM_CPP,
    "nf-seti-clock" => SETI_CLOCK,
    "nf-seti-bicep" => SETI_BICEP,
    "nf-seti-typescript" => SETI_TYPESCRIPT,
    "nf-seti-git" => SETI_GIT,
    "nf-seti-word" => SETI_WORD,
    "nf-seti-settings" => SETI_SETTINGS,
    "nf-custom-msdos" => CUSTOM_MSDOS,
    "nf-seti-stylus" => SETI_STYLUS,
    "nf-seti-gradle" => SETI_GRADLE,
    "nf-seti-ignored" => SETI_IGNORED,
    "nf-seti-docker" => SETI_DOCKER,
    "nf-seti-makefile" => SETI_MAKEFILE,
    "nf-seti-search" => SETI_SEARCH,
    "nf-seti-happenings" => SETI_HAPPENINGS,
    "nf-seti-godot" => SETI_GODOT,
    "nf-seti-rust" => SETI_RUST,
    "nf-seti-babel" => SETI_BABEL,
    "nf-seti-html" => SETI_HTML,
    "nf-custom-emacs" => CUSTOM_EMACS,
    "nf-seti-checkbox-unchecked" => SETI_CHECKBOX_UNCHECKED,
    "nf-seti-jade" => SETI_JADE,
    "nf-custom-home" => CUSTOM_HOME,
    "nf-seti-reasonml" => SETI_REASONML,
    "nf-seti-php" => SETI_PHP,
    "nf-custom-folder-config" => CUSTOM_FOLDER_CONFIG,
    "nf-seti-go2" => SETI_GO2,
    "nf-seti-cake-php" => SETI_CAKE_PHP,
    "nf-seti-font" => SETI_FONT,
    "nf-seti-livescript" => SETI_LIVESCRIPT,
    "nf-seti-cake" => SETI_CAKE,
    "nf-seti-coffee" => SETI_COFFEE,
    "nf-custom-default" => CUSTOM_DEFAULT,
    "nf-seti-graphql" => SETI_GRAPHQL,
    "nf-seti-ethereum" => SETI_ETHEREUM,
    "nf-seti-folder" => SETI_FOLDER,
    "nf-seti-image" => SETI_IMAGE,
    "nf-seti-firebase" => SETI_FIREBASE,
    "nf-seti-wat" => SETI_WAT,
    "nf-seti-nunjucks" => SETI_NUNJUCKS,
    "nf-seti-crystal-embedded" => SETI_CRYSTAL_EMBEDDED,
    "nf-seti-default" => SETI_DEFAULT,
    "nf-seti-twig" => SETI_TWIG,
    "nf-seti-cu" => SETI_CU,
    "nf-seti-bsl" => SETI_BSL,
    "nf-seti-db" => SETI_DB,
    "nf-custom-play-arrow" => CUSTOM_PLAY_ARROW,
    "nf-seti-d" => SETI_D,
    "nf-seti-bower" => SETI_BOWER,
    "nf-seti-vue" => SETI_VUE,
    "nf-seti-css" => SETI_CSS,
    "nf-seti-r" => SETI_R,
    "nf-seti-wgt" => SETI_WGT,
    "nf-custom-kotlin" => CUSTOM_KOTLIN,
    "nf-custom-asm" => CUSTOM_ASM,
    "nf-custom-v-lang" => CUSTOM_V_LANG,
    "nf-seti-todo" => SETI_TODO,
    "nf-seti-rollup" => SETI_ROLLUP,
    "nf-seti-firefox" => SETI_FIREFOX,
    "nf-seti-lua" => SETI_LUA,
    "nf-seti-tsconfig" => SETI_TSCONFIG,
    "nf-seti-zip" => SETI_ZIP,
    "nf-seti-spring" => SETI_SPRING,
    "nf-custom-folder-oct" => CUSTOM_FOLDER_OCT,
    "nf-seti-jenkins" => SETI_JENKINS,
    "nf-seti-elixir-script" => SETI_ELIXIR_SCRIPT,
    "nf-seti-eslint" => SETI_ESLINT,
    "nf-seti-checkbox" => SETI_CHECKBOX,
    "nf-custom-folder-git" => CUSTOM_FOLDER_GIT,
    "nf-seti-github" => SETI_GITHUB,
    "nf-seti-jinja" => SETI_JINJA,
    "nf-seti-photoshop" => SETI_PHOTOSHOP,
    "nf-custom-elm" => CUSTOM_ELM,
    "nf-seti-favicon" => SETI_FAVICON,
    "nf-seti-haml" => SETI_HAML,
    "nf-seti-video" => SETI_VIDEO,
    "nf-seti-rescript" => SETI_RESCRIPT,
    "nf-seti-sass" => SETI_SASS,
    "nf-seti-yarn" => SETI_YARN,
    "nf-seti-config" => SETI_CONFIG,
    "nf-seti-npm" => SETI_NPM,
    "nf-custom-purescript" => CUSTOM_PURESCRIPT,
    "nf-custom-vim" => CUSTOM_VIM,
    "nf-seti-stylelint" => SETI_STYLELINT,
    "nf-seti-odata" => SETI_ODATA,
    "nf-seti-haxe" => SETI_HAXE,
    "nf-seti-xml" => SETI_XML,
    "nf-seti-puppet" => SETI_PUPPET,
    "nf-seti-code-search" => SETI_CODE_SEARCH,
    "nf-seti-sublime" => SETI_SUBLIME,
    "nf-seti-terraform" => SETI_TERRAFORM,
    "nf-custom-folder-github" => CUSTOM_FOLDER_GITHUB,
    "nf-seti-code-climate" => SETI_CODE_CLIMATE,
    "nf-seti-gitlab" => SETI_GITLAB,
    "nf-seti-project" => SETI_PROJECT,
    "nf-custom-folder-open" => CUSTOM_FOLDER_OPEN,
    "nf-seti-ejs" => SETI_EJS,
    "nf-seti-grails" => SETI_GRAILS,
    "nf-seti-hex" => SETI_HEX,
    "nf-seti-info" => SETI_INFO,
    "nf-seti-new-file" => SETI_NEW_FILE,
    "nf-seti-plan" => SETI_PLAN,
    "nf-seti-prolog" => SETI_PROLOG,
    "nf-seti-tex" => SETI_TEX,
    "nf-seti-yml" => SETI_YML,
    "nf-seti-argdown" => SETI_ARGDOWN,
    "nf-seti-hacklang" => SETI_HACKLANG,
    "nf-seti-maven" => SETI_MAVEN,
    "nf-seti-json" => SETI_JSON,
    "nf-seti-perl" => SETI_PERL,
    "nf-seti-license" => SETI_LICENSE,
    "nf-seti-smarty" => SETI_SMARTY,
    "nf-seti-karma" => SETI_KARMA,
    "nf-seti-ionic" => SETI_IONIC,
    "nf-custom-crystal" => CUSTOM_CRYSTAL,
    "nf-seti-heroku" => SETI_HEROKU,
    "nf-seti-markdown" => SETI_MARKDOWN,
    "nf-seti-audio" => SETI_AUDIO,
    "nf-seti-editorconfig" => SETI_EDITORCONFIG,
    "nf-seti-f-sharp" => SETI_F_SHARP,
    "nf-seti-coldfusion" => SETI_COLDFUSION,
    "nf-custom-folder" => CUSTOM_FOLDER,
    "nf-seti-csv" => SETI_CSV,
    "nf-seti-liquid" => SETI_LIQUID,
    "nf-seti-ruby" => SETI_RUBY,
    "nf-seti-gulp" => SETI_GULP,
    "nf-custom-folder-npm" => CUSTOM_FOLDER_NPM,
    "nf-custom-windows" => CUSTOM_WINDOWS,
    "nf-seti-javascript" => SETI_JAVASCRIPT,
    "nf-seti-slim" => SETI_SLIM,
    "nf-seti-react" => SETI_REACT,
    "nf-seti-clojure" => SETI_CLOJURE,
    "nf-seti-pdf" => SETI_PDF,
    "nf-seti-swift" => SETI_SWIFT,
    "nf-custom-c" => CUSTOM_C,
    "nf-seti-rails" => SETI_RAILS,
    "nf-seti-sbt" => SETI_SBT,
    "nf-custom-go" => CUSTOM_GO,
    "nf-seti-pipeline" => SETI_PIPELINE,
    "nf-seti-dart" => SETI_DART,
    "nf-seti-grunt" => SETI_GRUNT,
    "nf-seti-salesforce" => SETI_SALESFORCE,
    "nf-seti-illustrator" => SETI_ILLUSTRATOR,
    "nf-seti-go" => SETI_GO,
    "nf-seti-java" => SETI_JAVA,
    "nf-seti-pddl" => SETI_PDDL,
    "nf-seti-vala" => SETI_VALA,
    "nf-seti-bazel" => SETI_BAZEL,
    "nf-seti-python" => SETI_PYTHON,
    "nf-seti-deprecation-cop" => SETI_DEPRECATION_COP,
    "nf-seti-pug" => SETI_PUG,
    "nf-seti-webpack" => SETI_WEBPACK,
    "nf-seti-svelte" => SETI_SVELTE,
    "nf-seti-apple" => SETI_APPLE,
    "nf-seti-cpp" => SETI_CPP,
    "nf-seti-ocaml" => SETI_OCAML,
    "nf-seti-xls" => SETI_XLS,
    "nf-seti-mustache" => SETI_MUSTACHE,
    "nf-seti-zig" => SETI_ZIG,
    "nf-custom-electron" => CUSTOM_ELECTRON,
    "nf-custom-elixir" => CUSTOM_ELIXIR,
    "nf-seti-scala" => SETI_SCALA,
    "nf-custom-orgmode" => CUSTOM_ORGMODE,
    "nf-seti-c" => SETI_C,
    "nf-seti-notebook" => SETI_NOTEBOOK,
    "nf-seti-wasm" => SETI_WASM,
    "nf-seti-error" => SETI_ERROR,
    "nf-seti-shell" => SETI_SHELL,
    "nf-seti-haskell" => SETI_HASKELL,
    "nf-seti-lock" => SETI_LOCK,
    "nf-seti-mdo" => SETI_MDO,
    "nf-seti-prisma" => SETI_PRISMA,
    "nf-seti-powershell" => SETI_POWERSHELL,
    "nf-seti-platformio" => SETI_PLATFORMIO,
    "nf-seti-julia" => SETI_JULIA,
    "nf-seti-svg" => SETI_SVG,
};
