// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_pom.sh


/// Count of all Pomicons Icons (Not an icon itself)
pub const POM_ICON_COUNT: usize = 11;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_CLEAN_CODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_SHORT_PAUSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_INTERNAL_INTERRUPTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_POMODORO_TICKING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_EXTERNAL_INTERRUPTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_POMODORO_ESTIMATED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_LONG_PAUSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_AWAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_PAIR_PROGRAMMING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_POMODORO_DONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const POM_POMODORO_SQUASHED: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const POM_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-pom-clean-code" => POM_CLEAN_CODE,
    "nf-pom-short-pause" => POM_SHORT_PAUSE,
    "nf-pom-internal-interruption" => POM_INTERNAL_INTERRUPTION,
    "nf-pom-pomodoro-ticking" => POM_POMODORO_TICKING,
    "nf-pom-external-interruption" => POM_EXTERNAL_INTERRUPTION,
    "nf-pom-pomodoro-estimated" => POM_POMODORO_ESTIMATED,
    "nf-pom-long-pause" => POM_LONG_PAUSE,
    "nf-pom-away" => POM_AWAY,
    "nf-pom-pair-programming" => POM_PAIR_PROGRAMMING,
    "nf-pom-pomodoro-done" => POM_POMODORO_DONE,
    "nf-pom-pomodoro-squashed" => POM_POMODORO_SQUASHED,
};
