// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_fae.sh


/// Count of all Font Awesome Extension Icons (Not an icon itself)
pub const FAE_ICON_COUNT: usize = 170;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SMALLER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CROWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_REMIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SOFA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PLANET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_UMBRELLA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MICROSCOPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_POPSICLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_NC_JP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_CC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_THERMOMETER_HIGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PIZZA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PLAYSTATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SNOWING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_HALTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CUP_COFFE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_REFRIGERATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_QUORA_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BANANA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SUN_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_THERMOMETER_LOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TOOLS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_ID_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHESS_PAWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PULSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_DONUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_COINS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BLOGGER_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SHIRT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MYSQL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MILK_BOTTLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_FINGERPRINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MAXIMIZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_WALLET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHILLI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_LIPSTICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_HOTDOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_FEEDLY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_WIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MEAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_FOOTPRINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TRIANGLE_RULER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_DRESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MUSHROOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_POPCORN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_HEXAGON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_DNA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GLASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHICKEN_THIGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_NC_EU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BIGGER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_COCKROACH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_QUORA_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BACTERIA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SODA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TELEGRAM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BIOHAZARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_ND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_BY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_JAVA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_ICE_CREAM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SPIN_DOUBLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_VIRUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MAKEUP_BRUSHES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TOOTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PLANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_POISON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TELESCOPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TREE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_RAINING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SUSHI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_THIN_CLOSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_W3C: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GOOGLE_PLAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_XBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_ORANGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PEACH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_UTERUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_LOYALTY_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHESS_TOWER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_STOMACH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GRAV: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_FLOPPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_ENVELOPE_OPEN_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_FILE_IMPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_REAL_HEART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MUSTACHE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GALERY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHESS_KING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_NINTENDO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SOUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BOOK_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_SHARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_LOLLIPOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_RING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GUITAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BENZENE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BRAIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_NC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_APPLE_FRUIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PEAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MOLECULE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_THERMOMETER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_COFFE_BEANS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_EQUAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BLOGGER_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_ZERO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GALAXY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BOOK_OPEN_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_ATOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MINIMIZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_INJECTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_RUBY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_RUBY_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MOON_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHESS_QUEEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_DROP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHERRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHESS_BISHOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MOUNTAINS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_HAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_COMB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_RADIOACTIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_DICE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_ENVELOPE_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CAROT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_INFINITY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_COMET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_LIVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PALETTE_COLOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_DISCO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_EQUAL_BIGGER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TELEGRAM_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_HIGH_HEEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_LIPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BATH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SPERMATOZOON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_IMDB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GIFT_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BREAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TOILET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHESS_HORSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BONES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_MEDICINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_HAMBURGER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CICLING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_STORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_ELLO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CC_SA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_TACOS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_ISLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_WALKING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GOOGLE_DRIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_LUNG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_BUTTERFLY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_RULER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHEESE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_LAYERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_GPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_RESTORE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_FILE_EXPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_SLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_CHECKLIST_O: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_FREECODECAMP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const FAE_PYTHON: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const FAE_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-fae-smaller" => FAE_SMALLER,
    "nf-fae-crown" => FAE_CROWN,
    "nf-fae-cc-remix" => FAE_CC_REMIX,
    "nf-fae-sofa" => FAE_SOFA,
    "nf-fae-planet" => FAE_PLANET,
    "nf-fae-umbrella" => FAE_UMBRELLA,
    "nf-fae-microscope" => FAE_MICROSCOPE,
    "nf-fae-popsicle" => FAE_POPSICLE,
    "nf-fae-cc-nc-jp" => FAE_CC_NC_JP,
    "nf-fae-cc-cc" => FAE_CC_CC,
    "nf-fae-thermometer-high" => FAE_THERMOMETER_HIGH,
    "nf-fae-pizza" => FAE_PIZZA,
    "nf-fae-playstation" => FAE_PLAYSTATION,
    "nf-fae-snowing" => FAE_SNOWING,
    "nf-fae-halter" => FAE_HALTER,
    "nf-fae-cup-coffe" => FAE_CUP_COFFE,
    "nf-fae-refrigerator" => FAE_REFRIGERATOR,
    "nf-fae-quora-circle" => FAE_QUORA_CIRCLE,
    "nf-fae-banana" => FAE_BANANA,
    "nf-fae-sun-cloud" => FAE_SUN_CLOUD,
    "nf-fae-thermometer-low" => FAE_THERMOMETER_LOW,
    "nf-fae-tools" => FAE_TOOLS,
    "nf-fae-id-card" => FAE_ID_CARD,
    "nf-fae-chess-pawn" => FAE_CHESS_PAWN,
    "nf-fae-cloud" => FAE_CLOUD,
    "nf-fae-pulse" => FAE_PULSE,
    "nf-fae-donut" => FAE_DONUT,
    "nf-fae-coins" => FAE_COINS,
    "nf-fae-blogger-circle" => FAE_BLOGGER_CIRCLE,
    "nf-fae-shirt" => FAE_SHIRT,
    "nf-fae-mysql" => FAE_MYSQL,
    "nf-fae-milk-bottle" => FAE_MILK_BOTTLE,
    "nf-fae-fingerprint" => FAE_FINGERPRINT,
    "nf-fae-maximize" => FAE_MAXIMIZE,
    "nf-fae-wallet" => FAE_WALLET,
    "nf-fae-chilli" => FAE_CHILLI,
    "nf-fae-gut" => FAE_GUT,
    "nf-fae-lipstick" => FAE_LIPSTICK,
    "nf-fae-hotdog" => FAE_HOTDOG,
    "nf-fae-feedly" => FAE_FEEDLY,
    "nf-fae-wind" => FAE_WIND,
    "nf-fae-meat" => FAE_MEAT,
    "nf-fae-footprint" => FAE_FOOTPRINT,
    "nf-fae-triangle-ruler" => FAE_TRIANGLE_RULER,
    "nf-fae-dress" => FAE_DRESS,
    "nf-fae-mushroom" => FAE_MUSHROOM,
    "nf-fae-popcorn" => FAE_POPCORN,
    "nf-fae-hexagon" => FAE_HEXAGON,
    "nf-fae-dna" => FAE_DNA,
    "nf-fae-glass" => FAE_GLASS,
    "nf-fae-chicken-thigh" => FAE_CHICKEN_THIGH,
    "nf-fae-cc-nc-eu" => FAE_CC_NC_EU,
    "nf-fae-bigger" => FAE_BIGGER,
    "nf-fae-cockroach" => FAE_COCKROACH,
    "nf-fae-quora-square" => FAE_QUORA_SQUARE,
    "nf-fae-bacteria" => FAE_BACTERIA,
    "nf-fae-soda" => FAE_SODA,
    "nf-fae-telegram" => FAE_TELEGRAM,
    "nf-fae-biohazard" => FAE_BIOHAZARD,
    "nf-fae-cc-nd" => FAE_CC_ND,
    "nf-fae-cc-by" => FAE_CC_BY,
    "nf-fae-java" => FAE_JAVA,
    "nf-fae-ice-cream" => FAE_ICE_CREAM,
    "nf-fae-spin-double" => FAE_SPIN_DOUBLE,
    "nf-fae-virus" => FAE_VIRUS,
    "nf-fae-makeup-brushes" => FAE_MAKEUP_BRUSHES,
    "nf-fae-tooth" => FAE_TOOTH,
    "nf-fae-plant" => FAE_PLANT,
    "nf-fae-poison" => FAE_POISON,
    "nf-fae-telescope" => FAE_TELESCOPE,
    "nf-fae-tree" => FAE_TREE,
    "nf-fae-raining" => FAE_RAINING,
    "nf-fae-sushi" => FAE_SUSHI,
    "nf-fae-thin-close" => FAE_THIN_CLOSE,
    "nf-fae-w3c" => FAE_W3C,
    "nf-fae-google-play" => FAE_GOOGLE_PLAY,
    "nf-fae-chip" => FAE_CHIP,
    "nf-fae-xbox" => FAE_XBOX,
    "nf-fae-orange" => FAE_ORANGE,
    "nf-fae-peach" => FAE_PEACH,
    "nf-fae-uterus" => FAE_UTERUS,
    "nf-fae-loyalty-card" => FAE_LOYALTY_CARD,
    "nf-fae-chess-tower" => FAE_CHESS_TOWER,
    "nf-fae-stomach" => FAE_STOMACH,
    "nf-fae-grav" => FAE_GRAV,
    "nf-fae-floppy" => FAE_FLOPPY,
    "nf-fae-envelope-open-o" => FAE_ENVELOPE_OPEN_O,
    "nf-fae-file-import" => FAE_FILE_IMPORT,
    "nf-fae-real-heart" => FAE_REAL_HEART,
    "nf-fae-mustache" => FAE_MUSTACHE,
    "nf-fae-galery" => FAE_GALERY,
    "nf-fae-chess-king" => FAE_CHESS_KING,
    "nf-fae-nintendo" => FAE_NINTENDO,
    "nf-fae-soup" => FAE_SOUP,
    "nf-fae-book-open" => FAE_BOOK_OPEN,
    "nf-fae-cc-share" => FAE_CC_SHARE,
    "nf-fae-lollipop" => FAE_LOLLIPOP,
    "nf-fae-ring" => FAE_RING,
    "nf-fae-guitar" => FAE_GUITAR,
    "nf-fae-benzene" => FAE_BENZENE,
    "nf-fae-brain" => FAE_BRAIN,
    "nf-fae-cc-nc" => FAE_CC_NC,
    "nf-fae-apple-fruit" => FAE_APPLE_FRUIT,
    "nf-fae-pear" => FAE_PEAR,
    "nf-fae-molecule" => FAE_MOLECULE,
    "nf-fae-thermometer" => FAE_THERMOMETER,
    "nf-fae-coffe-beans" => FAE_COFFE_BEANS,
    "nf-fae-equal" => FAE_EQUAL,
    "nf-fae-pi" => FAE_PI,
    "nf-fae-blogger-square" => FAE_BLOGGER_SQUARE,
    "nf-fae-cc-zero" => FAE_CC_ZERO,
    "nf-fae-galaxy" => FAE_GALAXY,
    "nf-fae-book-open-o" => FAE_BOOK_OPEN_O,
    "nf-fae-atom" => FAE_ATOM,
    "nf-fae-minimize" => FAE_MINIMIZE,
    "nf-fae-injection" => FAE_INJECTION,
    "nf-fae-ruby" => FAE_RUBY,
    "nf-fae-ruby-o" => FAE_RUBY_O,
    "nf-fae-moon-cloud" => FAE_MOON_CLOUD,
    "nf-fae-chess-queen" => FAE_CHESS_QUEEN,
    "nf-fae-drop" => FAE_DROP,
    "nf-fae-cherry" => FAE_CHERRY,
    "nf-fae-chess-bishop" => FAE_CHESS_BISHOP,
    "nf-fae-mountains" => FAE_MOUNTAINS,
    "nf-fae-hat" => FAE_HAT,
    "nf-fae-comb" => FAE_COMB,
    "nf-fae-radioactive" => FAE_RADIOACTIVE,
    "nf-fae-dice" => FAE_DICE,
    "nf-fae-envelope-open" => FAE_ENVELOPE_OPEN,
    "nf-fae-carot" => FAE_CAROT,
    "nf-fae-infinity" => FAE_INFINITY,
    "nf-fae-comet" => FAE_COMET,
    "nf-fae-liver" => FAE_LIVER,
    "nf-fae-palette-color" => FAE_PALETTE_COLOR,
    "nf-fae-disco" => FAE_DISCO,
    "nf-fae-bed" => FAE_BED,
    "nf-fae-equal-bigger" => FAE_EQUAL_BIGGER,
    "nf-fae-telegram-circle" => FAE_TELEGRAM_CIRCLE,
    "nf-fae-high-heel" => FAE_HIGH_HEEL,
    "nf-fae-lips" => FAE_LIPS,
    "nf-fae-bath" => FAE_BATH,
    "nf-fae-spermatozoon" => FAE_SPERMATOZOON,
    "nf-fae-imdb" => FAE_IMDB,
    "nf-fae-gift-card" => FAE_GIFT_CARD,
    "nf-fae-bread" => FAE_BREAD,
    "nf-fae-toilet" => FAE_TOILET,
    "nf-fae-chess-horse" => FAE_CHESS_HORSE,
    "nf-fae-bones" => FAE_BONES,
    "nf-fae-medicine" => FAE_MEDICINE,
    "nf-fae-hamburger" => FAE_HAMBURGER,
    "nf-fae-cicling" => FAE_CICLING,
    "nf-fae-storm" => FAE_STORM,
    "nf-fae-ello" => FAE_ELLO,
    "nf-fae-cc-sa" => FAE_CC_SA,
    "nf-fae-tacos" => FAE_TACOS,
    "nf-fae-isle" => FAE_ISLE,
    "nf-fae-walking" => FAE_WALKING,
    "nf-fae-google-drive" => FAE_GOOGLE_DRIVE,
    "nf-fae-lung" => FAE_LUNG,
    "nf-fae-butterfly" => FAE_BUTTERFLY,
    "nf-fae-ruler" => FAE_RULER,
    "nf-fae-cheese" => FAE_CHEESE,
    "nf-fae-layers" => FAE_LAYERS,
    "nf-fae-gps" => FAE_GPS,
    "nf-fae-restore" => FAE_RESTORE,
    "nf-fae-file-export" => FAE_FILE_EXPORT,
    "nf-fae-slash" => FAE_SLASH,
    "nf-fae-checklist-o" => FAE_CHECKLIST_O,
    "nf-fae-freecodecamp" => FAE_FREECODECAMP,
    "nf-fae-python" => FAE_PYTHON,
};
