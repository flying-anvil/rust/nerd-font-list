// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_dev.sh


/// Count of all Devicons Icons (Not an icon itself)
pub const DEV_ICON_COUNT: usize = 191;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ENVATO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_RESPONSIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CODE_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_NPM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_HTML5_CONNECTIVITY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_PERL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CSS3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_COFFEESCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_POSTGRESQL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CODA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_EMBER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_RUST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_UBUNTU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_APPCELERATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JEKYLL_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_PHOTOSHOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_HTML5_MULTIMEDIA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_RACKSPACE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_STREAMLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_STYLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ONEDRIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ATLASSIAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GROOVY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_NETMAGAZINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CSSDECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GRUNT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_EXTJS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_REQUIREJS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_REDIS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_KOMODO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CLOUD9: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MYSQL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_KRAKENJS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_HTML5_DEVICE_ACCESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GIT_COMPARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_RUBY_ON_RAILS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_FIREBASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_TERMINAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SCRUM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_METEORFULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_NODEJS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_AWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ILLUSTRATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_OPENSHIFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CHROME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_APTANA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_APPSTORE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_W3C: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_HASKELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_RASBERRY_PI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JOOMLA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ASTERISK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DLANG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DOJO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_YAHOO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SQLLITE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DROPBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GITHUB_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_STACKOVERFLOW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_LINUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CISCO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DOCKER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_INTELLIJ: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SAFARI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_RUBY_ROUGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BINTRAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CSS_TRICKS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SCRIPTCS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DOCTRINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SENCHATOUCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DEBIAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_FSHARP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JQUERY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JIRA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_TECHCRUNCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_PYTHON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_COMPOSER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_PHONEGAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BRACKETS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SUBLIME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_REACT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MSQL_SERVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_METEOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_HTML5_3D_EFFECTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GIT_PULL_REQUEST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ECLIPSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GHOST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_UNITY_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DREAMWEAVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DATABASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CLOJURE_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DIGITAL_OCEAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MARKDOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GULP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_VIM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CHART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JAVASCRIPT_SHIELD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_HEROKU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_UIKIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_HTML5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BACKBONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_HACKERNEWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CSS3_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SWIFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JAVASCRIPT_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_KRAKENJS_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MOOTOOLS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CREATIVECOMMONS_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BOOTSTRAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SCALA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_NANCY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CELLULOID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_TYPO3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GIT_BRANCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_WEBPLATFORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JQUERY_UI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_LESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GITHUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GOOGLE_DRIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MAGENTO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ANDROID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_RUBY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MODERNIZR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_YEOMAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CODEPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GHOST_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_FIREFOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CREATIVECOMMONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_TERMINAL_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GOOGLE_CLOUD_PLATFORM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MATERIALIZECSS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_PROLOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_WINDOWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_RAPHAEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JENKINS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CODEIGNITER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GITHUB_FULL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MONGODB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JAVA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_NODEJS_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_NGINX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_OPERA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MOZILLA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DRUPAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_TRELLO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SYMFONY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SYMFONY_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CLOJURE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_IONIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ERLANG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DOTNET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_REDHAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_LARAVEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_DJANGO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SNAP_SVG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_PHP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ATOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_TRAVIS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_YII: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_COMPASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BITBUCKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BING_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BLACKBERRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_IE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SIZZLEJS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GNU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BUGSENSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MAILCHIMP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_JAVASCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_OPENSOURCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_WORDPRESS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ZEND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_APPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GRAILS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_SMASHING_MAGAZINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MITLICENCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_BOWER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_YAHOO_SMALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_MOOTOOLS_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_NETBEANS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GITHUB_BADGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_CODROPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GIT_COMMIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_ANGULAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_GIT_MERGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const DEV_VISUALSTUDIO: &'static str = "";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const DEV_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-dev-envato" => DEV_ENVATO,
    "nf-dev-responsive" => DEV_RESPONSIVE,
    "nf-dev-code-badge" => DEV_CODE_BADGE,
    "nf-dev-npm" => DEV_NPM,
    "nf-dev-html5-connectivity" => DEV_HTML5_CONNECTIVITY,
    "nf-dev-perl" => DEV_PERL,
    "nf-dev-css3" => DEV_CSS3,
    "nf-dev-coffeescript" => DEV_COFFEESCRIPT,
    "nf-dev-postgresql" => DEV_POSTGRESQL,
    "nf-dev-coda" => DEV_CODA,
    "nf-dev-ember" => DEV_EMBER,
    "nf-dev-code" => DEV_CODE,
    "nf-dev-rust" => DEV_RUST,
    "nf-dev-ubuntu" => DEV_UBUNTU,
    "nf-dev-appcelerator" => DEV_APPCELERATOR,
    "nf-dev-jekyll-small" => DEV_JEKYLL_SMALL,
    "nf-dev-photoshop" => DEV_PHOTOSHOP,
    "nf-dev-html5-multimedia" => DEV_HTML5_MULTIMEDIA,
    "nf-dev-rackspace" => DEV_RACKSPACE,
    "nf-dev-streamline" => DEV_STREAMLINE,
    "nf-dev-stylus" => DEV_STYLUS,
    "nf-dev-onedrive" => DEV_ONEDRIVE,
    "nf-dev-atlassian" => DEV_ATLASSIAN,
    "nf-dev-groovy" => DEV_GROOVY,
    "nf-dev-netmagazine" => DEV_NETMAGAZINE,
    "nf-dev-cssdeck" => DEV_CSSDECK,
    "nf-dev-grunt" => DEV_GRUNT,
    "nf-dev-extjs" => DEV_EXTJS,
    "nf-dev-requirejs" => DEV_REQUIREJS,
    "nf-dev-redis" => DEV_REDIS,
    "nf-dev-komodo" => DEV_KOMODO,
    "nf-dev-cloud9" => DEV_CLOUD9,
    "nf-dev-mysql" => DEV_MYSQL,
    "nf-dev-krakenjs" => DEV_KRAKENJS,
    "nf-dev-html5-device-access" => DEV_HTML5_DEVICE_ACCESS,
    "nf-dev-git-compare" => DEV_GIT_COMPARE,
    "nf-dev-ruby-on-rails" => DEV_RUBY_ON_RAILS,
    "nf-dev-firebase" => DEV_FIREBASE,
    "nf-dev-terminal" => DEV_TERMINAL,
    "nf-dev-scrum" => DEV_SCRUM,
    "nf-dev-meteorfull" => DEV_METEORFULL,
    "nf-dev-nodejs" => DEV_NODEJS,
    "nf-dev-aws" => DEV_AWS,
    "nf-dev-illustrator" => DEV_ILLUSTRATOR,
    "nf-dev-openshift" => DEV_OPENSHIFT,
    "nf-dev-chrome" => DEV_CHROME,
    "nf-dev-aptana" => DEV_APTANA,
    "nf-dev-appstore" => DEV_APPSTORE,
    "nf-dev-w3c" => DEV_W3C,
    "nf-dev-haskell" => DEV_HASKELL,
    "nf-dev-rasberry-pi" => DEV_RASBERRY_PI,
    "nf-dev-joomla" => DEV_JOOMLA,
    "nf-dev-asterisk" => DEV_ASTERISK,
    "nf-dev-dlang" => DEV_DLANG,
    "nf-dev-dojo" => DEV_DOJO,
    "nf-dev-yahoo" => DEV_YAHOO,
    "nf-dev-sqllite" => DEV_SQLLITE,
    "nf-dev-dropbox" => DEV_DROPBOX,
    "nf-dev-github-alt" => DEV_GITHUB_ALT,
    "nf-dev-stackoverflow" => DEV_STACKOVERFLOW,
    "nf-dev-linux" => DEV_LINUX,
    "nf-dev-cisco" => DEV_CISCO,
    "nf-dev-docker" => DEV_DOCKER,
    "nf-dev-intellij" => DEV_INTELLIJ,
    "nf-dev-safari" => DEV_SAFARI,
    "nf-dev-ruby-rough" => DEV_RUBY_ROUGH,
    "nf-dev-bintray" => DEV_BINTRAY,
    "nf-dev-css-tricks" => DEV_CSS_TRICKS,
    "nf-dev-scriptcs" => DEV_SCRIPTCS,
    "nf-dev-doctrine" => DEV_DOCTRINE,
    "nf-dev-senchatouch" => DEV_SENCHATOUCH,
    "nf-dev-debian" => DEV_DEBIAN,
    "nf-dev-fsharp" => DEV_FSHARP,
    "nf-dev-jquery" => DEV_JQUERY,
    "nf-dev-jira" => DEV_JIRA,
    "nf-dev-techcrunch" => DEV_TECHCRUNCH,
    "nf-dev-python" => DEV_PYTHON,
    "nf-dev-go" => DEV_GO,
    "nf-dev-composer" => DEV_COMPOSER,
    "nf-dev-phonegap" => DEV_PHONEGAP,
    "nf-dev-brackets" => DEV_BRACKETS,
    "nf-dev-sublime" => DEV_SUBLIME,
    "nf-dev-react" => DEV_REACT,
    "nf-dev-msql-server" => DEV_MSQL_SERVER,
    "nf-dev-meteor" => DEV_METEOR,
    "nf-dev-html5-3d-effects" => DEV_HTML5_3D_EFFECTS,
    "nf-dev-git-pull-request" => DEV_GIT_PULL_REQUEST,
    "nf-dev-eclipse" => DEV_ECLIPSE,
    "nf-dev-ghost" => DEV_GHOST,
    "nf-dev-unity-small" => DEV_UNITY_SMALL,
    "nf-dev-dreamweaver" => DEV_DREAMWEAVER,
    "nf-dev-database" => DEV_DATABASE,
    "nf-dev-clojure-alt" => DEV_CLOJURE_ALT,
    "nf-dev-digital-ocean" => DEV_DIGITAL_OCEAN,
    "nf-dev-dart" => DEV_DART,
    "nf-dev-markdown" => DEV_MARKDOWN,
    "nf-dev-gulp" => DEV_GULP,
    "nf-dev-vim" => DEV_VIM,
    "nf-dev-chart" => DEV_CHART,
    "nf-dev-javascript-shield" => DEV_JAVASCRIPT_SHIELD,
    "nf-dev-heroku" => DEV_HEROKU,
    "nf-dev-uikit" => DEV_UIKIT,
    "nf-dev-html5" => DEV_HTML5,
    "nf-dev-backbone" => DEV_BACKBONE,
    "nf-dev-hackernews" => DEV_HACKERNEWS,
    "nf-dev-css3-full" => DEV_CSS3_FULL,
    "nf-dev-swift" => DEV_SWIFT,
    "nf-dev-javascript-badge" => DEV_JAVASCRIPT_BADGE,
    "nf-dev-krakenjs-badge" => DEV_KRAKENJS_BADGE,
    "nf-dev-mootools" => DEV_MOOTOOLS,
    "nf-dev-creativecommons-badge" => DEV_CREATIVECOMMONS_BADGE,
    "nf-dev-bootstrap" => DEV_BOOTSTRAP,
    "nf-dev-scala" => DEV_SCALA,
    "nf-dev-nancy" => DEV_NANCY,
    "nf-dev-celluloid" => DEV_CELLULOID,
    "nf-dev-typo3" => DEV_TYPO3,
    "nf-dev-git-branch" => DEV_GIT_BRANCH,
    "nf-dev-webplatform" => DEV_WEBPLATFORM,
    "nf-dev-jquery-ui" => DEV_JQUERY_UI,
    "nf-dev-less" => DEV_LESS,
    "nf-dev-github" => DEV_GITHUB,
    "nf-dev-google-drive" => DEV_GOOGLE_DRIVE,
    "nf-dev-magento" => DEV_MAGENTO,
    "nf-dev-android" => DEV_ANDROID,
    "nf-dev-ruby" => DEV_RUBY,
    "nf-dev-modernizr" => DEV_MODERNIZR,
    "nf-dev-yeoman" => DEV_YEOMAN,
    "nf-dev-codepen" => DEV_CODEPEN,
    "nf-dev-ghost-small" => DEV_GHOST_SMALL,
    "nf-dev-firefox" => DEV_FIREFOX,
    "nf-dev-creativecommons" => DEV_CREATIVECOMMONS,
    "nf-dev-terminal-badge" => DEV_TERMINAL_BADGE,
    "nf-dev-google-cloud-platform" => DEV_GOOGLE_CLOUD_PLATFORM,
    "nf-dev-materializecss" => DEV_MATERIALIZECSS,
    "nf-dev-prolog" => DEV_PROLOG,
    "nf-dev-windows" => DEV_WINDOWS,
    "nf-dev-raphael" => DEV_RAPHAEL,
    "nf-dev-jenkins" => DEV_JENKINS,
    "nf-dev-codeigniter" => DEV_CODEIGNITER,
    "nf-dev-github-full" => DEV_GITHUB_FULL,
    "nf-dev-mongodb" => DEV_MONGODB,
    "nf-dev-java" => DEV_JAVA,
    "nf-dev-nodejs-small" => DEV_NODEJS_SMALL,
    "nf-dev-nginx" => DEV_NGINX,
    "nf-dev-opera" => DEV_OPERA,
    "nf-dev-mozilla" => DEV_MOZILLA,
    "nf-dev-drupal" => DEV_DRUPAL,
    "nf-dev-trello" => DEV_TRELLO,
    "nf-dev-symfony" => DEV_SYMFONY,
    "nf-dev-symfony-badge" => DEV_SYMFONY_BADGE,
    "nf-dev-clojure" => DEV_CLOJURE,
    "nf-dev-ionic" => DEV_IONIC,
    "nf-dev-erlang" => DEV_ERLANG,
    "nf-dev-dotnet" => DEV_DOTNET,
    "nf-dev-redhat" => DEV_REDHAT,
    "nf-dev-laravel" => DEV_LARAVEL,
    "nf-dev-django" => DEV_DJANGO,
    "nf-dev-snap-svg" => DEV_SNAP_SVG,
    "nf-dev-php" => DEV_PHP,
    "nf-dev-atom" => DEV_ATOM,
    "nf-dev-travis" => DEV_TRAVIS,
    "nf-dev-yii" => DEV_YII,
    "nf-dev-compass" => DEV_COMPASS,
    "nf-dev-bitbucket" => DEV_BITBUCKET,
    "nf-dev-bing-small" => DEV_BING_SMALL,
    "nf-dev-blackberry" => DEV_BLACKBERRY,
    "nf-dev-ie" => DEV_IE,
    "nf-dev-sizzlejs" => DEV_SIZZLEJS,
    "nf-dev-gnu" => DEV_GNU,
    "nf-dev-bugsense" => DEV_BUGSENSE,
    "nf-dev-mailchimp" => DEV_MAILCHIMP,
    "nf-dev-javascript" => DEV_JAVASCRIPT,
    "nf-dev-opensource" => DEV_OPENSOURCE,
    "nf-dev-wordpress" => DEV_WORDPRESS,
    "nf-dev-zend" => DEV_ZEND,
    "nf-dev-git" => DEV_GIT,
    "nf-dev-apple" => DEV_APPLE,
    "nf-dev-grails" => DEV_GRAILS,
    "nf-dev-sass" => DEV_SASS,
    "nf-dev-smashing-magazine" => DEV_SMASHING_MAGAZINE,
    "nf-dev-mitlicence" => DEV_MITLICENCE,
    "nf-dev-bower" => DEV_BOWER,
    "nf-dev-yahoo-small" => DEV_YAHOO_SMALL,
    "nf-dev-mootools-badge" => DEV_MOOTOOLS_BADGE,
    "nf-dev-netbeans" => DEV_NETBEANS,
    "nf-dev-github-badge" => DEV_GITHUB_BADGE,
    "nf-dev-codrops" => DEV_CODROPS,
    "nf-dev-git-commit" => DEV_GIT_COMMIT,
    "nf-dev-angular" => DEV_ANGULAR,
    "nf-dev-git-merge" => DEV_GIT_MERGE,
    "nf-dev-visualstudio" => DEV_VISUALSTUDIO,
};
