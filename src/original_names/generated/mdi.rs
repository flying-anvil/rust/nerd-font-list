// https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_mdi.sh


/// Count of all Material Design Icons (legacy) Icons (Not an icon itself)
pub const MDI_ICON_COUNT: usize = 2119;

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOKMARK_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_FLOAT_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">賈</span>
pub const MDI_PIN_OFF: &'static str = "賈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">穀</span>
pub const MDI_VECTOR_ARRANGE_BELOW: &'static str = "穀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURRENCY_USD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_PROCESSING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_TEXT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CUBE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰚ</span>
pub const MDI_SOURCE_COMMIT_NEXT_LOCAL: &'static str = "ﰚ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEART_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">侮</span>
pub const MDI_TREE: &'static str = "侮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴨ</span>
pub const MDI_ICE_CREAM: &'static str = "ﴨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭥ</span>
pub const MDI_STOP_CIRCLE: &'static str = "ﭥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COUNTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">臨</span>
pub const MDI_TABLET_ANDROID: &'static str = "臨";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬿</span>
pub const MDI_COLLAGE: &'static str = "﬿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GAMEPAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTENT_SAVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_BROKEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">留</span>
pub const MDI_STAR: &'static str = "留";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮀ</span>
pub const MDI_TOWER_FIRE: &'static str = "ﮀ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUETOOTH_TRANSFER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭜ</span>
pub const MDI_SHAPE_CIRCLE_PLUS: &'static str = "ﭜ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALPHA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_CHART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MICROPHONE_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">驪</span>
pub const MDI_SELECT_OFF: &'static str = "驪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_PDF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭳ</span>
pub const MDI_BANDCAMP: &'static str = "ﭳ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳎ</span>
pub const MDI_HOME_ASSISTANT: &'static str = "ﳎ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DECIMAL_DECREASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">諾</span>
pub const MDI_RESPONSIVE: &'static str = "諾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲡ</span>
pub const MDI_CAMERA_METERING_MATRIX: &'static str = "ﲡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﭂</span>
pub const MDI_FACE: &'static str = "﭂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">蓮</span>
pub const MDI_SHOPPING: &'static str = "蓮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_BRACKETS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHART_PIE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FINGERPRINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KODI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NOTE_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">璉</span>
pub const MDI_SHAPE_PLUS: &'static str = "璉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">念</span>
pub const MDI_SILVERWARE_FORK: &'static str = "念";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">艹</span>
pub const MDI_VECTOR_POINT: &'static str = "艹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">לּ</span>
pub const MDI_CHECKBOX_MULTIPLE_MARKED_CIRCLE: &'static str = "לּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮮ</span>
pub const MDI_UPDATE: &'static str = "ﮮ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_POWERPOINT_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_QUOTE_CLOSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DROPBOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">旅</span>
pub const MDI_SECURITY_NETWORK: &'static str = "旅";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_APPLE_MOBILEME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_MULTIPLE_MARKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">陋</span>
pub const MDI_RELATIVE_SCALE: &'static str = "陋";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOWLING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴹ</span>
pub const MDI_TELEVISION_OFF: &'static str = "ﴹ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">אָ</span>
pub const MDI_ACCOUNT_SETTINGS: &'static str = "אָ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAYERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫣</span>
pub const MDI_CREDIT_CARD_OFF: &'static str = "﫣";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_STRIKETHROUGH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">陼</span>
pub const MDI_PLAYLIST_CHECK: &'static str = "陼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰙ</span>
pub const MDI_SOURCE_COMMIT_LOCAL: &'static str = "ﰙ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">愈</span>
pub const MDI_WATCH: &'static str = "愈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">滛</span>
pub const MDI_WEATHER_SUNNY: &'static str = "滛";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">螺</span>
pub const MDI_PLAYLIST_PLUS: &'static str = "螺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">落</span>
pub const MDI_PLUS_CIRCLE_OUTLINE: &'static str = "落";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_CLEAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮧ</span>
pub const MDI_ROBOT: &'static str = "ﮧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮱ</span>
pub const MDI_DOLBY: &'static str = "ﮱ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">爛</span>
pub const MDI_POLL: &'static str = "爛";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_STRING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">吏</span>
pub const MDI_SUBWAY_VARIANT: &'static str = "吏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AMBULANCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_PARTY_MODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮃ</span>
pub const MDI_HAMBURGER: &'static str = "ﮃ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHART_HISTOGRAM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUR_LINEAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱨ</span>
pub const MDI_CHART_BAR_STACKED: &'static str = "ﱨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_NOTE_QUARTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯹ</span>
pub const MDI_PAGE_LAYOUT_FOOTER: &'static str = "ﯹ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_UP_DROP_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMCORDER_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">梁</span>
pub const MDI_SEAT_FLAT: &'static str = "梁";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">醴</span>
pub const MDI_SOCCER: &'static str = "醴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲗ</span>
pub const MDI_ARROW_EXPAND_UP: &'static str = "ﲗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FAST_FORWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_70: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">精</span>
pub const MDI_TIMER_OFF: &'static str = "精";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯿ</span>
pub const MDI_PENTAGON_OUTLINE: &'static str = "ﯿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱞ</span>
pub const MDI_UNFOLD_LESS_VERTICAL: &'static str = "ﱞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫮</span>
pub const MDI_EMAIL_OPEN_OUTLINE: &'static str = "﫮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LED_VARIANT_ON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰸ</span>
pub const MDI_CANCEL: &'static str = "ﰸ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">溺</span>
pub const MDI_TABLE_COLUMN_PLUS_BEFORE: &'static str = "溺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_VIDEO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">虜</span>
pub const MDI_RADAR: &'static str = "虜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_WRAP_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HISTORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯕ</span>
pub const MDI_FORMAT_PAGE_BREAK: &'static str = "ﯕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">不</span>
pub const MDI_ROTATE_RIGHT_VARIANT: &'static str = "不";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰅ</span>
pub const MDI_PRINTER_SETTINGS: &'static str = "ﰅ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PENCIL_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">連</span>
pub const MDI_SHOPPING_MUSIC: &'static str = "連";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HUMAN_CHILD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_COLOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭟ</span>
pub const MDI_SHAPE_SQUARE_PLUS: &'static str = "ﭟ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">洛</span>
pub const MDI_PLUS_BOX: &'static str = "洛";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰛ</span>
pub const MDI_SOURCE_COMMIT_START: &'static str = "ﰛ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_50: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LABEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">略</span>
pub const MDI_SCREWDRIVER: &'static str = "略";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_NONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">省</span>
pub const MDI_RUN_FAST: &'static str = "省";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_7_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_IMAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">וּ</span>
pub const MDI_APPLE_KEYBOARD_SHIFT: &'static str = "וּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">蘭</span>
pub const MDI_POLL_BOX: &'static str = "蘭";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_LOG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">碑</span>
pub const MDI_UNDO: &'static str = "碑";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲸ</span>
pub const MDI_CURRENCY_CNY: &'static str = "ﲸ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_LOCK_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">煮</span>
pub const MDI_UMBRACO: &'static str = "煮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTRAST_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">倫</span>
pub const MDI_STEP_BACKWARD: &'static str = "倫";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMCORDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">郎</span>
pub const MDI_PROFESSIONAL_HEXAGON: &'static str = "郎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DELETE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﵆</span>
pub const MDI_YOUTUBE_GAMING: &'static str = "﵆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURRENCY_RUB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯷ</span>
pub const MDI_OCTAGRAM: &'static str = "ﯷ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LIGHTBULB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鹿</span>
pub const MDI_RAY_END_ARROW: &'static str = "鹿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">諸</span>
pub const MDI_XBOX_CONTROLLER_OFF: &'static str = "諸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_LOCKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">裏</span>
pub const MDI_SYNC_OFF: &'static str = "裏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰲ</span>
pub const MDI_ARROW_RIGHT_BOLD: &'static str = "ﰲ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PAPER_CUT_VERTICAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FISH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MONITOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GENDER_TRANSGENDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯸ</span>
pub const MDI_PAGE_LAYOUT_BODY: &'static str = "ﯸ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱵ</span>
pub const MDI_SELECTION_OFF: &'static str = "ﱵ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSE_OCTAGON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲎ</span>
pub const MDI_ANDROID_HEAD: &'static str = "ﲎ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DEBUG_STEP_OVER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲏ</span>
pub const MDI_APPROVAL: &'static str = "ﲏ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴓ</span>
pub const MDI_CELLPHONE_WIRELESS: &'static str = "ﴓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_CONVERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">蘒</span>
pub const MDI_TOGGLE_SWITCH: &'static str = "蘒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬘</span>
pub const MDI_BUFFER: &'static str = "﬘";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">礼</span>
pub const MDI_TIE: &'static str = "礼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DISQUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAN_CONNECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEVRON_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_4_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲻ</span>
pub const MDI_CURRENCY_KRW: &'static str = "ﲻ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮞ</span>
pub const MDI_GRADIENT: &'static str = "ﮞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOLOLENS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLACK_MESA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPBOARD_ARROW_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">瘟</span>
pub const MDI_WHITE_BALANCE_INCANDESCENT: &'static str = "瘟";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">尿</span>
pub const MDI_SORT_NUMERIC: &'static str = "尿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRIBBBLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰘ</span>
pub const MDI_SOURCE_COMMIT_END_LOCAL: &'static str = "ﰘ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱰ</span>
pub const MDI_LOADING: &'static str = "ﱰ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰨ</span>
pub const MDI_WASHING_MACHINE: &'static str = "ﰨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">節</span>
pub const MDI_VECTOR_CIRCLE_VARIANT: &'static str = "節";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫜</span>
pub const MDI_BULLSEYE: &'static str = "﫜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_LESS_THAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_DRAW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPBOARD_ALERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAPTOP_MAC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴵ</span>
pub const MDI_TABLE_ROW: &'static str = "ﴵ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIGHTNESS_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FRIDGE_FILLED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">懶</span>
pub const MDI_PLAY_PAUSE: &'static str = "懶";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LABEL_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEVRON_DOUBLE_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PI_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">囹</span>
pub const MDI_SITEMAP: &'static str = "囹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_MULTIPLE_IMAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">羽</span>
pub const MDI_TIMER_SAND: &'static str = "羽";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHART_LINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲑ</span>
pub const MDI_ARROW_COLLAPSE_LEFT: &'static str = "ﲑ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴱ</span>
pub const MDI_SHIP_WHEEL: &'static str = "ﴱ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴊ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_50: &'static str = "ﴊ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HANGOUTS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COFFEE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">流</span>
pub const MDI_SQUARE_INC_CASH: &'static str = "流";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">器</span>
pub const MDI_TROPHY_AWARD: &'static str = "器";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DICE_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_FLOAT_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">瀞</span>
pub const MDI_WEATHER_SUNSET_UP: &'static str = "瀞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">嬨</span>
pub const MDI_VPN: &'static str = "嬨";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DUCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮉ</span>
pub const MDI_TAG_HEART: &'static str = "ﮉ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鉶</span>
pub const MDI_VECTOR_RECTANGLE: &'static str = "鉶";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳄ</span>
pub const MDI_ELEPHANT: &'static str = "ﳄ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">謹</span>
pub const MDI_XML: &'static str = "謹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮵</span>
pub const MDI_MENU_UP_OUTLINE: &'static str = "﮵";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MINUS_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEADSET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_20: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ENGINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMCORDER_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲝ</span>
pub const MDI_BUS_SCHOOL: &'static str = "ﲝ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ךּ</span>
pub const MDI_CHECKBOX_MULTIPLE_BLANK_CIRCLE: &'static str = "ךּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSE_OCTAGON_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_INBOX_ARROW_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_REMOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_FLOAT_NONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUETOOTH_AUDIO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILMSTRIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭮ</span>
pub const MDI_DISCORD: &'static str = "ﭮ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮍ</span>
pub const MDI_BABY_BUGGY: &'static str = "ﮍ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HANGER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BELL_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HUMAN_MALE_FEMALE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MICROSOFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">漣</span>
pub const MDI_SETTINGS: &'static str = "漣";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">栗</span>
pub const MDI_STOP: &'static str = "栗";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_9_PLUS_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬞ</span>
pub const MDI_FORMAT_HORIZONTAL_ALIGN_LEFT: &'static str = "ﬞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONSOLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">戮</span>
pub const MDI_STEAM: &'static str = "戮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">著</span>
pub const MDI_VECTOR_POLYGON: &'static str = "著";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱀ</span>
pub const MDI_GESTURE_TWO_DOUBLE_TAP: &'static str = "ﱀ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_CENTER_FOCUS_WEAK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰝ</span>
pub const MDI_SPEAKER_WIRELESS: &'static str = "ﰝ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MICROPHONE_SETTINGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳂ</span>
pub const MDI_DOTS_VERTICAL_CIRCLE: &'static str = "ﳂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AIRPLAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯖ</span>
pub const MDI_FORMAT_PILCROW: &'static str = "ﯖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AMAZON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">什</span>
pub const MDI_TARGET: &'static str = "什";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DISQUS_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">禍</span>
pub const MDI_USB: &'static str = "禍";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NATURE_PEOPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱾ</span>
pub const MDI_SHIELD_HALF_FULL: &'static str = "ﱾ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﩯</span>
pub const MDI_VIEW_GRID: &'static str = "﩯";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">漢</span>
pub const MDI_UBUNTU: &'static str = "漢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_OUTGOING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">者</span>
pub const MDI_VECTOR_DIFFERENCE_BA: &'static str = "者";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_DOWN_THICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">麗</span>
pub const MDI_SELECTION: &'static str = "麗";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">שּׁ</span>
pub const MDI_SOURCE_MERGE: &'static str = "שּׁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲖ</span>
pub const MDI_ARROW_EXPAND_RIGHT: &'static str = "ﲖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭫ</span>
pub const MDI_CHART_GANTT: &'static str = "ﭫ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_EXPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲁ</span>
pub const MDI_SIGNAL_OFF: &'static str = "ﲁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰭ</span>
pub const MDI_ARROW_DOWN_BOLD_BOX: &'static str = "ﰭ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_MOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">類</span>
pub const MDI_STAR_OFF: &'static str = "類";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">謹</span>
pub const MDI_VECTOR_UNION: &'static str = "謹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰁ</span>
pub const MDI_PISTOL: &'static str = "ﰁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHAIR_SCHOOL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">憎</span>
pub const MDI_TSHIRT_V: &'static str = "憎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">喙</span>
pub const MDI_VK_CIRCLE: &'static str = "喙";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">惘</span>
pub const MDI_WALLET_TRAVEL: &'static str = "惘";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">אַ</span>
pub const MDI_WEBHOOK: &'static str = "אַ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLOWER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_APPLE_SAFARI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭧ</span>
pub const MDI_TEST_TUBE: &'static str = "ﭧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEXAGON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">掠</span>
pub const MDI_SCREEN_ROTATION_LOCK: &'static str = "掠";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FIRE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">婢</span>
pub const MDI_VOLUME_OFF: &'static str = "婢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">逸</span>
pub const MDI_VIDEO_OFF: &'static str = "逸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲊ</span>
pub const MDI_TIMER_SAND_FULL: &'static str = "ﲊ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭓ</span>
pub const MDI_MICROSCOPE: &'static str = "ﭓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">紐</span>
pub const MDI_STAR_HALF: &'static str = "紐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MICROPHONE_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">累</span>
pub const MDI_REFRESH: &'static str = "累";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">礪</span>
pub const MDI_SELECT_ALL: &'static str = "礪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭺ</span>
pub const MDI_OAR: &'static str = "ﭺ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PANORAMA_VERTICAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LOOKS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NEW_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILTER_REMOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAKE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_REAR_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">𢡊</span>
pub const MDI_STICKER: &'static str = "𢡊";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭛ</span>
pub const MDI_SERIAL_PORT: &'static str = "ﭛ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_9_PLUS_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">蠟</span>
pub const MDI_PRESENTATION: &'static str = "蠟";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳉ</span>
pub const MDI_GESTURE: &'static str = "ﳉ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯇</span>
pub const MDI_CLOSE_OUTLINE: &'static str = "﯇";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLOCK_HELPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴽ</span>
pub const MDI_VIDEO_INPUT_ANTENNA: &'static str = "ﴽ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">羅</span>
pub const MDI_PLAYLIST_MINUS: &'static str = "羅";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LIBRARY_MUSIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">呂</span>
pub const MDI_SEAT_RECLINE_EXTRA: &'static str = "呂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">פֿ</span>
pub const MDI_LEAD_PENCIL: &'static str = "פֿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">蓼</span>
pub const MDI_SPEAKER: &'static str = "蓼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_6_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CASH_100: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKERBOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳥ</span>
pub const MDI_POWER_SOCKET_EU: &'static str = "ﳥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEADSET_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲙ</span>
pub const MDI_BOOK_UNSECURE: &'static str = "ﲙ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CASH_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">輦</span>
pub const MDI_SHIELD_OUTLINE: &'static str = "輦";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭙ</span>
pub const MDI_POT: &'static str = "ﭙ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫼</span>
pub const MDI_LOGOUT_VARIANT: &'static str = "﫼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EVERNOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯘ</span>
pub const MDI_GARAGE_OPEN: &'static str = "ﯘ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">海</span>
pub const MDI_TWITTER_CIRCLE: &'static str = "海";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_UNDERLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭢ</span>
pub const MDI_SKIP_PREVIOUS_CIRCLE: &'static str = "ﭢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">視</span>
pub const MDI_XBOX: &'static str = "視";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MATERIAL_UI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_CHROME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">喇</span>
pub const MDI_PLAY_CIRCLE: &'static str = "喇";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">兀</span>
pub const MDI_THEATER: &'static str = "兀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮠ</span>
pub const MDI_MESSAGE_BULLETED: &'static str = "ﮠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BABY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASH_AUTO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DESKTOP_MAC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CASE_SENSITIVE_ALT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRAWING_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">理</span>
pub const MDI_SWORD: &'static str = "理";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">艹</span>
pub const MDI_VECTOR_LINE: &'static str = "艹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DUMBBELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">烈</span>
pub const MDI_SIGMA: &'static str = "烈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">句</span>
pub const MDI_PINTEREST: &'static str = "句";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴑ</span>
pub const MDI_BITCOIN: &'static str = "ﴑ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳭ</span>
pub const MDI_STANDARD_DEFINITION: &'static str = "ﳭ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEADSET_DOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">勉</span>
pub const MDI_TRENDING_NEUTRAL: &'static str = "勉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮸</span>
pub const MDI_PLEX: &'static str = "﮸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FULLSCREEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">索</span>
pub const MDI_RSS: &'static str = "索";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DESKTOP_TOWER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">奈</span>
pub const MDI_PLAY_CIRCLE_OUTLINE: &'static str = "奈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴯ</span>
pub const MDI_SHAPE: &'static str = "ﴯ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRUSH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨟</span>
pub const MDI_TIMETABLE: &'static str = "﨟";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_ALERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">率</span>
pub const MDI_ROAD_VARIANT: &'static str = "率";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">練</span>
pub const MDI_SHARE_VARIANT: &'static str = "練";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">見</span>
pub const MDI_TEXT_TO_SPEECH_OFF: &'static str = "見";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_ACCOUNT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">壘</span>
pub const MDI_RECORD_REC: &'static str = "壘";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLENDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">酪</span>
pub const MDI_PLUS_NETWORK: &'static str = "酪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">碌</span>
pub const MDI_RADIOACTIVE: &'static str = "碌";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MOUSE_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫫</span>
pub const MDI_DICE_D6: &'static str = "﫫";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BEACH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫶</span>
pub const MDI_HIGHWAY: &'static str = "﫶";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LIBRARY_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BEHANCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BACKSPACE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">צּ</span>
pub const MDI_GAS_CYLINDER: &'static str = "צּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬂ</span>
pub const MDI_PRIORITY_HIGH: &'static str = "ﬂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮏ</span>
pub const MDI_BOMB: &'static str = "ﮏ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰳ</span>
pub const MDI_ARROW_RIGHT_BOLD_BOX: &'static str = "ﰳ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_NOT_EQUAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMOTICON_POOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_LIST_BULLETED_TYPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LINK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰆ</span>
pub const MDI_REACT: &'static str = "ﰆ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MICROPHONE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AUTO_FIX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰟ</span>
pub const MDI_SVG: &'static str = "ﰟ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLAG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮢ</span>
pub const MDI_NUKE: &'static str = "ﮢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CUBE_UNFOLDED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯬ</span>
pub const MDI_MAILBOX: &'static str = "ﯬ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_UP_BOLD_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_9_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ORNAMENT_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯲ</span>
pub const MDI_DOWNLOAD_NETWORK: &'static str = "ﯲ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬍</span>
pub const MDI_TEXTBOX: &'static str = "﬍";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱇ</span>
pub const MDI_UBER: &'static str = "ﱇ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BELL_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭖ</span>
pub const MDI_PAW_OFF: &'static str = "ﭖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CELLPHONE_LINK_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLATTR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫝</span>
pub const MDI_COMMENT_REMOVE: &'static str = "﫝";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">懲</span>
pub const MDI_WATER: &'static str = "懲";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">福</span>
pub const MDI_TIMER_10: &'static str = "福";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">縷</span>
pub const MDI_REGEX: &'static str = "縷";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_CONTROLLER_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳫ</span>
pub const MDI_SEND_SECURE: &'static str = "ﳫ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_DOCUMENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱔ</span>
pub const MDI_FORMAT_LIST_CHECKS: &'static str = "ﱔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳣ</span>
pub const MDI_PIPE: &'static str = "ﳣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEYBOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">怜</span>
pub const MDI_SKIP_NEXT: &'static str = "怜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_SEND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BELL_RING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_BLANK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">龜</span>
pub const MDI_PIZZA: &'static str = "龜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EQUAL_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">暴</span>
pub const MDI_TENNIS: &'static str = "暴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱓ</span>
pub const MDI_FORMAT_ALIGN_TOP: &'static str = "ﱓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯫ</span>
pub const MDI_MAGNIFY_PLUS_OUTLINE: &'static str = "ﯫ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">יִ</span>
pub const MDI_FORMAT_HORIZONTAL_ALIGN_CENTER: &'static str = "יִ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">晴</span>
pub const MDI_WEATHER_HAIL: &'static str = "晴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_WRAP_INLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPBOARD_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴟ</span>
pub const MDI_FLOOR_PLAN: &'static str = "ﴟ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_BLANK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALERT_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">郞</span>
pub const MDI_TRANSCRIBE_CLOSE: &'static str = "郞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯙ</span>
pub const MDI_GITHUB_FACE: &'static str = "ﯙ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲒ</span>
pub const MDI_ARROW_COLLAPSE_RIGHT: &'static str = "ﲒ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALPHABETICAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARCHIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAP_MARKER_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FRIDGE_FILLED_BOTTOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURSOR_DEFAULT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_CIRCLES_COMMUNITIES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">襁</span>
pub const MDI_WRENCH: &'static str = "襁";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">龍</span>
pub const MDI_SPEEDOMETER: &'static str = "龍";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳜ</span>
pub const MDI_MOVIE_ROLL: &'static str = "ﳜ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAN_PENDING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯔ</span>
pub const MDI_FORMAT_FONT: &'static str = "ﯔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰾ</span>
pub const MDI_GESTURE_SWIPE_UP: &'static str = "ﰾ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲜ</span>
pub const MDI_BUS_DOUBLE_DECKER: &'static str = "ﲜ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_8_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NFC_TAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﩮</span>
pub const MDI_VIEW_DAY: &'static str = "﩮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BINOCULARS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬣ</span>
pub const MDI_HACKERNEWS: &'static str = "ﬣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮩ</span>
pub const MDI_SCANNER: &'static str = "ﮩ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_FORWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">炙</span>
pub const MDI_TAG_OUTLINE: &'static str = "炙";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURRENCY_INR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">惡</span>
pub const MDI_SORT: &'static str = "惡";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯼ</span>
pub const MDI_PAGE_LAYOUT_SIDEBAR_RIGHT: &'static str = "ﯼ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰢ</span>
pub const MDI_TICKET_PERCENT: &'static str = "ﰢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬷</span>
pub const MDI_CARDS: &'static str = "﬷";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MOUSE_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEY_MINUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨏</span>
pub const MDI_THERMOMETER_LINES: &'static str = "﨏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">車</span>
pub const MDI_PIN: &'static str = "車";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">祐</span>
pub const MDI_UNGROUP: &'static str = "祐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_STAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">类</span>
pub const MDI_WINDOW_MAXIMIZE: &'static str = "类";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴞ</span>
pub const MDI_FLASH_CIRCLE: &'static str = "ﴞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮄ</span>
pub const MDI_GONDOLA: &'static str = "ﮄ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">慎</span>
pub const MDI_WAN: &'static str = "慎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_CHARGING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMPASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰠ</span>
pub const MDI_TAG_PLUS: &'static str = "ﰠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱙ</span>
pub const MDI_MUSIC_OFF: &'static str = "ﱙ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECK_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳻ</span>
pub const MDI_VIDEO_3D: &'static str = "ﳻ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_TRANSLATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_LEFT_BOLD_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯃</span>
pub const MDI_BOMB_OFF: &'static str = "﯃";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEADPHONES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鶴</span>
pub const MDI_TRANSCRIBE: &'static str = "鶴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_ARRAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_WORD_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LIBRARY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬧ</span>
pub const MDI_MATRIX: &'static str = "ﬧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳨ</span>
pub const MDI_RICE: &'static str = "ﳨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAPTOP_WINDOWS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳧ</span>
pub const MDI_POWER_SOCKET_US: &'static str = "ﳧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴝ</span>
pub const MDI_FINANCE: &'static str = "ﴝ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">響</span>
pub const MDI_VIEW_AGENDA: &'static str = "響";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIGHTNESS_AUTO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_5_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲢ</span>
pub const MDI_CAMERA_METERING_PARTIAL: &'static str = "ﲢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲀ</span>
pub const MDI_SIGN_TEXT: &'static str = "ﲀ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">並</span>
pub const MDI_VIEW_HEADLINE: &'static str = "並";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">麟</span>
pub const MDI_TABLE_ROW_PLUS_BEFORE: &'static str = "麟";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫱</span>
pub const MDI_FOOD_FORK_DRINK: &'static str = "﫱";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯏</span>
pub const MDI_EYE_OFF_OUTLINE: &'static str = "﯏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOME_MODERN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURSOR_DEFAULT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEYBOARD_BACKSPACE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GUITAR_ELECTRIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳦ</span>
pub const MDI_POWER_SOCKET_UK: &'static str = "ﳦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DOTS_VERTICAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAR_CONNECTED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱸ</span>
pub const MDI_SET_CENTER_RIGHT: &'static str = "ﱸ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NFC_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MARGIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">嵐</span>
pub const MDI_POPCORN: &'static str = "嵐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLAG_CHECKERED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_JIRA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_HORIZONTAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">葉</span>
pub const MDI_SALE: &'static str = "葉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱩ</span>
pub const MDI_CHART_LINE_STACKED: &'static str = "ﱩ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIGHTNESS_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LINKEDIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">參</span>
pub const MDI_RSS_BOX: &'static str = "參";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰬ</span>
pub const MDI_ARROW_DOWN_BOLD: &'static str = "ﰬ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰶ</span>
pub const MDI_ARROW_UP_BOLD_BOX: &'static str = "ﰶ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">北</span>
pub const MDI_ROTATE_3D: &'static str = "北";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HARDDISK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMOTICON_SAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GIFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳇ</span>
pub const MDI_FORKLIFT: &'static str = "ﳇ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEVRON_DOUBLE_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CELLPHONE_LINK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴕ</span>
pub const MDI_COMMENT_QUESTION: &'static str = "ﴕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯌</span>
pub const MDI_EARTH_BOX_OFF: &'static str = "﯌";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLACKBERRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PANORAMA_FISHEYE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬚</span>
pub const MDI_CONTENT_SAVE_SETTINGS: &'static str = "﬚";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_TOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FAX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUETOOTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">閭</span>
pub const MDI_SELECT_INVERSE: &'static str = "閭";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">舘</span>
pub const MDI_VIEW_DASHBOARD: &'static str = "舘";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮚ</span>
pub const MDI_EMOTICON_EXCITED: &'static str = "ﮚ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳱ</span>
pub const MDI_SURROUND_SOUND_7_1: &'static str = "ﳱ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﵂</span>
pub const MDI_VUEJS: &'static str = "﵂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_CHARGING_20: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_ALERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_MARKED_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_FIND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">絛</span>
pub const MDI_WINDOW_MINIMIZE: &'static str = "絛";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_DOWN_BOLD_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">暈</span>
pub const MDI_SPELLCHECK: &'static str = "暈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BELL_SLEEP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_GOOGLE_DRIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LINK_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_DRAMA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GROUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_LEFT_DROP_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_NOTE_EIGHTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">廬</span>
pub const MDI_SECURITY: &'static str = "廬";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">糖</span>
pub const MDI_TEMPERATURE_CELSIUS: &'static str = "糖";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">磻</span>
pub const MDI_ROTATE_LEFT: &'static str = "磻";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURRENCY_BTC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ELEVATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GITHUB_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FERRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱃ</span>
pub const MDI_KICKSTARTER: &'static str = "ﱃ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOOD_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴍ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_80: &'static str = "ﴍ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">逸</span>
pub const MDI_TOOLTIP_OUTLINE: &'static str = "逸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭠ</span>
pub const MDI_SKIP_NEXT_CIRCLE: &'static str = "ﭠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲍ</span>
pub const MDI_ALARM_LIGHT: &'static str = "ﲍ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">勇</span>
pub const MDI_VIMEO: &'static str = "勇";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ONEDRIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COOKIE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">都</span>
pub const MDI_TOOLTIP_OUTLINE_PLUS: &'static str = "都";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">磊</span>
pub const MDI_YOUTUBE_TV: &'static str = "磊";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳼ</span>
pub const MDI_WALL: &'static str = "ﳼ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_1_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫧</span>
pub const MDI_DELETE_FOREVER: &'static str = "﫧";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MINUS_NETWORK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰔ</span>
pub const MDI_SIGNAL_HSPA_PLUS: &'static str = "ﰔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FACEBOOK_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_ALIGN_CENTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EXCLAMATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">兩</span>
pub const MDI_SD: &'static str = "兩";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫠</span>
pub const MDI_CHECK_CIRCLE_OUTLINE: &'static str = "﫠";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_40: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">רּ</span>
pub const MDI_HUMAN_FEMALE: &'static str = "רּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_POSITIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">望</span>
pub const MDI_WEATHER_NIGHT: &'static str = "望";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">洞</span>
pub const MDI_TEMPERATURE_KELVIN: &'static str = "洞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬎</span>
pub const MDI_VIOLIN: &'static str = "﬎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮆ</span>
pub const MDI_REORDER_HORIZONTAL: &'static str = "ﮆ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUR_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUETOOTH_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MULTIPLICATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OPEN_IN_NEW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NEEDLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">歹</span>
pub const MDI_WEATHER_POURING: &'static str = "歹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">猪</span>
pub const MDI_TICKET_ACCOUNT: &'static str = "猪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">滋</span>
pub const MDI_WEATHER_SUNSET: &'static str = "滋";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_MULTIPLE_MARKED_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EYEDROPPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲓ</span>
pub const MDI_ARROW_COLLAPSE_UP: &'static str = "ﲓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCESS_POINT_NETWORK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALL_MADE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_TILT_SHIFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASK_EMPTY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭾ</span>
pub const MDI_YIN_YANG: &'static str = "ﭾ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴪ</span>
pub const MDI_KARATE: &'static str = "ﴪ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_RIGHT_DROP_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">社</span>
pub const MDI_UNDO_VARIANT: &'static str = "社";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEYBOARD_CLOSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_BOTTOM_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_CLOUD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALARM_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CROP_FREE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鍊</span>
pub const MDI_SHREDDER: &'static str = "鍊";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">遼</span>
pub const MDI_SPEAKER_OFF: &'static str = "遼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭹ</span>
pub const MDI_FLASH_RED_EYE: &'static str = "ﭹ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">契</span>
pub const MDI_PLAY: &'static str = "契";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱊ</span>
pub const MDI_XBOX_CONTROLLER_BATTERY_EMPTY: &'static str = "ﱊ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOK_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EARTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴮ</span>
pub const MDI_POKER_CHIP: &'static str = "ﴮ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PAW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_OUTSIDE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨎</span>
pub const MDI_THERMOMETER: &'static str = "﨎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_MINUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">画</span>
pub const MDI_WHEELCHAIR_ACCESSIBILITY: &'static str = "画";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_TIMER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯰ</span>
pub const MDI_MINUS_BOX_OUTLINE: &'static str = "ﯰ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAP_MARKER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰿ</span>
pub const MDI_GESTURE_TAP: &'static str = "ﰿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱺ</span>
pub const MDI_SET_LEFT_CENTER: &'static str = "ﱺ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭬ</span>
pub const MDI_CHART_SCATTERPLOT_HEXBIN: &'static str = "ﭬ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_REMOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">秊</span>
pub const MDI_SHARE: &'static str = "秊";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_CHECK_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_80: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MARTINI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬈</span>
pub const MDI_SAXOPHONE: &'static str = "﬈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_0_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">濾</span>
pub const MDI_SELECT: &'static str = "濾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">柳</span>
pub const MDI_SQUARE_INC: &'static str = "柳";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯱ</span>
pub const MDI_NETWORK: &'static str = "ﯱ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱯ</span>
pub const MDI_GUITAR_ACOUSTIC: &'static str = "ﱯ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEY_CHANGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳝ</span>
pub const MDI_MUSHROOM: &'static str = "ﳝ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">襤</span>
pub const MDI_POWER: &'static str = "襤";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">揄</span>
pub const MDI_WATER_PERCENT: &'static str = "揄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">卵</span>
pub const MDI_POKEBALL: &'static str = "卵";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GENDER_MALE_FEMALE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">屢</span>
pub const MDI_RECYCLE: &'static str = "屢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIGHTNESS_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">拏</span>
pub const MDI_REPRODUCTION: &'static str = "拏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯵ</span>
pub const MDI_NPM: &'static str = "ﯵ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰧ</span>
pub const MDI_VIEW_SEQUENTIAL: &'static str = "ﰧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MORE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲘ</span>
pub const MDI_BOOK_SECURE: &'static str = "ﲘ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LOUPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮡ</span>
pub const MDI_MESSAGE_BULLETED_OFF: &'static str = "ﮡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴛ</span>
pub const MDI_FAN_OFF: &'static str = "ﴛ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫲</span>
pub const MDI_FOOD_OFF: &'static str = "﫲";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">褐</span>
pub const MDI_VECTOR_POLYLINE: &'static str = "褐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_9_PLUS_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BACKBURGER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARRANGE_BRING_TO_FRONT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">僚</span>
pub const MDI_SORT_ASCENDING: &'static str = "僚";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOKMARK_REMOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯋</span>
pub const MDI_EARTH_BOX: &'static str = "﯋";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMPASS_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮐ</span>
pub const MDI_CALENDAR_QUESTION: &'static str = "ﮐ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOUZZ_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱟ</span>
pub const MDI_UNFOLD_MORE_VERTICAL: &'static str = "ﱟ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">殮</span>
pub const MDI_SILVERWARE_VARIANT: &'static str = "殮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MOUSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMAIL_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GMAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">頻</span>
pub const MDI_NEAR_ME: &'static str = "頻";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">סּ</span>
pub const MDI_ERASER_VARIANT: &'static str = "סּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COLOR_HELPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳮ</span>
pub const MDI_SURROUND_SOUND_2_0: &'static str = "ﳮ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">杻</span>
pub const MDI_SPOTLIGHT_BEAM: &'static str = "杻";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴎ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_90: &'static str = "ﴎ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEYBOARD_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">齃</span>
pub const MDI_BOOK_MINUS: &'static str = "齃";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮊ</span>
pub const MDI_SKULL: &'static str = "ﮊ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_RIGHT_DROP_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">浪</span>
pub const MDI_PRINTER_3D: &'static str = "浪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAST_CONNECTED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">崙</span>
pub const MDI_STEP_BACKWARD_2: &'static str = "崙";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭗ</span>
pub const MDI_PHONE_MINUS: &'static str = "ﭗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BARLEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUR_RADIAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_INBOX_ARROW_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURRENCY_EUR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">漏</span>
pub const MDI_REDO_VARIANT: &'static str = "漏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">癩</span>
pub const MDI_PLAY_PROTECTED_CONTENT: &'static str = "癩";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_APPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮳</span>
pub const MDI_LAMP: &'static str = "﮳";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫴</span>
pub const MDI_GOOGLE_MAPS: &'static str = "﫴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬇</span>
pub const MDI_ROWING: &'static str = "﬇";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴈ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_30: &'static str = "ﴈ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮎ</span>
pub const MDI_BEAKER: &'static str = "ﮎ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">粒</span>
pub const MDI_TAG_FACES: &'static str = "粒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">難</span>
pub const MDI_VIDEO_SWITCH: &'static str = "難";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰥ</span>
pub const MDI_TRUCK_TRAILER: &'static str = "ﰥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALARM_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">辶</span>
pub const MDI_VIDEO: &'static str = "辶";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮲</span>
pub const MDI_EMBY: &'static str = "﮲";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮿</span>
pub const MDI_ARROW_LEFT_BOX: &'static str = "﮿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴖ</span>
pub const MDI_CONTENT_SAVE_OUTLINE: &'static str = "ﴖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">曆</span>
pub const MDI_SERVER_MINUS: &'static str = "曆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">撚</span>
pub const MDI_SERVER_SECURITY: &'static str = "撚";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭩ</span>
pub const MDI_TUNE_VERTICAL: &'static str = "ﭩ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MOUSE_VARIANT_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_9_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳠ</span>
pub const MDI_NULL: &'static str = "ﳠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴌ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_70: &'static str = "ﴌ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_DECREASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GAMEPAD_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_SUBSCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴬ</span>
pub const MDI_NOTEBOOK: &'static str = "ﴬ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">盛</span>
pub const MDI_WHITE_BALANCE_SUNNY: &'static str = "盛";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">刺</span>
pub const MDI_TEAMVIEWER: &'static str = "刺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_KEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">悔</span>
pub const MDI_TRUCK_DELIVERY: &'static str = "悔";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳡ</span>
pub const MDI_PASSPORT: &'static str = "ﳡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">變</span>
pub const MDI_YEAST: &'static str = "變";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲐ</span>
pub const MDI_ARROW_COLLAPSE_DOWN: &'static str = "ﲐ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴫ</span>
pub const MDI_LADYBUG: &'static str = "ﴫ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳰ</span>
pub const MDI_SURROUND_SOUND_5_1: &'static str = "ﳰ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCESS_POINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">裂</span>
pub const MDI_SIGN_CAUTION: &'static str = "裂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">玲</span>
pub const MDI_SKIP_PREVIOUS: &'static str = "玲";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰴ</span>
pub const MDI_ARROW_RIGHT_BOLD_BOX_OUTLINE: &'static str = "ﰴ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲛ</span>
pub const MDI_BUS_ARTICULATED_FRONT: &'static str = "ﲛ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴙ</span>
pub const MDI_DOOR_CLOSED: &'static str = "ﴙ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮅ</span>
pub const MDI_INBOX: &'static str = "ﮅ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰫ</span>
pub const MDI_WIIU: &'static str = "ﰫ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MONITOR_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALERT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPBOARD_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">零</span>
pub const MDI_SLEEP_OFF: &'static str = "零";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">勞</span>
pub const MDI_PULSE: &'static str = "勞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD_PRINT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮹</span>
pub const MDI_PLANE_SHIELD: &'static str = "﮹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMOTICON_TONGUE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬦ</span>
pub const MDI_LAMBDA: &'static str = "ﬦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯚ</span>
pub const MDI_GOOGLE_KEEP: &'static str = "ﯚ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰓ</span>
pub const MDI_SIGNAL_HSPA: &'static str = "ﰓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ADJUST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_CARDBOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">樂</span>
pub const MDI_PLUS: &'static str = "樂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CREDIT_CARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鸞</span>
pub const MDI_POLYMER: &'static str = "鸞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_3_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯀</span>
pub const MDI_ARROW_RIGHT_BOX: &'static str = "﯀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">讀</span>
pub const MDI_REPLY_ALL: &'static str = "讀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AIRPLANE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_MISSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">勤</span>
pub const MDI_TRENDING_UP: &'static str = "勤";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲶ</span>
pub const MDI_CORN: &'static str = "ﲶ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NOTE_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鬒</span>
pub const MDI_CLOCK_ALERT: &'static str = "鬒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PACKAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﵁</span>
pub const MDI_VIEW_DASHBOARD_VARIANT: &'static str = "﵁";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">綠</span>
pub const MDI_RADIOBOX_MARKED: &'static str = "綠";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">כֿ</span>
pub const MDI_INFORMATION_VARIANT: &'static str = "כֿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOOD_APPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILTER_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">寮</span>
pub const MDI_SORT_DESCENDING: &'static str = "寮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">䀹</span>
pub const MDI_AIRPLANE_TAKEOFF: &'static str = "䀹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_RIGHT_BOLD_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_ALBUM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DICE_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GENDER_MALE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOME_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MINUS_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPBOARD_ARROW_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PANORAMA_HORIZONTAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨨</span>
pub const MDI_TOOTH: &'static str = "﨨";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_6_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BELL_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ELEVATION_RISE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GLASS_FLUTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAKE_LAYERED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOK_MULTIPLE_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫦</span>
pub const MDI_CURSOR_TEXT: &'static str = "﫦";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">壟</span>
pub const MDI_RAY_START_ARROW: &'static str = "壟";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">諸</span>
pub const MDI_TOOLTIP: &'static str = "諸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲷ</span>
pub const MDI_CURRENCY_CHF: &'static str = "ﲷ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALERT_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOKMARK_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_PDF_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAGNET_ON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OFFICE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">稜</span>
pub const MDI_REPEAT_OFF: &'static str = "稜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AV_TIMER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">戀</span>
pub const MDI_SERVER_REMOVE: &'static str = "戀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">利</span>
pub const MDI_STOVE: &'static str = "利";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLOPPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">琉</span>
pub const MDI_STAIRS: &'static str = "琉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬅ</span>
pub const MDI_POOL: &'static str = "ﬅ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OCTAGON_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰌ</span>
pub const MDI_RUN: &'static str = "ﰌ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_TODAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DOMAIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_TEXTDIRECTION_L_TO_R: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲋ</span>
pub const MDI_WAVES: &'static str = "ﲋ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_PRESENTATION_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_GREATER_THAN_OR_EQUAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_DOWN_DROP_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">拉</span>
pub const MDI_POWER_SETTINGS: &'static str = "拉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">דּ</span>
pub const MDI_APPLE_KEYBOARD_CONTROL: &'static str = "דּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">勵</span>
pub const MDI_SEAT_LEGROOM_REDUCED: &'static str = "勵";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">廒</span>
pub const MDI_WALK: &'static str = "廒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱹ</span>
pub const MDI_SET_LEFT: &'static str = "ﱹ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴆ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_10: &'static str = "ﴆ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_4_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬃ</span>
pub const MDI_PRIORITY_LOW: &'static str = "ﬃ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEYBOARD_RETURN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰯ</span>
pub const MDI_ARROW_LEFT_BOLD: &'static str = "ﰯ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲞ</span>
pub const MDI_BUS_SIDE: &'static str = "ﲞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">瑱</span>
pub const MDI_WEIGHT_KILOGRAM: &'static str = "瑱";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AUTO_UPLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAGNET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">視</span>
pub const MDI_VECTOR_SELECTION: &'static str = "視";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴥ</span>
pub const MDI_HOME_HEART: &'static str = "ﴥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨑</span>
pub const MDI_THUMB_DOWN_OUTLINE: &'static str = "﨑";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭵ</span>
pub const MDI_ITUNES: &'static str = "ﭵ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_EARTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">練</span>
pub const MDI_VECTOR_COMBINE: &'static str = "練";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURRENCY_NGN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">煮</span>
pub const MDI_WEATHER_WINDY: &'static str = "煮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOOTBALL_AUSTRALIAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﴿</span>
pub const MDI_VIDEO_INPUT_HDMI: &'static str = "﴿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_UP_THICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CUP_WATER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DICE_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲵ</span>
pub const MDI_CONSOLE_LINE: &'static str = "ﲵ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫨</span>
pub const MDI_DELETE_SWEEP: &'static str = "﫨";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EJECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LED_VARIANT_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱥ</span>
pub const MDI_ALERT_OCTAGRAM: &'static str = "ﱥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_SWITCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳲ</span>
pub const MDI_TELEVISION_CLASSIC: &'static str = "ﳲ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DOTS_HORIZONTAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">כּ</span>
pub const MDI_CHECKBOX_MULTIPLE_BLANK_CIRCLE_OUTLINE: &'static str = "כּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OPEN_IN_APP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">䀘</span>
pub const MDI_AIRPLANE_LANDING: &'static str = "䀘";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_EXCEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮙ</span>
pub const MDI_EMOTICON_DEAD: &'static str = "ﮙ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEXAGON_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯾ</span>
pub const MDI_PENTAGON: &'static str = "ﯾ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱼ</span>
pub const MDI_SET_NONE: &'static str = "ﱼ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱽ</span>
pub const MDI_SET_RIGHT: &'static str = "ﱽ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_FLOAT_CENTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPBOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫬</span>
pub const MDI_DICE_D8: &'static str = "﫬";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BELL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BASECAMP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DIAMOND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳍ</span>
pub const MDI_HIGH_DEFINITION: &'static str = "ﳍ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">תּ</span>
pub const MDI_HUMAN_HANDSDOWN: &'static str = "תּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">מּ</span>
pub const MDI_CLOUD_SYNC: &'static str = "מּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱘ</span>
pub const MDI_MUSIC: &'static str = "ﱘ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲹ</span>
pub const MDI_CURRENCY_ETH: &'static str = "ﲹ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮦ</span>
pub const MDI_RESTORE: &'static str = "ﮦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PALETTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">犯</span>
pub const MDI_WEBCAM: &'static str = "犯";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">荒</span>
pub const MDI_WORDPRESS: &'static str = "荒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳋ</span>
pub const MDI_GOOGLE_ASSISTANT: &'static str = "ﳋ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱻ</span>
pub const MDI_SET_LEFT_RIGHT: &'static str = "ﱻ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">隷</span>
pub const MDI_TRANSFER: &'static str = "隷";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬥ</span>
pub const MDI_JSON: &'static str = "ﬥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_AREA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳒ</span>
pub const MDI_LANGUAGE_R: &'static str = "ﳒ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">珞</span>
pub const MDI_PLUS_CIRCLE_MULTIPLE_OUTLINE: &'static str = "珞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬓ</span>
pub const MDI_APPLICATION: &'static str = "ﬓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">פּ</span>
pub const MDI_FILE_TREE: &'static str = "פּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_APPLE_FINDER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮌ</span>
pub const MDI_ALARM_SNOOZE: &'static str = "ﮌ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰽ</span>
pub const MDI_GESTURE_SWIPE_RIGHT: &'static str = "ﰽ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BEER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_4_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_DELIMITED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳪ</span>
pub const MDI_SASS: &'static str = "ﳪ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">力</span>
pub const MDI_SERVER: &'static str = "力";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_WRAP_TIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_BOTTOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEVRON_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭝ</span>
pub const MDI_SHAPE_POLYGON_PLUS: &'static str = "ﭝ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CELLPHONE_ANDROID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CREATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">串</span>
pub const MDI_PINE_TREE_BOX: &'static str = "串";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOCK_START: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬁ</span>
pub const MDI_PHONE_CLASSIC: &'static str = "ﬁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">便</span>
pub const MDI_ROTATE_LEFT_VARIANT: &'static str = "便";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴢ</span>
pub const MDI_GOOGLE_HOME: &'static str = "ﴢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MENU_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSE_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">律</span>
pub const MDI_STETHOSCOPE: &'static str = "律";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">彩</span>
pub const MDI_WALLET_GIFTCARD: &'static str = "彩";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_LIST_BULLETED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_FRAMES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAUNCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">醙</span>
pub const MDI_SURROUND_SOUND: &'static str = "醙";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_LIST_NUMBERS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CELLPHONE_BASIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PALETTE_ADVANCED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯶ</span>
pub const MDI_NUT: &'static str = "ﯶ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱫ</span>
pub const MDI_DECAGRAM_OUTLINE: &'static str = "ﱫ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳅ</span>
pub const MDI_EVENTBRITE: &'static str = "ﳅ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOUZZ: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_TOP_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴘ</span>
pub const MDI_DOOR: &'static str = "ﴘ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">諒</span>
pub const MDI_SEAT_LEGROOM_EXTRA: &'static str = "諒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">擄</span>
pub const MDI_PUZZLE: &'static str = "擄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">者</span>
pub const MDI_WINDOWS: &'static str = "者";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰷ</span>
pub const MDI_ARROW_UP_BOLD_BOX_OUTLINE: &'static str = "ﰷ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_GREATER_THAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨓</span>
pub const MDI_THUMB_UP_OUTLINE: &'static str = "﨓";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭸ</span>
pub const MDI_CURRENCY_USD_OFF: &'static str = "ﭸ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMOTICON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮺</span>
pub const MDI_ACCOUNT_EDIT: &'static str = "﮺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_9_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴧ</span>
pub const MDI_HULU: &'static str = "ﴧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳓ</span>
pub const MDI_LAVA_LAMP: &'static str = "ﳓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAP_MARKER_RADIUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫭</span>
pub const MDI_DISK: &'static str = "﫭";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬖ</span>
pub const MDI_BOWL: &'static str = "ﬖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰊ</span>
pub const MDI_RHOMBUS_OUTLINE: &'static str = "ﰊ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱑ</span>
pub const MDI_FORMAT_ALIGN_BOTTOM: &'static str = "ﱑ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_PAGES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">劣</span>
pub const MDI_SHUFFLE_DISABLED: &'static str = "劣";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">神</span>
pub const MDI_TIMELAPSE: &'static str = "神";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLAG_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﵃</span>
pub const MDI_XAMARIN: &'static str = "﵃";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫛</span>
pub const MDI_BOOMBOX: &'static str = "﫛";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱿ</span>
pub const MDI_SIGN_DIRECTION: &'static str = "ﱿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯅</span>
pub const MDI_CARDS_VARIANT: &'static str = "﯅";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">גּ</span>
pub const MDI_APPLE_KEYBOARD_COMMAND: &'static str = "גּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DATABASE_MINUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬌</span>
pub const MDI_SUBDIRECTORY_ARROW_RIGHT: &'static str = "﬌";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OMEGA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰋ</span>
pub const MDI_ROOMBA: &'static str = "ﰋ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUTRITION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LANGUAGE_PHP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PANDORA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIGHTNESS_4: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_INSTAPAPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NEST_THERMOSTAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_SWITCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FACEBOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯎</span>
pub const MDI_EYE_OUTLINE: &'static str = "﯎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLAG_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHART_ARC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ETHERNET_CABLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰂ</span>
pub const MDI_PLUS_BOX_OUTLINE: &'static str = "ﰂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱁ</span>
pub const MDI_GESTURE_TWO_TAP: &'static str = "ﱁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_UPLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPBOARD_ACCOUNT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">磌</span>
pub const MDI_WIKIPEDIA: &'static str = "磌";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">硫</span>
pub const MDI_STAR_CIRCLE: &'static str = "硫";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALCULATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_6_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳙ</span>
pub const MDI_METRONOME_TICK: &'static str = "ﳙ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">𣏕</span>
pub const MDI_ACCOUNT_CARD_DETAILS: &'static str = "𣏕";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EQUAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">頋</span>
pub const MDI_OPACITY: &'static str = "頋";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_BOTTOM_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳳ</span>
pub const MDI_TEXTBOX_PASSWORD: &'static str = "ﳳ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MINECRAFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_RIGHT_THICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">祿</span>
pub const MDI_RADIOBOX_BLANK: &'static str = "祿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮨ</span>
pub const MDI_FORMAT_ROTATE_90: &'static str = "ﮨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">怒</span>
pub const MDI_ROAD: &'static str = "怒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱢ</span>
pub const MDI_SQUARE: &'static str = "ﱢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鱗</span>
pub const MDI_TABLE_ROW_PLUS_AFTER: &'static str = "鱗";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ATTACHMENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">黎</span>
pub const MDI_SEND: &'static str = "黎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAN_DISCONNECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">𤋮</span>
pub const MDI_VIEW_COLUMN: &'static str = "𤋮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHURCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭘ</span>
pub const MDI_PHONE_PLUS: &'static str = "ﭘ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BARCODE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮣ</span>
pub const MDI_POWER_PLUG: &'static str = "ﮣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰍ</span>
pub const MDI_SEARCH_WEB: &'static str = "ﰍ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">僧</span>
pub const MDI_TRELLO: &'static str = "僧";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳢ</span>
pub const MDI_PERIODIC_TABLE_CO2: &'static str = "ﳢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LED_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_WALLET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_CIRCLES_EXTENDED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬕ</span>
pub const MDI_ARROW_EXPAND: &'static str = "ﬕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴣ</span>
pub const MDI_GUY_FAWKES_MASK: &'static str = "ﴣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰐ</span>
pub const MDI_SIGNAL_2G: &'static str = "ﰐ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEYBOARD_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">罹</span>
pub const MDI_SYNC_ALERT: &'static str = "罹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱗ</span>
pub const MDI_HEART_OFF: &'static str = "ﱗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_PARAGRAPH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAPTOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">亮</span>
pub const MDI_SCRIPT: &'static str = "亮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫽</span>
pub const MDI_MUSIC_NOTE_BLUETOOTH: &'static str = "﫽";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_CONTROLLER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_5_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MENU_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOKMARK_PLUS_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">烙</span>
pub const MDI_PLUS_CIRCLE: &'static str = "烙";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_LINE_SPACING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">李</span>
pub const MDI_SWAP_VERTICAL: &'static str = "李";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HUMAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱧ</span>
pub const MDI_CEILING_LIGHT: &'static str = "ﱧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLAG_VARIANT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">免</span>
pub const MDI_TRENDING_DOWN: &'static str = "免";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NEWSPAPER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴜ</span>
pub const MDI_FILE_PERCENT: &'static str = "ﴜ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ASSISTANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PAUSE_OCTAGON_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">隣</span>
pub const MDI_TABLE_ROW_HEIGHT: &'static str = "隣";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰩ</span>
pub const MDI_WEBPACK: &'static str = "ﰩ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASK_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">燎</span>
pub const MDI_SOURCE_FORK: &'static str = "燎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_CHARGING_100: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_MINUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">狼</span>
pub const MDI_PRINTER_ALERT: &'static str = "狼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">輪</span>
pub const MDI_STEP_FORWARD_2: &'static str = "輪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮭ</span>
pub const MDI_UNITY: &'static str = "ﮭ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMAIL_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳆ</span>
pub const MDI_FOOD_CROISSANT: &'static str = "ﳆ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_7_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭕ</span>
pub const MDI_MOVE_RESIZE_VARIANT: &'static str = "ﭕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_TEXT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAGNIFY_MINUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">匿</span>
pub const MDI_TABLE_COLUMN_PLUS_AFTER: &'static str = "匿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫚</span>
pub const MDI_BOOK_PLUS: &'static str = "﫚";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮟ</span>
pub const MDI_HOME_OUTLINE: &'static str = "ﮟ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴩ</span>
pub const MDI_IMAGE_OFF: &'static str = "ﴩ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_DOCUMENT_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ETHERNET_CABLE_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ײַ</span>
pub const MDI_FORMAT_HORIZONTAL_ALIGN_RIGHT: &'static str = "ײַ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DECIMAL_INCREASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ףּ</span>
pub const MDI_FACE_PROFILE: &'static str = "ףּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲬ</span>
pub const MDI_CCTV: &'static str = "ﲬ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰎ</span>
pub const MDI_SHOVEL: &'static str = "ﰎ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_NONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴳ</span>
pub const MDI_TABLE_COLUMN: &'static str = "ﴳ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ETHERNET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_LEFT_BOLD_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEART_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫾</span>
pub const MDI_MUSIC_NOTE_BLUETOOTH_OFF: &'static str = "﫾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">龜</span>
pub const MDI_HUMAN_PREGNANT: &'static str = "龜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳃ</span>
pub const MDI_EAR_HEARING: &'static str = "ﳃ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOURSQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CUBE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱣ</span>
pub const MDI_CIRCLE: &'static str = "ﱣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EARTH_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">切</span>
pub const MDI_TELEGRAM: &'static str = "切";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮫ</span>
pub const MDI_TIMER_SAND_EMPTY: &'static str = "ﮫ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">例</span>
pub const MDI_SNAPCHAT: &'static str = "例";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱅ</span>
pub const MDI_ONENOTE: &'static str = "ﱅ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">拾</span>
pub const MDI_SCHOOL: &'static str = "拾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">突</span>
pub const MDI_VECTOR_CIRCLE: &'static str = "突";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">שׂ</span>
pub const MDI_SOURCE_BRANCH: &'static str = "שׂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CANDYCANE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTRAST_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_MARKED_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSE_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_GLASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰹ</span>
pub const MDI_FILE_ACCOUNT: &'static str = "ﰹ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_1_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">晴</span>
pub const MDI_THUMB_UP: &'static str = "晴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰺ</span>
pub const MDI_GESTURE_DOUBLE_TAP: &'static str = "ﰺ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLOGGER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLAG_TRIANGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FACEBOOK_MESSENGER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳵ</span>
pub const MDI_THOUGHT_BUBBLE_OUTLINE: &'static str = "ﳵ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯢ</span>
pub const MDI_INFINITY: &'static str = "ﯢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">塀</span>
pub const MDI_TROPHY_OUTLINE: &'static str = "塀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">調</span>
pub const MDI_XBOX_CONTROLLER: &'static str = "調";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMPARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DICE_5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">駱</span>
pub const MDI_PLUS_ONE: &'static str = "駱";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮕ</span>
pub const MDI_DEVELOPER_BOARD: &'static str = "ﮕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">况</span>
pub const MDI_VIEW_LIST: &'static str = "况";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILMSTRIP_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">聯</span>
pub const MDI_SHIELD: &'static str = "聯";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">丹</span>
pub const MDI_REWIND: &'static str = "丹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">笠</span>
pub const MDI_TAG: &'static str = "笠";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭷ</span>
pub const MDI_CALENDAR_RANGE: &'static str = "ﭷ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲦ</span>
pub const MDI_CAR_ESTATE: &'static str = "ﲦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯭ</span>
pub const MDI_MEDICAL_BAG: &'static str = "ﯭ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FRIDGE_FILLED_TOP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PARKING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲱ</span>
pub const MDI_CHILI_MEDIUM: &'static str = "ﲱ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰪ</span>
pub const MDI_WIDGETS: &'static str = "ﰪ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CITY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">了</span>
pub const MDI_SORT_ALPHABETICAL: &'static str = "了";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱄ</span>
pub const MDI_NETFLIX: &'static str = "ﱄ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_STYLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LED_ON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PANORAMA_WIDE_ANGLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫟</span>
pub const MDI_CHECK_CIRCLE: &'static str = "﫟";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯍</span>
pub const MDI_EMAIL_ALERT: &'static str = "﯍";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">館</span>
pub const MDI_TRAM: &'static str = "館";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_VERTICAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">轢</span>
pub const MDI_SERVER_NETWORK_OFF: &'static str = "轢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ORBIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭻ</span>
pub const MDI_PIANO: &'static str = "ﭻ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮈ</span>
pub const MDI_SECURITY_HOME: &'static str = "ﮈ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱛ</span>
pub const MDI_VOLUME_PLUS: &'static str = "ﱛ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CROP_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">琢</span>
pub const MDI_UMBRELLA_OUTLINE: &'static str = "琢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲕ</span>
pub const MDI_ARROW_EXPAND_LEFT: &'static str = "ﲕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨡</span>
pub const MDI_TOGGLE_SWITCH_OFF: &'static str = "﨡";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CELLPHONE_SETTINGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LOCK_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">贈</span>
pub const MDI_YELP: &'static str = "贈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰞ</span>
pub const MDI_STADIUM: &'static str = "ﰞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴇ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_20: &'static str = "ﴇ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭐ</span>
pub const MDI_MAP_MARKER_PLUS: &'static str = "ﭐ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">שׁ</span>
pub const MDI_SIGMA_LOWER: &'static str = "שׁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲭ</span>
pub const MDI_CHART_DONUT: &'static str = "ﲭ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALARM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰼ</span>
pub const MDI_GESTURE_SWIPE_LEFT: &'static str = "ﰼ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱍ</span>
pub const MDI_XBOX_CONTROLLER_BATTERY_MEDIUM: &'static str = "ﱍ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CROSSHAIRS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_REPLY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_NOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOK_OPEN_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱖ</span>
pub const MDI_GRID_LARGE: &'static str = "ﱖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲮ</span>
pub const MDI_CHART_DONUT_VARIANT: &'static str = "ﲮ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_MARKED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEART_BROKEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LANGUAGE_PYTHON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">菱</span>
pub const MDI_REPLAY: &'static str = "菱";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">良</span>
pub const MDI_SEAT_INDIVIDUAL_SUITE: &'static str = "良";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PAUSE_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯂</span>
pub const MDI_ASTERISK: &'static str = "﯂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯴ</span>
pub const MDI_UPLOAD_NETWORK: &'static str = "ﯴ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭿ</span>
pub const MDI_TOWER_BEACH: &'static str = "ﭿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BETA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LOGOUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱌ</span>
pub const MDI_XBOX_CONTROLLER_BATTERY_LOW: &'static str = "ﱌ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_INCOMING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱈ</span>
pub const MDI_VECTOR_RADIUS: &'static str = "ﱈ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LED_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭪ</span>
pub const MDI_CART_OFF: &'static str = "ﭪ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">邏</span>
pub const MDI_PLAYSTATION: &'static str = "邏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱕ</span>
pub const MDI_FORMAT_QUOTE_OPEN: &'static str = "ﱕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_IMPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MICROPHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_8_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬑</span>
pub const MDI_WATERMARK: &'static str = "﬑";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯽ</span>
pub const MDI_PENCIL_CIRCLE: &'static str = "ﯽ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NOTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CERTIFICATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">金</span>
pub const MDI_PLAY_BOX_OUTLINE: &'static str = "金";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">令</span>
pub const MDI_SIM_OFF: &'static str = "令";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_ALIGN_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_NOT_EQUAL_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">吝</span>
pub const MDI_TABLE_COLUMN_REMOVE: &'static str = "吝";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨤</span>
pub const MDI_TOOLTIP_IMAGE: &'static str = "﨤";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_BRACES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯒</span>
pub const MDI_FIND_REPLACE: &'static str = "﯒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GRID_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PANDA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">阮</span>
pub const MDI_SPOTIFY: &'static str = "阮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">塚</span>
pub const MDI_THUMB_DOWN: &'static str = "塚";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱏ</span>
pub const MDI_CLIPBOARD_PLUS: &'static str = "ﱏ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HELP_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LINUX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">劉</span>
pub const MDI_SPOTLIGHT: &'static str = "劉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">韛</span>
pub const MDI_TRANSLATE: &'static str = "韛";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_APPLE_IOS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_MULTIPLE_BLANK_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_LEFT_BOLD_HEXAGON_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_BROKEN_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTENT_DUPLICATE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LIGHTBULB_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮛ</span>
pub const MDI_FOLDER_STAR: &'static str = "ﮛ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GLASS_STANGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯤ</span>
pub const MDI_LANGUAGE_TYPESCRIPT: &'static str = "ﯤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">הּ</span>
pub const MDI_APPLE_KEYBOARD_OPTION: &'static str = "הּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AMAZON_CLOUDDRIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳹ</span>
pub const MDI_VAN_UTILITY: &'static str = "ﳹ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BULLHORN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮼</span>
pub const MDI_ALL_INCLUSIVE: &'static str = "﮼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲿ</span>
pub const MDI_DIP_SWITCH: &'static str = "ﲿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳁ</span>
pub const MDI_DOTS_HORIZONTAL_CIRCLE: &'static str = "ﳁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴼ</span>
pub const MDI_VIDEO_4K_BOX: &'static str = "ﴼ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">勺</span>
pub const MDI_VENMO: &'static str = "勺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">賓</span>
pub const MDI_VERIFIED: &'static str = "賓";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭔ</span>
pub const MDI_MOVE_RESIZE: &'static str = "ﭔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳸ</span>
pub const MDI_VAN_PASSENGER: &'static str = "ﳸ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴰ</span>
pub const MDI_SHAPE_OUTLINE: &'static str = "ﴰ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_VECTOR_SQUARE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱆ</span>
pub const MDI_PERISCOPE: &'static str = "ﱆ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴲ</span>
pub const MDI_SOCCER_FIELD: &'static str = "ﴲ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">凉</span>
pub const MDI_SEAL: &'static str = "凉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AUDIOBOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">錄</span>
pub const MDI_RAY_END: &'static str = "錄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲠ</span>
pub const MDI_CAMERA_METERING_CENTER: &'static str = "ﲠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">樓</span>
pub const MDI_REDDIT: &'static str = "樓";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯑</span>
pub const MDI_FEATHER: &'static str = "﯑";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_MUSIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PACKAGE_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱉ</span>
pub const MDI_XBOX_CONTROLLER_BATTERY_ALERT: &'static str = "ﱉ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_LEFT_DROP_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALL_MISSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRIBBBLE_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAP_MARKER_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD_PRINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OPERA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲫ</span>
pub const MDI_CARAVAN: &'static str = "ﲫ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">滑</span>
pub const MDI_PINE_TREE: &'static str = "滑";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">諭</span>
pub const MDI_XING_CIRCLE: &'static str = "諭";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOK_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">淋</span>
pub const MDI_TABLET: &'static str = "淋";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_3_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱝ</span>
pub const MDI_VOLUME_MUTE: &'static str = "ﱝ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">繁</span>
pub const MDI_VECTOR_DIFFERENCE: &'static str = "繁";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OIL_TEMPERATURE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_CHARGING_60: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">梨</span>
pub const MDI_SWIM: &'static str = "梨";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FIREFOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">櫓</span>
pub const MDI_QRCODE: &'static str = "櫓";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮘ</span>
pub const MDI_DOUBAN: &'static str = "ﮘ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">飯</span>
pub const MDI_TRAFFIC_LIGHT: &'static str = "飯";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱎ</span>
pub const MDI_XBOX_CONTROLLER_BATTERY_UNKNOWN: &'static str = "ﱎ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱡ</span>
pub const MDI_SQUARE_OUTLINE: &'static str = "ﱡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUETOOTH_SETTINGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">卑</span>
pub const MDI_TRIANGLE: &'static str = "卑";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯁</span>
pub const MDI_ARROW_UP_BOX: &'static str = "﯁";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">爵</span>
pub const MDI_WEB: &'static str = "爵";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">樂</span>
pub const MDI_RESIZE_BOTTOM_RIGHT: &'static str = "樂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲧ</span>
pub const MDI_CAR_HATCHBACK: &'static str = "ﲧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AIRBALLOON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EYE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MARKER_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭭ</span>
pub const MDI_CHART_TIMELINE: &'static str = "ﭭ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIGHTNESS_7: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭏ</span>
pub const MDI_MAP_MARKER_MINUS: &'static str = "ﭏ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬀ</span>
pub const MDI_PAGE_LAST: &'static str = "ﬀ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRAWING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳤ</span>
pub const MDI_PIPE_DISCONNECTED: &'static str = "ﳤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_COLLAPSE_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_INDENT_DECREASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮽</span>
pub const MDI_ANGULARJS: &'static str = "﮽";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOKMARK_MUSIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_REMOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">וֹ</span>
pub const MDI_HUMAN_HANDSUP: &'static str = "וֹ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MATH_COMPASS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_RIGHT_BOLD_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEYBOARD_TAB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEY_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_UP_DROP_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">練</span>
pub const MDI_WINDOW_OPEN: &'static str = "練";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮓ</span>
pub const MDI_COINS: &'static str = "ﮓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ELEVATION_DECLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">牢</span>
pub const MDI_READ: &'static str = "牢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BELL_RING_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">輸</span>
pub const MDI_YOUTUBE_PLAY: &'static str = "輸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GLASS_TULIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳐ</span>
pub const MDI_HOME_CIRCLE: &'static str = "ﳐ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BIBLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FRIDGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">隸</span>
pub const MDI_SOFA: &'static str = "隸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲤ</span>
pub const MDI_CANNABIS: &'static str = "ﲤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬩</span>
pub const MDI_MIXCLOUD: &'static str = "﬩";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GENDER_FEMALE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮷</span>
pub const MDI_NOTE_MULTIPLE_OUTLINE: &'static str = "﮷";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BULLETIN_BOARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSE_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARRANGE_BRING_FORWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫯</span>
pub const MDI_EMAIL_VARIANT: &'static str = "﫯";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱪ</span>
pub const MDI_DECAGRAM: &'static str = "ﱪ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_BLUETOOTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴉ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_40: &'static str = "ﴉ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_CENTER_FOCUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">憎</span>
pub const MDI_WATCH_EXPORT: &'static str = "憎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱒ</span>
pub const MDI_FORMAT_ALIGN_MIDDLE: &'static str = "ﱒ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">㮝</span>
pub const MDI_ACCOUNT_MULTIPLE_MINUS: &'static str = "㮝";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬒</span>
pub const MDI_FILE_HIDDEN: &'static str = "﬒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_ALERT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴶ</span>
pub const MDI_TABLE_SETTINGS: &'static str = "ﴶ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LOGIN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">祉</span>
pub const MDI_UNFOLD_LESS_HORIZONTAL: &'static str = "祉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫰</span>
pub const MDI_EV_STATION: &'static str = "﫰";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰻ</span>
pub const MDI_GESTURE_SWIPE_DOWN: &'static str = "ﰻ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰃ</span>
pub const MDI_PLUS_OUTLINE: &'static str = "ﰃ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOCK_END: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯯ</span>
pub const MDI_MESSAGE_SETTINGS_VARIANT: &'static str = "ﯯ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_PROCESSING_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_REMOVE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LED_VARIANT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯄</span>
pub const MDI_BOOTSTRAP: &'static str = "﯄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BIOHAZARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DETAILS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">論</span>
pub const MDI_RAY_START: &'static str = "論";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GAVEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_10: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_CIRCLES_GROUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮴</span>
pub const MDI_MENU_DOWN_OUTLINE: &'static str = "﮴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CUBE_SEND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MEMORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳀ</span>
pub const MDI_DONKEY: &'static str = "ﳀ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BASKET_UNFILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱋ</span>
pub const MDI_XBOX_CONTROLLER_BATTERY_FULL: &'static str = "ﱋ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DICE_3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_QUESTION_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">祖</span>
pub const MDI_UNTAPPD: &'static str = "祖";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALL_SPLIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_INSIDE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEADPHONES_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PACKAGE_VARIANT_CLOSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DIVISION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTENT_COPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫢</span>
pub const MDI_CHART_BUBBLE: &'static str = "﫢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FONT_AWESOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMOTICON_COOL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GUITAR_PICK_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PANORAMA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">若</span>
pub const MDI_SCREEN_ROTATION: &'static str = "若";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰑ</span>
pub const MDI_SIGNAL_3G: &'static str = "ﰑ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_CIRCLES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MOVIE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MICROPHONE_VARIANT_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">縉</span>
pub const MDI_VECTOR_CURVE: &'static str = "縉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEVRON_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱚ</span>
pub const MDI_TAB_PLUS: &'static str = "ﱚ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_INDENT_INCREASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯓ</span>
pub const MDI_FLASH_OUTLINE: &'static str = "ﯓ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﵅</span>
pub const MDI_YOUTUBE_CREATOR_STUDIO: &'static str = "﵅";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_DRIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴚ</span>
pub const MDI_DOOR_OPEN: &'static str = "ﴚ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳶ</span>
pub const MDI_TRACKPAD: &'static str = "ﳶ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳈ</span>
pub const MDI_FUEL: &'static str = "ﳈ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">祝</span>
pub const MDI_UPLOAD: &'static str = "祝";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MENU_UP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">飼</span>
pub const MDI_TRAIN: &'static str = "飼";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOOTBALL_HELMET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MINUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲚ</span>
pub const MDI_BUS_ARTICULATED_END: &'static str = "ﲚ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮂ</span>
pub const MDI_DNA: &'static str = "ﮂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_INFORMATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DICE_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">諾</span>
pub const MDI_XING_BOX: &'static str = "諾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">墳</span>
pub const MDI_VOLUME_HIGH: &'static str = "墳";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BARREL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_PLUS_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PENCIL_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">來</span>
pub const MDI_PROJECTOR: &'static str = "來";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_NOTE_HALF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮑ</span>
pub const MDI_CAMERA_BURST: &'static str = "ﮑ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲴ</span>
pub const MDI_CLOUD_TAGS: &'static str = "ﲴ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_UP_BOLD_HEXAGON_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮥ</span>
pub const MDI_PUBLISH: &'static str = "ﮥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴡ</span>
pub const MDI_GOLF: &'static str = "ﴡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_PAUSED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲔ</span>
pub const MDI_ARROW_EXPAND_DOWN: &'static str = "ﲔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTACT_MAIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DATABASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHART_AREASPLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DIRECTIONS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEYBOARD_CAPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰦ</span>
pub const MDI_VIEW_PARALLEL: &'static str = "ﰦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">行</span>
pub const MDI_TERRAIN: &'static str = "行";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">輻</span>
pub const MDI_TENT: &'static str = "輻";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">靖</span>
pub const MDI_FORMAT_LINE_WEIGHT: &'static str = "靖";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫸</span>
pub const MDI_INCOGNITO: &'static str = "﫸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴭ</span>
pub const MDI_PHONE_RETURN: &'static str = "ﴭ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_SUPERSCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">廙</span>
pub const MDI_WALLET: &'static str = "廙";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_ALERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">慨</span>
pub const MDI_TSHIRT_CREW: &'static str = "慨";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">塚</span>
pub const MDI_VOICEMAIL: &'static str = "塚";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BANK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LASTFM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PAPERCLIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭤ</span>
pub const MDI_SPRAY: &'static str = "ﭤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯠ</span>
pub const MDI_HOOK: &'static str = "ﯠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_JEEPNEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨧</span>
pub const MDI_TOOLTIP_TEXT: &'static str = "﨧";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOTEL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALBUM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DEBUG_STEP_INTO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LINKEDIN_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">菉</span>
pub const MDI_RASPBERRYPI: &'static str = "菉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">淪</span>
pub const MDI_STEP_FORWARD: &'static str = "淪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CROP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳊ</span>
pub const MDI_GOOGLE_ANALYTICS: &'static str = "ﳊ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_NETWORK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOSPITAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯝ</span>
pub const MDI_HEART_HALF: &'static str = "ﯝ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">瘝</span>
pub const MDI_WHITE_BALANCE_AUTO: &'static str = "瘝";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭨ</span>
pub const MDI_TEXT_SHADOW: &'static str = "ﭨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARRANGE_SEND_TO_BACK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BASKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DATABASE_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳺ</span>
pub const MDI_VANISH: &'static str = "ﳺ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳕ</span>
pub const MDI_LOCKER: &'static str = "ﳕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮇ</span>
pub const MDI_REORDER_VERTICAL: &'static str = "ﮇ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">泌</span>
pub const MDI_ROUTER_WIRELESS: &'static str = "泌";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">華</span>
pub const MDI_WORKER: &'static str = "華";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIGHTNESS_1: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NOTE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRAG_VERTICAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">謁</span>
pub const MDI_XING: &'static str = "謁";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴂ</span>
pub const MDI_ATLASSIAN: &'static str = "ﴂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GRID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_PLUS_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_JSFIDDLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">屮</span>
pub const MDI_TRUCK: &'static str = "屮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰄ</span>
pub const MDI_PRESCRIPTION: &'static str = "ﰄ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">搜</span>
pub const MDI_WATER_PUMP: &'static str = "搜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭦ</span>
pub const MDI_STOP_CIRCLE_OUTLINE: &'static str = "ﭦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GLASSDOOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_VIDEO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮪ</span>
pub const MDI_SUBWAY: &'static str = "ﮪ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_REAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭱ</span>
pub const MDI_LANGUAGE_CPP: &'static str = "ﭱ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳩ</span>
pub const MDI_RING: &'static str = "ﳩ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">燐</span>
pub const MDI_TABLE_COLUMN_WIDTH: &'static str = "燐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫷</span>
pub const MDI_HOME_MAP_MARKER: &'static str = "﫷";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_MULTIPLE_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴁ</span>
pub const MDI_ARTIST: &'static str = "ﴁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LINK_VARIANT_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">靈</span>
pub const MDI_SMOKING: &'static str = "靈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NODEJS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OPENID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">既</span>
pub const MDI_TWITCH: &'static str = "既";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURSOR_POINTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALERT_OCTAGON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOOD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭶ</span>
pub const MDI_BOW_TIE: &'static str = "ﭶ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EXPORT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">祥</span>
pub const MDI_TIMER: &'static str = "祥";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MEDIUM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯛ</span>
pub const MDI_GOOGLE_PHOTOS: &'static str = "ﯛ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BORDER_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭚ</span>
pub const MDI_POT_MIX: &'static str = "ﭚ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORUM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_XML: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">靖</span>
pub const MDI_TIMER_3: &'static str = "靖";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_HANGUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CELLPHONE_IPHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱴ</span>
pub const MDI_PENCIL_CIRCLE_OUTLINE: &'static str = "ﱴ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">流</span>
pub const MDI_WEATHER_SNOWY: &'static str = "流";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CROSSHAIRS_GPS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PENCIL_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_NEARBY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱜ</span>
pub const MDI_VOLUME_MINUS: &'static str = "ﱜ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">禮</span>
pub const MDI_SNOWMAN: &'static str = "禮";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEADPHONES_SETTINGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">綾</span>
pub const MDI_REPEAT_ONCE: &'static str = "綾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">甆</span>
pub const MDI_WHATSAPP: &'static str = "甆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬗ</span>
pub const MDI_BRIDGE: &'static str = "ﬗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODEPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CARROT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NEST_PROTECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">煉</span>
pub const MDI_SETTINGS_BOX: &'static str = "煉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯦ</span>
pub const MDI_LIGHTBULB_ON: &'static str = "ﯦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_ALIGN_JUSTIFY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭒ</span>
pub const MDI_MESSAGE_PLUS: &'static str = "ﭒ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲼ</span>
pub const MDI_CURRENCY_SIGN: &'static str = "ﲼ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">捻</span>
pub const MDI_SILVERWARE_SPOON: &'static str = "捻";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_INVERT_COLORS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ETSY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">敖</span>
pub const MDI_WEATHER_FOG: &'static str = "敖";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_EQUAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_DOWN_BOLD_HEXAGON_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳽ</span>
pub const MDI_XMPP: &'static str = "ﳽ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬋</span>
pub const MDI_SUBDIRECTORY_ARROW_LEFT: &'static str = "﬋";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫳</span>
pub const MDI_FORMAT_TITLE: &'static str = "﫳";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMOTICON_DEVIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_CHARGING_90: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ORNAMENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_IMAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NATURE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳾ</span>
pub const MDI_ACCOUNT_MULTIPLE_PLUS_OUTLINE: &'static str = "ﳾ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">益</span>
pub const MDI_TICKET_CONFIRMATION: &'static str = "益";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NOTE_PLUS_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯩ</span>
pub const MDI_LOOP: &'static str = "ﯩ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲯ</span>
pub const MDI_CHART_LINE_VARIANT: &'static str = "ﲯ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BROOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_FRONT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PERCENT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">領</span>
pub const MDI_SMOKING_OFF: &'static str = "領";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱮ</span>
pub const MDI_FOLDER_OPEN: &'static str = "ﱮ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳷ</span>
pub const MDI_ULTRA_HIGH_DEFINITION: &'static str = "ﳷ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">節</span>
pub const MDI_WINDOW_CLOSED: &'static str = "節";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">層</span>
pub const MDI_TROPHY_VARIANT_OUTLINE: &'static str = "層";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_NOTE_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭡ</span>
pub const MDI_SKIP_NEXT_CIRCLE_OUTLINE: &'static str = "ﭡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">墨</span>
pub const MDI_TROPHY_VARIANT: &'static str = "墨";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EYE_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯪ</span>
pub const MDI_MAGNIFY_MINUS_OUTLINE: &'static str = "ﯪ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_PLAY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">泥</span>
pub const MDI_SWITCH: &'static str = "泥";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">臭</span>
pub const MDI_VECTOR_INTERSECTION: &'static str = "臭";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">履</span>
pub const MDI_SUNGLASSES: &'static str = "履";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">徭</span>
pub const MDI_WALLET_MEMBERSHIP: &'static str = "徭";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MARKDOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">療</span>
pub const MDI_SOURCE_PULL: &'static str = "療";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_TOP_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OCTAGON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯜ</span>
pub const MDI_HEART_HALF_FULL: &'static str = "ﯜ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">奄</span>
pub const MDI_VOLUME_LOW: &'static str = "奄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳔ</span>
pub const MDI_LED_STRIP: &'static str = "ﳔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">裸</span>
pub const MDI_PLAYLIST_REMOVE: &'static str = "裸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AIR_CONDITIONER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">魯</span>
pub const MDI_RADIO_HANDHELD: &'static str = "魯";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">聆</span>
pub const MDI_SLACK: &'static str = "聆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_INSTAGRAM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEY_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_INCREASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOKMARK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HEART_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GAUGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">בֿ</span>
pub const MDI_HUMAN_MALE: &'static str = "בֿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_CHARGING_30: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_MARKED_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰮ</span>
pub const MDI_ARROW_DOWN_BOLD_BOX_OUTLINE: &'static str = "ﰮ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">裡</span>
pub const MDI_TAB: &'static str = "裡";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_UP_BOLD_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">敏</span>
pub const MDI_TUMBLR_REBLOG: &'static str = "敏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">戴</span>
pub const MDI_WATER_OFF: &'static str = "戴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬨ</span>
pub const MDI_METEOR: &'static str = "ﬨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">殺</span>
pub const MDI_WEATHER_RAINY: &'static str = "殺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳿ</span>
pub const MDI_ACCOUNT_PLUS_OUTLINE: &'static str = "ﳿ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮁ</span>
pub const MDI_DELETE_CIRCLE: &'static str = "ﮁ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰒ</span>
pub const MDI_SIGNAL_4G: &'static str = "ﰒ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_STRIKETHROUGH_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_60: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BIKE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_VOIP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">藍</span>
pub const MDI_POUND_BOX: &'static str = "藍";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAP_MARKER_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMAIL_SECURE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">難</span>
pub const MDI_FORMAT_LINE_STYLE: &'static str = "難";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">料</span>
pub const MDI_SORT_VARIANT: &'static str = "料";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲲ</span>
pub const MDI_CHILI_MILD: &'static str = "ﲲ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮬ</span>
pub const MDI_TRANSIT_TRANSFER: &'static str = "ﮬ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_PHYSICAL_WEB: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱭ</span>
pub const MDI_DICE_D10: &'static str = "ﱭ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">辰</span>
pub const MDI_SCALE: &'static str = "辰";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALARM_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASH_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﭅</span>
pub const MDI_FORMAT_ANNOTATION_PLUS: &'static str = "﭅";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">弄</span>
pub const MDI_RAY_START_END: &'static str = "弄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILTER_REMOVE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">寧</span>
pub const MDI_SKIP_BACKWARD: &'static str = "寧";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AIRPLANE_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLIPBOARD_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_ALERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CROP_PORTRAIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_ALIGN_LEFT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">暑</span>
pub const MDI_TWITTER: &'static str = "暑";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫪</span>
pub const MDI_DICE_D4: &'static str = "﫪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲃ</span>
pub const MDI_STICKER_EMOJI: &'static str = "ﲃ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮯ</span>
pub const MDI_WATCH_VIBRATE: &'static str = "ﮯ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰉ</span>
pub const MDI_RHOMBUS: &'static str = "ﰉ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲌ</span>
pub const MDI_ALARM_BELL: &'static str = "ﲌ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_INFORMATION_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯈</span>
pub const MDI_COFFEE_OUTLINE: &'static str = "﯈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮗ</span>
pub const MDI_DO_NOT_DISTURB_OFF: &'static str = "ﮗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲪ</span>
pub const MDI_CAR_SPORTS: &'static str = "ﲪ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILTER_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD_UPLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴦ</span>
pub const MDI_HOT_TUB: &'static str = "ﴦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴄ</span>
pub const MDI_BASKETBALL: &'static str = "ﴄ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BEATS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_8_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮜ</span>
pub const MDI_FORMAT_COLOR_TEXT: &'static str = "ﮜ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIEFCASE_UPLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳯ</span>
pub const MDI_SURROUND_SOUND_3_1: &'static str = "ﳯ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_ACCOUNT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">痢</span>
pub const MDI_SYNC: &'static str = "痢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">זּ</span>
pub const MDI_BOX_SHADOW: &'static str = "זּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">𥳐</span>
pub const MDI_ALTIMETER: &'static str = "𥳐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭽ</span>
pub const MDI_WEATHER_SNOWY_RAINY: &'static str = "ﭽ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳑ</span>
pub const MDI_LANGUAGE_GO: &'static str = "ﳑ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">睊</span>
pub const MDI_WIFI_OFF: &'static str = "睊";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HELP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳘ</span>
pub const MDI_METRONOME: &'static str = "ﳘ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">贈</span>
pub const MDI_VIBRATE: &'static str = "贈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOCK_IN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_PAINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮔ</span>
pub const MDI_CROP_ROTATE: &'static str = "ﮔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫡</span>
pub const MDI_CANDLE: &'static str = "﫡";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">朗</span>
pub const MDI_PRINTER: &'static str = "朗";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭼ</span>
pub const MDI_WEATHER_LIGHTNING_RAINY: &'static str = "ﭼ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLINDS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAKE_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">六</span>
pub const MDI_STAR_OUTLINE: &'static str = "六";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEVRON_DOUBLE_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_0_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DEBUG_STEP_OUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱳ</span>
pub const MDI_OCTAGRAM_OUTLINE: &'static str = "ﱳ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯆</span>
pub const MDI_CLIPBOARD_FLOW: &'static str = "﯆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">宅</span>
pub const MDI_TEMPERATURE_FAHRENHEIT: &'static str = "宅";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰱ</span>
pub const MDI_ARROW_LEFT_BOLD_BOX_OUTLINE: &'static str = "ﰱ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">歷</span>
pub const MDI_SERVER_NETWORK: &'static str = "歷";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲅ</span>
pub const MDI_SWORD_CROSS: &'static str = "ﲅ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">豈</span>
pub const MDI_PIG: &'static str = "豈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">列</span>
pub const MDI_SHUFFLE: &'static str = "列";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CELLPHONE_DOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">凌</span>
pub const MDI_REPEAT: &'static str = "凌";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FUNCTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">度</span>
pub const MDI_TELEVISION: &'static str = "度";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">立</span>
pub const MDI_TABLET_IPAD: &'static str = "立";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">קּ</span>
pub const MDI_GREASE_PENCIL: &'static str = "קּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURRENCY_GBP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">量</span>
pub const MDI_SEAT_LEGROOM_NORMAL: &'static str = "量";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BACKUP_RESTORE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_CHARGING_80: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_2_BOX_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_POWERPOINT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">嗀</span>
pub const MDI_THEME_LIGHT_DARK: &'static str = "嗀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_EXPAND_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">臘</span>
pub const MDI_POWER_SOCKET: &'static str = "臘";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴠ</span>
pub const MDI_FORUM_OUTLINE: &'static str = "ﴠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CART_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭞ</span>
pub const MDI_SHAPE_RECTANGLE_PLUS: &'static str = "ﭞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">窱</span>
pub const MDI_WINDOW_CLOSE: &'static str = "窱";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LANGUAGE_CSS3: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAPTOP_CHROMEBOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_2_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">懲</span>
pub const MDI_TUMBLR: &'static str = "懲";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEVRON_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">杖</span>
pub const MDI_WEATHER_PARTLYCLOUDY: &'static str = "杖";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_MULTIPLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯟ</span>
pub const MDI_HEXAGON_MULTIPLE: &'static str = "ﯟ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DIVISION_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">里</span>
pub const MDI_TAB_UNSELECTED: &'static str = "里";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱦ</span>
pub const MDI_ATOM: &'static str = "ﱦ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LANGUAGE_PYTHON_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲽ</span>
pub const MDI_CURRENCY_TWD: &'static str = "ﲽ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_IN_TALK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_BOLD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_BLACK_WHITE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫤</span>
pub const MDI_CUP_OFF: &'static str = "﫤";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">沈</span>
pub const MDI_SCALE_BATHROOM: &'static str = "沈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">摒</span>
pub const MDI_WEATHER_CLOUDY: &'static str = "摒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_BLANK_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﴾</span>
pub const MDI_VIDEO_INPUT_COMPONENT: &'static str = "﴾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">茶</span>
pub const MDI_TAXI: &'static str = "茶";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHART_BAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSED_CAPTION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱱ</span>
pub const MDI_LOCK_RESET: &'static str = "ﱱ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">憐</span>
pub const MDI_SERVER_PLUS: &'static str = "憐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">廉</span>
pub const MDI_SILVERWARE: &'static str = "廉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_EQUAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">奔</span>
pub const MDI_VOLUME_MEDIUM: &'static str = "奔";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CACHED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_DOWN_DROP_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴺ</span>
pub const MDI_TOWING: &'static str = "ﴺ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHONE_SETTINGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬢ</span>
pub const MDI_FORMAT_VERTICAL_ALIGN_TOP: &'static str = "ﬢ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">羚</span>
pub const MDI_SKYPE_BUSINESS: &'static str = "羚";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">益</span>
pub const MDI_WHITE_BALANCE_IRIDESCENT: &'static str = "益";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">祈</span>
pub const MDI_UNFOLD_MORE_HORIZONTAL: &'static str = "祈";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GHOST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ODNOKLASSNIKI: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲨ</span>
pub const MDI_CAR_PICKUP: &'static str = "ﲨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAGNIFY_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴤ</span>
pub const MDI_HOME_ACCOUNT: &'static str = "ﴤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">廊</span>
pub const MDI_PRESENTATION_PLAY: &'static str = "廊";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALL_MERGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﵄</span>
pub const MDI_XAMARIN_OUTLINE: &'static str = "﵄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOUD_OFF_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_1_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">慄</span>
pub const MDI_STOCKING: &'static str = "慄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫿</span>
pub const MDI_PAGE_FIRST: &'static str = "﫿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_PARENTHESES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_90: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬔ</span>
pub const MDI_ARROW_COLLAPSE: &'static str = "ﬔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">喝</span>
pub const MDI_VK: &'static str = "喝";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">בּ</span>
pub const MDI_APPLE_KEYBOARD_CAPS: &'static str = "בּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_6: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_30: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LINK_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲄ</span>
pub const MDI_SUMMIT: &'static str = "ﲄ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲾ</span>
pub const MDI_DESKTOP_CLASSIC: &'static str = "ﲾ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CISCO_WEBEX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">淚</span>
pub const MDI_REDO: &'static str = "淚";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LANGUAGE_HTML5: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">瑩</span>
pub const MDI_SKYPE: &'static str = "瑩";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">年</span>
pub const MDI_SERVER_OFF: &'static str = "年";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BITBUCKET: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">啕</span>
pub const MDI_VK_BOX: &'static str = "啕";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ESCALATOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鈴</span>
pub const MDI_SLEEP: &'static str = "鈴";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮋ</span>
pub const MDI_SOLID: &'static str = "ﮋ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">凜</span>
pub const MDI_RENAME_BOX: &'static str = "凜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_HDR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">寧</span>
pub const MDI_RIBBON: &'static str = "寧";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬄ</span>
pub const MDI_QQCHAT: &'static str = "ﬄ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMOTICON_HAPPY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">陵</span>
pub const MDI_REPLY: &'static str = "陵";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴏ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_ALERT: &'static str = "ﴏ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAGNIFY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AMPLIFIER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">禎</span>
pub const MDI_VECTOR_ARRANGE_ABOVE: &'static str = "禎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">覆</span>
pub const MDI_WUNDERLIST: &'static str = "覆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ANDROID_DEBUG_BRIDGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">陸</span>
pub const MDI_STEERING: &'static str = "陸";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯉</span>
pub const MDI_CONTACTS: &'static str = "﯉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﵀</span>
pub const MDI_VIDEO_INPUT_SVIDEO: &'static str = "﵀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰣ</span>
pub const MDI_TILDE: &'static str = "ﰣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_TAGS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯮ</span>
pub const MDI_MESSAGE_SETTINGS: &'static str = "ﯮ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">缾</span>
pub const MDI_WINDOW_RESTORE: &'static str = "缾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_HEADER_POUND: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">侀</span>
pub const MDI_VIEW_QUILT: &'static str = "侀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">龜</span>
pub const MDI_PINTEREST_BOX: &'static str = "龜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PACKAGE_DOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱂ</span>
pub const MDI_HUMBLE_BUNDLE: &'static str = "ﱂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳗ</span>
pub const MDI_MAP_MARKER_OUTLINE: &'static str = "ﳗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CASH_USD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬽</span>
pub const MDI_CHECKBOX_MULTIPLE_MARKED_CIRCLE_OUTLINE: &'static str = "﬽";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURSOR_MOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_IMAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">拓</span>
pub const MDI_TELEVISION_GUIDE: &'static str = "拓";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲰ</span>
pub const MDI_CHILI_HOT: &'static str = "ﲰ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_UNKNOWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSE_NETWORK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱐ</span>
pub const MDI_FILE_PLUS: &'static str = "ﱐ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_ITALIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮶</span>
pub const MDI_NOTE_MULTIPLE: &'static str = "﮶";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LANGUAGE_CSHARP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PAUSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">糧</span>
pub const MDI_SEAT_FLAT_ANGLED: &'static str = "糧";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬆ</span>
pub const MDI_ROUNDED_CORNER: &'static str = "ﬆ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEMICAL_WEAPON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_MULTIPLE_BLANK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯧ</span>
pub const MDI_LIGHTBULB_ON_OUTLINE: &'static str = "ﯧ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">直</span>
pub const MDI_WIFI: &'static str = "直";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAXCDN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯳ</span>
pub const MDI_HELP_NETWORK: &'static str = "ﯳ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOSPITAL_BUILDING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOKMARK_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">着</span>
pub const MDI_WII: &'static str = "着";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨣</span>
pub const MDI_TOOLTIP_EDIT: &'static str = "﨣";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬛</span>
pub const MDI_DIALPAD: &'static str = "﬛";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">יּ</span>
pub const MDI_CARDS_PLAYING_OUTLINE: &'static str = "יּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">喝</span>
pub const MDI_TRIANGLE_OUTLINE: &'static str = "喝";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬊</span>
pub const MDI_STACKEXCHANGE: &'static str = "﬊";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰡ</span>
pub const MDI_TAG_REMOVE: &'static str = "ﰡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳴ</span>
pub const MDI_THOUGHT_BUBBLE: &'static str = "ﳴ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">嗢</span>
pub const MDI_VLC: &'static str = "嗢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CROP_LANDSCAPE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LUMX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬡ</span>
pub const MDI_FORMAT_VERTICAL_ALIGN_CENTER: &'static str = "ﬡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MAIL_RU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭯ</span>
pub const MDI_FILE_RESTORE: &'static str = "ﭯ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭣ</span>
pub const MDI_SKIP_PREVIOUS_CIRCLE_OUTLINE: &'static str = "ﭣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯣ</span>
pub const MDI_LANGUAGE_SWIFT: &'static str = "ﯣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_5_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">嶺</span>
pub const MDI_SKIP_FORWARD: &'static str = "嶺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HOSPITAL_MARKER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOOTBALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">亂</span>
pub const MDI_POCKET: &'static str = "亂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EYEDROPPER_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫹</span>
pub const MDI_KETTLE: &'static str = "﫹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ENGINE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰖ</span>
pub const MDI_SOURCE_COMMIT: &'static str = "ﰖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARRANGE_SEND_BACKWARD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_APPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">藺</span>
pub const MDI_TABLE_LARGE: &'static str = "藺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬉</span>
pub const MDI_SIGNAL_VARIANT: &'static str = "﬉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MENU_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_AREA_CLOSE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MINUS_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">充</span>
pub const MDI_VIEW_STREAM: &'static str = "充";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_FRONT_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_RIGHT_BOLD_HEXAGON_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴃ</span>
pub const MDI_AZURE: &'static str = "ﴃ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIGHTNESS_2: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">欄</span>
pub const MDI_POLAROID: &'static str = "欄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴀ</span>
pub const MDI_ALLO: &'static str = "ﴀ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_ACCOUNT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_ENHANCE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">老</span>
pub const MDI_QUALITY_HIGH: &'static str = "老";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIEFCASE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTENT_PASTE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CASTLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FOLDER_LOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_CHARGING_40: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬜</span>
pub const MDI_DICTIONARY: &'static str = "﬜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱲ</span>
pub const MDI_NINJA: &'static str = "ﱲ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ERASER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">籠</span>
pub const MDI_RAY_VERTEX: &'static str = "籠";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOX_CUTTER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GOOGLE_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯻ</span>
pub const MDI_PAGE_LAYOUT_SIDEBAR_LEFT: &'static str = "ﯻ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_7_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">林</span>
pub const MDI_TABLE_ROW_REMOVE: &'static str = "林";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰜ</span>
pub const MDI_SOURCE_COMMIT_START_NEXT_LOCAL: &'static str = "ﰜ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">爫</span>
pub const MDI_UMBRELLA: &'static str = "爫";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯥ</span>
pub const MDI_LAPTOP_OFF: &'static str = "ﯥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">𢡄</span>
pub const MDI_SCALE_BALANCE: &'static str = "𢡄";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">數</span>
pub const MDI_ROUTES: &'static str = "數";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">署</span>
pub const MDI_VECTOR_DIFFERENCE_AB: &'static str = "署";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DELETE_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_DOWN_BOLD_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">蘆</span>
pub const MDI_QUICKTIME: &'static str = "蘆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BASKET_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">率</span>
pub const MDI_STORE: &'static str = "率";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫩</span>
pub const MDI_DICE_D20: &'static str = "﫩";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GAS_STATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">路</span>
pub const MDI_RADIATOR: &'static str = "路";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PENCIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAR_WASH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOK_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR_CLOCK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">獵</span>
pub const MDI_SIM_ALERT: &'static str = "獵";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">שּ</span>
pub const MDI_HUMAN_GREETING: &'static str = "שּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">慠</span>
pub const MDI_WATCH_IMPORT: &'static str = "慠";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">蝹</span>
pub const MDI_WRAP: &'static str = "蝹";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳛ</span>
pub const MDI_MIXER: &'static str = "ﳛ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">降</span>
pub const MDI_TEXT_TO_SPEECH: &'static str = "降";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALL_RECEIVED: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CELLPHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">異</span>
pub const MDI_ROCKET: &'static str = "異";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲟ</span>
pub const MDI_CAMERA_GOPRO: &'static str = "ﲟ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲇ</span>
pub const MDI_YAMMER: &'static str = "ﲇ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GITHUB_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CUP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰰ</span>
pub const MDI_ARROW_LEFT_BOLD_BOX: &'static str = "ﰰ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTRAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GLASSES: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">咽</span>
pub const MDI_SHUFFLE_VARIANT: &'static str = "咽";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DNS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOCK_OUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_SIZE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱬ</span>
pub const MDI_DICE_MULTIPLE: &'static str = "ﱬ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ANCHOR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EXIT_TO_APP: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_BLANK_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CURRENCY_TRY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_PROCESSING: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">爐</span>
pub const MDI_QRCODE_SCAN: &'static str = "爐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬐</span>
pub const MDI_WECHAT: &'static str = "﬐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">請</span>
pub const MDI_XDA: &'static str = "請";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲥ</span>
pub const MDI_CAR_CONVERTIBLE: &'static str = "ﲥ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">盧</span>
pub const MDI_QUADCOPTER: &'static str = "盧";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮖ</span>
pub const MDI_DO_NOT_DISTURB: &'static str = "ﮖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴷ</span>
pub const MDI_TELEVISION_BOX: &'static str = "ﴷ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫻</span>
pub const MDI_LOGIN_VARIANT: &'static str = "﫻";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LANGUAGE_JAVASCRIPT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭰ</span>
pub const MDI_LANGUAGE_C: &'static str = "ﭰ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴐ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_OUTLINE: &'static str = "ﴐ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">𥉉</span>
pub const MDI_ALERT_CIRCLE_OUTLINE: &'static str = "𥉉";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHEVRON_DOUBLE_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GLASS_MUG: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMERA_IRIS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯗ</span>
pub const MDI_GARAGE: &'static str = "ﯗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_SEARCH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BIO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">濫</span>
pub const MDI_POUND: &'static str = "濫";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">更</span>
pub const MDI_PILL: &'static str = "更";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">朗</span>
pub const MDI_WEATHER_LIGHTNING: &'static str = "朗";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OIL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRUPAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">טּ</span>
pub const MDI_CARDS_OUTLINE: &'static str = "טּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EDGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">謁</span>
pub const MDI_VECTOR_TRIANGLE: &'static str = "謁";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰵ</span>
pub const MDI_ARROW_UP_BOLD: &'static str = "ﰵ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">塞</span>
pub const MDI_RULER: &'static str = "塞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫺</span>
pub const MDI_LOCK_PLUS: &'static str = "﫺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴗ</span>
pub const MDI_DELETE_RESTORE: &'static str = "ﴗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴅ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS: &'static str = "ﴅ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨩</span>
pub const MDI_TOR: &'static str = "﨩";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮤ</span>
pub const MDI_POWER_PLUG_OFF: &'static str = "ﮤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUETOOTH_CONNECT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GNOME: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_GUITAR_PICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">露</span>
pub const MDI_RADIO: &'static str = "露";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">遲</span>
pub const MDI_ZIP_BOX: &'static str = "遲";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">響</span>
pub const MDI_VOICE: &'static str = "響";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_MULTIPLE_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">冀</span>
pub const MDI_VIEW_WEEK: &'static str = "冀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳏ</span>
pub const MDI_HOME_AUTOMATION: &'static str = "ﳏ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIEFCASE_CHECK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_NOTE_SIXTEENTH: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COMMENT_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASK_EMPTY_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLIP_TO_BACK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱤ</span>
pub const MDI_CIRCLE_OUTLINE: &'static str = "ﱤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴻ</span>
pub const MDI_UPLOAD_MULTIPLE: &'static str = "ﴻ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭑ</span>
pub const MDI_MARKER: &'static str = "ﭑ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTENT_CUT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫞</span>
pub const MDI_CAMERA_OFF: &'static str = "﫞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳞ</span>
pub const MDI_MUSHROOM_OUTLINE: &'static str = "ﳞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_HAND_POINTING_RIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LAYERS_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MESSAGE_REPLY_TEXT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAR_BATTERY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲺ</span>
pub const MDI_CURRENCY_JPY: &'static str = "ﲺ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PAUSE_CIRCLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">狀</span>
pub const MDI_TAG_MULTIPLE: &'static str = "狀";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PACKAGE_VARIANT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰗ</span>
pub const MDI_SOURCE_COMMIT_END: &'static str = "ﰗ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴸ</span>
pub const MDI_TELEVISION_CLASSIC_OFF: &'static str = "ﴸ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴒ</span>
pub const MDI_BRIEFCASE_OUTLINE: &'static str = "ﴒ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲣ</span>
pub const MDI_CAMERA_METERING_SPOT: &'static str = "ﲣ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">蘿</span>
pub const MDI_PLAYLIST_PLAY: &'static str = "蘿";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_EXCEL_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CREDIT_CARD_MULTIPLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬏</span>
pub const MDI_VISUALSTUDIO: &'static str = "﬏";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DRAG_HORIZONTAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">樂</span>
pub const MDI_SOUNDCLOUD: &'static str = "樂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DELTA: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">梅</span>
pub const MDI_TWITTER_BOX: &'static str = "梅";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯨ</span>
pub const MDI_LOCK_PATTERN: &'static str = "ﯨ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴔ</span>
pub const MDI_CLOVER: &'static str = "ﴔ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱶ</span>
pub const MDI_SET_ALL: &'static str = "ﱶ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLIP_TO_FRONT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">נּ</span>
pub const MDI_DIRECTIONS_FORK: &'static str = "נּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭴ</span>
pub const MDI_CREDIT_CARD_PLUS: &'static str = "ﭴ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳟ</span>
pub const MDI_NINTENDO_SWITCH: &'static str = "ﳟ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_AUTORENEW: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">識</span>
pub const MDI_TAG_TEXT_OUTLINE: &'static str = "識";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ARROW_LEFT_THICK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">漢</span>
pub const MDI_WEATHER_SUNSET_DOWN: &'static str = "漢";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﭲ</span>
pub const MDI_XAML: &'static str = "ﭲ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">全</span>
pub const MDI_VIEW_MODULE: &'static str = "全";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ANDROID: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOCK_FAST: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">嘆</span>
pub const MDI_TROPHY: &'static str = "嘆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">肋</span>
pub const MDI_REMOTE: &'static str = "肋";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CREDIT_CARD_SCAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰀ</span>
pub const MDI_PILLAR: &'static str = "ﰀ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DEVIANTART: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">璘</span>
pub const MDI_TABLE_EDIT: &'static str = "璘";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">龎</span>
pub const MDI_BOOK_OPEN_PAGE_VARIANT: &'static str = "龎";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲂ</span>
pub const MDI_SQUARE_ROOT: &'static str = "ﲂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">說</span>
pub const MDI_SATELLITE: &'static str = "說";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CAMCORDER_BOX_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰕ</span>
pub const MDI_SNOWFLAKE: &'static str = "ﰕ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BOOK: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LEAF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ACCOUNT_LOCATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LOCK_OPEN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">頻</span>
pub const MDI_VIEW_ARRAY: &'static str = "頻";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MOTORBIKE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CART_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FILE_WORD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_EMOTICON_NEUTRAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_COFFEE_TO_GO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_3_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NAVIGATION: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳬ</span>
pub const MDI_SOY_SAUCE: &'static str = "ﳬ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">簾</span>
pub const MDI_SIM: &'static str = "簾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">瞧</span>
pub const MDI_WEATHER_WINDY_VARIANT: &'static str = "瞧";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LOCK_OPEN_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">女</span>
pub const MDI_SEAT_RECLINE_NORMAL: &'static str = "女";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">隆</span>
pub const MDI_STORE_24_HOUR: &'static str = "隆";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">猪</span>
pub const MDI_WEIGHT: &'static str = "猪";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BATTERY_NEGATIVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲉ</span>
pub const MDI_HELP_BOX: &'static str = "ﲉ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_WRAP_TOP_BOTTOM: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NOTIFICATION_CLEAR_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_2_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">說</span>
pub const MDI_SIGNAL: &'static str = "說";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_IMAGE_FILTER_VINTAGE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">凞</span>
pub const MDI_TICKET: &'static str = "凞";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬠ</span>
pub const MDI_FORMAT_VERTICAL_ALIGN_BOTTOM: &'static str = "ﬠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰇ</span>
pub const MDI_RESTART: &'static str = "ﰇ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_TEXTDIRECTION_R_TO_L: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫵</span>
pub const MDI_HEART_PULSE: &'static str = "﫵";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CODE_LESS_THAN_OR_EQUAL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">雷</span>
pub const MDI_RECORD: &'static str = "雷";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">易</span>
pub const MDI_SWAP_HORIZONTAL: &'static str = "易";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">賂</span>
pub const MDI_RECEIPT: &'static str = "賂";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮾</span>
pub const MDI_ARROW_DOWN_BOX: &'static str = "﮾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯊</span>
pub const MDI_DELETE_EMPTY: &'static str = "﯊";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲆ</span>
pub const MDI_TRUCK_FAST: &'static str = "ﲆ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CROWN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CONTENT_SAVE_ALL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_INTERNET_EXPLORER: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﨔</span>
pub const MDI_THUMBS_UP_DOWN: &'static str = "﨔";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﫥</span>
pub const MDI_COPYRIGHT: &'static str = "﫥";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲳ</span>
pub const MDI_CLOUD_BRACES: &'static str = "ﲳ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASHLIGHT_OFF: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">𧻓</span>
pub const MDI_ANIMATION: &'static str = "𧻓";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">אּ</span>
pub const MDI_ACCOUNT_SETTINGS_VARIANT: &'static str = "אּ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﯐</span>
pub const MDI_FAST_FORWARD_OUTLINE: &'static str = "﯐";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">勒</span>
pub const MDI_RELOAD: &'static str = "勒";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FLASHLIGHT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_LIBRARY_BOOKS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CALENDAR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_OWL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯺ</span>
pub const MDI_PAGE_LAYOUT_HEADER: &'static str = "ﯺ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NFC: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ANDROID_STUDIO: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_NUMERIC_0_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">鷺</span>
pub const MDI_RADIO_TOWER: &'static str = "鷺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯞ</span>
pub const MDI_HEART_HALF_OUTLINE: &'static str = "ﯞ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BLUR: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MULTIPLICATION_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰏ</span>
pub const MDI_SHOVEL_OFF: &'static str = "ﰏ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲩ</span>
pub const MDI_CAR_SIDE: &'static str = "ﲩ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳌ</span>
pub const MDI_HEADPHONES_OFF: &'static str = "ﳌ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">復</span>
pub const MDI_ROTATE_RIGHT: &'static str = "復";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱷ</span>
pub const MDI_SET_CENTER: &'static str = "ﱷ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳖ</span>
pub const MDI_LOCKER_MULTIPLE: &'static str = "ﳖ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﲈ</span>
pub const MDI_CAST_OFF: &'static str = "ﲈ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﱠ</span>
pub const MDI_TACO: &'static str = "ﱠ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﮻</span>
pub const MDI_ALERT_DECAGRAM: &'static str = "﮻";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CHECKBOX_BLANK_CIRCLE_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﯡ</span>
pub const MDI_HOOK_OFF: &'static str = "ﯡ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﳚ</span>
pub const MDI_MICRO_SD: &'static str = "ﳚ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴋ</span>
pub const MDI_BATTERY_CHARGING_WIRELESS_60: &'static str = "ﴋ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_KEY_REMOVE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰈ</span>
pub const MDI_REWIND_OUTLINE: &'static str = "ﰈ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">聾</span>
pub const MDI_LASTPASS: &'static str = "聾";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮝ</span>
pub const MDI_FORMAT_SECTION: &'static str = "ﮝ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">שּׂ</span>
pub const MDI_TUNE: &'static str = "שּׂ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">離</span>
pub const MDI_TABLE: &'static str = "離";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PHARMACY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FACTORY: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DISK_ALERT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">﬙</span>
pub const MDI_CHIP: &'static str = "﬙";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_DESKPHONE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_ALARM_PLUS: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FULLSCREEN_EXIT: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MUSIC_NOTE_WHOLE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BARCODE_SCAN: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PAUSE_OCTAGON: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">溜</span>
pub const MDI_STACK_OVERFLOW: &'static str = "溜";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">廓</span>
pub const MDI_TEXTURE: &'static str = "廓";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">冷</span>
pub const MDI_PROJECTOR_SCREEN: &'static str = "冷";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﰤ</span>
pub const MDI_TREASURE_CHEST: &'static str = "ﰤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">殺</span>
pub const MDI_SATELLITE_VARIANT: &'static str = "殺";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﬤ</span>
pub const MDI_HELP_CIRCLE_OUTLINE: &'static str = "ﬤ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">恵</span>
pub const MDI_VIEW_CAROUSEL: &'static str = "恵";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_CLOSE_BOX: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_FORMAT_COLOR_FILL: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_PENCIL_BOX_OUTLINE: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_BRIEFCASE_DOWNLOAD: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮒ</span>
pub const MDI_CODE_TAGS_CHECK: &'static str = "ﮒ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;"></span>
pub const MDI_MENU: &'static str = "";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﴴ</span>
pub const MDI_TABLE_OF_CONTENTS: &'static str = "ﴴ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">ﮰ</span>
pub const MDI_ANGULAR: &'static str = "ﮰ";

/// <span style="font-family: 'MesloLGS NF'; font-size: 2em;">渚</span>
pub const MDI_TWITTER_RETWEET: &'static str = "渚";

#[cfg(feature = "search")]
use phf::phf_map;

// Names are based on the Cheat-Sheet (https://www.nerdfonts.com/cheat-sheet)
#[cfg(feature = "search")]
pub const MDI_MAPPING: phf::Map<&'static str, &'static str> = phf_map! {
    "nf-mdi-bookmark-outline" => MDI_BOOKMARK_OUTLINE,
    "nf-mdi-format-float-right" => MDI_FORMAT_FLOAT_RIGHT,
    "nf-mdi-pin-off" => MDI_PIN_OFF,
    "nf-mdi-vector-arrange-below" => MDI_VECTOR_ARRANGE_BELOW,
    "nf-mdi-currency-usd" => MDI_CURRENCY_USD,
    "nf-mdi-message-processing" => MDI_MESSAGE_PROCESSING,
    "nf-mdi-comment-text-outline" => MDI_COMMENT_TEXT_OUTLINE,
    "nf-mdi-cube-outline" => MDI_CUBE_OUTLINE,
    "nf-mdi-source-commit-next-local" => MDI_SOURCE_COMMIT_NEXT_LOCAL,
    "nf-mdi-heart-box" => MDI_HEART_BOX,
    "nf-mdi-account-box-outline" => MDI_ACCOUNT_BOX_OUTLINE,
    "nf-mdi-tree" => MDI_TREE,
    "nf-mdi-ice-cream" => MDI_ICE_CREAM,
    "nf-mdi-stop-circle" => MDI_STOP_CIRCLE,
    "nf-mdi-counter" => MDI_COUNTER,
    "nf-mdi-tablet-android" => MDI_TABLET_ANDROID,
    "nf-mdi-arrow-right" => MDI_ARROW_RIGHT,
    "nf-mdi-collage" => MDI_COLLAGE,
    "nf-mdi-gamepad" => MDI_GAMEPAD,
    "nf-mdi-content-save" => MDI_CONTENT_SAVE,
    "nf-mdi-image-broken" => MDI_IMAGE_BROKEN,
    "nf-mdi-star" => MDI_STAR,
    "nf-mdi-cash" => MDI_CASH,
    "nf-mdi-tower-fire" => MDI_TOWER_FIRE,
    "nf-mdi-bluetooth-transfer" => MDI_BLUETOOTH_TRANSFER,
    "nf-mdi-shape-circle-plus" => MDI_SHAPE_CIRCLE_PLUS,
    "nf-mdi-alpha" => MDI_ALPHA,
    "nf-mdi-file-chart" => MDI_FILE_CHART,
    "nf-mdi-microphone-variant" => MDI_MICROPHONE_VARIANT,
    "nf-mdi-select-off" => MDI_SELECT_OFF,
    "nf-mdi-file-pdf" => MDI_FILE_PDF,
    "nf-mdi-bandcamp" => MDI_BANDCAMP,
    "nf-mdi-home-assistant" => MDI_HOME_ASSISTANT,
    "nf-mdi-decimal-decrease" => MDI_DECIMAL_DECREASE,
    "nf-mdi-responsive" => MDI_RESPONSIVE,
    "nf-mdi-camera-metering-matrix" => MDI_CAMERA_METERING_MATRIX,
    "nf-mdi-face" => MDI_FACE,
    "nf-mdi-shopping" => MDI_SHOPPING,
    "nf-mdi-code-brackets" => MDI_CODE_BRACKETS,
    "nf-mdi-chart-pie" => MDI_CHART_PIE,
    "nf-mdi-fingerprint" => MDI_FINGERPRINT,
    "nf-mdi-kodi" => MDI_KODI,
    "nf-mdi-forward" => MDI_FORWARD,
    "nf-mdi-note-plus" => MDI_NOTE_PLUS,
    "nf-mdi-shape-plus" => MDI_SHAPE_PLUS,
    "nf-mdi-silverware-fork" => MDI_SILVERWARE_FORK,
    "nf-mdi-vector-point" => MDI_VECTOR_POINT,
    "nf-mdi-checkbox-multiple-marked-circle" => MDI_CHECKBOX_MULTIPLE_MARKED_CIRCLE,
    "nf-mdi-update" => MDI_UPDATE,
    "nf-mdi-file-powerpoint-box" => MDI_FILE_POWERPOINT_BOX,
    "nf-mdi-format-quote-close" => MDI_FORMAT_QUOTE_CLOSE,
    "nf-mdi-dropbox" => MDI_DROPBOX,
    "nf-mdi-security-network" => MDI_SECURITY_NETWORK,
    "nf-mdi-coin" => MDI_COIN,
    "nf-mdi-apple-mobileme" => MDI_APPLE_MOBILEME,
    "nf-mdi-checkbox-multiple-marked" => MDI_CHECKBOX_MULTIPLE_MARKED,
    "nf-mdi-relative-scale" => MDI_RELATIVE_SCALE,
    "nf-mdi-bowling" => MDI_BOWLING,
    "nf-mdi-television-off" => MDI_TELEVISION_OFF,
    "nf-mdi-account-settings" => MDI_ACCOUNT_SETTINGS,
    "nf-mdi-layers" => MDI_LAYERS,
    "nf-mdi-credit-card-off" => MDI_CREDIT_CARD_OFF,
    "nf-mdi-format-strikethrough" => MDI_FORMAT_STRIKETHROUGH,
    "nf-mdi-playlist-check" => MDI_PLAYLIST_CHECK,
    "nf-mdi-source-commit-local" => MDI_SOURCE_COMMIT_LOCAL,
    "nf-mdi-watch" => MDI_WATCH,
    "nf-mdi-weather-sunny" => MDI_WEATHER_SUNNY,
    "nf-mdi-playlist-plus" => MDI_PLAYLIST_PLUS,
    "nf-mdi-plus-circle-outline" => MDI_PLUS_CIRCLE_OUTLINE,
    "nf-mdi-format-clear" => MDI_FORMAT_CLEAR,
    "nf-mdi-robot" => MDI_ROBOT,
    "nf-mdi-dolby" => MDI_DOLBY,
    "nf-mdi-poll" => MDI_POLL,
    "nf-mdi-code-string" => MDI_CODE_STRING,
    "nf-mdi-subway-variant" => MDI_SUBWAY_VARIANT,
    "nf-mdi-ambulance" => MDI_AMBULANCE,
    "nf-mdi-camera-party-mode" => MDI_CAMERA_PARTY_MODE,
    "nf-mdi-hamburger" => MDI_HAMBURGER,
    "nf-mdi-chart-histogram" => MDI_CHART_HISTOGRAM,
    "nf-mdi-blur-linear" => MDI_BLUR_LINEAR,
    "nf-mdi-chart-bar-stacked" => MDI_CHART_BAR_STACKED,
    "nf-mdi-music-note-quarter" => MDI_MUSIC_NOTE_QUARTER,
    "nf-mdi-page-layout-footer" => MDI_PAGE_LAYOUT_FOOTER,
    "nf-mdi-arrow-up-drop-circle-outline" => MDI_ARROW_UP_DROP_CIRCLE_OUTLINE,
    "nf-mdi-camcorder-off" => MDI_CAMCORDER_OFF,
    "nf-mdi-seat-flat" => MDI_SEAT_FLAT,
    "nf-mdi-soccer" => MDI_SOCCER,
    "nf-mdi-arrow-expand-up" => MDI_ARROW_EXPAND_UP,
    "nf-mdi-fast-forward" => MDI_FAST_FORWARD,
    "nf-mdi-battery-70" => MDI_BATTERY_70,
    "nf-mdi-timer-off" => MDI_TIMER_OFF,
    "nf-mdi-pentagon-outline" => MDI_PENTAGON_OUTLINE,
    "nf-mdi-unfold-less-vertical" => MDI_UNFOLD_LESS_VERTICAL,
    "nf-mdi-email-open-outline" => MDI_EMAIL_OPEN_OUTLINE,
    "nf-mdi-led-variant-on" => MDI_LED_VARIANT_ON,
    "nf-mdi-cancel" => MDI_CANCEL,
    "nf-mdi-table-column-plus-before" => MDI_TABLE_COLUMN_PLUS_BEFORE,
    "nf-mdi-file-video" => MDI_FILE_VIDEO,
    "nf-mdi-radar" => MDI_RADAR,
    "nf-mdi-cast" => MDI_CAST,
    "nf-mdi-format-wrap-square" => MDI_FORMAT_WRAP_SQUARE,
    "nf-mdi-history" => MDI_HISTORY,
    "nf-mdi-format-page-break" => MDI_FORMAT_PAGE_BREAK,
    "nf-mdi-rotate-right-variant" => MDI_ROTATE_RIGHT_VARIANT,
    "nf-mdi-printer-settings" => MDI_PRINTER_SETTINGS,
    "nf-mdi-pencil-off" => MDI_PENCIL_OFF,
    "nf-mdi-shopping-music" => MDI_SHOPPING_MUSIC,
    "nf-mdi-human-child" => MDI_HUMAN_CHILD,
    "nf-mdi-border-color" => MDI_BORDER_COLOR,
    "nf-mdi-shape-square-plus" => MDI_SHAPE_SQUARE_PLUS,
    "nf-mdi-plus-box" => MDI_PLUS_BOX,
    "nf-mdi-source-commit-start" => MDI_SOURCE_COMMIT_START,
    "nf-mdi-battery-50" => MDI_BATTERY_50,
    "nf-mdi-label" => MDI_LABEL,
    "nf-mdi-format-header-5" => MDI_FORMAT_HEADER_5,
    "nf-mdi-screwdriver" => MDI_SCREWDRIVER,
    "nf-mdi-border-none" => MDI_BORDER_NONE,
    "nf-mdi-run-fast" => MDI_RUN_FAST,
    "nf-mdi-numeric-7-box-multiple-outline" => MDI_NUMERIC_7_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-message-image" => MDI_MESSAGE_IMAGE,
    "nf-mdi-apple-keyboard-shift" => MDI_APPLE_KEYBOARD_SHIFT,
    "nf-mdi-poll-box" => MDI_POLL_BOX,
    "nf-mdi-phone-log" => MDI_PHONE_LOG,
    "nf-mdi-undo" => MDI_UNDO,
    "nf-mdi-currency-cny" => MDI_CURRENCY_CNY,
    "nf-mdi-folder-lock-open" => MDI_FOLDER_LOCK_OPEN,
    "nf-mdi-umbraco" => MDI_UMBRACO,
    "nf-mdi-contrast-box" => MDI_CONTRAST_BOX,
    "nf-mdi-step-backward" => MDI_STEP_BACKWARD,
    "nf-mdi-camcorder" => MDI_CAMCORDER,
    "nf-mdi-professional-hexagon" => MDI_PROFESSIONAL_HEXAGON,
    "nf-mdi-delete" => MDI_DELETE,
    "nf-mdi-youtube-gaming" => MDI_YOUTUBE_GAMING,
    "nf-mdi-currency-rub" => MDI_CURRENCY_RUB,
    "nf-mdi-film" => MDI_FILM,
    "nf-mdi-octagram" => MDI_OCTAGRAM,
    "nf-mdi-lightbulb" => MDI_LIGHTBULB,
    "nf-mdi-ray-end-arrow" => MDI_RAY_END_ARROW,
    "nf-mdi-xbox-controller-off" => MDI_XBOX_CONTROLLER_OFF,
    "nf-mdi-phone-locked" => MDI_PHONE_LOCKED,
    "nf-mdi-sync-off" => MDI_SYNC_OFF,
    "nf-mdi-arrow-right-bold" => MDI_ARROW_RIGHT_BOLD,
    "nf-mdi-paper-cut-vertical" => MDI_PAPER_CUT_VERTICAL,
    "nf-mdi-fish" => MDI_FISH,
    "nf-mdi-monitor" => MDI_MONITOR,
    "nf-mdi-gender-transgender" => MDI_GENDER_TRANSGENDER,
    "nf-mdi-page-layout-body" => MDI_PAGE_LAYOUT_BODY,
    "nf-mdi-selection-off" => MDI_SELECTION_OFF,
    "nf-mdi-close-octagon" => MDI_CLOSE_OCTAGON,
    "nf-mdi-android-head" => MDI_ANDROID_HEAD,
    "nf-mdi-debug-step-over" => MDI_DEBUG_STEP_OVER,
    "nf-mdi-approval" => MDI_APPROVAL,
    "nf-mdi-cellphone-wireless" => MDI_CELLPHONE_WIRELESS,
    "nf-mdi-border-all" => MDI_BORDER_ALL,
    "nf-mdi-account-convert" => MDI_ACCOUNT_CONVERT,
    "nf-mdi-toggle-switch" => MDI_TOGGLE_SWITCH,
    "nf-mdi-calendar-check" => MDI_CALENDAR_CHECK,
    "nf-mdi-buffer" => MDI_BUFFER,
    "nf-mdi-tie" => MDI_TIE,
    "nf-mdi-disqus" => MDI_DISQUS,
    "nf-mdi-lan-connect" => MDI_LAN_CONNECT,
    "nf-mdi-clippy" => MDI_CLIPPY,
    "nf-mdi-chevron-up" => MDI_CHEVRON_UP,
    "nf-mdi-numeric-4-box" => MDI_NUMERIC_4_BOX,
    "nf-mdi-currency-krw" => MDI_CURRENCY_KRW,
    "nf-mdi-gradient" => MDI_GRADIENT,
    "nf-mdi-hololens" => MDI_HOLOLENS,
    "nf-mdi-black-mesa" => MDI_BLACK_MESA,
    "nf-mdi-clipboard-arrow-down" => MDI_CLIPBOARD_ARROW_DOWN,
    "nf-mdi-white-balance-incandescent" => MDI_WHITE_BALANCE_INCANDESCENT,
    "nf-mdi-sort-numeric" => MDI_SORT_NUMERIC,
    "nf-mdi-dribbble" => MDI_DRIBBBLE,
    "nf-mdi-source-commit-end-local" => MDI_SOURCE_COMMIT_END_LOCAL,
    "nf-mdi-loading" => MDI_LOADING,
    "nf-mdi-washing-machine" => MDI_WASHING_MACHINE,
    "nf-mdi-vector-circle-variant" => MDI_VECTOR_CIRCLE_VARIANT,
    "nf-mdi-bullseye" => MDI_BULLSEYE,
    "nf-mdi-code-less-than" => MDI_CODE_LESS_THAN,
    "nf-mdi-message-draw" => MDI_MESSAGE_DRAW,
    "nf-mdi-car" => MDI_CAR,
    "nf-mdi-clipboard-alert" => MDI_CLIPBOARD_ALERT,
    "nf-mdi-laptop-mac" => MDI_LAPTOP_MAC,
    "nf-mdi-table-row" => MDI_TABLE_ROW,
    "nf-mdi-brightness-5" => MDI_BRIGHTNESS_5,
    "nf-mdi-fridge-filled" => MDI_FRIDGE_FILLED,
    "nf-mdi-play-pause" => MDI_PLAY_PAUSE,
    "nf-mdi-label-outline" => MDI_LABEL_OUTLINE,
    "nf-mdi-chevron-double-up" => MDI_CHEVRON_DOUBLE_UP,
    "nf-mdi-pi-box" => MDI_PI_BOX,
    "nf-mdi-sitemap" => MDI_SITEMAP,
    "nf-mdi-folder-multiple-image" => MDI_FOLDER_MULTIPLE_IMAGE,
    "nf-mdi-timer-sand" => MDI_TIMER_SAND,
    "nf-mdi-chart-line" => MDI_CHART_LINE,
    "nf-mdi-arrow-collapse-left" => MDI_ARROW_COLLAPSE_LEFT,
    "nf-mdi-ship-wheel" => MDI_SHIP_WHEEL,
    "nf-mdi-battery-charging-wireless-50" => MDI_BATTERY_CHARGING_WIRELESS_50,
    "nf-mdi-hangouts" => MDI_HANGOUTS,
    "nf-mdi-border-right" => MDI_BORDER_RIGHT,
    "nf-mdi-coffee" => MDI_COFFEE,
    "nf-mdi-square-inc-cash" => MDI_SQUARE_INC_CASH,
    "nf-mdi-trophy-award" => MDI_TROPHY_AWARD,
    "nf-mdi-dice-2" => MDI_DICE_2,
    "nf-mdi-format-float-left" => MDI_FORMAT_FLOAT_LEFT,
    "nf-mdi-weather-sunset-up" => MDI_WEATHER_SUNSET_UP,
    "nf-mdi-vpn" => MDI_VPN,
    "nf-mdi-duck" => MDI_DUCK,
    "nf-mdi-tag-heart" => MDI_TAG_HEART,
    "nf-mdi-vector-rectangle" => MDI_VECTOR_RECTANGLE,
    "nf-mdi-elephant" => MDI_ELEPHANT,
    "nf-mdi-xml" => MDI_XML,
    "nf-mdi-menu-up-outline" => MDI_MENU_UP_OUTLINE,
    "nf-mdi-minus-box" => MDI_MINUS_BOX,
    "nf-mdi-headset" => MDI_HEADSET,
    "nf-mdi-battery-20" => MDI_BATTERY_20,
    "nf-mdi-engine" => MDI_ENGINE,
    "nf-mdi-format-header-3" => MDI_FORMAT_HEADER_3,
    "nf-mdi-camcorder-box" => MDI_CAMCORDER_BOX,
    "nf-mdi-bus-school" => MDI_BUS_SCHOOL,
    "nf-mdi-checkbox-multiple-blank-circle" => MDI_CHECKBOX_MULTIPLE_BLANK_CIRCLE,
    "nf-mdi-close-octagon-outline" => MDI_CLOSE_OCTAGON_OUTLINE,
    "nf-mdi-inbox-arrow-up" => MDI_INBOX_ARROW_UP,
    "nf-mdi-account-remove" => MDI_ACCOUNT_REMOVE,
    "nf-mdi-format-float-none" => MDI_FORMAT_FLOAT_NONE,
    "nf-mdi-bluetooth-audio" => MDI_BLUETOOTH_AUDIO,
    "nf-mdi-filmstrip" => MDI_FILMSTRIP,
    "nf-mdi-discord" => MDI_DISCORD,
    "nf-mdi-baby-buggy" => MDI_BABY_BUGGY,
    "nf-mdi-hanger" => MDI_HANGER,
    "nf-mdi-bell-plus" => MDI_BELL_PLUS,
    "nf-mdi-human-male-female" => MDI_HUMAN_MALE_FEMALE,
    "nf-mdi-microsoft" => MDI_MICROSOFT,
    "nf-mdi-settings" => MDI_SETTINGS,
    "nf-mdi-stop" => MDI_STOP,
    "nf-mdi-numeric-9-plus-box" => MDI_NUMERIC_9_PLUS_BOX,
    "nf-mdi-format-horizontal-align-left" => MDI_FORMAT_HORIZONTAL_ALIGN_LEFT,
    "nf-mdi-console" => MDI_CONSOLE,
    "nf-mdi-steam" => MDI_STEAM,
    "nf-mdi-vector-polygon" => MDI_VECTOR_POLYGON,
    "nf-mdi-gesture-two-double-tap" => MDI_GESTURE_TWO_DOUBLE_TAP,
    "nf-mdi-image-filter-center-focus-weak" => MDI_IMAGE_FILTER_CENTER_FOCUS_WEAK,
    "nf-mdi-speaker-wireless" => MDI_SPEAKER_WIRELESS,
    "nf-mdi-microphone-settings" => MDI_MICROPHONE_SETTINGS,
    "nf-mdi-dots-vertical-circle" => MDI_DOTS_VERTICAL_CIRCLE,
    "nf-mdi-airplay" => MDI_AIRPLAY,
    "nf-mdi-format-pilcrow" => MDI_FORMAT_PILCROW,
    "nf-mdi-amazon" => MDI_AMAZON,
    "nf-mdi-target" => MDI_TARGET,
    "nf-mdi-disqus-outline" => MDI_DISQUS_OUTLINE,
    "nf-mdi-usb" => MDI_USB,
    "nf-mdi-nature-people" => MDI_NATURE_PEOPLE,
    "nf-mdi-shield-half-full" => MDI_SHIELD_HALF_FULL,
    "nf-mdi-view-grid" => MDI_VIEW_GRID,
    "nf-mdi-ubuntu" => MDI_UBUNTU,
    "nf-mdi-format-header-1" => MDI_FORMAT_HEADER_1,
    "nf-mdi-phone-outgoing" => MDI_PHONE_OUTGOING,
    "nf-mdi-vector-difference-ba" => MDI_VECTOR_DIFFERENCE_BA,
    "nf-mdi-arrow-down-thick" => MDI_ARROW_DOWN_THICK,
    "nf-mdi-selection" => MDI_SELECTION,
    "nf-mdi-source-merge" => MDI_SOURCE_MERGE,
    "nf-mdi-arrow-expand-right" => MDI_ARROW_EXPAND_RIGHT,
    "nf-mdi-chart-gantt" => MDI_CHART_GANTT,
    "nf-mdi-file-export" => MDI_FILE_EXPORT,
    "nf-mdi-music-box" => MDI_MUSIC_BOX,
    "nf-mdi-signal-off" => MDI_SIGNAL_OFF,
    "nf-mdi-arrow-down-bold-box" => MDI_ARROW_DOWN_BOLD_BOX,
    "nf-mdi-folder-move" => MDI_FOLDER_MOVE,
    "nf-mdi-star-off" => MDI_STAR_OFF,
    "nf-mdi-vector-union" => MDI_VECTOR_UNION,
    "nf-mdi-pistol" => MDI_PISTOL,
    "nf-mdi-chair-school" => MDI_CHAIR_SCHOOL,
    "nf-mdi-tshirt-v" => MDI_TSHIRT_V,
    "nf-mdi-vk-circle" => MDI_VK_CIRCLE,
    "nf-mdi-wallet-travel" => MDI_WALLET_TRAVEL,
    "nf-mdi-webhook" => MDI_WEBHOOK,
    "nf-mdi-flower" => MDI_FLOWER,
    "nf-mdi-apple-safari" => MDI_APPLE_SAFARI,
    "nf-mdi-test-tube" => MDI_TEST_TUBE,
    "nf-mdi-hexagon" => MDI_HEXAGON,
    "nf-mdi-screen-rotation-lock" => MDI_SCREEN_ROTATION_LOCK,
    "nf-mdi-fire" => MDI_FIRE,
    "nf-mdi-volume-off" => MDI_VOLUME_OFF,
    "nf-mdi-video-off" => MDI_VIDEO_OFF,
    "nf-mdi-timer-sand-full" => MDI_TIMER_SAND_FULL,
    "nf-mdi-microscope" => MDI_MICROSCOPE,
    "nf-mdi-star-half" => MDI_STAR_HALF,
    "nf-mdi-microphone-off" => MDI_MICROPHONE_OFF,
    "nf-mdi-drag" => MDI_DRAG,
    "nf-mdi-refresh" => MDI_REFRESH,
    "nf-mdi-select-all" => MDI_SELECT_ALL,
    "nf-mdi-oar" => MDI_OAR,
    "nf-mdi-panorama-vertical" => MDI_PANORAMA_VERTICAL,
    "nf-mdi-looks" => MDI_LOOKS,
    "nf-mdi-new-box" => MDI_NEW_BOX,
    "nf-mdi-filter-remove" => MDI_FILTER_REMOVE,
    "nf-mdi-cake" => MDI_CAKE,
    "nf-mdi-camera-rear-variant" => MDI_CAMERA_REAR_VARIANT,
    "nf-mdi-sticker" => MDI_STICKER,
    "nf-mdi-serial-port" => MDI_SERIAL_PORT,
    "nf-mdi-numeric-9-plus-box-multiple-outline" => MDI_NUMERIC_9_PLUS_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-presentation" => MDI_PRESENTATION,
    "nf-mdi-gesture" => MDI_GESTURE,
    "nf-mdi-close-outline" => MDI_CLOSE_OUTLINE,
    "nf-mdi-block-helper" => MDI_BLOCK_HELPER,
    "nf-mdi-video-input-antenna" => MDI_VIDEO_INPUT_ANTENNA,
    "nf-mdi-playlist-minus" => MDI_PLAYLIST_MINUS,
    "nf-mdi-library-music" => MDI_LIBRARY_MUSIC,
    "nf-mdi-seat-recline-extra" => MDI_SEAT_RECLINE_EXTRA,
    "nf-mdi-lead-pencil" => MDI_LEAD_PENCIL,
    "nf-mdi-phone" => MDI_PHONE,
    "nf-mdi-speaker" => MDI_SPEAKER,
    "nf-mdi-numeric-6-box-multiple-outline" => MDI_NUMERIC_6_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-cash-100" => MDI_CASH_100,
    "nf-mdi-checkerboard" => MDI_CHECKERBOARD,
    "nf-mdi-power-socket-eu" => MDI_POWER_SOCKET_EU,
    "nf-mdi-headset-off" => MDI_HEADSET_OFF,
    "nf-mdi-book-unsecure" => MDI_BOOK_UNSECURE,
    "nf-mdi-cash-multiple" => MDI_CASH_MULTIPLE,
    "nf-mdi-shield-outline" => MDI_SHIELD_OUTLINE,
    "nf-mdi-pot" => MDI_POT,
    "nf-mdi-folder-plus" => MDI_FOLDER_PLUS,
    "nf-mdi-logout-variant" => MDI_LOGOUT_VARIANT,
    "nf-mdi-evernote" => MDI_EVERNOTE,
    "nf-mdi-garage-open" => MDI_GARAGE_OPEN,
    "nf-mdi-twitter-circle" => MDI_TWITTER_CIRCLE,
    "nf-mdi-format-underline" => MDI_FORMAT_UNDERLINE,
    "nf-mdi-skip-previous-circle" => MDI_SKIP_PREVIOUS_CIRCLE,
    "nf-mdi-xbox" => MDI_XBOX,
    "nf-mdi-material-ui" => MDI_MATERIAL_UI,
    "nf-mdi-google-chrome" => MDI_GOOGLE_CHROME,
    "nf-mdi-play-circle" => MDI_PLAY_CIRCLE,
    "nf-mdi-theater" => MDI_THEATER,
    "nf-mdi-message-bulleted" => MDI_MESSAGE_BULLETED,
    "nf-mdi-baby" => MDI_BABY,
    "nf-mdi-flash-auto" => MDI_FLASH_AUTO,
    "nf-mdi-filter" => MDI_FILTER,
    "nf-mdi-desktop-mac" => MDI_DESKTOP_MAC,
    "nf-mdi-case-sensitive-alt" => MDI_CASE_SENSITIVE_ALT,
    "nf-mdi-drawing-box" => MDI_DRAWING_BOX,
    "nf-mdi-sword" => MDI_SWORD,
    "nf-mdi-vector-line" => MDI_VECTOR_LINE,
    "nf-mdi-dumbbell" => MDI_DUMBBELL,
    "nf-mdi-sigma" => MDI_SIGMA,
    "nf-mdi-pinterest" => MDI_PINTEREST,
    "nf-mdi-bitcoin" => MDI_BITCOIN,
    "nf-mdi-standard-definition" => MDI_STANDARD_DEFINITION,
    "nf-mdi-headset-dock" => MDI_HEADSET_DOCK,
    "nf-mdi-fan" => MDI_FAN,
    "nf-mdi-trending-neutral" => MDI_TRENDING_NEUTRAL,
    "nf-mdi-plex" => MDI_PLEX,
    "nf-mdi-fullscreen" => MDI_FULLSCREEN,
    "nf-mdi-rss" => MDI_RSS,
    "nf-mdi-desktop-tower" => MDI_DESKTOP_TOWER,
    "nf-mdi-play-circle-outline" => MDI_PLAY_CIRCLE_OUTLINE,
    "nf-mdi-shape" => MDI_SHAPE,
    "nf-mdi-brush" => MDI_BRUSH,
    "nf-mdi-timetable" => MDI_TIMETABLE,
    "nf-mdi-battery-alert" => MDI_BATTERY_ALERT,
    "nf-mdi-road-variant" => MDI_ROAD_VARIANT,
    "nf-mdi-share-variant" => MDI_SHARE_VARIANT,
    "nf-mdi-text-to-speech-off" => MDI_TEXT_TO_SPEECH_OFF,
    "nf-mdi-comment-account-outline" => MDI_COMMENT_ACCOUNT_OUTLINE,
    "nf-mdi-record-rec" => MDI_RECORD_REC,
    "nf-mdi-blender" => MDI_BLENDER,
    "nf-mdi-plus-network" => MDI_PLUS_NETWORK,
    "nf-mdi-radioactive" => MDI_RADIOACTIVE,
    "nf-mdi-mouse-variant" => MDI_MOUSE_VARIANT,
    "nf-mdi-dice-d6" => MDI_DICE_D6,
    "nf-mdi-account" => MDI_ACCOUNT,
    "nf-mdi-beach" => MDI_BEACH,
    "nf-mdi-highway" => MDI_HIGHWAY,
    "nf-mdi-library-plus" => MDI_LIBRARY_PLUS,
    "nf-mdi-behance" => MDI_BEHANCE,
    "nf-mdi-backspace" => MDI_BACKSPACE,
    "nf-mdi-gas-cylinder" => MDI_GAS_CYLINDER,
    "nf-mdi-priority-high" => MDI_PRIORITY_HIGH,
    "nf-mdi-bomb" => MDI_BOMB,
    "nf-mdi-arrow-right-bold-box" => MDI_ARROW_RIGHT_BOLD_BOX,
    "nf-mdi-code-not-equal" => MDI_CODE_NOT_EQUAL,
    "nf-mdi-emoticon-poop" => MDI_EMOTICON_POOP,
    "nf-mdi-format-list-bulleted-type" => MDI_FORMAT_LIST_BULLETED_TYPE,
    "nf-mdi-link" => MDI_LINK,
    "nf-mdi-react" => MDI_REACT,
    "nf-mdi-account-multiple-outline" => MDI_ACCOUNT_MULTIPLE_OUTLINE,
    "nf-mdi-microphone-outline" => MDI_MICROPHONE_OUTLINE,
    "nf-mdi-auto-fix" => MDI_AUTO_FIX,
    "nf-mdi-svg" => MDI_SVG,
    "nf-mdi-flag" => MDI_FLAG,
    "nf-mdi-gate" => MDI_GATE,
    "nf-mdi-nuke" => MDI_NUKE,
    "nf-mdi-cube-unfolded" => MDI_CUBE_UNFOLDED,
    "nf-mdi-mailbox" => MDI_MAILBOX,
    "nf-mdi-arrow-up-bold-circle-outline" => MDI_ARROW_UP_BOLD_CIRCLE_OUTLINE,
    "nf-mdi-numeric-9-box" => MDI_NUMERIC_9_BOX,
    "nf-mdi-ornament-variant" => MDI_ORNAMENT_VARIANT,
    "nf-mdi-download-network" => MDI_DOWNLOAD_NETWORK,
    "nf-mdi-textbox" => MDI_TEXTBOX,
    "nf-mdi-uber" => MDI_UBER,
    "nf-mdi-bell-off" => MDI_BELL_OFF,
    "nf-mdi-paw-off" => MDI_PAW_OFF,
    "nf-mdi-cellphone-link-off" => MDI_CELLPHONE_LINK_OFF,
    "nf-mdi-flattr" => MDI_FLATTR,
    "nf-mdi-comment-remove" => MDI_COMMENT_REMOVE,
    "nf-mdi-water" => MDI_WATER,
    "nf-mdi-arrow-all" => MDI_ARROW_ALL,
    "nf-mdi-timer-10" => MDI_TIMER_10,
    "nf-mdi-regex" => MDI_REGEX,
    "nf-mdi-google-controller-off" => MDI_GOOGLE_CONTROLLER_OFF,
    "nf-mdi-send-secure" => MDI_SEND_SECURE,
    "nf-mdi-file-document" => MDI_FILE_DOCUMENT,
    "nf-mdi-format-list-checks" => MDI_FORMAT_LIST_CHECKS,
    "nf-mdi-pipe" => MDI_PIPE,
    "nf-mdi-format-header-4" => MDI_FORMAT_HEADER_4,
    "nf-mdi-keyboard" => MDI_KEYBOARD,
    "nf-mdi-skip-next" => MDI_SKIP_NEXT,
    "nf-mdi-file-send" => MDI_FILE_SEND,
    "nf-mdi-bell-ring" => MDI_BELL_RING,
    "nf-mdi-calendar-blank" => MDI_CALENDAR_BLANK,
    "nf-mdi-pizza" => MDI_PIZZA,
    "nf-mdi-equal-box" => MDI_EQUAL_BOX,
    "nf-mdi-tennis" => MDI_TENNIS,
    "nf-mdi-format-align-top" => MDI_FORMAT_ALIGN_TOP,
    "nf-mdi-lock" => MDI_LOCK,
    "nf-mdi-magnify-plus-outline" => MDI_MAGNIFY_PLUS_OUTLINE,
    "nf-mdi-format-horizontal-align-center" => MDI_FORMAT_HORIZONTAL_ALIGN_CENTER,
    "nf-mdi-weather-hail" => MDI_WEATHER_HAIL,
    "nf-mdi-format-wrap-inline" => MDI_FORMAT_WRAP_INLINE,
    "nf-mdi-clipboard-text" => MDI_CLIPBOARD_TEXT,
    "nf-mdi-floor-plan" => MDI_FLOOR_PLAN,
    "nf-mdi-checkbox-blank" => MDI_CHECKBOX_BLANK,
    "nf-mdi-alert-circle" => MDI_ALERT_CIRCLE,
    "nf-mdi-transcribe-close" => MDI_TRANSCRIBE_CLOSE,
    "nf-mdi-github-face" => MDI_GITHUB_FACE,
    "nf-mdi-arrow-collapse-right" => MDI_ARROW_COLLAPSE_RIGHT,
    "nf-mdi-alphabetical" => MDI_ALPHABETICAL,
    "nf-mdi-archive" => MDI_ARCHIVE,
    "nf-mdi-map-marker-circle" => MDI_MAP_MARKER_CIRCLE,
    "nf-mdi-fridge-filled-bottom" => MDI_FRIDGE_FILLED_BOTTOM,
    "nf-mdi-cursor-default-outline" => MDI_CURSOR_DEFAULT_OUTLINE,
    "nf-mdi-google-circles-communities" => MDI_GOOGLE_CIRCLES_COMMUNITIES,
    "nf-mdi-wrench" => MDI_WRENCH,
    "nf-mdi-speedometer" => MDI_SPEEDOMETER,
    "nf-mdi-movie-roll" => MDI_MOVIE_ROLL,
    "nf-mdi-cloud" => MDI_CLOUD,
    "nf-mdi-lan-pending" => MDI_LAN_PENDING,
    "nf-mdi-check" => MDI_CHECK,
    "nf-mdi-format-font" => MDI_FORMAT_FONT,
    "nf-mdi-gesture-swipe-up" => MDI_GESTURE_SWIPE_UP,
    "nf-mdi-bus-double-decker" => MDI_BUS_DOUBLE_DECKER,
    "nf-mdi-numeric-8-box-outline" => MDI_NUMERIC_8_BOX_OUTLINE,
    "nf-mdi-nfc-tap" => MDI_NFC_TAP,
    "nf-mdi-view-day" => MDI_VIEW_DAY,
    "nf-mdi-binoculars" => MDI_BINOCULARS,
    "nf-mdi-hackernews" => MDI_HACKERNEWS,
    "nf-mdi-scanner" => MDI_SCANNER,
    "nf-mdi-phone-forward" => MDI_PHONE_FORWARD,
    "nf-mdi-tag-outline" => MDI_TAG_OUTLINE,
    "nf-mdi-currency-inr" => MDI_CURRENCY_INR,
    "nf-mdi-sort" => MDI_SORT,
    "nf-mdi-page-layout-sidebar-right" => MDI_PAGE_LAYOUT_SIDEBAR_RIGHT,
    "nf-mdi-folder" => MDI_FOLDER,
    "nf-mdi-map" => MDI_MAP,
    "nf-mdi-ticket-percent" => MDI_TICKET_PERCENT,
    "nf-mdi-cards" => MDI_CARDS,
    "nf-mdi-mouse-off" => MDI_MOUSE_OFF,
    "nf-mdi-key-minus" => MDI_KEY_MINUS,
    "nf-mdi-thermometer-lines" => MDI_THERMOMETER_LINES,
    "nf-mdi-pin" => MDI_PIN,
    "nf-mdi-ungroup" => MDI_UNGROUP,
    "nf-mdi-account-star" => MDI_ACCOUNT_STAR,
    "nf-mdi-window-maximize" => MDI_WINDOW_MAXIMIZE,
    "nf-mdi-flash-circle" => MDI_FLASH_CIRCLE,
    "nf-mdi-gondola" => MDI_GONDOLA,
    "nf-mdi-wan" => MDI_WAN,
    "nf-mdi-battery-charging" => MDI_BATTERY_CHARGING,
    "nf-mdi-compass" => MDI_COMPASS,
    "nf-mdi-tag-plus" => MDI_TAG_PLUS,
    "nf-mdi-music-off" => MDI_MUSIC_OFF,
    "nf-mdi-check-all" => MDI_CHECK_ALL,
    "nf-mdi-video-3d" => MDI_VIDEO_3D,
    "nf-mdi-google-translate" => MDI_GOOGLE_TRANSLATE,
    "nf-mdi-arrow-left-bold-circle-outline" => MDI_ARROW_LEFT_BOLD_CIRCLE_OUTLINE,
    "nf-mdi-bomb-off" => MDI_BOMB_OFF,
    "nf-mdi-headphones" => MDI_HEADPHONES,
    "nf-mdi-transcribe" => MDI_TRANSCRIBE,
    "nf-mdi-code-array" => MDI_CODE_ARRAY,
    "nf-mdi-file-word-box" => MDI_FILE_WORD_BOX,
    "nf-mdi-library" => MDI_LIBRARY,
    "nf-mdi-matrix" => MDI_MATRIX,
    "nf-mdi-rice" => MDI_RICE,
    "nf-mdi-laptop-windows" => MDI_LAPTOP_WINDOWS,
    "nf-mdi-power-socket-us" => MDI_POWER_SOCKET_US,
    "nf-mdi-finance" => MDI_FINANCE,
    "nf-mdi-view-agenda" => MDI_VIEW_AGENDA,
    "nf-mdi-brightness-auto" => MDI_BRIGHTNESS_AUTO,
    "nf-mdi-numeric-5-box-outline" => MDI_NUMERIC_5_BOX_OUTLINE,
    "nf-mdi-import" => MDI_IMPORT,
    "nf-mdi-camera-metering-partial" => MDI_CAMERA_METERING_PARTIAL,
    "nf-mdi-sign-text" => MDI_SIGN_TEXT,
    "nf-mdi-view-headline" => MDI_VIEW_HEADLINE,
    "nf-mdi-table-row-plus-before" => MDI_TABLE_ROW_PLUS_BEFORE,
    "nf-mdi-food-fork-drink" => MDI_FOOD_FORK_DRINK,
    "nf-mdi-eye-off-outline" => MDI_EYE_OFF_OUTLINE,
    "nf-mdi-home-modern" => MDI_HOME_MODERN,
    "nf-mdi-cursor-default" => MDI_CURSOR_DEFAULT,
    "nf-mdi-keyboard-backspace" => MDI_KEYBOARD_BACKSPACE,
    "nf-mdi-guitar-electric" => MDI_GUITAR_ELECTRIC,
    "nf-mdi-power-socket-uk" => MDI_POWER_SOCKET_UK,
    "nf-mdi-dots-vertical" => MDI_DOTS_VERTICAL,
    "nf-mdi-car-connected" => MDI_CAR_CONNECTED,
    "nf-mdi-set-center-right" => MDI_SET_CENTER_RIGHT,
    "nf-mdi-nfc-variant" => MDI_NFC_VARIANT,
    "nf-mdi-margin" => MDI_MARGIN,
    "nf-mdi-popcorn" => MDI_POPCORN,
    "nf-mdi-flag-checkered" => MDI_FLAG_CHECKERED,
    "nf-mdi-jira" => MDI_JIRA,
    "nf-mdi-border-horizontal" => MDI_BORDER_HORIZONTAL,
    "nf-mdi-sale" => MDI_SALE,
    "nf-mdi-chart-line-stacked" => MDI_CHART_LINE_STACKED,
    "nf-mdi-brightness-3" => MDI_BRIGHTNESS_3,
    "nf-mdi-linkedin" => MDI_LINKEDIN,
    "nf-mdi-rss-box" => MDI_RSS_BOX,
    "nf-mdi-arrow-down-bold" => MDI_ARROW_DOWN_BOLD,
    "nf-mdi-arrow-up-bold-box" => MDI_ARROW_UP_BOLD_BOX,
    "nf-mdi-rotate-3d" => MDI_ROTATE_3D,
    "nf-mdi-harddisk" => MDI_HARDDISK,
    "nf-mdi-emoticon-sad" => MDI_EMOTICON_SAD,
    "nf-mdi-gift" => MDI_GIFT,
    "nf-mdi-forklift" => MDI_FORKLIFT,
    "nf-mdi-chevron-double-left" => MDI_CHEVRON_DOUBLE_LEFT,
    "nf-mdi-cellphone-link" => MDI_CELLPHONE_LINK,
    "nf-mdi-comment-question" => MDI_COMMENT_QUESTION,
    "nf-mdi-earth-box-off" => MDI_EARTH_BOX_OFF,
    "nf-mdi-blackberry" => MDI_BLACKBERRY,
    "nf-mdi-panorama-fisheye" => MDI_PANORAMA_FISHEYE,
    "nf-mdi-comment-check" => MDI_COMMENT_CHECK,
    "nf-mdi-content-save-settings" => MDI_CONTENT_SAVE_SETTINGS,
    "nf-mdi-border-top" => MDI_BORDER_TOP,
    "nf-mdi-fax" => MDI_FAX,
    "nf-mdi-bluetooth" => MDI_BLUETOOTH,
    "nf-mdi-select-inverse" => MDI_SELECT_INVERSE,
    "nf-mdi-view-dashboard" => MDI_VIEW_DASHBOARD,
    "nf-mdi-emoticon-excited" => MDI_EMOTICON_EXCITED,
    "nf-mdi-surround-sound-7-1" => MDI_SURROUND_SOUND_7_1,
    "nf-mdi-vuejs" => MDI_VUEJS,
    "nf-mdi-battery-charging-20" => MDI_BATTERY_CHARGING_20,
    "nf-mdi-message-alert" => MDI_MESSAGE_ALERT,
    "nf-mdi-checkbox-marked-circle" => MDI_CHECKBOX_MARKED_CIRCLE,
    "nf-mdi-file-find" => MDI_FILE_FIND,
    "nf-mdi-window-minimize" => MDI_WINDOW_MINIMIZE,
    "nf-mdi-arrow-down-bold-circle-outline" => MDI_ARROW_DOWN_BOLD_CIRCLE_OUTLINE,
    "nf-mdi-spellcheck" => MDI_SPELLCHECK,
    "nf-mdi-bell-sleep" => MDI_BELL_SLEEP,
    "nf-mdi-folder-google-drive" => MDI_FOLDER_GOOGLE_DRIVE,
    "nf-mdi-link-off" => MDI_LINK_OFF,
    "nf-mdi-image-filter-drama" => MDI_IMAGE_FILTER_DRAMA,
    "nf-mdi-group" => MDI_GROUP,
    "nf-mdi-arrow-left-drop-circle" => MDI_ARROW_LEFT_DROP_CIRCLE,
    "nf-mdi-music-note-eighth" => MDI_MUSIC_NOTE_EIGHTH,
    "nf-mdi-security" => MDI_SECURITY,
    "nf-mdi-temperature-celsius" => MDI_TEMPERATURE_CELSIUS,
    "nf-mdi-rotate-left" => MDI_ROTATE_LEFT,
    "nf-mdi-currency-btc" => MDI_CURRENCY_BTC,
    "nf-mdi-elevator" => MDI_ELEVATOR,
    "nf-mdi-github-box" => MDI_GITHUB_BOX,
    "nf-mdi-ferry" => MDI_FERRY,
    "nf-mdi-kickstarter" => MDI_KICKSTARTER,
    "nf-mdi-food-variant" => MDI_FOOD_VARIANT,
    "nf-mdi-battery-charging-wireless-80" => MDI_BATTERY_CHARGING_WIRELESS_80,
    "nf-mdi-cloud-check" => MDI_CLOUD_CHECK,
    "nf-mdi-tooltip-outline" => MDI_TOOLTIP_OUTLINE,
    "nf-mdi-skip-next-circle" => MDI_SKIP_NEXT_CIRCLE,
    "nf-mdi-alarm-light" => MDI_ALARM_LIGHT,
    "nf-mdi-vimeo" => MDI_VIMEO,
    "nf-mdi-onedrive" => MDI_ONEDRIVE,
    "nf-mdi-cookie" => MDI_COOKIE,
    "nf-mdi-tooltip-outline-plus" => MDI_TOOLTIP_OUTLINE_PLUS,
    "nf-mdi-youtube-tv" => MDI_YOUTUBE_TV,
    "nf-mdi-wall" => MDI_WALL,
    "nf-mdi-numeric-1-box-multiple-outline" => MDI_NUMERIC_1_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-delete-forever" => MDI_DELETE_FOREVER,
    "nf-mdi-minus-network" => MDI_MINUS_NETWORK,
    "nf-mdi-signal-hspa-plus" => MDI_SIGNAL_HSPA_PLUS,
    "nf-mdi-facebook-box" => MDI_FACEBOOK_BOX,
    "nf-mdi-format-align-center" => MDI_FORMAT_ALIGN_CENTER,
    "nf-mdi-exclamation" => MDI_EXCLAMATION,
    "nf-mdi-sd" => MDI_SD,
    "nf-mdi-check-circle-outline" => MDI_CHECK_CIRCLE_OUTLINE,
    "nf-mdi-battery-40" => MDI_BATTERY_40,
    "nf-mdi-human-female" => MDI_HUMAN_FEMALE,
    "nf-mdi-battery-positive" => MDI_BATTERY_POSITIVE,
    "nf-mdi-message-outline" => MDI_MESSAGE_OUTLINE,
    "nf-mdi-weather-night" => MDI_WEATHER_NIGHT,
    "nf-mdi-format-header-2" => MDI_FORMAT_HEADER_2,
    "nf-mdi-temperature-kelvin" => MDI_TEMPERATURE_KELVIN,
    "nf-mdi-violin" => MDI_VIOLIN,
    "nf-mdi-reorder-horizontal" => MDI_REORDER_HORIZONTAL,
    "nf-mdi-blur-off" => MDI_BLUR_OFF,
    "nf-mdi-bluetooth-off" => MDI_BLUETOOTH_OFF,
    "nf-mdi-multiplication" => MDI_MULTIPLICATION,
    "nf-mdi-open-in-new" => MDI_OPEN_IN_NEW,
    "nf-mdi-needle" => MDI_NEEDLE,
    "nf-mdi-weather-pouring" => MDI_WEATHER_POURING,
    "nf-mdi-ticket-account" => MDI_TICKET_ACCOUNT,
    "nf-mdi-weather-sunset" => MDI_WEATHER_SUNSET,
    "nf-mdi-checkbox-multiple-marked-outline" => MDI_CHECKBOX_MULTIPLE_MARKED_OUTLINE,
    "nf-mdi-eyedropper" => MDI_EYEDROPPER,
    "nf-mdi-arrow-collapse-up" => MDI_ARROW_COLLAPSE_UP,
    "nf-mdi-access-point-network" => MDI_ACCESS_POINT_NETWORK,
    "nf-mdi-call-made" => MDI_CALL_MADE,
    "nf-mdi-image-filter-tilt-shift" => MDI_IMAGE_FILTER_TILT_SHIFT,
    "nf-mdi-flask-empty" => MDI_FLASK_EMPTY,
    "nf-mdi-yin-yang" => MDI_YIN_YANG,
    "nf-mdi-karate" => MDI_KARATE,
    "nf-mdi-arrow-right-drop-circle" => MDI_ARROW_RIGHT_DROP_CIRCLE,
    "nf-mdi-undo-variant" => MDI_UNDO_VARIANT,
    "nf-mdi-keyboard-close" => MDI_KEYBOARD_CLOSE,
    "nf-mdi-arrow-bottom-left" => MDI_ARROW_BOTTOM_LEFT,
    "nf-mdi-file-cloud" => MDI_FILE_CLOUD,
    "nf-mdi-alarm-multiple" => MDI_ALARM_MULTIPLE,
    "nf-mdi-crop-free" => MDI_CROP_FREE,
    "nf-mdi-shredder" => MDI_SHREDDER,
    "nf-mdi-speaker-off" => MDI_SPEAKER_OFF,
    "nf-mdi-flash-red-eye" => MDI_FLASH_RED_EYE,
    "nf-mdi-play" => MDI_PLAY,
    "nf-mdi-xbox-controller-battery-empty" => MDI_XBOX_CONTROLLER_BATTERY_EMPTY,
    "nf-mdi-book-open" => MDI_BOOK_OPEN,
    "nf-mdi-earth" => MDI_EARTH,
    "nf-mdi-poker-chip" => MDI_POKER_CHIP,
    "nf-mdi-paw" => MDI_PAW,
    "nf-mdi-border-outside" => MDI_BORDER_OUTSIDE,
    "nf-mdi-thermometer" => MDI_THERMOMETER,
    "nf-mdi-battery-minus" => MDI_BATTERY_MINUS,
    "nf-mdi-wheelchair-accessibility" => MDI_WHEELCHAIR_ACCESSIBILITY,
    "nf-mdi-camera-timer" => MDI_CAMERA_TIMER,
    "nf-mdi-minus-box-outline" => MDI_MINUS_BOX_OUTLINE,
    "nf-mdi-map-marker" => MDI_MAP_MARKER,
    "nf-mdi-gesture-tap" => MDI_GESTURE_TAP,
    "nf-mdi-set-left-center" => MDI_SET_LEFT_CENTER,
    "nf-mdi-chart-scatterplot-hexbin" => MDI_CHART_SCATTERPLOT_HEXBIN,
    "nf-mdi-calendar-remove" => MDI_CALENDAR_REMOVE,
    "nf-mdi-share" => MDI_SHARE,
    "nf-mdi-comment-check-outline" => MDI_COMMENT_CHECK_OUTLINE,
    "nf-mdi-battery-80" => MDI_BATTERY_80,
    "nf-mdi-martini" => MDI_MARTINI,
    "nf-mdi-saxophone" => MDI_SAXOPHONE,
    "nf-mdi-numeric-0-box-multiple-outline" => MDI_NUMERIC_0_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-select" => MDI_SELECT,
    "nf-mdi-square-inc" => MDI_SQUARE_INC,
    "nf-mdi-network" => MDI_NETWORK,
    "nf-mdi-guitar-acoustic" => MDI_GUITAR_ACOUSTIC,
    "nf-mdi-key-change" => MDI_KEY_CHANGE,
    "nf-mdi-mushroom" => MDI_MUSHROOM,
    "nf-mdi-power" => MDI_POWER,
    "nf-mdi-water-percent" => MDI_WATER_PERCENT,
    "nf-mdi-pokeball" => MDI_POKEBALL,
    "nf-mdi-gender-male-female" => MDI_GENDER_MALE_FEMALE,
    "nf-mdi-recycle" => MDI_RECYCLE,
    "nf-mdi-brightness-6" => MDI_BRIGHTNESS_6,
    "nf-mdi-reproduction" => MDI_REPRODUCTION,
    "nf-mdi-npm" => MDI_NPM,
    "nf-mdi-view-sequential" => MDI_VIEW_SEQUENTIAL,
    "nf-mdi-more" => MDI_MORE,
    "nf-mdi-book-secure" => MDI_BOOK_SECURE,
    "nf-mdi-heart" => MDI_HEART,
    "nf-mdi-loupe" => MDI_LOUPE,
    "nf-mdi-message-bulleted-off" => MDI_MESSAGE_BULLETED_OFF,
    "nf-mdi-fan-off" => MDI_FAN_OFF,
    "nf-mdi-food-off" => MDI_FOOD_OFF,
    "nf-mdi-vector-polyline" => MDI_VECTOR_POLYLINE,
    "nf-mdi-numeric-9-plus-box-outline" => MDI_NUMERIC_9_PLUS_BOX_OUTLINE,
    "nf-mdi-backburger" => MDI_BACKBURGER,
    "nf-mdi-arrange-bring-to-front" => MDI_ARRANGE_BRING_TO_FRONT,
    "nf-mdi-sort-ascending" => MDI_SORT_ASCENDING,
    "nf-mdi-bookmark-remove" => MDI_BOOKMARK_REMOVE,
    "nf-mdi-earth-box" => MDI_EARTH_BOX,
    "nf-mdi-compass-outline" => MDI_COMPASS_OUTLINE,
    "nf-mdi-calendar-question" => MDI_CALENDAR_QUESTION,
    "nf-mdi-file-outline" => MDI_FILE_OUTLINE,
    "nf-mdi-houzz-box" => MDI_HOUZZ_BOX,
    "nf-mdi-camera" => MDI_CAMERA,
    "nf-mdi-unfold-more-vertical" => MDI_UNFOLD_MORE_VERTICAL,
    "nf-mdi-silverware-variant" => MDI_SILVERWARE_VARIANT,
    "nf-mdi-mouse" => MDI_MOUSE,
    "nf-mdi-email-outline" => MDI_EMAIL_OUTLINE,
    "nf-mdi-gmail" => MDI_GMAIL,
    "nf-mdi-near-me" => MDI_NEAR_ME,
    "nf-mdi-eraser-variant" => MDI_ERASER_VARIANT,
    "nf-mdi-color-helper" => MDI_COLOR_HELPER,
    "nf-mdi-surround-sound-2-0" => MDI_SURROUND_SOUND_2_0,
    "nf-mdi-folder-multiple" => MDI_FOLDER_MULTIPLE,
    "nf-mdi-spotlight-beam" => MDI_SPOTLIGHT_BEAM,
    "nf-mdi-battery-charging-wireless-90" => MDI_BATTERY_CHARGING_WIRELESS_90,
    "nf-mdi-keyboard-variant" => MDI_KEYBOARD_VARIANT,
    "nf-mdi-book-minus" => MDI_BOOK_MINUS,
    "nf-mdi-skull" => MDI_SKULL,
    "nf-mdi-arrow-right-drop-circle-outline" => MDI_ARROW_RIGHT_DROP_CIRCLE_OUTLINE,
    "nf-mdi-printer-3d" => MDI_PRINTER_3D,
    "nf-mdi-cast-connected" => MDI_CAST_CONNECTED,
    "nf-mdi-step-backward-2" => MDI_STEP_BACKWARD_2,
    "nf-mdi-phone-minus" => MDI_PHONE_MINUS,
    "nf-mdi-barley" => MDI_BARLEY,
    "nf-mdi-blur-radial" => MDI_BLUR_RADIAL,
    "nf-mdi-inbox-arrow-down" => MDI_INBOX_ARROW_DOWN,
    "nf-mdi-currency-eur" => MDI_CURRENCY_EUR,
    "nf-mdi-redo-variant" => MDI_REDO_VARIANT,
    "nf-mdi-play-protected-content" => MDI_PLAY_PROTECTED_CONTENT,
    "nf-mdi-apps" => MDI_APPS,
    "nf-mdi-lamp" => MDI_LAMP,
    "nf-mdi-google-maps" => MDI_GOOGLE_MAPS,
    "nf-mdi-rowing" => MDI_ROWING,
    "nf-mdi-git" => MDI_GIT,
    "nf-mdi-battery-charging-wireless-30" => MDI_BATTERY_CHARGING_WIRELESS_30,
    "nf-mdi-beaker" => MDI_BEAKER,
    "nf-mdi-tag-faces" => MDI_TAG_FACES,
    "nf-mdi-video-switch" => MDI_VIDEO_SWITCH,
    "nf-mdi-truck-trailer" => MDI_TRUCK_TRAILER,
    "nf-mdi-alarm-check" => MDI_ALARM_CHECK,
    "nf-mdi-video" => MDI_VIDEO,
    "nf-mdi-emby" => MDI_EMBY,
    "nf-mdi-arrow-left-box" => MDI_ARROW_LEFT_BOX,
    "nf-mdi-content-save-outline" => MDI_CONTENT_SAVE_OUTLINE,
    "nf-mdi-server-minus" => MDI_SERVER_MINUS,
    "nf-mdi-server-security" => MDI_SERVER_SECURITY,
    "nf-mdi-tune-vertical" => MDI_TUNE_VERTICAL,
    "nf-mdi-mouse-variant-off" => MDI_MOUSE_VARIANT_OFF,
    "nf-mdi-numeric-9-box-outline" => MDI_NUMERIC_9_BOX_OUTLINE,
    "nf-mdi-null" => MDI_NULL,
    "nf-mdi-battery-charging-wireless-70" => MDI_BATTERY_CHARGING_WIRELESS_70,
    "nf-mdi-format-header-decrease" => MDI_FORMAT_HEADER_DECREASE,
    "nf-mdi-gamepad-variant" => MDI_GAMEPAD_VARIANT,
    "nf-mdi-format-subscript" => MDI_FORMAT_SUBSCRIPT,
    "nf-mdi-notebook" => MDI_NOTEBOOK,
    "nf-mdi-white-balance-sunny" => MDI_WHITE_BALANCE_SUNNY,
    "nf-mdi-teamviewer" => MDI_TEAMVIEWER,
    "nf-mdi-account-key" => MDI_ACCOUNT_KEY,
    "nf-mdi-truck-delivery" => MDI_TRUCK_DELIVERY,
    "nf-mdi-passport" => MDI_PASSPORT,
    "nf-mdi-yeast" => MDI_YEAST,
    "nf-mdi-arrow-collapse-down" => MDI_ARROW_COLLAPSE_DOWN,
    "nf-mdi-ladybug" => MDI_LADYBUG,
    "nf-mdi-surround-sound-5-1" => MDI_SURROUND_SOUND_5_1,
    "nf-mdi-access-point" => MDI_ACCESS_POINT,
    "nf-mdi-sign-caution" => MDI_SIGN_CAUTION,
    "nf-mdi-skip-previous" => MDI_SKIP_PREVIOUS,
    "nf-mdi-arrow-right-bold-box-outline" => MDI_ARROW_RIGHT_BOLD_BOX_OUTLINE,
    "nf-mdi-bus-articulated-front" => MDI_BUS_ARTICULATED_FRONT,
    "nf-mdi-door-closed" => MDI_DOOR_CLOSED,
    "nf-mdi-inbox" => MDI_INBOX,
    "nf-mdi-wiiu" => MDI_WIIU,
    "nf-mdi-monitor-multiple" => MDI_MONITOR_MULTIPLE,
    "nf-mdi-alert-outline" => MDI_ALERT_OUTLINE,
    "nf-mdi-clipboard-check" => MDI_CLIPBOARD_CHECK,
    "nf-mdi-sleep-off" => MDI_SLEEP_OFF,
    "nf-mdi-pulse" => MDI_PULSE,
    "nf-mdi-cloud-print-outline" => MDI_CLOUD_PRINT_OUTLINE,
    "nf-mdi-plane-shield" => MDI_PLANE_SHIELD,
    "nf-mdi-emoticon-tongue" => MDI_EMOTICON_TONGUE,
    "nf-mdi-lambda" => MDI_LAMBDA,
    "nf-mdi-google-keep" => MDI_GOOGLE_KEEP,
    "nf-mdi-signal-hspa" => MDI_SIGNAL_HSPA,
    "nf-mdi-adjust" => MDI_ADJUST,
    "nf-mdi-google-cardboard" => MDI_GOOGLE_CARDBOARD,
    "nf-mdi-plus" => MDI_PLUS,
    "nf-mdi-credit-card" => MDI_CREDIT_CARD,
    "nf-mdi-polymer" => MDI_POLYMER,
    "nf-mdi-numeric-3-box-outline" => MDI_NUMERIC_3_BOX_OUTLINE,
    "nf-mdi-arrow-right-box" => MDI_ARROW_RIGHT_BOX,
    "nf-mdi-reply-all" => MDI_REPLY_ALL,
    "nf-mdi-airplane" => MDI_AIRPLANE,
    "nf-mdi-phone-missed" => MDI_PHONE_MISSED,
    "nf-mdi-trending-up" => MDI_TRENDING_UP,
    "nf-mdi-corn" => MDI_CORN,
    "nf-mdi-note-text" => MDI_NOTE_TEXT,
    "nf-mdi-clock-alert" => MDI_CLOCK_ALERT,
    "nf-mdi-package" => MDI_PACKAGE,
    "nf-mdi-view-dashboard-variant" => MDI_VIEW_DASHBOARD_VARIANT,
    "nf-mdi-radiobox-marked" => MDI_RADIOBOX_MARKED,
    "nf-mdi-information-variant" => MDI_INFORMATION_VARIANT,
    "nf-mdi-food-apple" => MDI_FOOD_APPLE,
    "nf-mdi-filter-variant" => MDI_FILTER_VARIANT,
    "nf-mdi-sort-descending" => MDI_SORT_DESCENDING,
    "nf-mdi-airplane-takeoff" => MDI_AIRPLANE_TAKEOFF,
    "nf-mdi-arrow-right-bold-circle-outline" => MDI_ARROW_RIGHT_BOLD_CIRCLE_OUTLINE,
    "nf-mdi-image-album" => MDI_IMAGE_ALBUM,
    "nf-mdi-dice-4" => MDI_DICE_4,
    "nf-mdi-gender-male" => MDI_GENDER_MALE,
    "nf-mdi-home-variant" => MDI_HOME_VARIANT,
    "nf-mdi-minus-circle-outline" => MDI_MINUS_CIRCLE_OUTLINE,
    "nf-mdi-clipboard-arrow-left" => MDI_CLIPBOARD_ARROW_LEFT,
    "nf-mdi-cloud-download" => MDI_CLOUD_DOWNLOAD,
    "nf-mdi-panorama-horizontal" => MDI_PANORAMA_HORIZONTAL,
    "nf-mdi-tooth" => MDI_TOOTH,
    "nf-mdi-numeric-6-box-outline" => MDI_NUMERIC_6_BOX_OUTLINE,
    "nf-mdi-bell-outline" => MDI_BELL_OUTLINE,
    "nf-mdi-elevation-rise" => MDI_ELEVATION_RISE,
    "nf-mdi-glass-flute" => MDI_GLASS_FLUTE,
    "nf-mdi-cake-layered" => MDI_CAKE_LAYERED,
    "nf-mdi-book-multiple-variant" => MDI_BOOK_MULTIPLE_VARIANT,
    "nf-mdi-cursor-text" => MDI_CURSOR_TEXT,
    "nf-mdi-folder-download" => MDI_FOLDER_DOWNLOAD,
    "nf-mdi-ray-start-arrow" => MDI_RAY_START_ARROW,
    "nf-mdi-download" => MDI_DOWNLOAD,
    "nf-mdi-tooltip" => MDI_TOOLTIP,
    "nf-mdi-currency-chf" => MDI_CURRENCY_CHF,
    "nf-mdi-alert-box" => MDI_ALERT_BOX,
    "nf-mdi-bookmark-plus" => MDI_BOOKMARK_PLUS,
    "nf-mdi-file-pdf-box" => MDI_FILE_PDF_BOX,
    "nf-mdi-magnet-on" => MDI_MAGNET_ON,
    "nf-mdi-office" => MDI_OFFICE,
    "nf-mdi-repeat-off" => MDI_REPEAT_OFF,
    "nf-mdi-av-timer" => MDI_AV_TIMER,
    "nf-mdi-server-remove" => MDI_SERVER_REMOVE,
    "nf-mdi-stove" => MDI_STOVE,
    "nf-mdi-floppy" => MDI_FLOPPY,
    "nf-mdi-stairs" => MDI_STAIRS,
    "nf-mdi-pool" => MDI_POOL,
    "nf-mdi-octagon-outline" => MDI_OCTAGON_OUTLINE,
    "nf-mdi-run" => MDI_RUN,
    "nf-mdi-calendar-today" => MDI_CALENDAR_TODAY,
    "nf-mdi-domain" => MDI_DOMAIN,
    "nf-mdi-format-textdirection-l-to-r" => MDI_FORMAT_TEXTDIRECTION_L_TO_R,
    "nf-mdi-waves" => MDI_WAVES,
    "nf-mdi-file-presentation-box" => MDI_FILE_PRESENTATION_BOX,
    "nf-mdi-code-greater-than-or-equal" => MDI_CODE_GREATER_THAN_OR_EQUAL,
    "nf-mdi-arrow-down-drop-circle" => MDI_ARROW_DOWN_DROP_CIRCLE,
    "nf-mdi-power-settings" => MDI_POWER_SETTINGS,
    "nf-mdi-battery" => MDI_BATTERY,
    "nf-mdi-apple-keyboard-control" => MDI_APPLE_KEYBOARD_CONTROL,
    "nf-mdi-seat-legroom-reduced" => MDI_SEAT_LEGROOM_REDUCED,
    "nf-mdi-walk" => MDI_WALK,
    "nf-mdi-comment" => MDI_COMMENT,
    "nf-mdi-set-left" => MDI_SET_LEFT,
    "nf-mdi-battery-charging-wireless-10" => MDI_BATTERY_CHARGING_WIRELESS_10,
    "nf-mdi-numeric-4-box-outline" => MDI_NUMERIC_4_BOX_OUTLINE,
    "nf-mdi-priority-low" => MDI_PRIORITY_LOW,
    "nf-mdi-keyboard-return" => MDI_KEYBOARD_RETURN,
    "nf-mdi-arrow-left-bold" => MDI_ARROW_LEFT_BOLD,
    "nf-mdi-bus-side" => MDI_BUS_SIDE,
    "nf-mdi-weight-kilogram" => MDI_WEIGHT_KILOGRAM,
    "nf-mdi-auto-upload" => MDI_AUTO_UPLOAD,
    "nf-mdi-magnet" => MDI_MAGNET,
    "nf-mdi-vector-selection" => MDI_VECTOR_SELECTION,
    "nf-mdi-home-heart" => MDI_HOME_HEART,
    "nf-mdi-thumb-down-outline" => MDI_THUMB_DOWN_OUTLINE,
    "nf-mdi-itunes" => MDI_ITUNES,
    "nf-mdi-google-earth" => MDI_GOOGLE_EARTH,
    "nf-mdi-vector-combine" => MDI_VECTOR_COMBINE,
    "nf-mdi-currency-ngn" => MDI_CURRENCY_NGN,
    "nf-mdi-weather-windy" => MDI_WEATHER_WINDY,
    "nf-mdi-football-australian" => MDI_FOOTBALL_AUSTRALIAN,
    "nf-mdi-video-input-hdmi" => MDI_VIDEO_INPUT_HDMI,
    "nf-mdi-arrow-up-thick" => MDI_ARROW_UP_THICK,
    "nf-mdi-cup-water" => MDI_CUP_WATER,
    "nf-mdi-dice-1" => MDI_DICE_1,
    "nf-mdi-console-line" => MDI_CONSOLE_LINE,
    "nf-mdi-delete-sweep" => MDI_DELETE_SWEEP,
    "nf-mdi-eject" => MDI_EJECT,
    "nf-mdi-led-variant-off" => MDI_LED_VARIANT_OFF,
    "nf-mdi-alert-octagram" => MDI_ALERT_OCTAGRAM,
    "nf-mdi-camera-switch" => MDI_CAMERA_SWITCH,
    "nf-mdi-television-classic" => MDI_TELEVISION_CLASSIC,
    "nf-mdi-dots-horizontal" => MDI_DOTS_HORIZONTAL,
    "nf-mdi-checkbox-multiple-blank-circle-outline" => MDI_CHECKBOX_MULTIPLE_BLANK_CIRCLE_OUTLINE,
    "nf-mdi-open-in-app" => MDI_OPEN_IN_APP,
    "nf-mdi-airplane-landing" => MDI_AIRPLANE_LANDING,
    "nf-mdi-file-excel" => MDI_FILE_EXCEL,
    "nf-mdi-emoticon-dead" => MDI_EMOTICON_DEAD,
    "nf-mdi-hexagon-outline" => MDI_HEXAGON_OUTLINE,
    "nf-mdi-pentagon" => MDI_PENTAGON,
    "nf-mdi-set-none" => MDI_SET_NONE,
    "nf-mdi-set-right" => MDI_SET_RIGHT,
    "nf-mdi-format-float-center" => MDI_FORMAT_FLOAT_CENTER,
    "nf-mdi-clipboard" => MDI_CLIPBOARD,
    "nf-mdi-dice-d8" => MDI_DICE_D8,
    "nf-mdi-flask" => MDI_FLASK,
    "nf-mdi-bell" => MDI_BELL,
    "nf-mdi-basecamp" => MDI_BASECAMP,
    "nf-mdi-diamond" => MDI_DIAMOND,
    "nf-mdi-high-definition" => MDI_HIGH_DEFINITION,
    "nf-mdi-human-handsdown" => MDI_HUMAN_HANDSDOWN,
    "nf-mdi-cloud-sync" => MDI_CLOUD_SYNC,
    "nf-mdi-music" => MDI_MUSIC,
    "nf-mdi-currency-eth" => MDI_CURRENCY_ETH,
    "nf-mdi-restore" => MDI_RESTORE,
    "nf-mdi-palette" => MDI_PALETTE,
    "nf-mdi-file" => MDI_FILE,
    "nf-mdi-webcam" => MDI_WEBCAM,
    "nf-mdi-wordpress" => MDI_WORDPRESS,
    "nf-mdi-google-assistant" => MDI_GOOGLE_ASSISTANT,
    "nf-mdi-set-left-right" => MDI_SET_LEFT_RIGHT,
    "nf-mdi-transfer" => MDI_TRANSFER,
    "nf-mdi-json" => MDI_JSON,
    "nf-mdi-image-area" => MDI_IMAGE_AREA,
    "nf-mdi-language-r" => MDI_LANGUAGE_R,
    "nf-mdi-plus-circle-multiple-outline" => MDI_PLUS_CIRCLE_MULTIPLE_OUTLINE,
    "nf-mdi-application" => MDI_APPLICATION,
    "nf-mdi-file-tree" => MDI_FILE_TREE,
    "nf-mdi-apple-finder" => MDI_APPLE_FINDER,
    "nf-mdi-alarm-snooze" => MDI_ALARM_SNOOZE,
    "nf-mdi-gesture-swipe-right" => MDI_GESTURE_SWIPE_RIGHT,
    "nf-mdi-beer" => MDI_BEER,
    "nf-mdi-numeric-4-box-multiple-outline" => MDI_NUMERIC_4_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-file-delimited" => MDI_FILE_DELIMITED,
    "nf-mdi-sass" => MDI_SASS,
    "nf-mdi-server" => MDI_SERVER,
    "nf-mdi-format-wrap-tight" => MDI_FORMAT_WRAP_TIGHT,
    "nf-mdi-border-bottom" => MDI_BORDER_BOTTOM,
    "nf-mdi-chevron-right" => MDI_CHEVRON_RIGHT,
    "nf-mdi-shape-polygon-plus" => MDI_SHAPE_POLYGON_PLUS,
    "nf-mdi-cellphone-android" => MDI_CELLPHONE_ANDROID,
    "nf-mdi-creation" => MDI_CREATION,
    "nf-mdi-pine-tree-box" => MDI_PINE_TREE_BOX,
    "nf-mdi-clock-start" => MDI_CLOCK_START,
    "nf-mdi-phone-classic" => MDI_PHONE_CLASSIC,
    "nf-mdi-rotate-left-variant" => MDI_ROTATE_LEFT_VARIANT,
    "nf-mdi-google-home" => MDI_GOOGLE_HOME,
    "nf-mdi-menu-down" => MDI_MENU_DOWN,
    "nf-mdi-close-circle-outline" => MDI_CLOSE_CIRCLE_OUTLINE,
    "nf-mdi-stethoscope" => MDI_STETHOSCOPE,
    "nf-mdi-wallet-giftcard" => MDI_WALLET_GIFTCARD,
    "nf-mdi-format-list-bulleted" => MDI_FORMAT_LIST_BULLETED,
    "nf-mdi-image-filter-frames" => MDI_IMAGE_FILTER_FRAMES,
    "nf-mdi-launch" => MDI_LAUNCH,
    "nf-mdi-surround-sound" => MDI_SURROUND_SOUND,
    "nf-mdi-format-list-numbers" => MDI_FORMAT_LIST_NUMBERS,
    "nf-mdi-cellphone-basic" => MDI_CELLPHONE_BASIC,
    "nf-mdi-palette-advanced" => MDI_PALETTE_ADVANCED,
    "nf-mdi-nut" => MDI_NUT,
    "nf-mdi-decagram-outline" => MDI_DECAGRAM_OUTLINE,
    "nf-mdi-eventbrite" => MDI_EVENTBRITE,
    "nf-mdi-houzz" => MDI_HOUZZ,
    "nf-mdi-arrow-top-right" => MDI_ARROW_TOP_RIGHT,
    "nf-mdi-door" => MDI_DOOR,
    "nf-mdi-seat-legroom-extra" => MDI_SEAT_LEGROOM_EXTRA,
    "nf-mdi-puzzle" => MDI_PUZZLE,
    "nf-mdi-windows" => MDI_WINDOWS,
    "nf-mdi-arrow-up-bold-box-outline" => MDI_ARROW_UP_BOLD_BOX_OUTLINE,
    "nf-mdi-code-greater-than" => MDI_CODE_GREATER_THAN,
    "nf-mdi-thumb-up-outline" => MDI_THUMB_UP_OUTLINE,
    "nf-mdi-currency-usd-off" => MDI_CURRENCY_USD_OFF,
    "nf-mdi-emoticon" => MDI_EMOTICON,
    "nf-mdi-account-edit" => MDI_ACCOUNT_EDIT,
    "nf-mdi-numeric-9-box-multiple-outline" => MDI_NUMERIC_9_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-hulu" => MDI_HULU,
    "nf-mdi-lava-lamp" => MDI_LAVA_LAMP,
    "nf-mdi-map-marker-radius" => MDI_MAP_MARKER_RADIUS,
    "nf-mdi-disk" => MDI_DISK,
    "nf-mdi-bowl" => MDI_BOWL,
    "nf-mdi-rhombus-outline" => MDI_RHOMBUS_OUTLINE,
    "nf-mdi-format-align-bottom" => MDI_FORMAT_ALIGN_BOTTOM,
    "nf-mdi-google-pages" => MDI_GOOGLE_PAGES,
    "nf-mdi-shuffle-disabled" => MDI_SHUFFLE_DISABLED,
    "nf-mdi-timelapse" => MDI_TIMELAPSE,
    "nf-mdi-pen" => MDI_PEN,
    "nf-mdi-flag-variant" => MDI_FLAG_VARIANT,
    "nf-mdi-xamarin" => MDI_XAMARIN,
    "nf-mdi-account-circle" => MDI_ACCOUNT_CIRCLE,
    "nf-mdi-boombox" => MDI_BOOMBOX,
    "nf-mdi-sign-direction" => MDI_SIGN_DIRECTION,
    "nf-mdi-cards-variant" => MDI_CARDS_VARIANT,
    "nf-mdi-apple-keyboard-command" => MDI_APPLE_KEYBOARD_COMMAND,
    "nf-mdi-database-minus" => MDI_DATABASE_MINUS,
    "nf-mdi-subdirectory-arrow-right" => MDI_SUBDIRECTORY_ARROW_RIGHT,
    "nf-mdi-omega" => MDI_OMEGA,
    "nf-mdi-roomba" => MDI_ROOMBA,
    "nf-mdi-image-filter" => MDI_IMAGE_FILTER,
    "nf-mdi-message" => MDI_MESSAGE,
    "nf-mdi-nutrition" => MDI_NUTRITION,
    "nf-mdi-language-php" => MDI_LANGUAGE_PHP,
    "nf-mdi-pandora" => MDI_PANDORA,
    "nf-mdi-brightness-4" => MDI_BRIGHTNESS_4,
    "nf-mdi-instapaper" => MDI_INSTAPAPER,
    "nf-mdi-nest-thermostat" => MDI_NEST_THERMOSTAT,
    "nf-mdi-account-switch" => MDI_ACCOUNT_SWITCH,
    "nf-mdi-facebook" => MDI_FACEBOOK,
    "nf-mdi-eye-outline" => MDI_EYE_OUTLINE,
    "nf-mdi-file-lock" => MDI_FILE_LOCK,
    "nf-mdi-flag-outline" => MDI_FLAG_OUTLINE,
    "nf-mdi-chart-arc" => MDI_CHART_ARC,
    "nf-mdi-ethernet-cable" => MDI_ETHERNET_CABLE,
    "nf-mdi-pi" => MDI_PI,
    "nf-mdi-plus-box-outline" => MDI_PLUS_BOX_OUTLINE,
    "nf-mdi-gesture-two-tap" => MDI_GESTURE_TWO_TAP,
    "nf-mdi-folder-upload" => MDI_FOLDER_UPLOAD,
    "nf-mdi-clipboard-account" => MDI_CLIPBOARD_ACCOUNT,
    "nf-mdi-wikipedia" => MDI_WIKIPEDIA,
    "nf-mdi-star-circle" => MDI_STAR_CIRCLE,
    "nf-mdi-calculator" => MDI_CALCULATOR,
    "nf-mdi-numeric-6-box" => MDI_NUMERIC_6_BOX,
    "nf-mdi-metronome-tick" => MDI_METRONOME_TICK,
    "nf-mdi-account-card-details" => MDI_ACCOUNT_CARD_DETAILS,
    "nf-mdi-equal" => MDI_EQUAL,
    "nf-mdi-opacity" => MDI_OPACITY,
    "nf-mdi-arrow-bottom-right" => MDI_ARROW_BOTTOM_RIGHT,
    "nf-mdi-account-check" => MDI_ACCOUNT_CHECK,
    "nf-mdi-textbox-password" => MDI_TEXTBOX_PASSWORD,
    "nf-mdi-minecraft" => MDI_MINECRAFT,
    "nf-mdi-arrow-right-thick" => MDI_ARROW_RIGHT_THICK,
    "nf-mdi-radiobox-blank" => MDI_RADIOBOX_BLANK,
    "nf-mdi-format-rotate-90" => MDI_FORMAT_ROTATE_90,
    "nf-mdi-road" => MDI_ROAD,
    "nf-mdi-square" => MDI_SQUARE,
    "nf-mdi-table-row-plus-after" => MDI_TABLE_ROW_PLUS_AFTER,
    "nf-mdi-attachment" => MDI_ATTACHMENT,
    "nf-mdi-send" => MDI_SEND,
    "nf-mdi-lan-disconnect" => MDI_LAN_DISCONNECT,
    "nf-mdi-view-column" => MDI_VIEW_COLUMN,
    "nf-mdi-church" => MDI_CHURCH,
    "nf-mdi-phone-plus" => MDI_PHONE_PLUS,
    "nf-mdi-barcode" => MDI_BARCODE,
    "nf-mdi-power-plug" => MDI_POWER_PLUG,
    "nf-mdi-search-web" => MDI_SEARCH_WEB,
    "nf-mdi-trello" => MDI_TRELLO,
    "nf-mdi-periodic-table-co2" => MDI_PERIODIC_TABLE_CO2,
    "nf-mdi-led-outline" => MDI_LED_OUTLINE,
    "nf-mdi-google-wallet" => MDI_GOOGLE_WALLET,
    "nf-mdi-google-circles-extended" => MDI_GOOGLE_CIRCLES_EXTENDED,
    "nf-mdi-arrow-expand" => MDI_ARROW_EXPAND,
    "nf-mdi-account-box" => MDI_ACCOUNT_BOX,
    "nf-mdi-guy-fawkes-mask" => MDI_GUY_FAWKES_MASK,
    "nf-mdi-image-multiple" => MDI_IMAGE_MULTIPLE,
    "nf-mdi-signal-2g" => MDI_SIGNAL_2G,
    "nf-mdi-keyboard-off" => MDI_KEYBOARD_OFF,
    "nf-mdi-sync-alert" => MDI_SYNC_ALERT,
    "nf-mdi-heart-off" => MDI_HEART_OFF,
    "nf-mdi-format-paragraph" => MDI_FORMAT_PARAGRAPH,
    "nf-mdi-laptop" => MDI_LAPTOP,
    "nf-mdi-script" => MDI_SCRIPT,
    "nf-mdi-music-note-bluetooth" => MDI_MUSIC_NOTE_BLUETOOTH,
    "nf-mdi-google-controller" => MDI_GOOGLE_CONTROLLER,
    "nf-mdi-numeric-5-box-multiple-outline" => MDI_NUMERIC_5_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-menu-left" => MDI_MENU_LEFT,
    "nf-mdi-bookmark-plus-outline" => MDI_BOOKMARK_PLUS_OUTLINE,
    "nf-mdi-plus-circle" => MDI_PLUS_CIRCLE,
    "nf-mdi-arrow-down" => MDI_ARROW_DOWN,
    "nf-mdi-message-text" => MDI_MESSAGE_TEXT,
    "nf-mdi-format-line-spacing" => MDI_FORMAT_LINE_SPACING,
    "nf-mdi-swap-vertical" => MDI_SWAP_VERTICAL,
    "nf-mdi-human" => MDI_HUMAN,
    "nf-mdi-ceiling-light" => MDI_CEILING_LIGHT,
    "nf-mdi-flag-variant-outline" => MDI_FLAG_VARIANT_OUTLINE,
    "nf-mdi-trending-down" => MDI_TRENDING_DOWN,
    "nf-mdi-newspaper" => MDI_NEWSPAPER,
    "nf-mdi-file-percent" => MDI_FILE_PERCENT,
    "nf-mdi-assistant" => MDI_ASSISTANT,
    "nf-mdi-battery-plus" => MDI_BATTERY_PLUS,
    "nf-mdi-pause-octagon-outline" => MDI_PAUSE_OCTAGON_OUTLINE,
    "nf-mdi-table-row-height" => MDI_TABLE_ROW_HEIGHT,
    "nf-mdi-webpack" => MDI_WEBPACK,
    "nf-mdi-flask-outline" => MDI_FLASK_OUTLINE,
    "nf-mdi-source-fork" => MDI_SOURCE_FORK,
    "nf-mdi-battery-charging-100" => MDI_BATTERY_CHARGING_100,
    "nf-mdi-account-minus" => MDI_ACCOUNT_MINUS,
    "nf-mdi-printer-alert" => MDI_PRINTER_ALERT,
    "nf-mdi-step-forward-2" => MDI_STEP_FORWARD_2,
    "nf-mdi-unity" => MDI_UNITY,
    "nf-mdi-email-open" => MDI_EMAIL_OPEN,
    "nf-mdi-food-croissant" => MDI_FOOD_CROISSANT,
    "nf-mdi-numeric-7-box-outline" => MDI_NUMERIC_7_BOX_OUTLINE,
    "nf-mdi-move-resize-variant" => MDI_MOVE_RESIZE_VARIANT,
    "nf-mdi-message-text-outline" => MDI_MESSAGE_TEXT_OUTLINE,
    "nf-mdi-home" => MDI_HOME,
    "nf-mdi-magnify-minus" => MDI_MAGNIFY_MINUS,
    "nf-mdi-table-column-plus-after" => MDI_TABLE_COLUMN_PLUS_AFTER,
    "nf-mdi-book-plus" => MDI_BOOK_PLUS,
    "nf-mdi-home-outline" => MDI_HOME_OUTLINE,
    "nf-mdi-image-off" => MDI_IMAGE_OFF,
    "nf-mdi-file-document-box" => MDI_FILE_DOCUMENT_BOX,
    "nf-mdi-ethernet-cable-off" => MDI_ETHERNET_CABLE_OFF,
    "nf-mdi-format-horizontal-align-right" => MDI_FORMAT_HORIZONTAL_ALIGN_RIGHT,
    "nf-mdi-decimal-increase" => MDI_DECIMAL_INCREASE,
    "nf-mdi-face-profile" => MDI_FACE_PROFILE,
    "nf-mdi-cctv" => MDI_CCTV,
    "nf-mdi-shovel" => MDI_SHOVEL,
    "nf-mdi-image-filter-none" => MDI_IMAGE_FILTER_NONE,
    "nf-mdi-table-column" => MDI_TABLE_COLUMN,
    "nf-mdi-ethernet" => MDI_ETHERNET,
    "nf-mdi-arrow-left-bold-circle" => MDI_ARROW_LEFT_BOLD_CIRCLE,
    "nf-mdi-heart-outline" => MDI_HEART_OUTLINE,
    "nf-mdi-music-note-bluetooth-off" => MDI_MUSIC_NOTE_BLUETOOTH_OFF,
    "nf-mdi-human-pregnant" => MDI_HUMAN_PREGNANT,
    "nf-mdi-ear-hearing" => MDI_EAR_HEARING,
    "nf-mdi-foursquare" => MDI_FOURSQUARE,
    "nf-mdi-cube" => MDI_CUBE,
    "nf-mdi-circle" => MDI_CIRCLE,
    "nf-mdi-calendar-plus" => MDI_CALENDAR_PLUS,
    "nf-mdi-earth-off" => MDI_EARTH_OFF,
    "nf-mdi-comment-outline" => MDI_COMMENT_OUTLINE,
    "nf-mdi-telegram" => MDI_TELEGRAM,
    "nf-mdi-timer-sand-empty" => MDI_TIMER_SAND_EMPTY,
    "nf-mdi-snapchat" => MDI_SNAPCHAT,
    "nf-mdi-onenote" => MDI_ONENOTE,
    "nf-mdi-school" => MDI_SCHOOL,
    "nf-mdi-vector-circle" => MDI_VECTOR_CIRCLE,
    "nf-mdi-source-branch" => MDI_SOURCE_BRANCH,
    "nf-mdi-candycane" => MDI_CANDYCANE,
    "nf-mdi-contrast-circle" => MDI_CONTRAST_CIRCLE,
    "nf-mdi-checkbox-marked-circle-outline" => MDI_CHECKBOX_MARKED_CIRCLE_OUTLINE,
    "nf-mdi-close-circle" => MDI_CLOSE_CIRCLE,
    "nf-mdi-google-glass" => MDI_GOOGLE_GLASS,
    "nf-mdi-file-account" => MDI_FILE_ACCOUNT,
    "nf-mdi-numeric-1-box-outline" => MDI_NUMERIC_1_BOX_OUTLINE,
    "nf-mdi-google" => MDI_GOOGLE,
    "nf-mdi-thumb-up" => MDI_THUMB_UP,
    "nf-mdi-gesture-double-tap" => MDI_GESTURE_DOUBLE_TAP,
    "nf-mdi-blogger" => MDI_BLOGGER,
    "nf-mdi-flag-triangle" => MDI_FLAG_TRIANGLE,
    "nf-mdi-facebook-messenger" => MDI_FACEBOOK_MESSENGER,
    "nf-mdi-thought-bubble-outline" => MDI_THOUGHT_BUBBLE_OUTLINE,
    "nf-mdi-infinity" => MDI_INFINITY,
    "nf-mdi-trophy-outline" => MDI_TROPHY_OUTLINE,
    "nf-mdi-xbox-controller" => MDI_XBOX_CONTROLLER,
    "nf-mdi-compare" => MDI_COMPARE,
    "nf-mdi-dice-5" => MDI_DICE_5,
    "nf-mdi-plus-one" => MDI_PLUS_ONE,
    "nf-mdi-developer-board" => MDI_DEVELOPER_BOARD,
    "nf-mdi-view-list" => MDI_VIEW_LIST,
    "nf-mdi-filmstrip-off" => MDI_FILMSTRIP_OFF,
    "nf-mdi-shield" => MDI_SHIELD,
    "nf-mdi-rewind" => MDI_REWIND,
    "nf-mdi-tag" => MDI_TAG,
    "nf-mdi-calendar-range" => MDI_CALENDAR_RANGE,
    "nf-mdi-car-estate" => MDI_CAR_ESTATE,
    "nf-mdi-medical-bag" => MDI_MEDICAL_BAG,
    "nf-mdi-fridge-filled-top" => MDI_FRIDGE_FILLED_TOP,
    "nf-mdi-parking" => MDI_PARKING,
    "nf-mdi-chili-medium" => MDI_CHILI_MEDIUM,
    "nf-mdi-widgets" => MDI_WIDGETS,
    "nf-mdi-city" => MDI_CITY,
    "nf-mdi-sort-alphabetical" => MDI_SORT_ALPHABETICAL,
    "nf-mdi-netflix" => MDI_NETFLIX,
    "nf-mdi-border-style" => MDI_BORDER_STYLE,
    "nf-mdi-led-on" => MDI_LED_ON,
    "nf-mdi-panorama-wide-angle" => MDI_PANORAMA_WIDE_ANGLE,
    "nf-mdi-check-circle" => MDI_CHECK_CIRCLE,
    "nf-mdi-email-alert" => MDI_EMAIL_ALERT,
    "nf-mdi-tram" => MDI_TRAM,
    "nf-mdi-border-vertical" => MDI_BORDER_VERTICAL,
    "nf-mdi-server-network-off" => MDI_SERVER_NETWORK_OFF,
    "nf-mdi-orbit" => MDI_ORBIT,
    "nf-mdi-piano" => MDI_PIANO,
    "nf-mdi-security-home" => MDI_SECURITY_HOME,
    "nf-mdi-volume-plus" => MDI_VOLUME_PLUS,
    "nf-mdi-crop-square" => MDI_CROP_SQUARE,
    "nf-mdi-umbrella-outline" => MDI_UMBRELLA_OUTLINE,
    "nf-mdi-arrow-expand-left" => MDI_ARROW_EXPAND_LEFT,
    "nf-mdi-toggle-switch-off" => MDI_TOGGLE_SWITCH_OFF,
    "nf-mdi-cellphone-settings" => MDI_CELLPHONE_SETTINGS,
    "nf-mdi-lock-outline" => MDI_LOCK_OUTLINE,
    "nf-mdi-yelp" => MDI_YELP,
    "nf-mdi-stadium" => MDI_STADIUM,
    "nf-mdi-arrow-up" => MDI_ARROW_UP,
    "nf-mdi-battery-charging-wireless-20" => MDI_BATTERY_CHARGING_WIRELESS_20,
    "nf-mdi-map-marker-plus" => MDI_MAP_MARKER_PLUS,
    "nf-mdi-sigma-lower" => MDI_SIGMA_LOWER,
    "nf-mdi-chart-donut" => MDI_CHART_DONUT,
    "nf-mdi-alarm" => MDI_ALARM,
    "nf-mdi-gesture-swipe-left" => MDI_GESTURE_SWIPE_LEFT,
    "nf-mdi-xbox-controller-battery-medium" => MDI_XBOX_CONTROLLER_BATTERY_MEDIUM,
    "nf-mdi-crosshairs" => MDI_CROSSHAIRS,
    "nf-mdi-message-reply" => MDI_MESSAGE_REPLY,
    "nf-mdi-music-note" => MDI_MUSIC_NOTE,
    "nf-mdi-book-open-variant" => MDI_BOOK_OPEN_VARIANT,
    "nf-mdi-grid-large" => MDI_GRID_LARGE,
    "nf-mdi-chart-donut-variant" => MDI_CHART_DONUT_VARIANT,
    "nf-mdi-checkbox-marked" => MDI_CHECKBOX_MARKED,
    "nf-mdi-hops" => MDI_HOPS,
    "nf-mdi-heart-broken" => MDI_HEART_BROKEN,
    "nf-mdi-language-python" => MDI_LANGUAGE_PYTHON,
    "nf-mdi-replay" => MDI_REPLAY,
    "nf-mdi-seat-individual-suite" => MDI_SEAT_INDIVIDUAL_SUITE,
    "nf-mdi-pause-circle-outline" => MDI_PAUSE_CIRCLE_OUTLINE,
    "nf-mdi-asterisk" => MDI_ASTERISK,
    "nf-mdi-upload-network" => MDI_UPLOAD_NETWORK,
    "nf-mdi-tower-beach" => MDI_TOWER_BEACH,
    "nf-mdi-beta" => MDI_BETA,
    "nf-mdi-logout" => MDI_LOGOUT,
    "nf-mdi-xbox-controller-battery-low" => MDI_XBOX_CONTROLLER_BATTERY_LOW,
    "nf-mdi-phone-incoming" => MDI_PHONE_INCOMING,
    "nf-mdi-vector-radius" => MDI_VECTOR_RADIUS,
    "nf-mdi-led-off" => MDI_LED_OFF,
    "nf-mdi-cart-off" => MDI_CART_OFF,
    "nf-mdi-playstation" => MDI_PLAYSTATION,
    "nf-mdi-format-quote-open" => MDI_FORMAT_QUOTE_OPEN,
    "nf-mdi-file-import" => MDI_FILE_IMPORT,
    "nf-mdi-drone" => MDI_DRONE,
    "nf-mdi-microphone" => MDI_MICROPHONE,
    "nf-mdi-numeric-8-box-multiple-outline" => MDI_NUMERIC_8_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-watermark" => MDI_WATERMARK,
    "nf-mdi-pencil-circle" => MDI_PENCIL_CIRCLE,
    "nf-mdi-note" => MDI_NOTE,
    "nf-mdi-certificate" => MDI_CERTIFICATE,
    "nf-mdi-play-box-outline" => MDI_PLAY_BOX_OUTLINE,
    "nf-mdi-sim-off" => MDI_SIM_OFF,
    "nf-mdi-format-align-right" => MDI_FORMAT_ALIGN_RIGHT,
    "nf-mdi-code-not-equal-variant" => MDI_CODE_NOT_EQUAL_VARIANT,
    "nf-mdi-table-column-remove" => MDI_TABLE_COLUMN_REMOVE,
    "nf-mdi-tooltip-image" => MDI_TOOLTIP_IMAGE,
    "nf-mdi-code-braces" => MDI_CODE_BRACES,
    "nf-mdi-find-replace" => MDI_FIND_REPLACE,
    "nf-mdi-grid-off" => MDI_GRID_OFF,
    "nf-mdi-panda" => MDI_PANDA,
    "nf-mdi-spotify" => MDI_SPOTIFY,
    "nf-mdi-thumb-down" => MDI_THUMB_DOWN,
    "nf-mdi-clipboard-plus" => MDI_CLIPBOARD_PLUS,
    "nf-mdi-help-circle" => MDI_HELP_CIRCLE,
    "nf-mdi-linux" => MDI_LINUX,
    "nf-mdi-spotlight" => MDI_SPOTLIGHT,
    "nf-mdi-translate" => MDI_TRANSLATE,
    "nf-mdi-apple-ios" => MDI_APPLE_IOS,
    "nf-mdi-checkbox-multiple-blank-outline" => MDI_CHECKBOX_MULTIPLE_BLANK_OUTLINE,
    "nf-mdi-arrow-left-bold-hexagon-outline" => MDI_ARROW_LEFT_BOLD_HEXAGON_OUTLINE,
    "nf-mdi-image-broken-variant" => MDI_IMAGE_BROKEN_VARIANT,
    "nf-mdi-content-duplicate" => MDI_CONTENT_DUPLICATE,
    "nf-mdi-lightbulb-outline" => MDI_LIGHTBULB_OUTLINE,
    "nf-mdi-folder-star" => MDI_FOLDER_STAR,
    "nf-mdi-glass-stange" => MDI_GLASS_STANGE,
    "nf-mdi-language-typescript" => MDI_LANGUAGE_TYPESCRIPT,
    "nf-mdi-apple-keyboard-option" => MDI_APPLE_KEYBOARD_OPTION,
    "nf-mdi-amazon-clouddrive" => MDI_AMAZON_CLOUDDRIVE,
    "nf-mdi-van-utility" => MDI_VAN_UTILITY,
    "nf-mdi-bullhorn" => MDI_BULLHORN,
    "nf-mdi-all-inclusive" => MDI_ALL_INCLUSIVE,
    "nf-mdi-dip-switch" => MDI_DIP_SWITCH,
    "nf-mdi-dots-horizontal-circle" => MDI_DOTS_HORIZONTAL_CIRCLE,
    "nf-mdi-video-4k-box" => MDI_VIDEO_4K_BOX,
    "nf-mdi-keg" => MDI_KEG,
    "nf-mdi-venmo" => MDI_VENMO,
    "nf-mdi-verified" => MDI_VERIFIED,
    "nf-mdi-move-resize" => MDI_MOVE_RESIZE,
    "nf-mdi-van-passenger" => MDI_VAN_PASSENGER,
    "nf-mdi-shape-outline" => MDI_SHAPE_OUTLINE,
    "nf-mdi-vector-square" => MDI_VECTOR_SQUARE,
    "nf-mdi-periscope" => MDI_PERISCOPE,
    "nf-mdi-soccer-field" => MDI_SOCCER_FIELD,
    "nf-mdi-seal" => MDI_SEAL,
    "nf-mdi-audiobook" => MDI_AUDIOBOOK,
    "nf-mdi-ray-end" => MDI_RAY_END,
    "nf-mdi-camera-metering-center" => MDI_CAMERA_METERING_CENTER,
    "nf-mdi-reddit" => MDI_REDDIT,
    "nf-mdi-feather" => MDI_FEATHER,
    "nf-mdi-email" => MDI_EMAIL,
    "nf-mdi-file-music" => MDI_FILE_MUSIC,
    "nf-mdi-package-up" => MDI_PACKAGE_UP,
    "nf-mdi-xbox-controller-battery-alert" => MDI_XBOX_CONTROLLER_BATTERY_ALERT,
    "nf-mdi-arrow-left-drop-circle-outline" => MDI_ARROW_LEFT_DROP_CIRCLE_OUTLINE,
    "nf-mdi-call-missed" => MDI_CALL_MISSED,
    "nf-mdi-dribbble-box" => MDI_DRIBBBLE_BOX,
    "nf-mdi-map-marker-multiple" => MDI_MAP_MARKER_MULTIPLE,
    "nf-mdi-cloud-print" => MDI_CLOUD_PRINT,
    "nf-mdi-opera" => MDI_OPERA,
    "nf-mdi-caravan" => MDI_CARAVAN,
    "nf-mdi-pine-tree" => MDI_PINE_TREE,
    "nf-mdi-xing-circle" => MDI_XING_CIRCLE,
    "nf-mdi-alert" => MDI_ALERT,
    "nf-mdi-close" => MDI_CLOSE,
    "nf-mdi-book-variant" => MDI_BOOK_VARIANT,
    "nf-mdi-tablet" => MDI_TABLET,
    "nf-mdi-numeric-3-box-multiple-outline" => MDI_NUMERIC_3_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-volume-mute" => MDI_VOLUME_MUTE,
    "nf-mdi-vector-difference" => MDI_VECTOR_DIFFERENCE,
    "nf-mdi-oil-temperature" => MDI_OIL_TEMPERATURE,
    "nf-mdi-battery-charging-60" => MDI_BATTERY_CHARGING_60,
    "nf-mdi-swim" => MDI_SWIM,
    "nf-mdi-firefox" => MDI_FIREFOX,
    "nf-mdi-file-multiple" => MDI_FILE_MULTIPLE,
    "nf-mdi-qrcode" => MDI_QRCODE,
    "nf-mdi-douban" => MDI_DOUBAN,
    "nf-mdi-traffic-light" => MDI_TRAFFIC_LIGHT,
    "nf-mdi-xbox-controller-battery-unknown" => MDI_XBOX_CONTROLLER_BATTERY_UNKNOWN,
    "nf-mdi-square-outline" => MDI_SQUARE_OUTLINE,
    "nf-mdi-bluetooth-settings" => MDI_BLUETOOTH_SETTINGS,
    "nf-mdi-triangle" => MDI_TRIANGLE,
    "nf-mdi-arrow-up-box" => MDI_ARROW_UP_BOX,
    "nf-mdi-web" => MDI_WEB,
    "nf-mdi-resize-bottom-right" => MDI_RESIZE_BOTTOM_RIGHT,
    "nf-mdi-car-hatchback" => MDI_CAR_HATCHBACK,
    "nf-mdi-airballoon" => MDI_AIRBALLOON,
    "nf-mdi-eye" => MDI_EYE,
    "nf-mdi-marker-check" => MDI_MARKER_CHECK,
    "nf-mdi-chart-timeline" => MDI_CHART_TIMELINE,
    "nf-mdi-brightness-7" => MDI_BRIGHTNESS_7,
    "nf-mdi-map-marker-minus" => MDI_MAP_MARKER_MINUS,
    "nf-mdi-page-last" => MDI_PAGE_LAST,
    "nf-mdi-drawing" => MDI_DRAWING,
    "nf-mdi-pipe-disconnected" => MDI_PIPE_DISCONNECTED,
    "nf-mdi-arrow-collapse-all" => MDI_ARROW_COLLAPSE_ALL,
    "nf-mdi-format-indent-decrease" => MDI_FORMAT_INDENT_DECREASE,
    "nf-mdi-angularjs" => MDI_ANGULARJS,
    "nf-mdi-bookmark-music" => MDI_BOOKMARK_MUSIC,
    "nf-mdi-folder-remove" => MDI_FOLDER_REMOVE,
    "nf-mdi-human-handsup" => MDI_HUMAN_HANDSUP,
    "nf-mdi-math-compass" => MDI_MATH_COMPASS,
    "nf-mdi-arrow-right-bold-circle" => MDI_ARROW_RIGHT_BOLD_CIRCLE,
    "nf-mdi-keyboard-tab" => MDI_KEYBOARD_TAB,
    "nf-mdi-key-variant" => MDI_KEY_VARIANT,
    "nf-mdi-arrow-up-drop-circle" => MDI_ARROW_UP_DROP_CIRCLE,
    "nf-mdi-window-open" => MDI_WINDOW_OPEN,
    "nf-mdi-coins" => MDI_COINS,
    "nf-mdi-account-off" => MDI_ACCOUNT_OFF,
    "nf-mdi-elevation-decline" => MDI_ELEVATION_DECLINE,
    "nf-mdi-read" => MDI_READ,
    "nf-mdi-bell-ring-outline" => MDI_BELL_RING_OUTLINE,
    "nf-mdi-youtube-play" => MDI_YOUTUBE_PLAY,
    "nf-mdi-glass-tulip" => MDI_GLASS_TULIP,
    "nf-mdi-home-circle" => MDI_HOME_CIRCLE,
    "nf-mdi-bible" => MDI_BIBLE,
    "nf-mdi-fridge" => MDI_FRIDGE,
    "nf-mdi-sofa" => MDI_SOFA,
    "nf-mdi-cannabis" => MDI_CANNABIS,
    "nf-mdi-mixcloud" => MDI_MIXCLOUD,
    "nf-mdi-gender-female" => MDI_GENDER_FEMALE,
    "nf-mdi-note-multiple-outline" => MDI_NOTE_MULTIPLE_OUTLINE,
    "nf-mdi-bulletin-board" => MDI_BULLETIN_BOARD,
    "nf-mdi-close-box-outline" => MDI_CLOSE_BOX_OUTLINE,
    "nf-mdi-arrange-bring-forward" => MDI_ARRANGE_BRING_FORWARD,
    "nf-mdi-email-variant" => MDI_EMAIL_VARIANT,
    "nf-mdi-decagram" => MDI_DECAGRAM,
    "nf-mdi-phone-bluetooth" => MDI_PHONE_BLUETOOTH,
    "nf-mdi-battery-charging-wireless-40" => MDI_BATTERY_CHARGING_WIRELESS_40,
    "nf-mdi-image-filter-center-focus" => MDI_IMAGE_FILTER_CENTER_FOCUS,
    "nf-mdi-watch-export" => MDI_WATCH_EXPORT,
    "nf-mdi-format-align-middle" => MDI_FORMAT_ALIGN_MIDDLE,
    "nf-mdi-account-multiple-minus" => MDI_ACCOUNT_MULTIPLE_MINUS,
    "nf-mdi-file-hidden" => MDI_FILE_HIDDEN,
    "nf-mdi-comment-alert-outline" => MDI_COMMENT_ALERT_OUTLINE,
    "nf-mdi-table-settings" => MDI_TABLE_SETTINGS,
    "nf-mdi-login" => MDI_LOGIN,
    "nf-mdi-unfold-less-horizontal" => MDI_UNFOLD_LESS_HORIZONTAL,
    "nf-mdi-ev-station" => MDI_EV_STATION,
    "nf-mdi-gesture-swipe-down" => MDI_GESTURE_SWIPE_DOWN,
    "nf-mdi-plus-outline" => MDI_PLUS_OUTLINE,
    "nf-mdi-clock-end" => MDI_CLOCK_END,
    "nf-mdi-message-settings-variant" => MDI_MESSAGE_SETTINGS_VARIANT,
    "nf-mdi-comment-processing-outline" => MDI_COMMENT_PROCESSING_OUTLINE,
    "nf-mdi-comment-remove-outline" => MDI_COMMENT_REMOVE_OUTLINE,
    "nf-mdi-led-variant-outline" => MDI_LED_VARIANT_OUTLINE,
    "nf-mdi-bootstrap" => MDI_BOOTSTRAP,
    "nf-mdi-folder-multiple-outline" => MDI_FOLDER_MULTIPLE_OUTLINE,
    "nf-mdi-biohazard" => MDI_BIOHAZARD,
    "nf-mdi-details" => MDI_DETAILS,
    "nf-mdi-ray-start" => MDI_RAY_START,
    "nf-mdi-gavel" => MDI_GAVEL,
    "nf-mdi-battery-10" => MDI_BATTERY_10,
    "nf-mdi-google-circles-group" => MDI_GOOGLE_CIRCLES_GROUP,
    "nf-mdi-menu-down-outline" => MDI_MENU_DOWN_OUTLINE,
    "nf-mdi-cube-send" => MDI_CUBE_SEND,
    "nf-mdi-memory" => MDI_MEMORY,
    "nf-mdi-donkey" => MDI_DONKEY,
    "nf-mdi-basket-unfill" => MDI_BASKET_UNFILL,
    "nf-mdi-xbox-controller-battery-full" => MDI_XBOX_CONTROLLER_BATTERY_FULL,
    "nf-mdi-dice-3" => MDI_DICE_3,
    "nf-mdi-comment-question-outline" => MDI_COMMENT_QUESTION_OUTLINE,
    "nf-mdi-format-text" => MDI_FORMAT_TEXT,
    "nf-mdi-untappd" => MDI_UNTAPPD,
    "nf-mdi-calendar-text" => MDI_CALENDAR_TEXT,
    "nf-mdi-call-split" => MDI_CALL_SPLIT,
    "nf-mdi-border-inside" => MDI_BORDER_INSIDE,
    "nf-mdi-headphones-box" => MDI_HEADPHONES_BOX,
    "nf-mdi-package-variant-closed" => MDI_PACKAGE_VARIANT_CLOSED,
    "nf-mdi-division" => MDI_DIVISION,
    "nf-mdi-content-copy" => MDI_CONTENT_COPY,
    "nf-mdi-chart-bubble" => MDI_CHART_BUBBLE,
    "nf-mdi-font-awesome" => MDI_FONT_AWESOME,
    "nf-mdi-emoticon-cool" => MDI_EMOTICON_COOL,
    "nf-mdi-guitar-pick-outline" => MDI_GUITAR_PICK_OUTLINE,
    "nf-mdi-panorama" => MDI_PANORAMA,
    "nf-mdi-account-multiple" => MDI_ACCOUNT_MULTIPLE,
    "nf-mdi-screen-rotation" => MDI_SCREEN_ROTATION,
    "nf-mdi-signal-3g" => MDI_SIGNAL_3G,
    "nf-mdi-google-circles" => MDI_GOOGLE_CIRCLES,
    "nf-mdi-movie" => MDI_MOVIE,
    "nf-mdi-microphone-variant-off" => MDI_MICROPHONE_VARIANT_OFF,
    "nf-mdi-vector-curve" => MDI_VECTOR_CURVE,
    "nf-mdi-chevron-left" => MDI_CHEVRON_LEFT,
    "nf-mdi-tab-plus" => MDI_TAB_PLUS,
    "nf-mdi-format-indent-increase" => MDI_FORMAT_INDENT_INCREASE,
    "nf-mdi-flash-outline" => MDI_FLASH_OUTLINE,
    "nf-mdi-youtube-creator-studio" => MDI_YOUTUBE_CREATOR_STUDIO,
    "nf-mdi-google-drive" => MDI_GOOGLE_DRIVE,
    "nf-mdi-door-open" => MDI_DOOR_OPEN,
    "nf-mdi-trackpad" => MDI_TRACKPAD,
    "nf-mdi-fuel" => MDI_FUEL,
    "nf-mdi-upload" => MDI_UPLOAD,
    "nf-mdi-menu-up" => MDI_MENU_UP,
    "nf-mdi-train" => MDI_TRAIN,
    "nf-mdi-folder-outline" => MDI_FOLDER_OUTLINE,
    "nf-mdi-football-helmet" => MDI_FOOTBALL_HELMET,
    "nf-mdi-minus" => MDI_MINUS,
    "nf-mdi-bus-articulated-end" => MDI_BUS_ARTICULATED_END,
    "nf-mdi-dna" => MDI_DNA,
    "nf-mdi-information" => MDI_INFORMATION,
    "nf-mdi-cloud-circle" => MDI_CLOUD_CIRCLE,
    "nf-mdi-dice-6" => MDI_DICE_6,
    "nf-mdi-xing-box" => MDI_XING_BOX,
    "nf-mdi-volume-high" => MDI_VOLUME_HIGH,
    "nf-mdi-barrel" => MDI_BARREL,
    "nf-mdi-google-plus-box" => MDI_GOOGLE_PLUS_BOX,
    "nf-mdi-pencil-lock" => MDI_PENCIL_LOCK,
    "nf-mdi-projector" => MDI_PROJECTOR,
    "nf-mdi-music-note-half" => MDI_MUSIC_NOTE_HALF,
    "nf-mdi-camera-burst" => MDI_CAMERA_BURST,
    "nf-mdi-cloud-tags" => MDI_CLOUD_TAGS,
    "nf-mdi-arrow-up-bold-hexagon-outline" => MDI_ARROW_UP_BOLD_HEXAGON_OUTLINE,
    "nf-mdi-publish" => MDI_PUBLISH,
    "nf-mdi-golf" => MDI_GOLF,
    "nf-mdi-phone-paused" => MDI_PHONE_PAUSED,
    "nf-mdi-arrow-expand-down" => MDI_ARROW_EXPAND_DOWN,
    "nf-mdi-contact-mail" => MDI_CONTACT_MAIL,
    "nf-mdi-database" => MDI_DATABASE,
    "nf-mdi-chart-areaspline" => MDI_CHART_AREASPLINE,
    "nf-mdi-directions" => MDI_DIRECTIONS,
    "nf-mdi-keyboard-caps" => MDI_KEYBOARD_CAPS,
    "nf-mdi-view-parallel" => MDI_VIEW_PARALLEL,
    "nf-mdi-terrain" => MDI_TERRAIN,
    "nf-mdi-tent" => MDI_TENT,
    "nf-mdi-format-line-weight" => MDI_FORMAT_LINE_WEIGHT,
    "nf-mdi-incognito" => MDI_INCOGNITO,
    "nf-mdi-phone-return" => MDI_PHONE_RETURN,
    "nf-mdi-format-superscript" => MDI_FORMAT_SUPERSCRIPT,
    "nf-mdi-wallet" => MDI_WALLET,
    "nf-mdi-comment-alert" => MDI_COMMENT_ALERT,
    "nf-mdi-tshirt-crew" => MDI_TSHIRT_CREW,
    "nf-mdi-voicemail" => MDI_VOICEMAIL,
    "nf-mdi-bank" => MDI_BANK,
    "nf-mdi-lastfm" => MDI_LASTFM,
    "nf-mdi-box" => MDI_BOX,
    "nf-mdi-paperclip" => MDI_PAPERCLIP,
    "nf-mdi-spray" => MDI_SPRAY,
    "nf-mdi-hook" => MDI_HOOK,
    "nf-mdi-jeepney" => MDI_JEEPNEY,
    "nf-mdi-tooltip-text" => MDI_TOOLTIP_TEXT,
    "nf-mdi-hotel" => MDI_HOTEL,
    "nf-mdi-album" => MDI_ALBUM,
    "nf-mdi-debug-step-into" => MDI_DEBUG_STEP_INTO,
    "nf-mdi-linkedin-box" => MDI_LINKEDIN_BOX,
    "nf-mdi-raspberrypi" => MDI_RASPBERRYPI,
    "nf-mdi-step-forward" => MDI_STEP_FORWARD,
    "nf-mdi-crop" => MDI_CROP,
    "nf-mdi-google-analytics" => MDI_GOOGLE_ANALYTICS,
    "nf-mdi-account-network" => MDI_ACCOUNT_NETWORK,
    "nf-mdi-hospital" => MDI_HOSPITAL,
    "nf-mdi-heart-half" => MDI_HEART_HALF,
    "nf-mdi-white-balance-auto" => MDI_WHITE_BALANCE_AUTO,
    "nf-mdi-text-shadow" => MDI_TEXT_SHADOW,
    "nf-mdi-arrange-send-to-back" => MDI_ARRANGE_SEND_TO_BACK,
    "nf-mdi-basket" => MDI_BASKET,
    "nf-mdi-database-plus" => MDI_DATABASE_PLUS,
    "nf-mdi-vanish" => MDI_VANISH,
    "nf-mdi-locker" => MDI_LOCKER,
    "nf-mdi-reorder-vertical" => MDI_REORDER_VERTICAL,
    "nf-mdi-bing" => MDI_BING,
    "nf-mdi-router-wireless" => MDI_ROUTER_WIRELESS,
    "nf-mdi-worker" => MDI_WORKER,
    "nf-mdi-brightness-1" => MDI_BRIGHTNESS_1,
    "nf-mdi-note-outline" => MDI_NOTE_OUTLINE,
    "nf-mdi-drag-vertical" => MDI_DRAG_VERTICAL,
    "nf-mdi-xing" => MDI_XING,
    "nf-mdi-atlassian" => MDI_ATLASSIAN,
    "nf-mdi-grid" => MDI_GRID,
    "nf-mdi-comment-plus-outline" => MDI_COMMENT_PLUS_OUTLINE,
    "nf-mdi-jsfiddle" => MDI_JSFIDDLE,
    "nf-mdi-truck" => MDI_TRUCK,
    "nf-mdi-prescription" => MDI_PRESCRIPTION,
    "nf-mdi-water-pump" => MDI_WATER_PUMP,
    "nf-mdi-stop-circle-outline" => MDI_STOP_CIRCLE_OUTLINE,
    "nf-mdi-glassdoor" => MDI_GLASSDOOR,
    "nf-mdi-clock" => MDI_CLOCK,
    "nf-mdi-message-video" => MDI_MESSAGE_VIDEO,
    "nf-mdi-subway" => MDI_SUBWAY,
    "nf-mdi-camera-rear" => MDI_CAMERA_REAR,
    "nf-mdi-language-cpp" => MDI_LANGUAGE_CPP,
    "nf-mdi-ring" => MDI_RING,
    "nf-mdi-table-column-width" => MDI_TABLE_COLUMN_WIDTH,
    "nf-mdi-home-map-marker" => MDI_HOME_MAP_MARKER,
    "nf-mdi-calendar-multiple-check" => MDI_CALENDAR_MULTIPLE_CHECK,
    "nf-mdi-artist" => MDI_ARTIST,
    "nf-mdi-link-variant-off" => MDI_LINK_VARIANT_OFF,
    "nf-mdi-smoking" => MDI_SMOKING,
    "nf-mdi-nodejs" => MDI_NODEJS,
    "nf-mdi-openid" => MDI_OPENID,
    "nf-mdi-twitch" => MDI_TWITCH,
    "nf-mdi-cursor-pointer" => MDI_CURSOR_POINTER,
    "nf-mdi-alert-octagon" => MDI_ALERT_OCTAGON,
    "nf-mdi-food" => MDI_FOOD,
    "nf-mdi-bow-tie" => MDI_BOW_TIE,
    "nf-mdi-export" => MDI_EXPORT,
    "nf-mdi-timer" => MDI_TIMER,
    "nf-mdi-medium" => MDI_MEDIUM,
    "nf-mdi-google-photos" => MDI_GOOGLE_PHOTOS,
    "nf-mdi-border-left" => MDI_BORDER_LEFT,
    "nf-mdi-pot-mix" => MDI_POT_MIX,
    "nf-mdi-forum" => MDI_FORUM,
    "nf-mdi-file-xml" => MDI_FILE_XML,
    "nf-mdi-timer-3" => MDI_TIMER_3,
    "nf-mdi-phone-hangup" => MDI_PHONE_HANGUP,
    "nf-mdi-cellphone-iphone" => MDI_CELLPHONE_IPHONE,
    "nf-mdi-pencil-circle-outline" => MDI_PENCIL_CIRCLE_OUTLINE,
    "nf-mdi-weather-snowy" => MDI_WEATHER_SNOWY,
    "nf-mdi-crosshairs-gps" => MDI_CROSSHAIRS_GPS,
    "nf-mdi-pencil-box" => MDI_PENCIL_BOX,
    "nf-mdi-google-nearby" => MDI_GOOGLE_NEARBY,
    "nf-mdi-volume-minus" => MDI_VOLUME_MINUS,
    "nf-mdi-snowman" => MDI_SNOWMAN,
    "nf-mdi-headphones-settings" => MDI_HEADPHONES_SETTINGS,
    "nf-mdi-repeat-once" => MDI_REPEAT_ONCE,
    "nf-mdi-whatsapp" => MDI_WHATSAPP,
    "nf-mdi-bridge" => MDI_BRIDGE,
    "nf-mdi-codepen" => MDI_CODEPEN,
    "nf-mdi-carrot" => MDI_CARROT,
    "nf-mdi-nest-protect" => MDI_NEST_PROTECT,
    "nf-mdi-settings-box" => MDI_SETTINGS_BOX,
    "nf-mdi-lightbulb-on" => MDI_LIGHTBULB_ON,
    "nf-mdi-format-align-justify" => MDI_FORMAT_ALIGN_JUSTIFY,
    "nf-mdi-message-plus" => MDI_MESSAGE_PLUS,
    "nf-mdi-currency-sign" => MDI_CURRENCY_SIGN,
    "nf-mdi-silverware-spoon" => MDI_SILVERWARE_SPOON,
    "nf-mdi-invert-colors" => MDI_INVERT_COLORS,
    "nf-mdi-etsy" => MDI_ETSY,
    "nf-mdi-weather-fog" => MDI_WEATHER_FOG,
    "nf-mdi-cloud-outline" => MDI_CLOUD_OUTLINE,
    "nf-mdi-code-equal" => MDI_CODE_EQUAL,
    "nf-mdi-arrow-down-bold-hexagon-outline" => MDI_ARROW_DOWN_BOLD_HEXAGON_OUTLINE,
    "nf-mdi-xmpp" => MDI_XMPP,
    "nf-mdi-subdirectory-arrow-left" => MDI_SUBDIRECTORY_ARROW_LEFT,
    "nf-mdi-format-title" => MDI_FORMAT_TITLE,
    "nf-mdi-emoticon-devil" => MDI_EMOTICON_DEVIL,
    "nf-mdi-battery-charging-90" => MDI_BATTERY_CHARGING_90,
    "nf-mdi-ornament" => MDI_ORNAMENT,
    "nf-mdi-file-image" => MDI_FILE_IMAGE,
    "nf-mdi-nature" => MDI_NATURE,
    "nf-mdi-account-multiple-plus-outline" => MDI_ACCOUNT_MULTIPLE_PLUS_OUTLINE,
    "nf-mdi-file-check" => MDI_FILE_CHECK,
    "nf-mdi-ticket-confirmation" => MDI_TICKET_CONFIRMATION,
    "nf-mdi-note-plus-outline" => MDI_NOTE_PLUS_OUTLINE,
    "nf-mdi-loop" => MDI_LOOP,
    "nf-mdi-chart-line-variant" => MDI_CHART_LINE_VARIANT,
    "nf-mdi-broom" => MDI_BROOM,
    "nf-mdi-camera-front" => MDI_CAMERA_FRONT,
    "nf-mdi-percent" => MDI_PERCENT,
    "nf-mdi-smoking-off" => MDI_SMOKING_OFF,
    "nf-mdi-folder-open" => MDI_FOLDER_OPEN,
    "nf-mdi-ultra-high-definition" => MDI_ULTRA_HIGH_DEFINITION,
    "nf-mdi-window-closed" => MDI_WINDOW_CLOSED,
    "nf-mdi-trophy-variant-outline" => MDI_TROPHY_VARIANT_OUTLINE,
    "nf-mdi-music-note-off" => MDI_MUSIC_NOTE_OFF,
    "nf-mdi-skip-next-circle-outline" => MDI_SKIP_NEXT_CIRCLE_OUTLINE,
    "nf-mdi-trophy-variant" => MDI_TROPHY_VARIANT,
    "nf-mdi-eye-off" => MDI_EYE_OFF,
    "nf-mdi-magnify-minus-outline" => MDI_MAGNIFY_MINUS_OUTLINE,
    "nf-mdi-google-play" => MDI_GOOGLE_PLAY,
    "nf-mdi-switch" => MDI_SWITCH,
    "nf-mdi-vector-intersection" => MDI_VECTOR_INTERSECTION,
    "nf-mdi-arrow-left" => MDI_ARROW_LEFT,
    "nf-mdi-sunglasses" => MDI_SUNGLASSES,
    "nf-mdi-wallet-membership" => MDI_WALLET_MEMBERSHIP,
    "nf-mdi-markdown" => MDI_MARKDOWN,
    "nf-mdi-image" => MDI_IMAGE,
    "nf-mdi-source-pull" => MDI_SOURCE_PULL,
    "nf-mdi-arrow-top-left" => MDI_ARROW_TOP_LEFT,
    "nf-mdi-octagon" => MDI_OCTAGON,
    "nf-mdi-heart-half-full" => MDI_HEART_HALF_FULL,
    "nf-mdi-volume-low" => MDI_VOLUME_LOW,
    "nf-mdi-led-strip" => MDI_LED_STRIP,
    "nf-mdi-playlist-remove" => MDI_PLAYLIST_REMOVE,
    "nf-mdi-air-conditioner" => MDI_AIR_CONDITIONER,
    "nf-mdi-radio-handheld" => MDI_RADIO_HANDHELD,
    "nf-mdi-slack" => MDI_SLACK,
    "nf-mdi-lan" => MDI_LAN,
    "nf-mdi-instagram" => MDI_INSTAGRAM,
    "nf-mdi-key-plus" => MDI_KEY_PLUS,
    "nf-mdi-format-header-increase" => MDI_FORMAT_HEADER_INCREASE,
    "nf-mdi-bookmark" => MDI_BOOKMARK,
    "nf-mdi-heart-box-outline" => MDI_HEART_BOX_OUTLINE,
    "nf-mdi-gauge" => MDI_GAUGE,
    "nf-mdi-human-male" => MDI_HUMAN_MALE,
    "nf-mdi-battery-charging-30" => MDI_BATTERY_CHARGING_30,
    "nf-mdi-checkbox-marked-outline" => MDI_CHECKBOX_MARKED_OUTLINE,
    "nf-mdi-arrow-down-bold-box-outline" => MDI_ARROW_DOWN_BOLD_BOX_OUTLINE,
    "nf-mdi-tab" => MDI_TAB,
    "nf-mdi-arrow-up-bold-circle" => MDI_ARROW_UP_BOLD_CIRCLE,
    "nf-mdi-tumblr-reblog" => MDI_TUMBLR_REBLOG,
    "nf-mdi-water-off" => MDI_WATER_OFF,
    "nf-mdi-meteor" => MDI_METEOR,
    "nf-mdi-weather-rainy" => MDI_WEATHER_RAINY,
    "nf-mdi-music-box-outline" => MDI_MUSIC_BOX_OUTLINE,
    "nf-mdi-account-plus-outline" => MDI_ACCOUNT_PLUS_OUTLINE,
    "nf-mdi-delete-circle" => MDI_DELETE_CIRCLE,
    "nf-mdi-signal-4g" => MDI_SIGNAL_4G,
    "nf-mdi-format-strikethrough-variant" => MDI_FORMAT_STRIKETHROUGH_VARIANT,
    "nf-mdi-battery-60" => MDI_BATTERY_60,
    "nf-mdi-bike" => MDI_BIKE,
    "nf-mdi-phone-voip" => MDI_PHONE_VOIP,
    "nf-mdi-pound-box" => MDI_POUND_BOX,
    "nf-mdi-map-marker-off" => MDI_MAP_MARKER_OFF,
    "nf-mdi-email-secure" => MDI_EMAIL_SECURE,
    "nf-mdi-format-line-style" => MDI_FORMAT_LINE_STYLE,
    "nf-mdi-sort-variant" => MDI_SORT_VARIANT,
    "nf-mdi-chili-mild" => MDI_CHILI_MILD,
    "nf-mdi-transit-transfer" => MDI_TRANSIT_TRANSFER,
    "nf-mdi-cat" => MDI_CAT,
    "nf-mdi-google-physical-web" => MDI_GOOGLE_PHYSICAL_WEB,
    "nf-mdi-dice-d10" => MDI_DICE_D10,
    "nf-mdi-scale" => MDI_SCALE,
    "nf-mdi-alarm-off" => MDI_ALARM_OFF,
    "nf-mdi-flash-off" => MDI_FLASH_OFF,
    "nf-mdi-format-annotation-plus" => MDI_FORMAT_ANNOTATION_PLUS,
    "nf-mdi-ray-start-end" => MDI_RAY_START_END,
    "nf-mdi-filter-remove-outline" => MDI_FILTER_REMOVE_OUTLINE,
    "nf-mdi-skip-backward" => MDI_SKIP_BACKWARD,
    "nf-mdi-airplane-off" => MDI_AIRPLANE_OFF,
    "nf-mdi-clipboard-outline" => MDI_CLIPBOARD_OUTLINE,
    "nf-mdi-account-alert" => MDI_ACCOUNT_ALERT,
    "nf-mdi-crop-portrait" => MDI_CROP_PORTRAIT,
    "nf-mdi-format-align-left" => MDI_FORMAT_ALIGN_LEFT,
    "nf-mdi-twitter" => MDI_TWITTER,
    "nf-mdi-dice-d4" => MDI_DICE_D4,
    "nf-mdi-sticker-emoji" => MDI_STICKER_EMOJI,
    "nf-mdi-watch-vibrate" => MDI_WATCH_VIBRATE,
    "nf-mdi-rhombus" => MDI_RHOMBUS,
    "nf-mdi-alarm-bell" => MDI_ALARM_BELL,
    "nf-mdi-information-outline" => MDI_INFORMATION_OUTLINE,
    "nf-mdi-coffee-outline" => MDI_COFFEE_OUTLINE,
    "nf-mdi-do-not-disturb-off" => MDI_DO_NOT_DISTURB_OFF,
    "nf-mdi-car-sports" => MDI_CAR_SPORTS,
    "nf-mdi-filter-outline" => MDI_FILTER_OUTLINE,
    "nf-mdi-cloud-upload" => MDI_CLOUD_UPLOAD,
    "nf-mdi-hot-tub" => MDI_HOT_TUB,
    "nf-mdi-basketball" => MDI_BASKETBALL,
    "nf-mdi-beats" => MDI_BEATS,
    "nf-mdi-numeric-8-box" => MDI_NUMERIC_8_BOX,
    "nf-mdi-format-color-text" => MDI_FORMAT_COLOR_TEXT,
    "nf-mdi-briefcase-upload" => MDI_BRIEFCASE_UPLOAD,
    "nf-mdi-surround-sound-3-1" => MDI_SURROUND_SOUND_3_1,
    "nf-mdi-comment-account" => MDI_COMMENT_ACCOUNT,
    "nf-mdi-sync" => MDI_SYNC,
    "nf-mdi-box-shadow" => MDI_BOX_SHADOW,
    "nf-mdi-altimeter" => MDI_ALTIMETER,
    "nf-mdi-weather-snowy-rainy" => MDI_WEATHER_SNOWY_RAINY,
    "nf-mdi-language-go" => MDI_LANGUAGE_GO,
    "nf-mdi-wifi-off" => MDI_WIFI_OFF,
    "nf-mdi-help" => MDI_HELP,
    "nf-mdi-metronome" => MDI_METRONOME,
    "nf-mdi-vibrate" => MDI_VIBRATE,
    "nf-mdi-clock-in" => MDI_CLOCK_IN,
    "nf-mdi-format-paint" => MDI_FORMAT_PAINT,
    "nf-mdi-crop-rotate" => MDI_CROP_ROTATE,
    "nf-mdi-candle" => MDI_CANDLE,
    "nf-mdi-printer" => MDI_PRINTER,
    "nf-mdi-weather-lightning-rainy" => MDI_WEATHER_LIGHTNING_RAINY,
    "nf-mdi-blinds" => MDI_BLINDS,
    "nf-mdi-cake-variant" => MDI_CAKE_VARIANT,
    "nf-mdi-star-outline" => MDI_STAR_OUTLINE,
    "nf-mdi-chevron-double-down" => MDI_CHEVRON_DOUBLE_DOWN,
    "nf-mdi-numeric-0-box" => MDI_NUMERIC_0_BOX,
    "nf-mdi-debug-step-out" => MDI_DEBUG_STEP_OUT,
    "nf-mdi-octagram-outline" => MDI_OCTAGRAM_OUTLINE,
    "nf-mdi-bus" => MDI_BUS,
    "nf-mdi-clipboard-flow" => MDI_CLIPBOARD_FLOW,
    "nf-mdi-temperature-fahrenheit" => MDI_TEMPERATURE_FAHRENHEIT,
    "nf-mdi-arrow-left-bold-box-outline" => MDI_ARROW_LEFT_BOLD_BOX_OUTLINE,
    "nf-mdi-server-network" => MDI_SERVER_NETWORK,
    "nf-mdi-sword-cross" => MDI_SWORD_CROSS,
    "nf-mdi-pig" => MDI_PIG,
    "nf-mdi-flash" => MDI_FLASH,
    "nf-mdi-shuffle" => MDI_SHUFFLE,
    "nf-mdi-cellphone-dock" => MDI_CELLPHONE_DOCK,
    "nf-mdi-repeat" => MDI_REPEAT,
    "nf-mdi-function" => MDI_FUNCTION,
    "nf-mdi-television" => MDI_TELEVISION,
    "nf-mdi-tablet-ipad" => MDI_TABLET_IPAD,
    "nf-mdi-grease-pencil" => MDI_GREASE_PENCIL,
    "nf-mdi-currency-gbp" => MDI_CURRENCY_GBP,
    "nf-mdi-seat-legroom-normal" => MDI_SEAT_LEGROOM_NORMAL,
    "nf-mdi-backup-restore" => MDI_BACKUP_RESTORE,
    "nf-mdi-battery-charging-80" => MDI_BATTERY_CHARGING_80,
    "nf-mdi-numeric-2-box-multiple-outline" => MDI_NUMERIC_2_BOX_MULTIPLE_OUTLINE,
    "nf-mdi-cow" => MDI_COW,
    "nf-mdi-file-powerpoint" => MDI_FILE_POWERPOINT,
    "nf-mdi-theme-light-dark" => MDI_THEME_LIGHT_DARK,
    "nf-mdi-arrow-expand-all" => MDI_ARROW_EXPAND_ALL,
    "nf-mdi-power-socket" => MDI_POWER_SOCKET,
    "nf-mdi-forum-outline" => MDI_FORUM_OUTLINE,
    "nf-mdi-cart-outline" => MDI_CART_OUTLINE,
    "nf-mdi-shape-rectangle-plus" => MDI_SHAPE_RECTANGLE_PLUS,
    "nf-mdi-window-close" => MDI_WINDOW_CLOSE,
    "nf-mdi-language-css3" => MDI_LANGUAGE_CSS3,
    "nf-mdi-laptop-chromebook" => MDI_LAPTOP_CHROMEBOOK,
    "nf-mdi-numeric-2-box-outline" => MDI_NUMERIC_2_BOX_OUTLINE,
    "nf-mdi-tumblr" => MDI_TUMBLR,
    "nf-mdi-chevron-down" => MDI_CHEVRON_DOWN,
    "nf-mdi-weather-partlycloudy" => MDI_WEATHER_PARTLYCLOUDY,
    "nf-mdi-comment-multiple-outline" => MDI_COMMENT_MULTIPLE_OUTLINE,
    "nf-mdi-hexagon-multiple" => MDI_HEXAGON_MULTIPLE,
    "nf-mdi-division-box" => MDI_DIVISION_BOX,
    "nf-mdi-tab-unselected" => MDI_TAB_UNSELECTED,
    "nf-mdi-atom" => MDI_ATOM,
    "nf-mdi-language-python-text" => MDI_LANGUAGE_PYTHON_TEXT,
    "nf-mdi-currency-twd" => MDI_CURRENCY_TWD,
    "nf-mdi-account-outline" => MDI_ACCOUNT_OUTLINE,
    "nf-mdi-phone-in-talk" => MDI_PHONE_IN_TALK,
    "nf-mdi-format-bold" => MDI_FORMAT_BOLD,
    "nf-mdi-image-filter-black-white" => MDI_IMAGE_FILTER_BLACK_WHITE,
    "nf-mdi-cup-off" => MDI_CUP_OFF,
    "nf-mdi-scale-bathroom" => MDI_SCALE_BATHROOM,
    "nf-mdi-weather-cloudy" => MDI_WEATHER_CLOUDY,
    "nf-mdi-checkbox-blank-circle" => MDI_CHECKBOX_BLANK_CIRCLE,
    "nf-mdi-key" => MDI_KEY,
    "nf-mdi-video-input-component" => MDI_VIDEO_INPUT_COMPONENT,
    "nf-mdi-taxi" => MDI_TAXI,
    "nf-mdi-chart-bar" => MDI_CHART_BAR,
    "nf-mdi-closed-caption" => MDI_CLOSED_CAPTION,
    "nf-mdi-lock-reset" => MDI_LOCK_RESET,
    "nf-mdi-server-plus" => MDI_SERVER_PLUS,
    "nf-mdi-silverware" => MDI_SILVERWARE,
    "nf-mdi-format-header-equal" => MDI_FORMAT_HEADER_EQUAL,
    "nf-mdi-volume-medium" => MDI_VOLUME_MEDIUM,
    "nf-mdi-cached" => MDI_CACHED,
    "nf-mdi-arrow-down-drop-circle-outline" => MDI_ARROW_DOWN_DROP_CIRCLE_OUTLINE,
    "nf-mdi-towing" => MDI_TOWING,
    "nf-mdi-phone-settings" => MDI_PHONE_SETTINGS,
    "nf-mdi-format-vertical-align-top" => MDI_FORMAT_VERTICAL_ALIGN_TOP,
    "nf-mdi-skype-business" => MDI_SKYPE_BUSINESS,
    "nf-mdi-white-balance-iridescent" => MDI_WHITE_BALANCE_IRIDESCENT,
    "nf-mdi-unfold-more-horizontal" => MDI_UNFOLD_MORE_HORIZONTAL,
    "nf-mdi-ghost" => MDI_GHOST,
    "nf-mdi-bug" => MDI_BUG,
    "nf-mdi-odnoklassniki" => MDI_ODNOKLASSNIKI,
    "nf-mdi-car-pickup" => MDI_CAR_PICKUP,
    "nf-mdi-magnify-plus" => MDI_MAGNIFY_PLUS,
    "nf-mdi-home-account" => MDI_HOME_ACCOUNT,
    "nf-mdi-presentation-play" => MDI_PRESENTATION_PLAY,
    "nf-mdi-call-merge" => MDI_CALL_MERGE,
    "nf-mdi-at" => MDI_AT,
    "nf-mdi-xamarin-outline" => MDI_XAMARIN_OUTLINE,
    "nf-mdi-cloud-off-outline" => MDI_CLOUD_OFF_OUTLINE,
    "nf-mdi-numeric-1-box" => MDI_NUMERIC_1_BOX,
    "nf-mdi-stocking" => MDI_STOCKING,
    "nf-mdi-page-first" => MDI_PAGE_FIRST,
    "nf-mdi-code-parentheses" => MDI_CODE_PARENTHESES,
    "nf-mdi-battery-90" => MDI_BATTERY_90,
    "nf-mdi-arrow-collapse" => MDI_ARROW_COLLAPSE,
    "nf-mdi-vk" => MDI_VK,
    "nf-mdi-apple-keyboard-caps" => MDI_APPLE_KEYBOARD_CAPS,
    "nf-mdi-format-header-6" => MDI_FORMAT_HEADER_6,
    "nf-mdi-battery-30" => MDI_BATTERY_30,
    "nf-mdi-link-variant" => MDI_LINK_VARIANT,
    "nf-mdi-summit" => MDI_SUMMIT,
    "nf-mdi-desktop-classic" => MDI_DESKTOP_CLASSIC,
    "nf-mdi-cisco-webex" => MDI_CISCO_WEBEX,
    "nf-mdi-redo" => MDI_REDO,
    "nf-mdi-language-html5" => MDI_LANGUAGE_HTML5,
    "nf-mdi-skype" => MDI_SKYPE,
    "nf-mdi-server-off" => MDI_SERVER_OFF,
    "nf-mdi-bitbucket" => MDI_BITBUCKET,
    "nf-mdi-vk-box" => MDI_VK_BOX,
    "nf-mdi-escalator" => MDI_ESCALATOR,
    "nf-mdi-sleep" => MDI_SLEEP,
    "nf-mdi-solid" => MDI_SOLID,
    "nf-mdi-rename-box" => MDI_RENAME_BOX,
    "nf-mdi-image-filter-hdr" => MDI_IMAGE_FILTER_HDR,
    "nf-mdi-ribbon" => MDI_RIBBON,
    "nf-mdi-qqchat" => MDI_QQCHAT,
    "nf-mdi-emoticon-happy" => MDI_EMOTICON_HAPPY,
    "nf-mdi-reply" => MDI_REPLY,
    "nf-mdi-battery-charging-wireless-alert" => MDI_BATTERY_CHARGING_WIRELESS_ALERT,
    "nf-mdi-magnify" => MDI_MAGNIFY,
    "nf-mdi-amplifier" => MDI_AMPLIFIER,
    "nf-mdi-vector-arrange-above" => MDI_VECTOR_ARRANGE_ABOVE,
    "nf-mdi-wunderlist" => MDI_WUNDERLIST,
    "nf-mdi-android-debug-bridge" => MDI_ANDROID_DEBUG_BRIDGE,
    "nf-mdi-steering" => MDI_STEERING,
    "nf-mdi-contacts" => MDI_CONTACTS,
    "nf-mdi-video-input-svideo" => MDI_VIDEO_INPUT_SVIDEO,
    "nf-mdi-tilde" => MDI_TILDE,
    "nf-mdi-code-tags" => MDI_CODE_TAGS,
    "nf-mdi-calendar-multiple" => MDI_CALENDAR_MULTIPLE,
    "nf-mdi-message-settings" => MDI_MESSAGE_SETTINGS,
    "nf-mdi-window-restore" => MDI_WINDOW_RESTORE,
    "nf-mdi-format-header-pound" => MDI_FORMAT_HEADER_POUND,
    "nf-mdi-view-quilt" => MDI_VIEW_QUILT,
    "nf-mdi-pinterest-box" => MDI_PINTEREST_BOX,
    "nf-mdi-package-down" => MDI_PACKAGE_DOWN,
    "nf-mdi-humble-bundle" => MDI_HUMBLE_BUNDLE,
    "nf-mdi-map-marker-outline" => MDI_MAP_MARKER_OUTLINE,
    "nf-mdi-cash-usd" => MDI_CASH_USD,
    "nf-mdi-checkbox-multiple-marked-circle-outline" => MDI_CHECKBOX_MULTIPLE_MARKED_CIRCLE_OUTLINE,
    "nf-mdi-cursor-move" => MDI_CURSOR_MOVE,
    "nf-mdi-folder-image" => MDI_FOLDER_IMAGE,
    "nf-mdi-television-guide" => MDI_TELEVISION_GUIDE,
    "nf-mdi-chili-hot" => MDI_CHILI_HOT,
    "nf-mdi-battery-unknown" => MDI_BATTERY_UNKNOWN,
    "nf-mdi-close-network" => MDI_CLOSE_NETWORK,
    "nf-mdi-file-plus" => MDI_FILE_PLUS,
    "nf-mdi-format-italic" => MDI_FORMAT_ITALIC,
    "nf-mdi-note-multiple" => MDI_NOTE_MULTIPLE,
    "nf-mdi-language-csharp" => MDI_LANGUAGE_CSHARP,
    "nf-mdi-pause" => MDI_PAUSE,
    "nf-mdi-seat-flat-angled" => MDI_SEAT_FLAT_ANGLED,
    "nf-mdi-rounded-corner" => MDI_ROUNDED_CORNER,
    "nf-mdi-chemical-weapon" => MDI_CHEMICAL_WEAPON,
    "nf-mdi-checkbox-multiple-blank" => MDI_CHECKBOX_MULTIPLE_BLANK,
    "nf-mdi-lightbulb-on-outline" => MDI_LIGHTBULB_ON_OUTLINE,
    "nf-mdi-wifi" => MDI_WIFI,
    "nf-mdi-maxcdn" => MDI_MAXCDN,
    "nf-mdi-help-network" => MDI_HELP_NETWORK,
    "nf-mdi-hospital-building" => MDI_HOSPITAL_BUILDING,
    "nf-mdi-bookmark-check" => MDI_BOOKMARK_CHECK,
    "nf-mdi-wii" => MDI_WII,
    "nf-mdi-tooltip-edit" => MDI_TOOLTIP_EDIT,
    "nf-mdi-dialpad" => MDI_DIALPAD,
    "nf-mdi-cards-playing-outline" => MDI_CARDS_PLAYING_OUTLINE,
    "nf-mdi-triangle-outline" => MDI_TRIANGLE_OUTLINE,
    "nf-mdi-stackexchange" => MDI_STACKEXCHANGE,
    "nf-mdi-tag-remove" => MDI_TAG_REMOVE,
    "nf-mdi-thought-bubble" => MDI_THOUGHT_BUBBLE,
    "nf-mdi-numeric" => MDI_NUMERIC,
    "nf-mdi-vlc" => MDI_VLC,
    "nf-mdi-crop-landscape" => MDI_CROP_LANDSCAPE,
    "nf-mdi-lumx" => MDI_LUMX,
    "nf-mdi-format-vertical-align-center" => MDI_FORMAT_VERTICAL_ALIGN_CENTER,
    "nf-mdi-mail-ru" => MDI_MAIL_RU,
    "nf-mdi-file-restore" => MDI_FILE_RESTORE,
    "nf-mdi-skip-previous-circle-outline" => MDI_SKIP_PREVIOUS_CIRCLE_OUTLINE,
    "nf-mdi-language-swift" => MDI_LANGUAGE_SWIFT,
    "nf-mdi-numeric-5-box" => MDI_NUMERIC_5_BOX,
    "nf-mdi-skip-forward" => MDI_SKIP_FORWARD,
    "nf-mdi-hospital-marker" => MDI_HOSPITAL_MARKER,
    "nf-mdi-football" => MDI_FOOTBALL,
    "nf-mdi-pocket" => MDI_POCKET,
    "nf-mdi-eyedropper-variant" => MDI_EYEDROPPER_VARIANT,
    "nf-mdi-kettle" => MDI_KETTLE,
    "nf-mdi-engine-outline" => MDI_ENGINE_OUTLINE,
    "nf-mdi-source-commit" => MDI_SOURCE_COMMIT,
    "nf-mdi-arrange-send-backward" => MDI_ARRANGE_SEND_BACKWARD,
    "nf-mdi-apple" => MDI_APPLE,
    "nf-mdi-table-large" => MDI_TABLE_LARGE,
    "nf-mdi-signal-variant" => MDI_SIGNAL_VARIANT,
    "nf-mdi-menu-right" => MDI_MENU_RIGHT,
    "nf-mdi-image-area-close" => MDI_IMAGE_AREA_CLOSE,
    "nf-mdi-minus-circle" => MDI_MINUS_CIRCLE,
    "nf-mdi-view-stream" => MDI_VIEW_STREAM,
    "nf-mdi-camera-front-variant" => MDI_CAMERA_FRONT_VARIANT,
    "nf-mdi-arrow-right-bold-hexagon-outline" => MDI_ARROW_RIGHT_BOLD_HEXAGON_OUTLINE,
    "nf-mdi-azure" => MDI_AZURE,
    "nf-mdi-brightness-2" => MDI_BRIGHTNESS_2,
    "nf-mdi-polaroid" => MDI_POLAROID,
    "nf-mdi-allo" => MDI_ALLO,
    "nf-mdi-folder-account" => MDI_FOLDER_ACCOUNT,
    "nf-mdi-camera-enhance" => MDI_CAMERA_ENHANCE,
    "nf-mdi-quality-high" => MDI_QUALITY_HIGH,
    "nf-mdi-briefcase" => MDI_BRIEFCASE,
    "nf-mdi-content-paste" => MDI_CONTENT_PASTE,
    "nf-mdi-castle" => MDI_CASTLE,
    "nf-mdi-folder-lock" => MDI_FOLDER_LOCK,
    "nf-mdi-battery-charging-40" => MDI_BATTERY_CHARGING_40,
    "nf-mdi-dictionary" => MDI_DICTIONARY,
    "nf-mdi-ninja" => MDI_NINJA,
    "nf-mdi-eraser" => MDI_ERASER,
    "nf-mdi-ray-vertex" => MDI_RAY_VERTEX,
    "nf-mdi-box-cutter" => MDI_BOX_CUTTER,
    "nf-mdi-google-plus" => MDI_GOOGLE_PLUS,
    "nf-mdi-page-layout-sidebar-left" => MDI_PAGE_LAYOUT_SIDEBAR_LEFT,
    "nf-mdi-cart" => MDI_CART,
    "nf-mdi-numeric-7-box" => MDI_NUMERIC_7_BOX,
    "nf-mdi-table-row-remove" => MDI_TABLE_ROW_REMOVE,
    "nf-mdi-source-commit-start-next-local" => MDI_SOURCE_COMMIT_START_NEXT_LOCAL,
    "nf-mdi-umbrella" => MDI_UMBRELLA,
    "nf-mdi-laptop-off" => MDI_LAPTOP_OFF,
    "nf-mdi-scale-balance" => MDI_SCALE_BALANCE,
    "nf-mdi-music-circle" => MDI_MUSIC_CIRCLE,
    "nf-mdi-routes" => MDI_ROUTES,
    "nf-mdi-vector-difference-ab" => MDI_VECTOR_DIFFERENCE_AB,
    "nf-mdi-delete-variant" => MDI_DELETE_VARIANT,
    "nf-mdi-arrow-down-bold-circle" => MDI_ARROW_DOWN_BOLD_CIRCLE,
    "nf-mdi-quicktime" => MDI_QUICKTIME,
    "nf-mdi-basket-fill" => MDI_BASKET_FILL,
    "nf-mdi-store" => MDI_STORE,
    "nf-mdi-dice-d20" => MDI_DICE_D20,
    "nf-mdi-gas-station" => MDI_GAS_STATION,
    "nf-mdi-radiator" => MDI_RADIATOR,
    "nf-mdi-battery-outline" => MDI_BATTERY_OUTLINE,
    "nf-mdi-pencil" => MDI_PENCIL,
    "nf-mdi-car-wash" => MDI_CAR_WASH,
    "nf-mdi-book-multiple" => MDI_BOOK_MULTIPLE,
    "nf-mdi-calendar-clock" => MDI_CALENDAR_CLOCK,
    "nf-mdi-sim-alert" => MDI_SIM_ALERT,
    "nf-mdi-human-greeting" => MDI_HUMAN_GREETING,
    "nf-mdi-watch-import" => MDI_WATCH_IMPORT,
    "nf-mdi-wrap" => MDI_WRAP,
    "nf-mdi-mixer" => MDI_MIXER,
    "nf-mdi-text-to-speech" => MDI_TEXT_TO_SPEECH,
    "nf-mdi-call-received" => MDI_CALL_RECEIVED,
    "nf-mdi-cellphone" => MDI_CELLPHONE,
    "nf-mdi-rocket" => MDI_ROCKET,
    "nf-mdi-camera-gopro" => MDI_CAMERA_GOPRO,
    "nf-mdi-yammer" => MDI_YAMMER,
    "nf-mdi-github-circle" => MDI_GITHUB_CIRCLE,
    "nf-mdi-cup" => MDI_CUP,
    "nf-mdi-arrow-left-bold-box" => MDI_ARROW_LEFT_BOLD_BOX,
    "nf-mdi-contrast" => MDI_CONTRAST,
    "nf-mdi-glasses" => MDI_GLASSES,
    "nf-mdi-shuffle-variant" => MDI_SHUFFLE_VARIANT,
    "nf-mdi-dns" => MDI_DNS,
    "nf-mdi-clock-out" => MDI_CLOCK_OUT,
    "nf-mdi-format-size" => MDI_FORMAT_SIZE,
    "nf-mdi-dice-multiple" => MDI_DICE_MULTIPLE,
    "nf-mdi-anchor" => MDI_ANCHOR,
    "nf-mdi-account-plus" => MDI_ACCOUNT_PLUS,
    "nf-mdi-exit-to-app" => MDI_EXIT_TO_APP,
    "nf-mdi-checkbox-blank-outline" => MDI_CHECKBOX_BLANK_OUTLINE,
    "nf-mdi-currency-try" => MDI_CURRENCY_TRY,
    "nf-mdi-comment-processing" => MDI_COMMENT_PROCESSING,
    "nf-mdi-qrcode-scan" => MDI_QRCODE_SCAN,
    "nf-mdi-wechat" => MDI_WECHAT,
    "nf-mdi-xda" => MDI_XDA,
    "nf-mdi-car-convertible" => MDI_CAR_CONVERTIBLE,
    "nf-mdi-quadcopter" => MDI_QUADCOPTER,
    "nf-mdi-do-not-disturb" => MDI_DO_NOT_DISTURB,
    "nf-mdi-television-box" => MDI_TELEVISION_BOX,
    "nf-mdi-login-variant" => MDI_LOGIN_VARIANT,
    "nf-mdi-language-javascript" => MDI_LANGUAGE_JAVASCRIPT,
    "nf-mdi-language-c" => MDI_LANGUAGE_C,
    "nf-mdi-battery-charging-wireless-outline" => MDI_BATTERY_CHARGING_WIRELESS_OUTLINE,
    "nf-mdi-alert-circle-outline" => MDI_ALERT_CIRCLE_OUTLINE,
    "nf-mdi-chevron-double-right" => MDI_CHEVRON_DOUBLE_RIGHT,
    "nf-mdi-glass-mug" => MDI_GLASS_MUG,
    "nf-mdi-camera-iris" => MDI_CAMERA_IRIS,
    "nf-mdi-garage" => MDI_GARAGE,
    "nf-mdi-account-search" => MDI_ACCOUNT_SEARCH,
    "nf-mdi-bio" => MDI_BIO,
    "nf-mdi-pound" => MDI_POUND,
    "nf-mdi-pill" => MDI_PILL,
    "nf-mdi-weather-lightning" => MDI_WEATHER_LIGHTNING,
    "nf-mdi-oil" => MDI_OIL,
    "nf-mdi-drupal" => MDI_DRUPAL,
    "nf-mdi-cards-outline" => MDI_CARDS_OUTLINE,
    "nf-mdi-edge" => MDI_EDGE,
    "nf-mdi-vector-triangle" => MDI_VECTOR_TRIANGLE,
    "nf-mdi-arrow-up-bold" => MDI_ARROW_UP_BOLD,
    "nf-mdi-ruler" => MDI_RULER,
    "nf-mdi-lock-plus" => MDI_LOCK_PLUS,
    "nf-mdi-delete-restore" => MDI_DELETE_RESTORE,
    "nf-mdi-battery-charging-wireless" => MDI_BATTERY_CHARGING_WIRELESS,
    "nf-mdi-tor" => MDI_TOR,
    "nf-mdi-power-plug-off" => MDI_POWER_PLUG_OFF,
    "nf-mdi-bluetooth-connect" => MDI_BLUETOOTH_CONNECT,
    "nf-mdi-gnome" => MDI_GNOME,
    "nf-mdi-guitar-pick" => MDI_GUITAR_PICK,
    "nf-mdi-radio" => MDI_RADIO,
    "nf-mdi-zip-box" => MDI_ZIP_BOX,
    "nf-mdi-voice" => MDI_VOICE,
    "nf-mdi-account-multiple-plus" => MDI_ACCOUNT_MULTIPLE_PLUS,
    "nf-mdi-view-week" => MDI_VIEW_WEEK,
    "nf-mdi-home-automation" => MDI_HOME_AUTOMATION,
    "nf-mdi-briefcase-check" => MDI_BRIEFCASE_CHECK,
    "nf-mdi-bone" => MDI_BONE,
    "nf-mdi-music-note-sixteenth" => MDI_MUSIC_NOTE_SIXTEENTH,
    "nf-mdi-comment-text" => MDI_COMMENT_TEXT,
    "nf-mdi-flask-empty-outline" => MDI_FLASK_EMPTY_OUTLINE,
    "nf-mdi-flip-to-back" => MDI_FLIP_TO_BACK,
    "nf-mdi-circle-outline" => MDI_CIRCLE_OUTLINE,
    "nf-mdi-upload-multiple" => MDI_UPLOAD_MULTIPLE,
    "nf-mdi-marker" => MDI_MARKER,
    "nf-mdi-content-cut" => MDI_CONTENT_CUT,
    "nf-mdi-camera-off" => MDI_CAMERA_OFF,
    "nf-mdi-mushroom-outline" => MDI_MUSHROOM_OUTLINE,
    "nf-mdi-hand-pointing-right" => MDI_HAND_POINTING_RIGHT,
    "nf-mdi-layers-off" => MDI_LAYERS_OFF,
    "nf-mdi-message-reply-text" => MDI_MESSAGE_REPLY_TEXT,
    "nf-mdi-car-battery" => MDI_CAR_BATTERY,
    "nf-mdi-currency-jpy" => MDI_CURRENCY_JPY,
    "nf-mdi-pause-circle" => MDI_PAUSE_CIRCLE,
    "nf-mdi-tag-multiple" => MDI_TAG_MULTIPLE,
    "nf-mdi-package-variant" => MDI_PACKAGE_VARIANT,
    "nf-mdi-source-commit-end" => MDI_SOURCE_COMMIT_END,
    "nf-mdi-television-classic-off" => MDI_TELEVISION_CLASSIC_OFF,
    "nf-mdi-briefcase-outline" => MDI_BRIEFCASE_OUTLINE,
    "nf-mdi-camera-metering-spot" => MDI_CAMERA_METERING_SPOT,
    "nf-mdi-playlist-play" => MDI_PLAYLIST_PLAY,
    "nf-mdi-file-excel-box" => MDI_FILE_EXCEL_BOX,
    "nf-mdi-credit-card-multiple" => MDI_CREDIT_CARD_MULTIPLE,
    "nf-mdi-visualstudio" => MDI_VISUALSTUDIO,
    "nf-mdi-drag-horizontal" => MDI_DRAG_HORIZONTAL,
    "nf-mdi-soundcloud" => MDI_SOUNDCLOUD,
    "nf-mdi-delta" => MDI_DELTA,
    "nf-mdi-twitter-box" => MDI_TWITTER_BOX,
    "nf-mdi-lock-pattern" => MDI_LOCK_PATTERN,
    "nf-mdi-clover" => MDI_CLOVER,
    "nf-mdi-set-all" => MDI_SET_ALL,
    "nf-mdi-flip-to-front" => MDI_FLIP_TO_FRONT,
    "nf-mdi-directions-fork" => MDI_DIRECTIONS_FORK,
    "nf-mdi-credit-card-plus" => MDI_CREDIT_CARD_PLUS,
    "nf-mdi-nintendo-switch" => MDI_NINTENDO_SWITCH,
    "nf-mdi-autorenew" => MDI_AUTORENEW,
    "nf-mdi-tag-text-outline" => MDI_TAG_TEXT_OUTLINE,
    "nf-mdi-arrow-left-thick" => MDI_ARROW_LEFT_THICK,
    "nf-mdi-weather-sunset-down" => MDI_WEATHER_SUNSET_DOWN,
    "nf-mdi-xaml" => MDI_XAML,
    "nf-mdi-view-module" => MDI_VIEW_MODULE,
    "nf-mdi-android" => MDI_ANDROID,
    "nf-mdi-clock-fast" => MDI_CLOCK_FAST,
    "nf-mdi-trophy" => MDI_TROPHY,
    "nf-mdi-remote" => MDI_REMOTE,
    "nf-mdi-credit-card-scan" => MDI_CREDIT_CARD_SCAN,
    "nf-mdi-pillar" => MDI_PILLAR,
    "nf-mdi-deviantart" => MDI_DEVIANTART,
    "nf-mdi-table-edit" => MDI_TABLE_EDIT,
    "nf-mdi-book-open-page-variant" => MDI_BOOK_OPEN_PAGE_VARIANT,
    "nf-mdi-square-root" => MDI_SQUARE_ROOT,
    "nf-mdi-satellite" => MDI_SATELLITE,
    "nf-mdi-camcorder-box-off" => MDI_CAMCORDER_BOX_OFF,
    "nf-mdi-snowflake" => MDI_SNOWFLAKE,
    "nf-mdi-book" => MDI_BOOK,
    "nf-mdi-leaf" => MDI_LEAF,
    "nf-mdi-account-location" => MDI_ACCOUNT_LOCATION,
    "nf-mdi-lock-open" => MDI_LOCK_OPEN,
    "nf-mdi-view-array" => MDI_VIEW_ARRAY,
    "nf-mdi-motorbike" => MDI_MOTORBIKE,
    "nf-mdi-cart-plus" => MDI_CART_PLUS,
    "nf-mdi-file-word" => MDI_FILE_WORD,
    "nf-mdi-emoticon-neutral" => MDI_EMOTICON_NEUTRAL,
    "nf-mdi-coffee-to-go" => MDI_COFFEE_TO_GO,
    "nf-mdi-numeric-3-box" => MDI_NUMERIC_3_BOX,
    "nf-mdi-navigation" => MDI_NAVIGATION,
    "nf-mdi-soy-sauce" => MDI_SOY_SAUCE,
    "nf-mdi-sim" => MDI_SIM,
    "nf-mdi-weather-windy-variant" => MDI_WEATHER_WINDY_VARIANT,
    "nf-mdi-lock-open-outline" => MDI_LOCK_OPEN_OUTLINE,
    "nf-mdi-seat-recline-normal" => MDI_SEAT_RECLINE_NORMAL,
    "nf-mdi-store-24-hour" => MDI_STORE_24_HOUR,
    "nf-mdi-weight" => MDI_WEIGHT,
    "nf-mdi-battery-negative" => MDI_BATTERY_NEGATIVE,
    "nf-mdi-help-box" => MDI_HELP_BOX,
    "nf-mdi-format-wrap-top-bottom" => MDI_FORMAT_WRAP_TOP_BOTTOM,
    "nf-mdi-notification-clear-all" => MDI_NOTIFICATION_CLEAR_ALL,
    "nf-mdi-numeric-2-box" => MDI_NUMERIC_2_BOX,
    "nf-mdi-signal" => MDI_SIGNAL,
    "nf-mdi-image-filter-vintage" => MDI_IMAGE_FILTER_VINTAGE,
    "nf-mdi-ticket" => MDI_TICKET,
    "nf-mdi-format-vertical-align-bottom" => MDI_FORMAT_VERTICAL_ALIGN_BOTTOM,
    "nf-mdi-restart" => MDI_RESTART,
    "nf-mdi-format-textdirection-r-to-l" => MDI_FORMAT_TEXTDIRECTION_R_TO_L,
    "nf-mdi-heart-pulse" => MDI_HEART_PULSE,
    "nf-mdi-code-less-than-or-equal" => MDI_CODE_LESS_THAN_OR_EQUAL,
    "nf-mdi-record" => MDI_RECORD,
    "nf-mdi-swap-horizontal" => MDI_SWAP_HORIZONTAL,
    "nf-mdi-receipt" => MDI_RECEIPT,
    "nf-mdi-arrow-down-box" => MDI_ARROW_DOWN_BOX,
    "nf-mdi-delete-empty" => MDI_DELETE_EMPTY,
    "nf-mdi-truck-fast" => MDI_TRUCK_FAST,
    "nf-mdi-crown" => MDI_CROWN,
    "nf-mdi-content-save-all" => MDI_CONTENT_SAVE_ALL,
    "nf-mdi-internet-explorer" => MDI_INTERNET_EXPLORER,
    "nf-mdi-thumbs-up-down" => MDI_THUMBS_UP_DOWN,
    "nf-mdi-copyright" => MDI_COPYRIGHT,
    "nf-mdi-cloud-braces" => MDI_CLOUD_BRACES,
    "nf-mdi-flashlight-off" => MDI_FLASHLIGHT_OFF,
    "nf-mdi-animation" => MDI_ANIMATION,
    "nf-mdi-account-settings-variant" => MDI_ACCOUNT_SETTINGS_VARIANT,
    "nf-mdi-fast-forward-outline" => MDI_FAST_FORWARD_OUTLINE,
    "nf-mdi-reload" => MDI_RELOAD,
    "nf-mdi-flashlight" => MDI_FLASHLIGHT,
    "nf-mdi-library-books" => MDI_LIBRARY_BOOKS,
    "nf-mdi-calendar" => MDI_CALENDAR,
    "nf-mdi-owl" => MDI_OWL,
    "nf-mdi-page-layout-header" => MDI_PAGE_LAYOUT_HEADER,
    "nf-mdi-nfc" => MDI_NFC,
    "nf-mdi-android-studio" => MDI_ANDROID_STUDIO,
    "nf-mdi-numeric-0-box-outline" => MDI_NUMERIC_0_BOX_OUTLINE,
    "nf-mdi-radio-tower" => MDI_RADIO_TOWER,
    "nf-mdi-heart-half-outline" => MDI_HEART_HALF_OUTLINE,
    "nf-mdi-blur" => MDI_BLUR,
    "nf-mdi-multiplication-box" => MDI_MULTIPLICATION_BOX,
    "nf-mdi-shovel-off" => MDI_SHOVEL_OFF,
    "nf-mdi-car-side" => MDI_CAR_SIDE,
    "nf-mdi-headphones-off" => MDI_HEADPHONES_OFF,
    "nf-mdi-rotate-right" => MDI_ROTATE_RIGHT,
    "nf-mdi-set-center" => MDI_SET_CENTER,
    "nf-mdi-locker-multiple" => MDI_LOCKER_MULTIPLE,
    "nf-mdi-cast-off" => MDI_CAST_OFF,
    "nf-mdi-taco" => MDI_TACO,
    "nf-mdi-alert-decagram" => MDI_ALERT_DECAGRAM,
    "nf-mdi-checkbox-blank-circle-outline" => MDI_CHECKBOX_BLANK_CIRCLE_OUTLINE,
    "nf-mdi-hook-off" => MDI_HOOK_OFF,
    "nf-mdi-micro-sd" => MDI_MICRO_SD,
    "nf-mdi-battery-charging-wireless-60" => MDI_BATTERY_CHARGING_WIRELESS_60,
    "nf-mdi-key-remove" => MDI_KEY_REMOVE,
    "nf-mdi-rewind-outline" => MDI_REWIND_OUTLINE,
    "nf-mdi-lastpass" => MDI_LASTPASS,
    "nf-mdi-format-section" => MDI_FORMAT_SECTION,
    "nf-mdi-tune" => MDI_TUNE,
    "nf-mdi-table" => MDI_TABLE,
    "nf-mdi-pharmacy" => MDI_PHARMACY,
    "nf-mdi-factory" => MDI_FACTORY,
    "nf-mdi-disk-alert" => MDI_DISK_ALERT,
    "nf-mdi-chip" => MDI_CHIP,
    "nf-mdi-deskphone" => MDI_DESKPHONE,
    "nf-mdi-alarm-plus" => MDI_ALARM_PLUS,
    "nf-mdi-fullscreen-exit" => MDI_FULLSCREEN_EXIT,
    "nf-mdi-music-note-whole" => MDI_MUSIC_NOTE_WHOLE,
    "nf-mdi-barcode-scan" => MDI_BARCODE_SCAN,
    "nf-mdi-pause-octagon" => MDI_PAUSE_OCTAGON,
    "nf-mdi-stack-overflow" => MDI_STACK_OVERFLOW,
    "nf-mdi-texture" => MDI_TEXTURE,
    "nf-mdi-projector-screen" => MDI_PROJECTOR_SCREEN,
    "nf-mdi-treasure-chest" => MDI_TREASURE_CHEST,
    "nf-mdi-satellite-variant" => MDI_SATELLITE_VARIANT,
    "nf-mdi-help-circle-outline" => MDI_HELP_CIRCLE_OUTLINE,
    "nf-mdi-view-carousel" => MDI_VIEW_CAROUSEL,
    "nf-mdi-close-box" => MDI_CLOSE_BOX,
    "nf-mdi-format-color-fill" => MDI_FORMAT_COLOR_FILL,
    "nf-mdi-pencil-box-outline" => MDI_PENCIL_BOX_OUTLINE,
    "nf-mdi-briefcase-download" => MDI_BRIEFCASE_DOWNLOAD,
    "nf-mdi-code-tags-check" => MDI_CODE_TAGS_CHECK,
    "nf-mdi-menu" => MDI_MENU,
    "nf-mdi-table-of-contents" => MDI_TABLE_OF_CONTENTS,
    "nf-mdi-angular" => MDI_ANGULAR,
    "nf-mdi-twitter-retweet" => MDI_TWITTER_RETWEET,
};
