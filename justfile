lint:
  cargo check
  cargo clippy

generate-icons:
  cd list-generator && (cargo run; cd ..)
