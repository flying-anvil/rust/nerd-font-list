# Nerd Fonts List

This library contains a list of Nerd-Fonts icons as Rust constants.

It also contains a CLI utility to list them (organized in different groups) and search by names.

# Links

- <https://github.com/ryanoasis/nerd-fonts/wiki/Icon-Names-in-Shell>
- <https://github.com/ryanoasis/nerd-fonts/tree/master/bin/scripts/lib>
- <https://github.com/ryanoasis/nerd-fonts/wiki/Glyph-Sets-and-Code-Points>
- <https://www.nerdfonts.com/cheat-sheet>
